<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExoneratedGreenFeePartnerReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partner_reservations', function (Blueprint $table) {
			$table->tinyInteger('exonerated')->default(0)->after('id');
			$table->float('green_fee', 8, 2)->default(0.0)->after('exonerated');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partner_reservations', function (Blueprint $table) {
			$table->dropColumn(['exonerated','green_fee']);
		});
	}
}

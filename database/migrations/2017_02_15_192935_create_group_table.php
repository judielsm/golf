<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('groups', function(Blueprint $table){

			$table->increments('id');
			$table->string('name');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('teacher_id')->unsigned()->nullable()->index();
			$table->foreign('teacher_id')
				  ->references('id')->on('teachers')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();

		});       
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('groups');
	}
}
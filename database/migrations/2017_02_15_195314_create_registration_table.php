<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registrations', function (Blueprint $table) {
			$table->increments('id');
			$table->enum('status', [
						 'Cancelada',
						 'Procesada',
						 'En proceso',
						 'Finalizada',
						])->default('En proceso');

			$table->integer('group_id')->unsigned()->index();
			$table->foreign('group_id')
				  ->references('id')->on('groups')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->integer('payment_plan_id')->unsigned()->index();
			$table->foreign('payment_plan_id')
				  ->references('id')->on('payment_plans')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->integer('student_id')->unsigned()->index();
			$table->foreign('student_id')
				  ->references('id')->on('students')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('registrations');
	}
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	   Schema::create('companies', function(Blueprint $table){

			$table->increments('id');
			$table->string('name');
			$table->string('phone');
			$table->string('rif');
			$table->string('address');
			$table->string('tax');
			$table->tinyInteger('enabled')->default(1);

			$table->timestamps();
			$table->softDeletes();

		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('companies');
	}
}
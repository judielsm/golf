<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeBlockedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_blockeds', function (Blueprint $table) {
			$table->increments('id');
            $table->time('start_time');
            $table->time('end_time');

			$table->timestamps();
			$table->softDeletes();
		});

        Schema::table('time_blockeds', function($table){
            $table->integer('day_blocked_id')->unsigned();
			$table->foreign('day_blocked_id')
                ->references('id')->on('day_blockeds')
				->onUpdate('cascade')
				->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_blockeds');
    }
}

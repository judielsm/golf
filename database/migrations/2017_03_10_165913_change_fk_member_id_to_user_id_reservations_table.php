<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFkMemberIdToUserIdReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservations', function (Blueprint $table) {

			$table->dropForeign('reservations_member_id_foreign');
			$table->dropColumn('member_id');

			$table->integer('user_id')->unsigned()->nullable()->after('enabled')->index();
			$table->foreign('user_id')
				 ->references('id')->on('users')
				 ->onUpdate('set null')
				 ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservations', function (Blueprint $table) {

			$table->dropForeign('reservations_user_id_foreign');
			$table->dropColumn('user_id');

			$table->integer('member_id')->unsigned()->nullable()->after('enabled')->index();
			$table->foreign('member_id')
				 ->references('id')->on('members')
				 ->onUpdate('set null')
				 ->onDelete('cascade');

		});
	}
}
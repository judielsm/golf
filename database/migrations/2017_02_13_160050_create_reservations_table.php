<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('receipt_payment', 50);
			$table->string('detail', 255);
			$table->date('date');
			$table->time('start_time');
			$table->enum('status', [
					'En proceso',
					'Sorteable',
					'Aprobada',
					'Cancelada',
					'Cerrada',
					'Finalizada',
				])->default('Aprobada');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('member_id')->unsigned()->index();
			$table->foreign('member_id')
					->references('id')->on('members')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('reservations');
	}
}
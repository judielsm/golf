<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExoneratedPartnersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partners', function (Blueprint $table) {
			$table->enum('exonerated', [
						 'Parcial',
						 'Total',
						 'Reservacion',
						])->nullable()->after('handicap');
			$table->dropColumn('type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partners', function (Blueprint $table) {
			$table->dropColumn('exonerated');
			$table->tinyInteger('type')->default(1)->after('handicap');
		});
	}
}
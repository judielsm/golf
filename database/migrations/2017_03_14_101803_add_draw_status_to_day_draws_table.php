<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDrawStatusToDayDrawsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('day_draws', function (Blueprint $table) {
			$table->tinyInteger('day_draw_status')->after('date')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('day_draws', function (Blueprint $table) {
			$table->dropColumn('day_draw_status');
		});
	}
}
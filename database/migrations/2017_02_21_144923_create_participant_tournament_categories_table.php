<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantTournamentCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('participant_tournament_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('order')->nullable();
			$table->enum('status', [
					'Aprobado',
					'En espera',
					'Excluido',
				]);
			$table->tinyInteger('enabled')->default(1);

			$table->integer('participant_id')->unsigned()->index();

			$table->integer('category_id')->unsigned()->index();
			$table->foreign('category_id')
				  ->references('id')->on('categories')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('participant_tournament_categories');
	}
}

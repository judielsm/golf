<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('partners', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('last_name', 50);
			$table->string('identity_card', 20);
			$table->enum('sex', [
						 'Femenino',
						 'Masculino',
						])->default('Femenino');
			$table->string('email')->unique();
			$table->string('phone', 20);
			$table->tinyInteger('type')->default(1);
			$table->tinyInteger('enabled')->default(1);

			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')
					->references('id')->on('users')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('partners');
	}
}
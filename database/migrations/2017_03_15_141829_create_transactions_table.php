<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function (Blueprint $table) {
			$table->increments('id');
			$table->float('amount');
			$table->enum('type_payer', [
						 '1',
						 '2',
						])->comment('1: Socio, 2: Invitado');
			$table->tinyInteger('enabled')->default(1);
			$table->integer('payer_id')->unsigned()->index();

			$table->integer('product_id')->unsigned()->index()->comment('id que corresponde a la tabla del tipo de producto');

			$table->integer('type_product_id')->unsigned()->index();
			$table->foreign('type_product_id')
					->references('id')->on('products')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('transaction_status_id')->unsigned()->index();
			$table->foreign('transaction_status_id')
					->references('id')->on('transaction_status')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('type_payment_id')->unsigned()->index();
			$table->foreign('type_payment_id')
					->references('id')->on('type_payments')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('transactions');
	}
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGreenFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('green_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->double('amount', 8, 2);

            $table->timestamps();
        });
        Schema::table('green_fees', function ( $table){
          $table->integer('club_id')->unsigned();

              $table->foreign('club_id')->references('id')->on('clubs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('green_fees');
    }
}

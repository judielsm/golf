<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservations', function (Blueprint $table) {

			$table->dropColumn('processed');
			$table->enum('status', [
					'En proceso',
					'Sorteable',
					'Aprobado',
					'Cancelado',
					'Cerrado',
					'Disfrutado',
				])->default('Aprobado');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservations', function (Blueprint $table) {
			$table->dropColumn('status');
			$table->tinyInteger('processed');
		});
	}
}
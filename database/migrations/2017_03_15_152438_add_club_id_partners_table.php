<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClubIdPartnersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partners', function (Blueprint $table) {

			$table->integer('club_id')->unsigned()->nullable()->after('enabled')->index();
			$table->foreign('club_id')
				  ->references('id')->on('clubs')
				  ->onUpdate('set null')
				  ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partners', function (Blueprint $table) {

			$table->dropForeign('partners_club_id_foreign');
			$table->dropColumn('club_id');

		});
	}
}
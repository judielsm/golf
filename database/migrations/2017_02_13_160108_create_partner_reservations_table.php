<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('partner_reservations', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('partner_id')->unsigned()->index();
			$table->foreign('partner_id')
					->references('id')->on('partners')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('reservation_id')->unsigned()->index();
			$table->foreign('reservation_id')
					->references('id')->on('reservations')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('partner_reservations');
	}
}
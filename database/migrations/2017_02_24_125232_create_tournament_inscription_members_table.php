<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentInscriptionMembersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_inscription_members', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('waiting')->default(0);
			$table->tinyInteger('enabled')->default(1);

			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')
					->references('id')->on('users')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('tournament_id')->unsigned()->index();
			$table->foreign('tournament_id')
					->references('id')->on('tournaments')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('member_id')->unsigned()->index();
			$table->foreign('member_id')
					->references('id')->on('members')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('category_id')->unsigned()->index();
			$table->foreign('category_id')
					->references('id')->on('categories')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tournament_inscription_members');
	}
}
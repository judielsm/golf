<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkDrawDayDrawTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('day_draws', function (Blueprint $table) {

			$table->integer('draw_id')->unsigned()->nullable()->after('enabled')->index();
			$table->foreign('draw_id')
				 ->references('id')->on('draws')
				 ->onUpdate('set null')
				 ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('day_draws', function (Blueprint $table) {

			$table->dropForeign('day_draws_draw_id_foreign');
			$table->dropColumn('draw_id');

		});
	}
}
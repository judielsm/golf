<?php

use Illuminate\Database\Seeder;

class DrawingDaysTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$reservations = Golf\Reservation::select('id')->get();
		$members = Golf\Member::select('id')->get();

		$reservations_array = array();

		foreach ($reservations as $key => $value) {
			$reservations_array[$key] = $value->id;
		}

		$members_array = array();

		foreach ($members as $key => $value) {
			$members_array[$key] = $value->id;
		}

		for ($i=0; $i < 300; $i++) { 

			DB::table('drawing_days')->insert([
				'date' => $faker->dateTimeBetween( 'now', '+30 years', date_default_timezone_get()),
			]);
		}
	}
}

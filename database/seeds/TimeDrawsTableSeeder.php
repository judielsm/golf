<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class TimeDrawsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$day_draws = Golf\DayDraw::select('id')->orderBy("draw_id","asc")->get();

		$day_draws_array = array();

		foreach ($day_draws as $key => $value) {
			$day_draws_array[$key] = $value->id;
		}

		for ($i=0; $i < 35; $i++) {

			DB::table('time_draws')->insert([
				'time' => $faker->time('H:i'),
				'day_draw_id' => $faker->randomElement($day_draws_array),
				'created_at' => Carbon::now(),
			]);
		}
	}
}

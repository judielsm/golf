<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class MemberReservationsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$reservations = Golf\Reservation::select('id')->get();
		$members = Golf\Member::select('id')->get();

		$reservations_array = array();

		foreach ($reservations as $key => $value) {
			$reservations_array[$key] = $value->id;
		}

		$members_array = array();

		foreach ($members as $key => $value) {
			$members_array[$key] = $value->id;
		}

		for ($i=0; $i < 30; $i++) {

			DB::table('member_reservations')->insert([
				'member_id' => $faker->randomElement($members_array),
				'reservation_id' => $faker->randomElement($reservations_array),
				'created_at' => Carbon::now(),
			]);
		}
	}
}

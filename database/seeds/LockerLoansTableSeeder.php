<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LockerLoansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker::create('es_ES');
		$status = [
				 'Operativo',
				 'Prestamo',
				 'Indisponible',
				];

		$lockers = Golf\Locker::select('id')->get();

		$lockers_array = array();

		foreach ($lockers as $key => $value) {
			$lockers_array[$key] = $value->id;
		}

		$mebers = Golf\Member::select('id')->get();

		$members_array = array();

		foreach ($mebers as $key => $value) {
			$members_array[$key] = $value->id;
		}

		for ($i=0; $i < 20; $i++) {

			DB::table('locker_loans')->insert([
				'observations' => $faker->realText(50),

				'locker_id' =>  $faker->randomElement($lockers_array),
				'member_id' =>  $faker->randomElement($members_array),

				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			]);
		}
    }
}

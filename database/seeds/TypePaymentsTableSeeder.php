<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class TypePaymentsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$types=[
				'Efectivo',
				'TDC',
				'TDD',
				'Cuenta de socio',
				'Transferencia',
				'Cheque',
			];

		foreach ($types as  $name) {

			DB::table('type_payments')->insert([
				'name' =>  $name,
				'created_at' => Carbon::now(),
			]);
		}
	}
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class TournamentsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$modalities = Golf\Modality::get();
		$modalities_array = array();
		foreach ($modalities as $key => $value) {
			$modalities_array[$key] = $value->id;
		}

		for ($i=0; $i < 10; $i++) {

			$start_date_inscription = $faker->dateTimeBetween( 'now', '+1 years', date_default_timezone_get());

			$start_date_inscription = new Carbon($start_date_inscription->format('Y-m-d'));

			$end_date_inscription = new Carbon($start_date_inscription->addWeeks(rand(1, 4))->format('Y-m-d'));

			$start_date = new Carbon($end_date_inscription->addWeeks(rand(2, 10))->format('Y-m-d'));

			$end_date = new Carbon($end_date_inscription->addWeeks(rand(2, 4))->format('Y-m-d'));

			DB::table('tournaments')->insert([
				'name' => $faker->realText(50),
				'inscription_fee' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 99),
				// dateTime($max, $tz)
				'start_date' => $start_date,
				'end_date' => $end_date,
				'start_date_inscription' => $start_date_inscription,
				'end_date_inscription' => $end_date_inscription,
				'start_time' => $faker->time('H:i'),
				'end_time' => $faker->time('H:i'),
				'modality_id' =>  $faker->randomElement($modalities_array),
				'created_at' => Carbon::now(),
			]);
		}
	}
}

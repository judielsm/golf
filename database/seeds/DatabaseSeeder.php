<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//catalogos
		$this->call(GolfCoursesTableSeeder::class);
		$this->call(ClubsTableSeeder::class);
		$this->call(ModalitiesTableSeeder::class);
		$this->call(ProductsTableSeeder::class);
		$this->call(TransactionStatusTableSeeder::class);
		$this->call(TypePaymentsTableSeeder::class);
		$this->call(ReservationDefinitionsTableSeeder::class);
		$this->call(RolesTableSeeder::class);

		$this->call(UsersTableSeeder::class);
		$this->call(MembersTableSeeder::class);
		$this->call(DrawsTableSeeder::class);
		$this->call(DayDrawsTableSeeder::class);
		$this->call(TimeDrawsTableSeeder::class);
		$this->call(PartnersTableSeeder::class);
		$this->call(ReservationsTableSeeder::class);
		$this->call(MemberReservationsTableSeeder::class);
		$this->call(PartnerReservationsTableSeeder::class);
		$this->call(DrawReservationsTableSeeder::class);
		//$this->call(StartTimesTableSeeder::class);
		$this->call(CategoriesTableSeeder::class);
		$this->call(TournamentsTableSeeder::class);
		$this->call(ParticipantTournamentCategoriesTableSeeder::class);
		$this->call(LockersTableSeeder::class);
		
	}
}

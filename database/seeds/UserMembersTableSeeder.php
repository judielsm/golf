<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class UserMembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = Golf\User::where('role_id', '=', 6)->select('id')->get();

		$faker = Faker\Factory::create('es_ES');

		for ($i=0; $i < 10; $i++) {
			DB::table('users')->insert([
				'name' => $faker->name,
				'email' => $faker->email,
				'password' => bcrypt('123456'),
				'role_id' => 6,
				'created_at' => Carbon::now(),
			]);
		}

		$users = Golf\User::where('role_id', '=', 6)->select('id')->take(9999)->offset(sizeof($users)-1)->get();

		$sex = ['Femenino','Masculino'];

		foreach ($users as $key => $value) {
			DB::table('members')->insert([
				'name' => $faker->firstname,
				'last_name' => $faker->lastname,
				'identity_card' => $faker->randomNumber(8),
				'sex' => $sex[rand(0,1)],
				'number_action' => $faker->randomNumber(8),
				'phone' => $faker->phoneNumber,
				'handicap' => $faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 36),
				'user_id' => $value->id,
				'created_at' => Carbon::now(),
			]);
		}
    }
}

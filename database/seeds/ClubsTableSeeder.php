<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ClubsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');
		$variable = [
			'BARQUISIMETO GOLF CLUB',
			'CARABALLEDA GOLF & YACHT CLUB',
			'CARACAS COUNTRY CLUB',
			'CARDON GOLF CLUB',
			'CLUB DE GOLFISTAS ASOCIADOS  A.C.  (Municipio El Hatillo, Edo. Miranda) "Lagunita"',
			'CARDONCLUB DE GOLFISTAS COMUNIDAD SOCIAL Y DEPORTIVA EL MENITO GOLF',
			'CLUB DEPORTIVO DE GOLF CARONOCO, A.C',
			'CLUB DE GOLF DEL CENTRO',
			'CLUB DE GOLFISTAS DE MARACAY',
			'CLUB DE GOLF JUVENIL DE ARAGUA',
			'ASOCIACION CIVIL DE GOLF DE MARACAY',
			'CLUB DE GOLF SAN VALENTÍN',
			'CUMANAGOTO GOLF CLUB',
			'EL MORRO GOLF CLUB',
			'GUATAPARO COUNTRY CLUB',
			'ISLA DE MARGARITA GOLF CLUB',
			'IZCARAGUA COUNTRY CLUB',
			'JUNKO GOLF CLUB',
			'LA CUMACA GOLF CLUB',
			'LOS ANAUCOS GOLF CLUB',
			'LOS CANALES GOLF CLUB',
			'MARACAIBO COUNTRY CLUB',
			'PGA DE VENEZUELA',
			'PUERTO LA CRUZ GOLF & COUNTRY CLUB',
			'SAN LUIS COUNTRY CLUB',
			'SAN MIGUEL COUNTRY CLUB',
			'VALLE ARRIBA GOLF CLUB',
		];

		foreach ($variable as $key => $value) {
			DB::table('clubs')->insert([
				'name' => $value,
				'green_fee' => $faker->randomFloat($nbMaxDecimals = 2, $min = 30, $max = 99),
				'created_at' => Carbon::now(),
			]);
		}
	}
}

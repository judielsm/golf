<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class StartTimesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		for ($i=0; $i < 20; $i++) {

			DB::table('start_times')->insert([
				'start_time' => $faker->time('H:i'),
				'end_time' => $faker->time('H:i'),
				'created_at' => Carbon::now(),
			]);
		}
	}
}

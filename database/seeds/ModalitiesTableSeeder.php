<?php

use Illuminate\Database\Seeder;

class ModalitiesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$modality=['Dobles',
				'Threesomes',
				'Shotgun',
				'Horario Continuo',
				'Un rango de horario por un tee',
				'Dos rango de horario por un tee',
				'Un rango de horario por dos tee',
				'Dos rango de horario por dos tee'
			];

		foreach ($modality as $mod) {

			DB::table('modalities')->insert([
				'name' =>$mod,
				'created_at' => date("Y-m-d H:i:s"),
				'updated_at' => date("Y-m-d H:i:s")
			]);
		}

	}
}

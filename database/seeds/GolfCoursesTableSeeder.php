<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GolfCoursesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('golf_courses')->insert([
			'interval' => 10,
			'start_time' => "7:00",
			'end_time' => "18:00",
			'created_at' => Carbon::now(),
		]);
	}
}

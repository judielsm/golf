<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class ReservationsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$users = Golf\User::get();
		$users_array = array();
		foreach ($users as $key => $value) {
			$users_array[$key] = $value->id;
		}

		$status = array(
					'En proceso',
					'Sorteable',
					'Aprobada',
					'Cancelada',
					'Cerrada',
					'Finalizada',
				);

		for ($i=0; $i < 20; $i++) {

			DB::table('reservations')->insert([
				'receipt_payment' => $faker->randomNumber(2),
				'detail' => $faker->realText(50),
				// dateTime($max, $tz)
				'date' => date('Y-m-d', rand(strtotime(date('Y-m-d')), strtotime( date('(Y + 50)-m-d')) )),
				'start_time' => $faker->time('H:i'),
				'status' => $status[rand(0,2)],
				'enabled' => 1,
				//'reservation_definition_id' =>  $faker->randomElement($reservation_definitions_array),
				'user_id' =>  $faker->randomElement($users_array),
				'created_at' => Carbon::now(),
			]);
		}
	}
}

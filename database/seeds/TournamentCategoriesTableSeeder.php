<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class TournamentCategoriesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$categories = Golf\Category::get();
		$categories_array = array();
		foreach ($categories as $key => $value) {
			$categories_array[$key] = $value->id;
		}

		$tournaments = Golf\Tournament::get();
		$tournaments_array = array();
		foreach ($tournaments as $key => $value) {
			for ($i=0; $i < 3; $i++) {

				DB::table('tournament_categories')->insert([

					//'reservation_definition_id' =>  $faker->randomElement($reservation_definitions_array),
					'category_id' =>  $faker->randomElement($categories_array),

					//'reservation_definition_id' =>  $faker->randomElement($reservation_definitions_array),
					'tournament_id' =>  $value->id,
					'created_at' => Carbon::now(),
				]);
			}
		}

	}
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create('es_ES');

		DB::table('users')->insert([
			'name' => 'Root',
			'email' => 'LR@dementecreativo.com',
			'password' => bcrypt('123456'),
			'role_id' => 1,
			'created_at' => Carbon::now(),
		]);
		DB::table('users')->insert([
			'name' => 'Admin',
			'email' => 'AdminRervation@dementecreativo.com',
			'password' => bcrypt('123456'),
			'role_id' => 2,
			'created_at' => Carbon::now(),
		]);
		DB::table('users')->insert([
			'name' => 'Admin Financiero',
			'email' => 'AdminFinanciero@dementecreativo.com',
			'password' => bcrypt('123456'),
			'role_id' => 3,
			'created_at' => Carbon::now(),
		]);
		DB::table('users')->insert([
			'name' => 'Starter',
			'email' => 'Starter@dementecreativo.com',
			'password' => bcrypt('123456'),
			'role_id' => 4,
			'created_at' => Carbon::now(),
		]);
		DB::table('users')->insert([
			'name' => 'Maletero',
			'email' => 'Maletero@dementecreativo.com',
			'password' => bcrypt('123456'),
			'role_id' => 5,
			'created_at' => Carbon::now(),
		]);
		DB::table('users')->insert([
			'name' => 'JotaJota',
			'email' => 'jd@dementecreativo.com',
			'password' => bcrypt('123456'),
			'role_id' => 2,
			'created_at' => Carbon::now(),
		]);
		DB::table('users')->insert([
			'name' => 'JotaJota',
			'email' => 'jd@lagunita.com',
			'password' => bcrypt('123456'),
			'role_id' => 6,
			'created_at' => Carbon::now(),
		]);

		for ($i=0; $i < 10; $i++) {
			DB::table('users')->insert([
				'name' => $faker->name,
				'email' => $faker->email,
				'password' => bcrypt('123456'),
				'role_id' => 6,
				'created_at' => Carbon::now(),
			]);
		}
    }
}

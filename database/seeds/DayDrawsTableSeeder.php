<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class DayDrawsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$draws = Golf\Draw::select('id')->get();

		$draws_array = array();

		foreach ($draws as $key => $value) {
			$draws_array[$key] = $value->id;
		}

		// $members = Golf\Member::select('id')->take(5)->get();
		// $members_array = array();

		// foreach ($members as $key => $value) {
		// 	$members_array[$key] = $value->id;
		// }

		for ($i=0; $i < 35; $i++) {
			DB::table('day_draws')->insert([
				'date' => date('Y-m-d', rand(strtotime('now'), strtotime( '+10 years', strtotime('now') ) ) ),
				'draw_id' =>  $faker->randomElement($draws_array),
				'created_at' => Carbon::now(),
			]);
		}
	}
}

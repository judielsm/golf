$(document).ready(function(){
	select_tournament();
});


function select_tournament() {
	var date = new Date().toLocaleString('es-Es', { year: "numeric", month: "2-digit", day: '2-digit'}).split('/').reverse().join('/');
	var hora_inicio = new Date(date);
	var hora_final = new Date(date);
	hora_inicio.setHours(06,00,00);
	hora_final.setHours(17,00,00);
	var start_time = new Date(date+' '+value_start_time);
	var end_time = new Date(date+' '+value_end_time);

	var minutos_totales = ((hora_final.getHours() - hora_inicio.getHours()) + 1) * 6;
	var options = '';
	for (var i = 0; i < minutos_totales; i++) {
		if (i !== 0) {
			hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
		}
		var minuto = hora_inicio;
		minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
		if (minuto.split(':')[0].length < 2) {
			minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
		}
		options += '<option value="'+minuto+'">'+minuto+'</option>';
	}

	$('#start_time').html(options);
	$('#end_time').html(options);

	$('#start_time option').filter('[value="'+value_start_time+'"]').prop('selected', true);
	$('#end_time option').filter('[value="'+value_end_time+'"]').prop('selected', true);
	// console.log($('#start_time option').filter('[value="'+value_start_time+'"]'));
	$('.select_tournament').select2();
}
// console.log(start_time);

var detalles_torneo = $('#detalles_torneo');
$('.btn_detalles_torneo').click(function(){
	var ruta = $(this).attr('href');
	$.ajax({
		type: "GET",
		url: ruta,
		// data: {id: id,exonerated: exonerated},
		success: function(respuesta) {
			var tournament = respuesta.tournament;

			$('#detalles_torneo #nombre').html(tournament.name);
			var fecha_inscripcion = new Date(tournament.start_date_inscription.split('-').reverse().join('-').replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			var fecha_tope_inscripcion = new Date(tournament.end_date_inscription.split('-').reverse().join('-').replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			$('#detalles_torneo #fecha_inscripcion span').html(fecha_inscripcion);
			$('#detalles_torneo #fecha_tope_inscripcion span').html(fecha_tope_inscripcion);

// 			$('#detalles_torneo #modalidad').html(tournament.modality.name);

			var categorias = '';
			for (var i = 0; i < tournament.categories.length; i++) {
				categorias += '<h4>'+tournament.categories[i].name+'</h4>';
			}
			$('#detalles_torneo #categorias').html(categorias);


			var fecha_inicio = new Date(tournament.start_date.split('-').reverse().join('-').replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			var fecha_fin = new Date(tournament.end_date.split('-').reverse().join('-').replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			$('#detalles_torneo #fecha_inicio span').html(fecha_inicio);
			$('#detalles_torneo #fecha_fin span').html(fecha_fin);

			$('#detalles_torneo #condicion_torneo').fileinput('destroy');
			if (tournament.conditions_file != null) {
				$('#detalles_torneo #condicion_torneo').fileinput({
					initialPreview: [
						public_directory+tournament.conditions_file
					],
					initialPreviewAsData: true,
					initialPreviewConfig: [
						{type: "pdf", caption: tournament.name, showDelete: false, width: "250px"},
					],

					showUpload: false,
					showRemove: false,
					showBrowse: false,
					showClose: false,
					showCaption: false,
					// showCancel: fasle,
					// showUploadedThumbs: false,
					// 'previewFileType': 'any'
					maxFileCount: 1,
					browseLabel: 'Buscar',
					msgZoomModalHeading: 'Vista previa',
					allowedFileExtensions: ['pdf', 'docx'],
					previewFileIconSettings: {
						'doc': '<i class="fa fa-file-word-o text-primary"></i>',
						'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
					},
					msgInvalidFileExtension: 'Error, solo puede agregar documentos "pdf" o "docx"',
					msgNoFilesSelected: 'No hay archivo seleccionado'
					// showBrowse: false
				});
			}
			else{
				$('#detalles_torneo #condicion_torneo').addClass('hidden');
			}



			setTimeout(function(){
				detalles_torneo.removeClass('none');
				setTimeout(function(){
					detalles_torneo.addClass('active');
				});
			}, 500);
		}
	});
	return false;
});

var modal_lista_espera = $('#modal_lista_espera');
$('.btn_lista_espera').click(function(){
	var ruta = $(this).attr('href');
	$.ajax({
		type: "GET",
		url: ruta,
		// data: {id: id,exonerated: exonerated},
		success: function(respuesta) {
			$('#modal_lista_espera #content').html("");
			var categories = respuesta.categories;

			var table = '';
			if (categories.length > 0) {
				for (var i = 0; i < categories.length; i++) {
					var category = categories[i];
					table += '<thead>';
					table += '	<tr>';
					table += '		<th colspan="5"><h4>Categoria: '+category.name+'</h4></th>';
					table += '	</tr>';
					table += '	<tr>';
					table += '		<th class="th_nombre">Nombre</th>';
					table += '		<th class="th_apellido">Apellido</th>';
					table += '		<th class="th_ci">C.I.</th>';
					table += '		<th class="th_n_accion">Nª accion</th>';
					table += '		<th class="th_telefono">Teléfono</th>';
					table += '	</tr>';
					table += '</thead>';
					table += '<tbody>';
					if (category.members.length > 0) {
						for (var im = 0; im < category.members.length; im++) {
							var member = category.members[im];
							table += '	<tr>';
							table += '		<td>'+member.name+'</td>';
							table += '		<td>'+member.last_name+'</td>';
							table += '		<td>'+member.identity_card+'</td>';
							table += '		<td>'+member.number_action+'</td>';
							table += '		<td>'+member.phone+'</td>';
							table += '	</tr>';
						}
					}else {
						table += '<tr>';
						table += '	  <td colspan="5" class="no_inscrito">No hay usuarios en lista de espera</td>';
						table += '</tr>';
					}
					table += '</tbody>';
				}
			}
			$('#modal_lista_espera #content_listado_espera table').html(table);
			$('#modal_lista_espera').modal();
		}
	});
	return false;
});

$('.detalles_torneo .cerrar_detalles_torneo').click(function(){
	detalles_torneo.removeClass('active');
	setTimeout(function(){
		detalles_torneo.addClass('none');
		$('.home_torneos .owl-carousel').trigger('destroy.owl.carousel');
	}, 700);
});

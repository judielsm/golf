var cont_sorteo = $('.cont_sorteo');
var cancelar_sorteo = $('.cancelar_sorteo');
var aceptar_sorteo = $('.aceptar_sorteo');
var confirmacion = $('.cont_sorteo .sorteo .confirmacion');
var cargando = $('.cont_sorteo .sorteo .cargando');
var parametros = {};
var intervalos_agregados = [];
var date_select = [];
var end_date_draw;

$(document).ready(function(){
	elminar_intervalos();
	agregar_intervalos();
	$('.intervalo_sorteo').select2();
});


function sorteo_dropdown(li_this) {
	var $this = li_this.parent();
	if ($this.filter('.active').length <= 0) {
		$this.addClass('active');
	}else {
		$this.removeClass('active');
	}
}



$("#sortearConfirm").click(function(){
	$("#modalsorteo").modal();
});
$("#modalsorteo").on('hide.bs.modal', function () {
	setTimeout(function(){
		// alert('');
		$('#modalsorteo .resultado').fadeOut(0);
		$('#modalsorteo .cargando').fadeOut(0);
		$('#modalsorteo .confirmacion').fadeIn(0);
		$('#modalsorteo').removeClass('cargando').removeClass('result');
	}, 700);
});

$('#sortear').click(function(){
	var ruta = $(this).attr('ruta');
	var draw_id = $(this).attr('draw_id');
	$.ajax({
		type: "POST",
		url: ruta,
		data: {draw_id: draw_id},
		beforeSend: function(x){
			$('#modalsorteo .cargando').fadeIn(0);
			$('#modalsorteo').addClass('cargando');
		},
		success: function(respuesta) {
			// console.log(respuesta);
			var draw = respuesta;
			var li = '';

			$('#modalsorteo .resultado_sorteo').html('');
			if (draw.type == 'success') {

				if (draw.code === 0) {
					li = '<ul style="list-style: none">';
					li += '<li><p> '+draw.status+' </p></li>';
					li += '</ul>';
				}else {
					var body='';
				    li = '<table class="table table-responsive">';
					li += '<thead>';
					li += '<tr>';
					li += '<td> Fecha  </td>';
					li += '<td> Hora </td>';
					li += '<td> Participantes </td>';
					li += '<tr>';
					li += '</thead>';
					li += '<tbody>';
					for (var i = 0; i < draw.reservations.length; i++) {
						var reservacion = draw.reservations[i];
						body +='<tr>';
						body += '<td>' + reservacion.date + '</td>';
						body += '<td>' + reservacion.start_time + '</td>';
						body += '<td style="text-align:left;">';
						body += '<span> <b> Socios </b> </span> </br>';
						for (var index in reservacion.members) {
							body += '<span>' + reservacion.members[index].name + ' '+ reservacion.members[index].last_name+ '</span> </br>';
						}


						if ( reservacion.partners.length >0) {
							body += '<span> <b> Invitados </b> </span> </br>';
							for (var index in reservacion.partners) {
								body += '<span>' + reservacion.partners[index].name + ' '+ reservacion.partners[index].last_name+ '</span> </br>';
							}

						}else{
							body += '<span> Sin invitados </span>';
						}
							body += '</td>';

					}
					li += body + '</tbody>';
					li += ' </table>';
				}
				$('#modalsorteo .resultado_sorteo').append(li);

				//$('#modalsorteo').modal();
			}else {
				// si la consulta retorna en error
			}
			setTimeout(function(){
				$($('#sortear').attr('li-id')).remove();
				$('#modalsorteo .resultado').fadeIn(0);
				cancelar_draw();
				$('#modalsorteo .confirmacion').fadeOut(0);
				$('#modalsorteo').removeClass('cargando').addClass('result');
				setTimeout(function(){
					$('#modalsorteo .cargando').fadeOut(0);
				},700);
			}, 2000);
		}
	});
});

cancelar_sorteo.click(function(){
	cerrarSortear();
});

aceptar_sorteo.click(function(){
	cont_sorteo.addClass('cargando');
	setTimeout(function(){
		confirmacion.addClass('none');
	},400);
});

function cerrarSortear() {
	cont_sorteo.removeClass('active');
	setTimeout(function(){
		cont_sorteo.addClass('none');
	}, 700);
}

setInterval(function () {
	var random = parseInt(Math.random() * (9999 - 0) + 0);
	$('#modalsorteo .cargando h1').html(random);
}, 100);



/* Datepicker Fecha de Inicio de Crear y Editar Sorteo*/
$( ".date_draw" ).datepicker({
	dateFormat: 'dd-mm-yy',
	minDate: new Date(),
	beforeShowDay: function( date ) {
		var tipo_dia = beforeShowDay(date);
		if( tipo_dia.dias_bloqueados ) {
			 return [true, "dia_bloqueado", 'Dia Bloqueado'];
		}else if( tipo_dia.sorteable ) {
			 return [true, "sorteable", 'Dia Sorteable'];
		}else if (tipo_dia.torneo) {
			return [true, "torneo", 'Dia de torneo'];
		}else {
			return [true, '', ''];
		}
	}

});


function date_draw(min_date) {
	$('#end_date_draw').datepicker( "destroy" );
	$( "#end_date_draw" ).datepicker({
		dateFormat: 'dd-mm-yy',
		minDate: min_date,
		onSelect: function (date) {
			var date_fotmat = date.split('-')[2]+'-'+date.split('-')[1]+'-'+date.split('-')[0];
			var vacio = true;
			$('.date_draw').each(function(){
					if ($(this).val() !== '') {
							vacio = false;
					}
			});
			if (vacio === false) {
					datepicker_draw(date_fotmat);
			}
			setTimeout(function(){
					end_date_draw = $('#end_date_draw').val();
			},20);
		},
		beforeShowDay: function( date ) {
			var tipo_dia = beforeShowDay(date);
			if( tipo_dia.dias_bloqueados ) {
				 return [true, "dia_bloqueado", 'Dia Bloqueado'];
			}else if( tipo_dia.sorteable ) {
				 return [true, "sorteable", 'Dia Sorteable'];
			}else if (tipo_dia.torneo) {
				return [true, "torneo", 'Dia de torneo'];
			}else {
				return [true, '', ''];
			}
		}
	});
}

$('#start_date_draw').change(function(){
		date_draw($('#start_date_draw').val());
});

function agregar_intervalos() {
	$('.drawing ul.dias li .cont .agregar .inputs .boton a').off('click');
	$('.drawing ul.dias li .cont .agregar .inputs .boton a').click(function(e){
		e.preventDefault();
		var inputs = $(this).parent().parent();
		var date = $(this).attr('date');
		var start_time = $('#start_time', inputs);
		var end_time = $('#end_time', inputs);

		var hora_inicio = new Date(date.replace(/-/g, "/")+' '+ start_time.val());
		var hora_final = new Date(date.replace(/-/g, "/")+' '+ end_time.val());
		var format_hora_inicio = hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
		var format_hora_final = hora_final.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });

		var id = hora_inicio.getTime()+hora_final.getTime();
		var li = '';
		var cont = $(this).parent().parent().parent().parent();
		var intervalos = $('.intervalos ul', cont);
		li += '<li id="'+id+'">';
		li += '	<div class="text">Desde: '+format_hora_inicio+'</div>';
		li += '	<div class="text">Hasta: '+format_hora_final+'</div>';
		li += '	<div class="icon">';
		li += '		<a href="" id="'+id+'">';
		li += '			<i class="fa fa-remove"></i>';
		li += '		</a>';
		li += '	</div>';
		li += '</li>';
		li = $.parseHTML(li);

		if (hora_inicio.getTime() <= hora_final.getTime()) {
			if (hora_inicio.getTime() !== hora_final.getTime()) {
				var error = null;
				for (var i = 0; i < intervalos_agregados.length; i++) {
					var intervalo = intervalos_agregados[i];
					if (intervalo.date == date) {
						if (hora_inicio >= intervalo.hora_inicio && hora_inicio <= intervalo.hora_final) {
							error_draw('La hora de inicio ya existe en otro intervalo');
							error = intervalo.id;
							break;
						}else if (hora_final >= intervalo.hora_inicio && hora_final <= intervalo.hora_final) {
							error_draw('La hora final ya existe en otro intervalo');
							error = intervalo.id;
							break;
						}else if (hora_inicio <= intervalo.hora_inicio && hora_final >= intervalo.hora_final) {
							error_draw('Hay un intervalo que esta comprendido en el intervalo selecionado');
							error = intervalo.id;
							break;
						}
					}
				}

				if (error === null) {
					intervalos_agregados.push({ date: date, id:id, hora_inicio: hora_inicio, hora_final: hora_final });
					intervalos.append(li);
				}else {
					$('li', intervalos).each(function(){
						if (parseInt($(this).attr('id')) == error) {
							$('li', intervalos).not($(this)).removeClass('marca');
							$(this).addClass('marca');
							setTimeout(function(){
								$('li', intervalos).removeClass('marca');
							}, 3000);
						}
					});
				}
			}else {
				// input_error($('.divider.diass'), 'Los intervalos no pueden ser iguales', true);
				// setTimeout(function(){
				// 	input_error($('.divider.diass'), null, false);
				// }, 3000);
				error_draw('Los intervalos no pueden ser iguales');


			}
		}else {
			error_draw('La hora de inicio no puedes ser mayor a la hora final');
		}
		elminar_intervalos();
	});
}
function elminar_intervalos() {
	$('.drawing ul.dias li .cont .intervalos ul li .icon a').off('click');
	$('.drawing ul.dias li .cont .intervalos ul li .icon a').click(function(e){
		e.preventDefault();
		var id = parseInt($(this).attr('id'));
		var li = $(this).parent().parent();

		for (var i = 0; i < intervalos_agregados.length; i++) {
			var intervalo = intervalos_agregados[i];
			if (intervalo.id == id) {
				intervalos_agregados.splice(i, 1);
				li.remove();
			}
		}
	});
}
function datepicker_draw(minDate) {
	var defaultDate = ($( ".drawing_days_calendar" ).datepicker( "getDate" ) === null) ? null : $( ".drawing_days_calendar" ).datepicker( "getDate" );
	var min_date = new Date(minDate.split('-')[0]+'.'+minDate.split('-')[1]+'.'+(parseInt(minDate.split('-')[2])));
	var suma = min_date.setDate(min_date.getDate()+1);
	$( ".drawing_days_calendar" ).datepicker("destroy");
	$('#tit_calendar').fadeIn();
	$( ".drawing_days_calendar" ).datepicker({
			minDate: min_date,
			dateFormat: 'yy-mm-dd',
			defaultDate: defaultDate,
			onSelect: function(date){
				if ($('#'+date.replace(/-/g, '_')).length < 1) { //Si ya esta agregada la fecha seleccionada
					var hora_bloqueada = false;
					function encontrar_dia_bloqueado(day_blocked) {
						return day_blocked.date === date;
					}
					var day_blocked = day_blockeds.find(encontrar_dia_bloqueado);
					// console.log(day_blocked);

					var date_object = new Date(date.replace(/-/g, '/'));
					var hora_inicio = new Date(date.replace(/-/g, '/'));
					var hora_final = new Date(date.replace(/-/g, '/'));
					hora_inicio.setHours(06,00,00);
					hora_final.setHours(17,00,00);
					var minutos_totales = ((hora_final.getHours() - hora_inicio.getHours()) + 1) * 6;
					var dia_completo = date_object.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});

					var options = '';
					for (var i = 0; i < minutos_totales; i++) {
						if (i !== 0) {
							hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
						}
						var hora_bloqueada = false;
						if (day_blocked !== undefined) {
							day_blocked.time_blockeds.forEach(function(time_blocked){
								var start_time = new Date(date.replace(/-/g, '/')+' '+time_blocked.start_time);
								var end_time = new Date(date.replace(/-/g, '/')+' '+time_blocked.end_time);
								if (hora_inicio.getTime() >= start_time.getTime() && hora_inicio.getTime() <= end_time.getTime()) {
									hora_bloqueada = true;
								}
							});
						}

						var minuto = hora_inicio;
						minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
						if (minuto.split(':')[0].length < 2) {
							minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
						}
						options += '<option value="'+minuto+'" '+((hora_bloqueada) ? 'disabled' : '')+'>'+minuto+'</option>';
					}




					var html = '';
					html += '<li id="'+date.replace(/-/g, '_')+'" class="li_dias" date="'+date+'">';
					html += '	<div class="head">';
					html += '		<div class="icon">';
					html += '			<a href="#" class="select_date" onclick="select_date($(this)); return false;">';
					html += '				<i class="glyphicon glyphicon-menu-right"></i>';
					html += '			</a>';
					html += '		</div>';
					html += '		<span>'+dia_completo+'</span>';
					html += '		<div class="remove">';
					html += '			<a href="#" class="remove_date" onclick="remove_date($(this)); return false;" date="'+date+'"> <i class="glyphicon glyphicon-remove"></i> </a>';
					html += '		</div>';
					html += '	</div>';
					html += '	<div class="cont">';
					html += '		<div class="agregar">';
					html += '			<div class="sub_titulo">';
					html += '				<h5>Agregar Intervalos</h5>';
					html += '			</div>';
					html += '			<div class="inputs">';
					html += '				<div class="inpt">';
					html += '					<label for=""> Desde </label>';
					html += '					<select class="intervalo_sorteo" name="" id="start_time">';
					html += options;
					html += '					</select>';
					html += '				</div>';
					html += '				<div class="inpt">';
					html += '					<label for=""> Hasta </label>';
					html += '					<select class="intervalo_sorteo" name="" id="end_time">';
					html += options;
					html += '					</select>';
					html += '				</div>';
					html += '				<div class="boton">';
					html += '					<a href="" date="'+date+'">';
					html += '						<i class="fa fa-plus"></i>';
					html += '					</a>';
					html += '				</div>';
					html += '			</div>';
					html += '		</div>';
					html += '		<div class="intervalos">';
					html += '			<div class="sub_titulo">';
					html += '				<h5>Intervalos</h5>';
					html += '			</div>';
					html += '			<ul>';

					html += '			</ul>';
					html += '		</div>';
					html += '	</div>';
					html += '</li>';
					html = $.parseHTML(html);
					$('.cont .agregar .inputs .inpt select', html).select2({
						placeholder: 'Hola',
					});
					// $('.cont .agregar .inputs .inpt select', html).empty().trigger('change');
					$('.drawing ul.dias').append(html);
					agregar_intervalos();
					date_select.push(date);
				}else {
					error_draw('la fecha seleccionada ya esta agregada');

				}
			},
			beforeShowDay: function( date ) {
				var tipo_dia = beforeShowDay(date);
				var dia_selecionado = false;
				for (var i = 0; i < date_select.length; i++) {
					var date_selec = new Date(date_select[i].replace(/-/g, '/'));
					if (date.getTime() == date_selec.getTime()) {
						dia_selecionado = true;
					}
				}
				if (dia_selecionado) {
					return [true, "dia_selecionado", 'Dia selecionado'];
				}else if( tipo_dia.dias_bloqueados ) {
					 return [true, "dia_bloqueado", 'Dia Bloqueado'];
				}else if( tipo_dia.sorteable ) {
					 return [false, "sorteable", 'Dia Sorteable'];
				}else if (tipo_dia.torneo) {
					return [true, "torneo", 'Dia de torneo'];
				}else {
					return [true, '', ''];
				}
			}
	});
}
$('#form_draw').submit(function(e){
	e.preventDefault();

	var error = false;
	date_select.forEach(function(date_selec){
		function encontrar_intervalo_agregado(intervalos_agregado) {
			return intervalos_agregado.date === date_selec;
		}
		var intervalos_agregado = intervalos_agregados.find(encontrar_intervalo_agregado);
		console.log(intervalos_agregado);
		if (intervalos_agregado === undefined) {
			error = true;
		}

	});

	if (error) {
		error_draw('Falta agregar el/los intervalo(s)');
	}else {
		parametros_array();
		setTimeout(function(){
			$('#form_draw')[0].submit();
		}, 10);
	}

});


function error_draw(mensaje) {
	$('.error_draw p').html(mensaje);
	$('.error_draw').addClass('active');
	setTimeout(function(){
		$('.error_draw').removeClass('active');
	}, 4000);
}

function parametros_array() {
		parametros = {};
		var start_date = $('#start_date_draw').val();
		var draw_date = $('#end_date_draw').val();
		var draw_days = [];

		for (var i = 0; i < intervalos_agregados.length; i++) {
			var intervalos_agregado = intervalos_agregados[i];
			var times = [];
			var hora_inicio = intervalos_agregado.hora_inicio;

			hora_inicio = intervalos_agregado.hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
			if (hora_inicio.split(':')[0].length < 2) {
				hora_inicio = 0+hora_inicio.split(':')[0]+':'+hora_inicio.split(':')[1];
			}
			hora_final = intervalos_agregado.hora_final.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
			if (hora_final.split(':')[0].length < 2) {
				hora_final = 0+hora_final.split(':')[0]+':'+hora_final.split(':')[1];
			}

			function encontrar_draw_day(date) {
			    return date.date === intervalos_agregado.date;
			}
			var draw_day = draw_days.find(encontrar_draw_day);

			if (draw_day === undefined) {
				draw_days.push({
					date: intervalos_agregado.date,
					times: [{ start_time: hora_inicio, end_time: hora_final }]
				});
			}else {
				draw_day.times.push({ start_time: hora_inicio, end_time: hora_final });
			}
		}

		parametros = {
			start_date: start_date,
			draw_date: draw_date,
			draw_days: draw_days
		}
		$('#day_draws').val(JSON.stringify(parametros));
}


/* Mostrar Sorteo Creados */
$('.draws_month').click(function(){
		var li = $(this).parent();

		if (li.filter(".active").length <= 0) {
				$('.mes_draws').removeClass('active');
				li.addClass("active");
		}else {
				li.removeClass("active");
		}
		return false;
});
function select_date($this) {
	var id_li = '#'+$this.parent().parent().parent().attr('id');
	var li = $this.parent().parent().parent();
	if (li.filter('.active').length <= 0 ) {
	// if ($(id_li+'.active').length <= 0) {
			$('.drawing ul.dias li:not('+id_li+'):not(.li_hour)').removeClass('active');
			setTimeout(function(){
					li.addClass('active');
			},500);
	}else {
			li.removeClass('active');
	}
}
function remove_date($this) {
	// console.log($this);
	var id_li = '#'+$this.parent().parent().parent().attr('id');
	var li = $this.parent().parent().parent();
	var date = $this.attr('date');


	$('.cont ul li', li).each(function(){
		var id_intervalo = parseInt($(this).attr('id'));
		for (var i = 0; i < intervalos_agregados.length; i++) {
			var intervalo = intervalos_agregados[i];
			if (id_intervalo === intervalo.id) {
				intervalos_agregados.splice(i, 1);
			}
		}
	});
	var index_date_select = date_select.indexOf(date);
	date_select.splice(index_date_select, 1);
	var end_date_draw = $('#end_date_draw').val();
	end_date_draw = end_date_draw.split('-')[2]+'-'+end_date_draw.split('-')[1]+'-'+end_date_draw.split('-')[0];
	datepicker_draw(end_date_draw);

	li.addClass('mostrar');
	setTimeout(function(){
			li.remove();
	},500);
}


$('.mostrar_draw').click(function(e){
	e.preventDefault();
	var draw_id = parseInt($(this).attr('draw'));
	var li = $(this).parent().parent().parent();
	var ruta = $(this).attr('ruta');
	$('#add_method').html($('#method_put').html());
	function encontrar_draw(id) {
		return id.id === draw_id;
	}
	var draw = draws.find(encontrar_draw);
	$('#sortear').attr('draw_id', draw.id);
	intervalos_agregados = [];
	date_select = [];
	$('.drawing ul.dias').html('');
	draw.day_draws.forEach(function(day_draw){
		var date = day_draw.date;
		var date_object = new Date(date.replace(/-/g, '/'));
		var hora_inicio = new Date(date);
		var hora_final = new Date(date);
		hora_inicio.setHours(06,00,00);
		hora_final.setHours(17,00,00);
		var minutos_totales = ((hora_final.getHours() - hora_inicio.getHours()) + 1) * 6;
		var dia_completo = date_object.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});
		var html = '';
		html += '<li id="'+date.replace(/-/g, '_')+'" class="li_dias" date="'+date+'">';
		html += '	<div class="head">';
		html += '		<div class="icon">';
		html += '			<a href="#" class="select_date" onclick="select_date($(this)); return false;">';
		html += '				<i class="glyphicon glyphicon-menu-right"></i>';
		html += '			</a>';
		html += '		</div>';
		html += '		<span>'+dia_completo+'</span>';
		html += '		<div class="remove">';
		html += '			<a href="#" class="remove_date" onclick="remove_date($(this)); return false;" date="'+date+'"> <i class="glyphicon glyphicon-remove"></i> </a>';
		html += '		</div>';
		html += '	</div>';
		html += '	<div class="cont">';
		html += '		<div class="agregar">';
		html += '			<div class="sub_titulo">';
		html += '				<h5>Agregar Intervalos</h5>';
		html += '			</div>';
		html += '			<div class="inputs">';
		html += '				<div class="inpt">';
		html += '					<label for=""> Desde </label>';
		html += '					<select class="intervalo_sorteo" name="" id="start_time">';
		for (var i = 0; i < minutos_totales; i++) {
			if (i !== 0) {
				hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
			}
			var minuto = hora_inicio;
			minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
			if (minuto.split(':')[0].length < 2) {
				minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
			}
			html += '<option value="'+minuto+'">'+minuto+'</option>';
		}
		hora_inicio.setHours(06,00,00);
		html += '					</select>';
		html += '				</div>';
		html += '				<div class="inpt">';
		html += '					<label for=""> Hasta </label>';
		html += '					<select class="intervalo_sorteo" name="" id="end_time">';
		for (var i = 0; i < minutos_totales; i++) {
			if (i !== 0) {
				hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
			}
			var minuto = hora_inicio;
			minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
			if (minuto.split(':')[0].length < 2) {
				minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
			}
			html += '<option value="'+minuto+'">'+minuto+'</option>';
		}
		html += '					</select>';
		html += '				</div>';
		html += '				<div class="boton">';
		html += '					<a href="" date="'+date+'">';
		html += '						<i class="fa fa-plus"></i>';
		html += '					</a>';
		html += '				</div>';
		html += '			</div>';
		html += '		</div>';
		html += '		<div class="intervalos">';
		html += '			<div class="sub_titulo">';
		html += '				<h5>Intervalos</h5>';
		html += '			</div>';
		html += '			<ul>';
		day_draw.time_draws.forEach(function(time_draw){
			var hora_inicio = new Date(date+' '+ time_draw.start_time);
			var hora_final = new Date(date+' '+ time_draw.end_time);
			var id = hora_inicio.getTime()+hora_final.getTime();
			var format_hora_inicio = hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
			var format_hora_final = hora_final.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });

			html += '<li id="'+id+'">';
			html += '	<div class="text">Desde: '+format_hora_inicio+'</div>';
			html += '	<div class="text">Hasta: '+format_hora_final+'</div>';
			html += '	<div class="icon">';
			html += '		<a href="" id="'+id+'">';
			html += '			<i class="fa fa-remove"></i>';
			html += '		</a>';
			html += '	</div>';
			html += '</li>';
			intervalos_agregados.push({ date: date, id:id, hora_inicio: hora_inicio, hora_final: hora_final });
		});

		html += '			</ul>';
		html += '		</div>';
		html += '	</div>';
		html += '</li>';
		html = $.parseHTML(html);
		$('.cont .agregar .inputs .inpt select', html).select2({
			placeholder: 'Hola',
		});
		$('.drawing ul.dias').append(html);
		agregar_intervalos();
		elminar_intervalos();
		date_select.push(date);
	});


	var draw_start_date = draw.start_date.split('-')[2]+'-'+draw.start_date.split('-')[1]+'-'+draw.start_date.split('-')[0];
	var draw_draw_date = draw.draw_date.split('-')[2]+'-'+draw.draw_date.split('-')[1]+'-'+draw.draw_date.split('-')[0];

	$('#start_date_draw').val(draw_start_date);
	date_draw(draw_draw_date);
	$('#end_date_draw').val(draw_draw_date);
	datepicker_draw(draw.draw_date);

	$('#form_draw').attr('action', ruta);
	$('#form_draw').attr('estado', 1);

	$('#sortearConfirm').fadeIn();
	$('#destroy_draw').fadeIn();
	$('#destroy_draw').attr('ruta', $(this).attr('destroy'));
	li.removeClass('active');
});
$('#cancelar_draw').click(function(){
	cancelar_draw();
});
function cancelar_draw() {
	intervalos_agregados = [];
	$('.drawing ul.dias').html('');
	$('#sortearConfirm').fadeOut();
	$('#form_draw').attr('estado', 0);
	$('#form_draw').attr('action', $('#form_draw').attr('store'));
	$('#add_method').html('');
	$( ".drawing_days_calendar" ).datepicker("destroy");
	$('#start_date_draw').val('');
	$('#end_date_draw').val('');
	$('#destroy_draw').fadeOut();
	$('#end_date_draw').datepicker('destroy');
	date_select = [];
}

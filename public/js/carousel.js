	new WOW().init();
			$('#enviar-btnUser').click(function() {
				if ($('#pw').val() !=  $('#pw2').val()) {
					$('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>ERROR!</strong> La Contraseña no coinside.</div>');
					return false;
				} else {
					$.ajax({
						type: "POST",
						url: "controladores/RegistrarUser.php",
						data: $('#register-form').serialize(),
						success: function(respuesta) {
							console.log(respuesta);
							if (respuesta == 1) {
								window.location.assign("ingresar.php");
							} else {
								$('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>ERROR!</strong> Usuario ya existente.</div>');
							}
						}
					});
					return false;
				}
			});

			$('.boton .bot button.ingresar_, .boton .bot button.registrarse').click(function(){
				if ($(this).hasClass('ingresar_')) {
					$('#ingresar').removeClass('none');
					$('.nosotros .ingresar').removeClass('active');
					setTimeout(function(){
						$('#ingresar').addClass('active');
						$('#registrarse').removeClass('none');
					},500);
				}else if ($(this).hasClass('registrarse')) {
					$('#registrarse').removeClass('none');
					$('.nosotros .ingresar').removeClass('active');
					setTimeout(function(){
						$('#registrarse').addClass('active');
						$('#ingresar').removeClass('none');
					},500);
				}
				return false;
			});
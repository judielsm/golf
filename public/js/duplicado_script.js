var members;
var partners;

$(".carousel").carousel({
		interval: 5000
});
$(".collapse").collapse({
	toggle: false
});
$(document).ready(function(){

	$("a#crear_reporte_pdf,a#crear_reporte_xls").click(function(e) {
		e.preventDefault();
		parametros = '';
		if ($("select#status").length > 0) {
			status_val = $("select#status").val();
			parametros = "?status="+status_val;
		}
		if ($("select#tournament_id").length > 0) {
			tournament_val = $("select#tournament_id").val();
			if (parametros.length == 0) {
				parametros = "?tournament_id="+tournament_val;
			} else {
				parametros = "&tournament_id="+tournament_val;
			}
		}
		if ($("select#group_id").length > 0) {
			group_val = $("select#group_id").val();
			if (parametros.length == 0) {
				parametros = "?group_id="+group_val;
			} else {
				parametros = "&group_id="+group_val;
			}
		}
		start_date_val = $("input#start_date").val();
		end_date_val = $("input#end_date").val();
		location.href = $(this).attr("href")+parametros+"&start_date="+start_date_val+"&end_date="+end_date_val;
	});

	$("#myBtn").click(function(){
		$("#myModal").modal();
	});

		$("#insc").click(function(){
		$("#newModal").modal();
	});

	$("#capture").submit(function(e){

		e.preventDefault();
		e.stopPropagation();
		var form= $('#capture').serializeArray();
		//console.log(form.length);
		//console.log($('#capture #name').val());
		var array = {
		  name: $('#capture #name').val(),
		  number_participants: $('#capture #number_participants').val(),
		  sex: $('#capture #sex').val(),
		  handicap_min: $('#capture #handicap_min').val(),
		  handicap_max: $('#capture #handicap_max').val(),
		};
		//console.log(JSON.stringify(array));
		//console.log(array);
		var formtosend= JSON.stringify(array);
		  //console.log(formtosend);
		  //console.log($('#name').val());
		//console.log(JSON.stringify(form));
		var a= "<option value='"+formtosend+"' selected>"+$('#capture #name').val()+"</option>";
		$('#capture')[0].reset();
		$('#myModal').modal('hide');
		$('#category').append(a);
		//console.log(form);
	});

	$("ul.pagination li a,div.table.report li a").click(function (e) {
		e.stopPropagation();
		e.preventDefault();
		status_val = $("select#status").val();
		start_date_val = $("input#start_date").val();
		end_date_val = $("input#end_date").val();
		location.href = $(this).attr("href")+"&+status="+status_val+"&start_date="+start_date_val+"&end_date="+end_date_val;
	 });
});

$('.select_categories').select2();





function sorteo_dropdown(li_this) {
	var $this = li_this.parent();
	// console.log(li_date, li_date.filter('.active').length);
	if ($this.filter('.active').length <= 0) {
		$this.addClass('active');
	}else {
		$this.removeClass('active');
	}
}

var cont_sorteo = $('.cont_sorteo');
var cancelar_sorteo = $('.cancelar_sorteo');
var aceptar_sorteo = $('.aceptar_sorteo');
var confirmacion = $('.cont_sorteo .sorteo .confirmacion');
var cargando = $('.cont_sorteo .sorteo .cargando');

$("#sortearConfirm").click(function(){
	$("#modalsorteo").modal();
});
$("#modalsorteo").on('hide.bs.modal', function () {
	setTimeout(function(){
		// alert('');
		$('#modalsorteo .resultado').fadeOut(0);
		$('#modalsorteo .cargando').fadeOut(0);
		$('#modalsorteo .confirmacion').fadeIn(0);
		$('#modalsorteo').removeClass('cargando').removeClass('result');
	}, 700);
});

$('#sortear').click(function(){
	var ruta = $(this).attr('ruta');
	var draw_id = $(this).attr('draw_id');
	$.ajax({
		type: "POST",
		url: ruta,
		data: {draw_id: draw_id},
		beforeSend: function(x){
			$('#modalsorteo .cargando').fadeIn(0);
			$('#modalsorteo').addClass('cargando');
		},
		success: function(respuesta) {
			var draw = respuesta;
			var li = '';

			$('#modalsorteo .resultado_sorteo').html('');
			if (draw.type == 'success') {

				if (draw.code === 0) {
					li = '<li style="list-style: none">';
					li += '<p> '+draw.status+' </p>';
					li += '</li>';
				}else {
					for (var i = 0; i < draw.list_drawn.day_draws.length; i++) {
						var sorteo = draw.list_drawn.day_draws[i];
						// list_drawns
						var fecha = sorteo.date.split('-')[0]+'/'+sorteo.date.split('-')[1]+'/'+sorteo.date.split('-')[2];
						var date = new Date(fecha);
						var dia = date.toLocaleString('es-Es', { weekday: 'long'});
						var mes_ano = date.toLocaleString('es-Es', {  day: '2-digit', month: "long", year: "numeric"});
						var fecha_completa = dia+', '+mes_ano;

						// console.log(date, fecha_completa, fecha);

						// if (sorteo.list_drawns.length >= 1) {
							li += '<li class="li_date">';
							li += '	<a href="#" class="a_date" onclick="sorteo_dropdown($(this)); return false;">';
							li += '		<div class="icon">';
							li += '			<i class="glyphicon glyphicon-menu-right"></i>';
							li += '		</div>';
							li += '		<div class="text">';
							li += '			<span>';
							li += '				'+fecha_completa+'';
							li += '			</span>';
							li += '		</div>';
							li += '	</a>';
							li += '	<ul>';
							if (sorteo.list_drawns.length >= 1) {
								for (var i_r = 0; i_r < sorteo.list_drawns.length; i_r++) {
									var reservation = sorteo.list_drawns[i_r].reservation;
									li += '		<li class="li_hour">';
									li += '			<a href="#" class="a_hour" onclick="sorteo_dropdown($(this)); return false;">';
									li += '				<div class="icon">';
									li += '					<i class="glyphicon glyphicon-menu-right"></i>';
									li += '				</div>';
									li += '				<div class="text">';
									li += '					<span>';
									li += '						'+reservation.start_time+'';
									li += '					</span>';
									li += '				</div>';
									li += '			</a>';
									li += '			<ul class="datos">';
									for (var i_m = 0; i_m < reservation.members.length; i_m++) {
										var member = reservation.members[i_m];
										li += '				<li>';
										li += '					<span style="font-weight: bolder;">'+member.name+' '+member.last_name+'</span>';
										li += '				</li>';
									}
									for (var i_p = 0; i_p < reservation.partners.length; i_p++) {
										var partner = reservation.partners[i_p];
										li += '				<li>';
										li += '					<span>'+partner.name+' '+partner.last_name+'</span>';
										li += '				</li>';
									}

									li += '			</ul>';
									li += '		</li>';
								}
							}else {
								li += '<li style="list-style: none; padding-left: 40px;">';
								li += '			<a href="#" class="a_hour" style="background: #fff; color: #000;">';
								li += '				<div class="text">';
								li += '					<span> No hay reservaciones para este dia</span>';
								li += '				</div>';
								li += '			</a>';
								li += '</li>';
							}

							li += '	</ul>';
							li += '</li>';
						// }else {
						// 	// si no hay dias sorteados
						// }
					}
				}
				$('#modalsorteo .resultado_sorteo').append(li);

				//$('#modalsorteo').modal();
			}else {
				// si la consulta retorna en error
			}
			setTimeout(function(){
				$($('#sortear').attr('li-id')).remove();
				$('#modalsorteo .resultado').fadeIn(0);
				cancelar_draw();
				$('#modalsorteo .confirmacion').fadeOut(0);
				$('#modalsorteo').removeClass('cargando').addClass('result');
				setTimeout(function(){
					$('#modalsorteo .cargando').fadeOut(0);
				},700);
			}, 2000);
		}
	});
});

cancelar_sorteo.click(function(){
	cerrarSortear();
});

aceptar_sorteo.click(function(){
	cont_sorteo.addClass('cargando');
	setTimeout(function(){
		confirmacion.addClass('none');
	},400);
});

function cerrarSortear() {
	cont_sorteo.removeClass('active');
	setTimeout(function(){
		cont_sorteo.addClass('none');
	}, 700);
}

/*Mask*/
$('.money').mask('0000000.00', {reverse: true});
$('.numParticiped').mask('000');
$('.date').mask('00-00-0000');
$('.phone').mask('0000 000-00-00');
$('.cedula').mask('00000000');
$('.input_time').mask('00:00 AA');
$('.3_digit').mask('000');
$('.2_digit').mask('00');
$('.handican').mask('00.0');

function number_format (number, decimals, dec_point, thousands_sep) {
		// Strip all characters but numerical ones.
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
				var k = Math.pow(10, prec);
				return '' + Math.round(n * k) / k;
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

$("#enviar").click(function(){
	Login();
});

setInterval(function () {
	var random = parseInt(Math.random() * (9999 - 0) + 0);
	$('#modalsorteo .cargando h1').html(random);
}, 100);

/*Modal*/

$(document).ready(function(){
		$("#myBtn1").click(function(){
				$("#myModal1").modal();
		});
});

$(document).ready(function(){
		$(".modaldelete").click(function(){
				var ruta = $(this).attr('ruta');
				$('#modaldelete form').attr('action', ruta );

				$("#modaldelete").modal();
		});
		$(".modalcreate").click(function(){
				$("#modalcreate").modal();
		});
		$(".modal_validation").click(function(){
			var url = $(this).attr('href');
			var route = $(this).attr('route');
			console.log(url);
			$.ajax({
				type: "GET",
				url: route,
				//data: {id: id,exonerated: exonerated},
				success: function(respuesta) {
					console.log(respuesta.status);
					console.log(url);
					if (respuesta.status == true) {
						window.location.href = url;
					} else {
						$("#form_modal_validate").attr('action', url);
						$("#modal_validation").modal();
					}
				}
			});
			return false;
		});

		$(".partner_show").click(function(){
			var url = $(this).attr('href');
			$.ajax({
				type: "GET",
				url: url,
				//data: {id: id,exonerated: exonerated},
				success: function(respuesta) {
					var partner = respuesta.partner;
					$('#d_name').html(partner.name);
					$('#d_last_name').html(partner.last_name);
					$('#d_identity_card').html(partner.identity_card);
					$('#d_sex').html(partner.sex);
					$('#d_phone').html(partner.phone);
					$('#d_email').html(partner.email);
					$('#d_handicap').html(partner.handicap);
					$('#d_exonerated').html(partner.exonerated);
					$('#d_club').html(partner.club);

					$("#modaldetalles").modal();
				}
			});
			return false;
		});

		$(".boton_ver_detalle").click(function(){
			var url = $(this).attr('href');
			var id = $(this).attr('id');
			$.ajax({
				type: "GET",
				url: url,
				data: {id: id},
				success: function(torneo) {
					$('#ver_detalles_torneo #name').val(torneo.name);
					$('#ver_detalles_torneo #start_date_tournament').val(torneo.start_date);
					$('#ver_detalles_torneo #end_date_tournament').val(torneo.end_date);
					$('#ver_detalles_torneo #inscription_fee').val(torneo.inscription_fee);
					$('#ver_detalles_torneo #modality').html("<option value="+torneo.modality.id+" disabled selected>"+torneo.modality.name+"</option>");
					html = "";
					for (var i = torneo.categories.length - 1; i >= 0; i--) {
						html += "<option value="+torneo.categories[i].id+" disabled selected>"+torneo.categories[i].name+"</option>";
					}
					$('#ver_detalles_torneo #category').html(html);
					$('#ver_detalles_torneo #start_time').val(torneo.start_time);
					$('#ver_detalles_torneo #end_time').val(torneo.end_time);
					$('#ver_detalles_torneo #start_date_inscription').val(torneo.start_date_inscription);
					$('#ver_detalles_torneo #end_date_inscription').val(torneo.end_date_inscription);
					$("#file_tournament_detail").fileinput('destroy');
					$("#file_tournament_detail").fileinput({
						initialPreview: [
							$('#file_tournament_detail').attr('asset')+torneo.conditions_file
						],
						initialPreviewAsData: true,
						initialPreviewConfig: [
							{type: "pdf", caption: torneo.conditions_file, showDelete: false},
						],

						showUpload: false,
						showRemove: false,
						showBrowse: false,
						showClose: false,
						showCaption: false,
						// showCancel: fasle,
						// showUploadedThumbs: false,
						// 'previewFileType': 'any'
						maxFileCount: 1,
						msgZoomModalHeading: 'Vista previa',
						allowedFileExtensions: ['pdf', 'docx'],
						previewFileIconSettings: {
							'doc': '<i class="fa fa-file-word-o text-primary"></i>',
							'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
						},
						// showBrowse: false
					});
				}
			});
			$("#ver_detalles_torneo").modal();
			return false;
		});

		$(".partner_show").click(function(){
			var url = $(this).attr('href');
			$.ajax({
				type: "GET",
				url: url,
				//data: {id: id,exonerated: exonerated},
				success: function(respuesta) {
					var partner = respuesta.partner;
					$('#d_name').html(partner.name);
					$('#d_last_name').html(partner.last_name);
					$('#d_identity_card').html(partner.identity_card);
					$('#d_sex').html(partner.sex);
					$('#d_phone').html(partner.phone);
					$('#d_email').html(partner.email);
					$('#d_handicap').html(partner.handicap);
					$('#d_exonerated').html(partner.exonerated);
					$('#d_club').html(partner.club);

					$("#modaldetalles").modal();
				}
			});
			return false;
		});

		$(".boton_ver_detalle").click(function(){
			var url = $(this).attr('href');
			var id = $(this).attr('id');
			$.ajax({
				type: "GET",
				url: url,
				data: {id: id},
				success: function(torneo) {
					$('#ver_detalles_torneo #name').val(torneo.name);
					$('#ver_detalles_torneo #start_date_tournament').val(torneo.start_date);
					$('#ver_detalles_torneo #end_date_tournament').val(torneo.end_date);
					$('#ver_detalles_torneo #inscription_fee').val(torneo.inscription_fee);
					$('#ver_detalles_torneo #modality').html("<option value="+torneo.modality.id+" disabled selected>"+torneo.modality.name+"</option>");
					html = "";
					for (var i = torneo.categories.length - 1; i >= 0; i--) {
						html += "<option value="+torneo.categories[i].id+" disabled selected>"+torneo.categories[i].name+"</option>";
					}
					$('#ver_detalles_torneo #category').html(html);
					$('#ver_detalles_torneo #start_time').val(torneo.start_time);
					$('#ver_detalles_torneo #end_time').val(torneo.end_time);
					$('#ver_detalles_torneo #start_date_inscription').val(torneo.start_date_inscription);
					$('#ver_detalles_torneo #end_date_inscription').val(torneo.end_date_inscription);
					$("#file_tournament_detail").fileinput('destroy');
					$("#file_tournament_detail").fileinput({
						initialPreview: [
							$('#file_tournament_detail').attr('asset')+torneo.conditions_file
						],
						initialPreviewAsData: true,
						initialPreviewConfig: [
							{type: "pdf", caption: torneo.conditions_file, showDelete: false},
						],

						showUpload: false,
						showRemove: false,
						showBrowse: false,
						showClose: false,
						showCaption: false,
						// showCancel: fasle,
						// showUploadedThumbs: false,
						// 'previewFileType': 'any'
						maxFileCount: 1,
						msgZoomModalHeading: 'Vista previa',
						allowedFileExtensions: ['pdf', 'docx'],
						previewFileIconSettings: {
							'doc': '<i class="fa fa-file-word-o text-primary"></i>',
							'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
						},
						// showBrowse: false
					});
				}
			});
			$("#ver_detalles_torneo").modal();
			return false;
		});



			/******************************************************/
			/******************* detalle de estudiante *******************/
			/******************************************************/

		$(".modaldetalle").click(function(){
				var ruta = $(this).attr('ruta');
				$.ajax({
				type: "GET",
				url: ruta,
				//data: {date: date, start_time:start_time},
				success: function(response) {
				var student = response.student;


				var name = $('#name');
				var last_name = $('#last_name');
				var identity_card = $('#identity_card');
				var phone = $('#phone');
				var birthdate = $('#birthdate');
				var member_id = $('#member_id');
				var group_id = $('#group_id');
				var period_id = $('#period_id');

				name.val(student.name);
				last_name.val(student.last_name);
				identity_card.val(student.identity_card);
				phone.val(student.phone);
				birthdate.val(student.birthdate);
				member_id.val(student.member.name);
				// group_id.val(student.group.name);
				// period_id.val(student.period.name);

				$("#modaldetalle").modal();


				}
		});

		});

		$(".addgroup").click(function(){
				$("#addgroup").modal();
		});

		$(".addperiod").click(function(){
				$("#addperiod").modal();
		});
});

$(document).ready(function(){
		$("#myBtn").click(function(){
				$("#myModal").modal();
		});
});
$(document).ready(function(){
		$("#myBtn3").click(function(){
				$("#myModal3").modal();
		});
});
$(document).ready(function(){
		$('#myBtn').click(function(){
			$('.hiden').fadeIn();
		});
});
$(document).ready(function(){
		$('#myBtn').click(function(){
			$('#myBtn1').prop('disabled',false);
		});
});
$(document).ready(function(){
		$('#myBtn').click(function(){
			$('#myBtn3').prop('disabled',false);
		});
});
$(document).ready(function () {

	(function ($) {

			$('#filtrar').keyup(function () {

					var rex = new RegExp($(this).val(), 'i');
					$('.buscar tr').hide();
					$('.buscar tr').filter(function () {
							return rex.test($(this).text());
					}).show();

			});

	}(jQuery));

});
// function modal_activate(ruta, div) {
//   $(div+' form').attr('action', ruta );
// }
$(document).ready(function(){
		$('.jugador').fadeOut();
});
$(document).ready(function(){
		$(".singlebutton1").click(function(){
				$('.jugador').fadeIn();
		});
});
$(document).ready(function(){
		$(".singlebutton2").click(function(){
				$('.jugador').fadeIn();
		});
});

$('.boton .bot button.ingresar_, .boton .bot button.registrarse').click(function(){
	if ($(this).hasClass('ingresar_')) {
		$('#ingresar').removeClass('none');
		$('.nosotros .ingresar').removeClass('active');
		setTimeout(function(){
			$('#ingresar').addClass('active');
			$('#registrarse').removeClass('none');
		},500);
	}else if ($(this).hasClass('registrarse')) {
		$('#registrarse').removeClass('none');
		$('.nosotros .ingresar').removeClass('active');
		setTimeout(function(){
			$('#registrarse').addClass('active');
			$('#ingresar').removeClass('none');
		},500);
	}
	return false;

});
$(document).ready(function(){
	$('.datetime').fadeOut();
});

$(document).ready(function(){
 $('.for').click(function() {

		//Se verifica si la opcion del select esta vacia
		if ($('.timeframe').val().trim() === '') {
				alert('Debes seleccionar una opcion');
		} else {
			 $('.datetime').fadeIn();
		}
	});
});

$(document).ready(function(){
	$(".exonerated").click(function(){
		var url = $(this).attr('route');
		var id = $(this).attr('id');
		var exonerated = $(this).val();
		$.ajax({
			type: "GET",
			url: url,
			data: {id: id,exonerated: exonerated},
			success: function(respuesta) {
				//console.log(respuesta);
			}
		});
		return false;

	});
});

//$('.selectpicker').selectpicker();



$(document).ready(function(){
	$("#procesar_transaccion").click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$('#payers').html($('#lista_payer partner').html());

		$('#modalPayer').modal();

	});
});
var vista_reservacion = ($('#reservacion').length > 0) ? true : false;
function buscar_socios_reservacion() {
	$.ajax({
		type: "GET",
		url: '/socios/alls',
		success: function(respuesta) {
			if (respuesta.status === true) {
				members = respuesta.response;
				var option = '';
				option = '<option value="" disabled selected>Seleccionar Socio</option>';

				for (var i = 0; i < members.length; i++) {
					var member = members[i];
					// var nombre = member.name + ' ' + member.last_name;
					var id = member.id;
					option += '<option value="'+id+'">'+ member.nombre +'</option>';
				}
				$('#add_members').html(option);
			}
		}
	});
}
function buscar_invitados_reservacion() {
	var url = ($('.type_member').length < 1) ? '/invitados/alls' : '/s_invitados/alls';
	var data = ($('.type_member').length < 1) ? {} : {user_id: 17};
	$.ajax({
		type: "GET",
		url: url,
		data: data,
		success: function(respuesta) {
			if (respuesta.status === true) {
				partners = respuesta.response;
				var option = '';
				option = '<option value="" disabled selected>Seleccionar Invitado</option>';

				for (var i = 0; i < partners.length; i++) {
					var partner = partners[i];
					// var nombre = partner.name + ' ' + partner.last_name;
					var id = partner.id;
					option += '<option value="'+id+'">'+ partner.nombre +'</option>';
				}
				$('#add_partners').html(option);
			}
		}
	});
}
function mensaje_reservation($this, mensaje_, tipo, usuario) {
	var mensaje = $('.mensaje', $this.parent().parent());
	text = '<p> '+ mensaje_ +' </p>';
	mensaje.html(text);
	mensaje.addClass('mostrar');
	$('#'+tipo+'_'+usuario.id).addClass('marcar');
	setTimeout(function(){
		$('#'+tipo+'_'+usuario.id).removeClass('marcar');
		mensaje.removeClass('mostrar');
	}, 5000);

}
function input_error(selector, mensaje, estado) {
	var contenedor = selector.parent();
	var input_error = $('.input_error', contenedor);
	if (estado) {
		$('p', input_error).html(mensaje);
		input_error.removeClass('none');
		setTimeout(function(){
			contenedor.addClass('error');
		}, 10);

		console.log(input_error, contenedor);
	}else {
		contenedor.removeClass('error');
		setTimeout(function(){
			input_error.addClass('none');
			$('p', input_error).html('');
		}, 400);

	}
}
var date_reservation_selected = '';
function select_hour_reservation($this) {
	$('#time_reservacion').val($this.val());
	var check = $this.prop('checked');
	var icon = $this.parent();
	var icon_not = $('.edit .horas_reservacion ul li .minutos ul li .icon').not(icon);
	if (check === true) {
		icon_not.removeClass('selected');
		icon.addClass('selected');
		input_error($('.horas_reservacion ul'), null, false);
	}else {
		icon.removeClass('selected');
	}
}
function exonerar(id_selector) {
	var id = parseInt(id_selector.split('_')[1]);
	if (partners_green_fee.indexOf(id) == -1) {
		partners_green_fee.push(id);
		$(id_selector).addClass('select_green_fee');
		if ($('.green_fee select', $(id_selector)).val() !== '') {
			green_fee_mounts.push({ id: id, id_green_fee: parseInt($('.green_fee select', $(id_selector)).val()) });
		}
	}else {
		partners_green_fee.splice(partners_green_fee.indexOf(id), 1);
		$(id_selector).removeClass('select_green_fee');
		for (var i = 0; i < green_fee_mounts.length; i++) {
			var green_fee_mount = green_fee_mounts[i];
			if (green_fee_mount.id == id) {
				green_fee_mounts.splice(i, 1);
			}
		}
	}





	if (partners_green_fee.length < 1) {
		$('.reserv .inputs .boton').removeClass('pagar');
	}else {
		$('.reserv .inputs .boton').addClass('pagar');
	}
}

function cerrar_add(cerrar) {
	var tipo = cerrar.split('_')[0].split('#')[1];
	var id = parseInt(cerrar.split('_')[1]);
	if (tipo == 'partner') {
		if (partners_green_fee.indexOf(id) != -1) {
			partners_green_fee.splice(partners_green_fee.indexOf(id), 1);
		}
		partners_agregados.splice(partners_agregados.indexOf(id), 1);
	}else {
		var pagador = '#option_member_'+cerrar.split('_')[1];
		$(pagador).remove();
		members_agregados.splice(members_agregados.indexOf(id), 1);

	}
	for (var i = 0; i < green_fee_mounts.length; i++) {
		var green_fee_mount = green_fee_mounts[i];
		if (green_fee_mount.id == id) {
			green_fee_mounts.splice(i, 1);
		}
	}
	if (partners_green_fee.length < 1) {
		$('.reserv .inputs .boton').removeClass('pagar');
	}
	$(cerrar).fadeOut(400);
	setTimeout(function(){
		$(cerrar).remove();
	},400);
}
function select_green_fee(selector, id) {
	input_error(selector, null, false);

	var object = { id: id, id_green_fee: parseInt(selector.val()) };
	var index;
	if (green_fee_mounts.length > 0) {
		for (var i = 0; i < green_fee_mounts.length; i++) {
			var green_fee_mount = green_fee_mounts[i];
			if (green_fee_mount.id == id) {
				index = i;
			}
		}
		if (index === undefined) {
			green_fee_mounts.push(object);
		}else {
			green_fee_mounts.splice(index, 1, object);
		}
	}else {
		green_fee_mounts.push(object);
	}
}

function mostrar_procesar(estado, tipo_pago) {
	var monto = 0;
	for (var igfm = 0; igfm < green_fee_mounts.length; igfm++) {
		var green_fee_mount = green_fee_mounts[igfm];
		for (var igf = 0; igf < green_fees.length; igf++) {
			var green_fee = green_fees[igf];
			if (green_fee_mount.id_green_fee == green_fee.id) {

				monto += green_fee.green_fee;
			}
		}
	}
	var procesar = $('.procesar');
	if (estado) {
		var tipo_pago_nombre = tipo_pago.split('-')[0];
		var tipo_pago_id = tipo_pago.split('-')[1];
		var html = '';
		html += '<div class="titulo">';
		html += '	<h1> Procesar Pago </h1>';
		html += '</div>';
		html += '<ul class="tipo_pago">';
		html += '	<li>';
		html += '		<p> '+ ( (tipo_pago == 'pago_online') ? 'Online' : 'En el club' ) +' </p>';
		html += '	</li>';
		html += '	<li>';
		html += '		<img src="/img/icons/'+tipo_pago_nombre+'.png" alt="">';
		html += '		<input type="hidden" name="type_payment_id" value="'+tipo_pago_id+'">';
		html += '	</li>';
		html += '</ul>';
		html += '<div class="inpt socio_pagador">';
		html += '	<label class="" for="receipt_payment">Buscar socio a cancelar la reserva</label>';
		html += '	<select class="js-example-basic-single form-control input-md" id="socio_pagador" name="payer_id">';
		html += '			<option value="" disabled selected>Seleccionar socio</option>';
							for (var i = 0; i < members.length; i++) {
								var member = members[i];
								if (members_agregados.indexOf(member.id) != -1) {
									html += '<option value="'+member.id+'">'+member.nombre+'</option>';
								}
							}
		html += '	</select>';
		html += '	<div class="input_error none">';
		html += '		<p></p>';
		html += '	</div>';
		html += '</div>';
		html += '<div class="monto">';
		html += '	<h1>'+ number_format(monto, 2, ',', '.') +' BsF</h1>';
		html += '</div>';
		html += '<div class="condiciones">';
		html += '	<div class="inpt condiciones">';
		html += '		<input type="checkbox" name="condiciones" value="true" id="condiciones">';
		html += '		<label for="">Acepto las <a href="#">Condiciones de pago</a></label>';
		html += '		<div class="input_error none">';
		html += '			<p></p>';
		html += '		</div>';
		html += '	</div>';
		html += '</div>';
		html += '<div class="form-group boton">';
		html += '	<button type="button" class="btn btn-primary singlebutton1 submit">Procesar</button>';
		html += '	<button type="button" class="btn btn-primary singlebutton1" onclick="mostrar_procesar(false);">Cancelar</button>';
		html += '</div>';
		html = $.parseHTML(html);

		if ($('.type_member').length > 0) {
			$('.socio_pagador', html).remove();
		}

		$('.procesar .form').html(html);
		$('#socio_pagador').select2();
		procesar.removeClass('none');
		setTimeout(function(){
			procesar.addClass('active');
		}, 10);

		$('.procesar .form .boton button').filter('.submit').click(function(){
			var enviar = false;

			if ( $('#socio_pagador').val() === '' || $('#socio_pagador').val() === null ) {
				input_error($('#socio_pagador'), 'Debes seleccionar el socio a cancelar la reservacion', true);
			}else if ($('#condiciones').prop('checked') === false) {
				input_error($('#condiciones'), 'Indicar si esta de acuerdo con las condiciones de pago', true);
			}else {
				enviar = true;
			}

			if (enviar) {
				$('#form_reservacion')[0].submit();
			}
		});
		$('#socio_pagador').on('select2:select', function (evt) {
			input_error($('#socio_pagador'), null, false);
		});
		$('#condiciones').change(function (evt) {
			input_error($('#condiciones'), null, false);
		});
	}else {
		procesar.removeClass('active');
		setTimeout(function(){
			$('.procesar .form').html('');
			procesar.addClass('none');
		}, 700);
	}
}
// function procesar_pago() {
// 	console.log('');
// }
var green_fee_mounts = [];
if (vista_reservacion) {
	var members_agregados = [];
	var partners_agregados = [];
	var partners_green_fee = [];

	// setInterval(function () {
	// 	console.log(members_agregados);
	// }, 1000);

	$('#add_members').on('select2:select', function (evt) {
		var $this = $(this);
		var id = parseInt(evt.params.data.id);
		var member;
		for (var i = 0; i < members.length; i++) {
			if (members[i].id == id) {
				member = members[i];
			}
		}
		var li, text;
		li =	'<li id="member_'+member.id+'" class="member_reservation" id-="'+member.id+'">';
		li +=		'<input type="hidden" class="member_id" name="member_id[]" value="'+member.id+'">';
		li +=		'<div class="nombre">';
		li +=			member.nombre;
		li +=		'</div>';
		li +=		'<div>';
		if (member.user == 2) {
			li +=			'C.I: '+ number_format(member.identity_card, 0, ',', '.');
		}else{
			li +=			'N° Accion: '+ member.number_action;
		}
		li +=		'</div>';
		li +=		'<div class="cerrar" onclick="cerrar_add(\'#member_'+member.id+'\'); return false;">';
		li +=			'<a href="#" class="cerrar_add">';
		li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
		li +=			'</a>';
		li +=		'</div>';
		li +=	'</li>';
		li = $.parseHTML(li);


		if ($('.type_member').length <= 0) { //Verifico si es tipo admin
			if ($('#member_'+member.id).length <= 0) {
				$('#socio_pagador').append('<option id="option_member_'+member.id+'" value="'+member.id+'">'+member.nombre+'</option>');
				$('.add_socios ul').append(li);
				input_error($('#add_members'), null, false);
				members_agregados.push(member.id);
			}else {
				mensaje_reservation($this, 'El socio '+member.nombre+' esta en la reservacion', 'member', member);
			}
		}else { //sino se agregan validaciones para tipos miembros
			$.ajax({
				type: "GET",
				url: '/socios/'+user_id+'/validar_reservacion_socio',
				// url: 'http://dementecreativo.com/lagunita/golf/socios/'+user_id+'/validar_reservacion_socio'
				data: {
					member_id: member.id,
					date: $('#date_reservation').val(),
				},
				success: function(respuesta) {
					if (respuesta.validation === true) {
						var total = partners_agregados.length + members_agregados.length;
						if (total < 3) {
							if ($('#member_'+member.id).length <= 0) {
								$('.add_socios ul').append(li);
								members_agregados.push(member.id);
							}else {
								mensaje_reservation($this, 'El socio '+member.nombre+' esta en la reservacion', 'member', member);
							}
						}else {
							mensaje_reservation($this,'Ya la reservacion esta completa', 'member', member);
						}
					} else {
						mensaje_reservation($this, respuesta.message, 'member', member);

					}
				}
			});
		}
	});



	$('#add_partners').on('select2:select', function (evt) {
		var $this = $(this);
		var id = parseInt(evt.params.data.id);
		var partner;
		for (var i = 0; i < partners.length; i++) {
			if (partners[i].id == id) {
				partner = partners[i];
			}
		}
		 var mensaje = $('.mensaje', $('#add_partners').parent().parent());
		 var li, text;
		 li =	'<li id="partner_'+partner.id+'" exonerado="" class="'+( ((partner.exonerated === null)) ? 'select_green_fee' : '' )+'">';
		 li +=		'<input type="hidden" class="partner_id" name="partner_id[]" value="'+partner.id+'">';
		 li +=		'<div>';
		 li +=			partner.nombre;
		 li +=		'</div>';
		 li +=		'<div>';
		 li +=			'C.I: '+ number_format(partner.identity_card, 0, ',', '.');
		 li +=		'</div>';
		 li += 		'<div class="green_fee">';
		 li += 			'<select name="reservation_definition_id[]" id="" class="form-control input-md" onchange="select_green_fee($(this), '+partner.id+');">';
		 li += 			'<option value="" selected disabled>Seleccionar gren fee</option>';
						for (var igf = 0; igf < green_fees.length; igf++) {
							var green_fee = green_fees[igf];
							li += '<option value="'+green_fee.id+'">'+green_fee.name+'</option>';
						}
		 li += 			'</select>';
		 li += 			'<div class="input_error none">';
		 li += 			'	<p></p>';
		 li += 			'</div>';
		 li += 		'</div>';
		 li +=		'<div class="cerrar" onclick="cerrar_add(\'#partner_'+partner.id+'\'); return false;">';
		 li +=			'<a href="#" class="cerrar_add">';
		 li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
		 li +=			'</a>';
		 li +=		'</div>';

		 li +=	'</li>';
		//  li = $.parseHTML(li);
		//  console.log(li);


		if ($('.type_member').length <= 0) { //Verifico si es tipo admin
			if (members_agregados.length > 0) {
				if ($('#partner_'+partner.id).length <= 0) {
					var exonerated =	'<div class="check_exonerated">';
					exonerated +=		'<input type="checkbox" name="exonerated[]" value="'+partner.id+'" onclick="exonerar(\'#partner_'+partner.id+'\')">';
					exonerated +=	'</div>';
					// select_green_fee = '';
					//
					// select_green_fee = $.parseHTML(select_green_fee);

					var exo = $.parseHTML(exonerated);
					var html = $.parseHTML(li);
					$('.cerrar', html).prev().after(exo);

					if (partner.exonerated === null) {
						// $('.check_exonerated', html).prev().after(select_green_fee);
					}

					$('.add_invitados ul').append(html);
					partners_agregados.push(partner.id);
					// console.log('/************************/');
					// console.log((partner.exonerated === null));
					// console.log((partner.exonerated));
					// console.log('/************************/');

					if (partner.exonerated === null) {

						// $('.add_invitados').addClass('select_green_fee');
						$('.reserv .inputs .boton').addClass('pagar');
						partners_green_fee.push(partner.id);
					}else {
						$('.check_exonerated input', html).prop('checked', true);
						mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
					}
				}else {
						mensaje_reservation($this, 'El invitado '+partner.nombre+' esta en la reservacion', 'partner', partner);
				}
			}else {
				mensaje_reservation($this, 'Debes seleccionar un socio para la reservacion', 'partner', partner);
			}
		}else { //sino se agregan validaciones para tipos miembros
			$.ajax({
				type: "GET",
				url: '/socios/'+user_id+'/validar_reservacion_invitado',
				// url: 'http://dementecreativo.com/lagunita/golf/socios/'+user_id+'/validar_reservacion_invitado',
				data: {
					partner_id: partner.id,
					date: $('#date_reservation').val(),
				},
				success: function(respuesta) {
					if (partner.exonerated === null) {
						// $('.cerrar', html).prev().after(select_green_fee);
						// $('.add_invitados').addClass('select_green_fee');
					}

					// console.log(respuesta.validation);
					if (respuesta.validation === true) {
						var total = partners_agregados.length + members_agregados.length;
						 if (total < 3) {
								 if (partners_agregados.length < 2) {
										 if ($('#partner_'+partner.id).length <= 0) {
												 $('.add_invitados ul').append(li);
												 partners_agregados.push(partner.id);
												if (partner.exonerated === null) {
													$('.reserv .inputs .boton').addClass('pagar');
													partners_green_fee.push(partner.id);
												}else {
													mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
												}
										 }else {
											 mensaje_reservation($this, 'El invitado '+partner.nombre+' esta en la reservacion', 'partner', partner);
										 }
								 }else {
										 mensaje_reservation($this, 'Ya hay 2 invitados en la reservacion', 'partner', partner);
								 }
						 }else {
								 mensaje_reservation($this, 'Ya la reservacion esta completa', 'partner', partner);
						 }
					}else {
						mensaje_reservation($this, respuesta.message, partner);
					}
				}
			});
		}


	});
	$(document).ready(function(){
		buscar_socios_reservacion();
		$(".select2").select2();
		buscar_invitados_reservacion();
	});
	$('.reserv .inputs .boton a').click(function(event){
		event.preventDefault();
		var form = $('#form_reservacion');
		var num_members = members_agregados.length;
		var num_times = $('input', $('.edit .horas_reservacion ul li .minutos ul li .icon.selected')).length;
		var estado = false;
		var green_fee = ($('.green_fee').filter('.active').length > 0) ? true : false;
		var procesar = $('.procesar', form);


		if ($('.type_member').length < 1) {
			if (num_members < 1) {
				input_error($('#add_members'), 'Debes agregar un socio', true);
			}
		}
		if (num_times < 1) {
			input_error($('.horas_reservacion ul'), 'Debes seleccionar una hora para la reserva', true);
		}

		if (num_members > 0 && num_times > 0) {
			estado = true;
		}

		if (estado === true) {
			var date = $(".date_reservation").datepicker( "getDate" ).toLocaleString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' }).replace(/\//g, '-');
			var time = $('input', $('.edit .horas_reservacion ul li .minutos ul li .icon.selected')).val();
			$($('#date_reservacion').val(date));
			$($('#time_reservacion').val(time));

			if (partners_green_fee.length < 1) {
				form[0].submit();
			}else {
				var tipo_pago = $(this).attr('tipo-pago');
				var enviar = false;
				var num = 0;
				$('.add_.add_invitados ul li.select_green_fee select').each(function(index, element){
					if (num === 0) {
						if ($(this).val() === '' || $(this).val() === null) {
							input_error($(this), 'Debes seleccionar el green fee de los invitados selecionados', true);
							num++;
						}
					}
				});
				if (num === 0) {
					mostrar_procesar(true, tipo_pago);
				}
			}
		}


		//
		//
		// $('#form_reservacion')[0].submit();

	});
	// $('#type_payment_id').change(function(){
	// 	input_error($(this), null, false);
	// });
}



$(function(){
	$("#search, #search_member").autocomplete({
		source:  $("#search, #search_member").attr('ruta'),
		minLength: 1,
		select: function(event, ui) {
			$('#search').val(ui.item.value);
		}
	});
		$( "#search_partners" ).autocomplete({
				source: $( "#search_partners" ).attr('ruta'),
				minLength: 1,
				select: function(event, ui) {
					$( "#search_partners" ).val(ui.item.value);
				}
		});
		$( "#search_student" ).autocomplete({
			source: $( "#search_student" ).attr('ruta'),
			minLength: 1,
			select: function(event, ui) {
				$( "#search_student" ).val(ui.item.value);
			}
		});



});



/*Deshabilitar botones*/
$(".inhabilitar").submit(function(){
	$('button[type="submit"]', this).prop("disabled",true);
});


/******************************************************/
/****************** Abrir Sub_menus *******************/
/******************************************************/
$('.sb_mn').click(function(){

		var li = $(this).parent();
		var sub_menu = $('.sub_menu', li);

		if (li.filter('.active').length <= 0) {
				$('.sub_menu').removeClass('active');
				li.addClass('active');
		}else {
				li.removeClass('active');
		}

		return false;
});









	/******************************************************/
	/******************* Cerrar Alertas *******************/
	/******************************************************/
	$('.alerta .alert button').click(function(){
			var alerta = $(this).parent().parent();
			alerta.removeClass('mostrar');
			setTimeout(function(){
					alerta.remove();
			},1000);
	});

	$(document).ready(function(){
			var alerta = $('.alerta');
			setTimeout(function(){
					if (alerta.length >= 1) {
							alerta.addClass('mostrar');
					}
					setTimeout(function(){
							alerta.removeClass('mostrar');
							setTimeout(function(){
									//alerta.remove();
							},1000);
					},7000);
			}, 1000);
	});


/******************************************************/
/****************** Date Time Picker ******************/
/******************************************************/
// var fech = new Date();
// var dd = fech.getDate();
// var mm = fech.getMonth() +1;
// var yyyy = fech.getFullYear();
// var yyyy_10 = yyyy + 10;

// console.log(fech.getDate() + "." + (fech.getMonth() +1) + "." + fech.getFullYear());
// console.log(dd + '.' + mm + '.' + yyyy_10);

// var jose = ['es'];
$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '< Ant',
		 nextText: 'Sig >',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);


 $( ".birthdate" ).datepicker({
 		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true,
		//minDate: '01-01-1960'
 });

 /* Datepicker de Reporte */

 $( ".datepicker_alls" ).datepicker({
 		dateFormat: 'dd-mm-yy',
 	// 	minDate: new Date(),
 });
 function end_datepciker_alls() {
 		$('.end_datepciker_alls').datepicker( "destroy" );
 		$( ".end_datepciker_alls" ).datepicker({
 				dateFormat: 'dd-mm-yy',
 				minDate: $('.datepicker_alls').val(),
 		});
 }
 $(document).ready(end_datepciker_alls);

 $('.datepicker_alls').change(function(){
 	$('.end_datepciker_alls').val('');
 	end_datepciker_alls();
 });

/* Datepicker de Torneos */




$( ".date_tournament" ).datepicker({
		dateFormat: 'dd-mm-yy',
		minDate: new Date(),
		beforeShowDay: function( date ) {
	        var day = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        if( dias_de_torneo[day] ) {
	             return [false, "sorteable", 'Tornament'];

	        }else {
	             return [true, '', ''];
	        }
	    }
});
function date_tournament() {
		$('#end_date_tournament').datepicker( "destroy" );
		$( "#end_date_tournament" ).datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: $('#start_date_tournament').val(),
				beforeShowDay: function( date ) {
			        var day = jQuery.datepicker.formatDate('yy-mm-dd', date);
			        if( dias_de_torneo[day] ) {
			             return [false, "", ''];

			        }else {
			             return [true, '', ''];
			        }
			    }
		});
}
$(document).ready(date_tournament);

$('#start_date_tournament').change(function(){
	$('#end_date_tournament').val('');
	date_tournament();
});

/* Datepicker de Periodos */
//$( ".date_periods" ).fadeOut();
$( ".date_periods" ).datepicker({
		dateFormat: 'dd-mm-yy',
		minDate: new Date(),

});
function date_periods() {
		$('#end_date_periods').datepicker( "destroy" );
		$( "#end_date_periods" ).datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: $('#start_date_periods').val(),

		});
}
$(document).ready(date_periods);

$('#start_date_periods').change(function(){
	$('#end_date_periods').val('');
	date_periods();
});

$('#end_date_tournament').change(function(){
	$('#start_date_inscription').val('');
	date_inscription1();
	//date_tournament();
	//console.log($('#end_date_tournament'));

});

$(document).ready(date_inscription1);

function date_inscription1() {
	$( ".date_inscription" ).datepicker('destroy');
	$( ".date_inscription" ).datepicker({
		dateFormat: 'dd-mm-yy',
		maxDate: $('#start_date_tournament').val(),
		//maxDate: $('#end_date_tournament').val(),
	});
}

// $( ".date_inscription" ).datepicker({
// 		dateFormat: 'dd-mm-yy',
// 		minDate: new Date(),
// });
function date_inscription() {
		$('#end_date_inscription').datepicker( "destroy" );
		$( "#end_date_inscription" ).datepicker('destroy');
		$( "#end_date_inscription" ).datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: $('#start_date_inscription').val(),
				maxDate: $('#start_date_tournament').val(),
		});
}
$(document).ready(date_inscription);

$('#start_date_inscription').change(function(){
	$('#end_date_inscription').val('');
	date_inscription();
});


/* Time picker de Horarios de Salida */
$('.time_start_time').timepicker({
		step: 10,
		timeFormat: 'h:i A',
		minTime: '7:00 AM',
		maxTime: '5:00 PM',
});
function end_start_time() {
		$('#end_start_time').timepicker("remove");
		$('#end_start_time').timepicker({
				step: 10,
				timeFormat: 'h:i A',
				minTime: '7:00 AM',
				maxTime: '5:00 PM',
				disableTimeRanges: [
						['7:00 AM', $('#start_start_time').val()],
				]
		});
}
$(document).ready(end_start_time);

$('#start_start_time').change(function(){
		end_start_time();
});

$('#start_time').timepicker({
		step: 10,
		timeFormat: 'h:i A',
		minTime: '7:00 AM',
		maxTime: '5:00 PM',
});
function end_time() {
		$('#end_time').timepicker("remove");
		$('#end_time').timepicker({
				step: 10,
				timeFormat: 'h:i A',
				minTime: $('#start_time').val(),
				maxTime: '5:00 PM',

		});
}
$(document).ready(end_time);
//
$('#start_time').change(function(){
	$('#end_time').val('');
	end_time();
});

// var dias['13_03_2017'] = 20;

var date_select = {};

$( ".date_draw" ).datepicker({
		dateFormat: 'dd-mm-yy',
		minDate: new Date(),
});

var end_date_draw;
function date_draw(min_date) {
	//console.log(min_date);
		// console.log(min_date);
		$('#end_date_draw').datepicker( "destroy" );
		$( "#end_date_draw" ).datepicker({
			dateFormat: 'dd-mm-yy',
			minDate: min_date,
				onSelect: function (date) {
					var date_fotmat = date.split('-')[2]+'-'+date.split('-')[1]+'-'+date.split('-')[0];
					var vacio = true;
					$('.date_draw').each(function(){
							if ($(this).val() !== '') {
									vacio = false;
							}
					});
					if (vacio === false) {
							datepicker_draw(date_fotmat);
							// console.log(date_fotmat);
					}
					setTimeout(function(){
							end_date_draw = $('#end_date_draw').val();
					},20);
				}
		});
}

$('#start_date_draw').change(function(){
		// console.log('asd');
		date_draw($('#start_date_draw').val());
});





function datepicker_draw(minDate) {
		var min_date = new Date(minDate.split('-')[0]+'.'+minDate.split('-')[1]+'.'+(parseInt(minDate.split('-')[2])));
		var suma = min_date.setDate(min_date.getDate()+1);
		$( ".drawing_days_calendar" ).datepicker("destroy");
		$('#tit_calendar').fadeIn();
		$( ".drawing_days_calendar" ).datepicker({
				minDate: min_date,
				dateFormat: 'yy-mm-dd',
				// setDate: '2017-04-20',
				onSelect: function(date){
						var li, f, dia, mes, day, ano, dia_completo, id, hora_apertura, hora_cierre, start_time, end_time;
						date_select[date] = date;
						f = new Date( date );
						// f = new Date( f.getFullYear() + "-" + (f.getMonth() +1) + "-" + (f.getDate()+1) );
						f.setDate(f.getDate()+1);
						// console.log('s'+f.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",}));
						var options = {
								weekday: "long",
								year: "numeric",
								month: "long",
								day: "numeric",
						};
						dia = f.toLocaleString('es-Es', { weekday: "long"});
						month = f.toLocaleString('es-Es', { month: "2-digit"});
						day = f.toLocaleString('es-Es', { day: "2-digit"});
						year = f.toLocaleString('es-Es', {year: "numeric"});

						fecha = year + '_' + month + '_' + day;

						dia_completo = f.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});

						id = fecha;
						start_time = new Date(date+' 7:00:00');
						end_time = new Date(date+' 17:00:00');

						li = '<li id="'+id+'" class="active li_dias" date="'+date+'">';
						li += '    <div class="head">';
						li += '        <div class="icon">';
						li += '            <a href="#" class="select_date" onclick="select_date($(this)); return false;">';
						li += '                <i class="glyphicon glyphicon-menu-right"></i>';
						li += '            </a>';
						li += '        </div>';
						li += '        <span>'+ dia_completo +'</span>';
						li += '        <div class="remove">';
						li += '            <a href="#" class="remove_date" onclick="remove_date($(this)); return false;" date="'+date+'">';
						li += '                <i class="glyphicon glyphicon-remove"></i>';
						li += '            </a>';
						li += '        </div>';
						li += '    </div>';
						li += '    <div class="cont">';
						li += '        <ul class="horas">';

						for (var i = start_time.getHours(); i < end_time.getHours()+1; i++) {
								var hora = new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }).split(' ')[0];
								var meridiano = new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }).split(' ')[1];
								li += '            <li class="li_hour" date="'+date+'">';
								li += '                <div class="hour">';
								li += '                <a href="#" class="select_hour" onclick="select_hour($(this)); return false;" date="'+date+'">';
								li += '                    <i class="glyphicon glyphicon-menu-right"></i>';

								li += '                </a>';
								li += '                    <span> <input type="checkbox" onclick="select_desselect_min($(this));" class="select_desselect_min">'+ new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }) +'</span>';
								li += '                <div class="icon_checked">';
								li += '                    <i class="glyphicon glyphicon-ok"></i>';
								li += '                </div>';
								li += '                </div>';
								li += '                <div class="checkbox_">';
								for (var min = 0; min < 6; min++) {
										li += '                  <div class="check">';
										li += '                      <input type="checkbox" name="hour[]" value="'+i+':'+min+'0:00" date="'+date+'" onclick="select_min($(this));" class="select_min">';
										li += '                      <span>'+hora+':'+min+'0 '+meridiano+'</span>';
										li += '                  </div>';
								}

								li += '                </div>';
								li += '            </li>';
						}
						li += '        </ul>';
						li += '    </div>';
						li += '</li>';


						if ($('#'+id).length <= 0) {
								$('.drawing ul.dias li:not(#'+id+'):not(.li_hour)').removeClass('active');
								setTimeout(function(){
										$('.dias').append(li);
										parametros_array();
										setTimeout(function(){
												$('#'+id).removeClass('mostrar');
										},50);
								},500);
						}
						$( ".drawing_days_calendar" ).datepicker('destroy');
						var date_o = $('#end_date_draw').val();
						var date_format = date_o.split('-')[2]+'-'+date_o.split('-')[1]+'-'+date_o.split('-')[0];
						datepicker_draw(date_format);


				},
				beforeShowDay: function( date ) {
						var f = date;


						month = f.toLocaleString('es-Es', { month: "2-digit"});
						day_ = f.toLocaleString('es-Es', { day: "2-digit"});
						year = f.toLocaleString('es-Es', {year: "numeric"});
						day =  year + '-' + month + '-' + day_;



						var sorteable = date_select[day];
						var torneo = (window.torneos[day]) ? torneos[day] : '';
						if( sorteable ) {
							 return [true, "sorteable", 'Dia Sorteable'];

						}else if (torneo) {
							return [false, "torneo", 'Dia de torneo'];
						}else {
								 return [true, '', ''];
						}
				}
				// inline: true
		});
}
// $(document).ready(datepicker_draw);

function select_date($this) {
		var id_li = '#'+$this.parent().parent().parent().attr('id');
		var li = $this.parent().parent().parent();


		if (li.filter('.active').length <= 0 ) {
		// if ($(id_li+'.active').length <= 0) {
				$('.drawing ul.dias li:not('+id_li+'):not(.li_hour)').removeClass('active');
				setTimeout(function(){
						li.addClass('active');

				},500);
		}else {
				li.removeClass('active');
		}
}
function remove_date($this) {

		// console.log($('#end_date_draw').val(""));
		// console.log($('#end_date_draw').val());
		var id_li = '#'+$this.parent().parent().parent().attr('id');
		var li = $this.parent().parent().parent();
		var date = $this.attr('date');
		setTimeout(function(){
				// console.log(end_date_draw);
				delete date_select[date];
				$( ".drawing_days_calendar" ).datepicker('destroy');
				datepicker_draw(end_date_draw);
		},100);

		li.addClass('mostrar');
		setTimeout(function(){
				li.remove();
				parametros_array();
		},500);

}

function select_hour($this) {
		var id_li = '#'+$this.parent().parent().attr('id');
		var li = $this.parent().parent();
		var date = $this.attr('date');
		var li_date = li.parent().parent().parent();
		// console.log(li_date);
		// console.log(date);

		if (li.filter('.active').length <= 0 ) {
				li.addClass('active');
				parametros_array();
		}else {
				li.removeClass('active');
				parametros_array();
		}


}
function select_min($this) {
		// var date = $this.attr('date');
		// var li_date = $this.parent().parent().parent().parent().parent().parent();
		var li = $this.parent().parent().parent();


		if ($('input.select_min:checked', li).length !== 0) {
				li.addClass('checked');
				$('.select_desselect_min', li).prop('checked', false);
		}else {
				li.removeClass('checked');
				$('.select_desselect_min', li).prop('checked', false);
		}

		if ($('input.select_min:checked', li).length == 6) {
			$('.select_desselect_min', li).prop('checked', true);

		}


		parametros_array();
}

function select_desselect_min($this) {
	var li = $this.parent().parent().parent()

	if ($this.prop('checked')) {
		$('.checkbox_ input', li).prop('checked', true);
		li.addClass('checked');
	}else {
		$('.checkbox_ input', li).prop('checked', false);
		li.removeClass('checked');

	}
	parametros_array();
}


var parametros = {};

function parametros_array() {
		var parametros = {};
		var hours = [];
		var start_date = $('#start_date_draw').val();
		var draw_date = $('#end_date_draw').val();
		var draw_days = [];
		var draw_id;
		// var id_draw
		// var parametros = [];
		// console.log($('.drawing ul.dias .li_dias').length);

		$('.drawing ul.dias .li_dias').each(function(){
				var date_param = [];
				// draw_days = [];
				if (parseInt($('#form_draw').attr('estado')) === 1) {
						date_param.push({
								date: $(this).attr('date'),
								id: $(this).attr('day_draw')
						});
				}

				var date = $(this).attr('date');
				draw_id = $(this).attr('draw');
				var hours = [];
				var hours_param = [];

				for (var ii = 0; ii < parametros.length; ii++) {
						if (parseInt($('#form_draw').attr('estado')) === 0) {
								if (parametros[ii].draw_days.date == date) {
										parametros.splice(ii);
								}
						}else {
								if (parametros[ii].draw_days.date[0].date == date) {
										parametros.splice(ii);
								}
						}
				}
				$('.drawing ul.dias .li_hour input').each(function(){
						if ($(this).prop('checked') === true && $(this).attr('date') == date) {
										hours.push($(this).val());
								if (parseInt($('#form_draw').attr('estado')) === 1) {
										hours_param.push({
												id: $(this).attr('time_draw'),
												times: $(this).val()
										});
								}

						}
				});

				if (parseInt($('#form_draw').attr('estado')) === 0) {
						draw_days.push({
								date: date,
								times: hours
						});
				}else {
						draw_days.push({
								date: date_param,
								times: hours_param
						});
				}

				// console.log(date, hours);




		});
		if (parseInt($('#form_draw').attr('estado')) === 0) {
				parametros = {
						start_date: start_date,
						draw_date: draw_date,
						draw_days: draw_days
				};
		}else {
				parametros = {
						id: draw_id,
						start_date: start_date,
						draw_date: draw_date,
						draw_days: draw_days
				};
		}
		// console.log(draw_days);

		$('#day_draws').val(JSON.stringify(parametros));
		// console.log($('#day_draws'));
		// console.log(parametros);


}

$('#button_drawing').click(function(){
		// console.log(parametros);
		var ruta = $(this).attr('ruta');
		// console.log(parametros[0].times.length);
		if (parametros !== '') {
				$("#modalverificar").modal();
		}else {
				alert('Debes sleccionar una fecha y las horas para el sorteo');
		}

		return false;
});
$('#button_drawing_store').click(function(){
		$('#store_draw').submit();
});

$('.mostrar_draw').click(function(){
		date_select = {};
		$('.dias').html("");
		var id = $(this).attr('draw');
		var $li = $(this).parent();
		var li = '';
		var ruta = $(this).attr('ruta');
		var draw_id;

		$('button#sortear').attr('draw_id', id);
		$('button#sortear').attr('li-id', $(this).attr('li-id'));


		for (var i_draw = 0; i_draw < draws.length; i_draw++) {
				if (draws[i_draw].id == id) {
						var draw = draws[i_draw];
						draw_id = draw.id;
						$('#start_date_draw').val(draw.start_date.split('-')[2]+'-'+draw.start_date.split('-')[1]+'-'+draw.start_date.split('-')[0]);
						$('#end_date_draw').val(draw.draw_date.split('-')[2]+'-'+draw.draw_date.split('-')[1]+'-'+draw.draw_date.split('-')[0]);
						end_date_draw = draw.draw_date;
						date_draw(draw.draw_date.split('-')[2]+'-'+draw.draw_date.split('-')[1]+'-'+draw.draw_date.split('-')[0]);
						for (var i_day_draw = 0; i_day_draw < draw.day_draws.length; i_day_draw++) {
							var day_draw = draw.day_draws[i_day_draw];
							var date = day_draw.date;

							date_select[date] = date;


							var f, dia, mes, day, ano, dia_completo, hora_apertura, hora_cierre, start_time, end_time;

							f = new Date( date );
							// f = new Date( f.getFullYear() + "-" + (f.getMonth() +1) + "-" + (f.getDate()+1) );
							f.setDate(f.getDate()+1);
							// console.log('s'+f.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",}));

							var options = {
									weekday: "long",
									year: "numeric",
									month: "long",
									day: "numeric",
							};
							dia = f.toLocaleString('es-Es', { weekday: "long"});
							month = f.toLocaleString('es-Es', { month: "2-digit"});
							day = f.toLocaleString('es-Es', { day: "2-digit"});
							year = f.toLocaleString('es-Es', {year: "numeric"});

							fecha = year + '_' + month + '_' + day;

							dia_completo = f.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});

							id_ = fecha;
							start_time = new Date(date+' 7:00:00');
							end_time = new Date(date+' 17:00:00');

							// console.log(day_draw);
							li += '<li id="'+id_+'" class="li_dias" date="'+date+'" day_draw="'+day_draw.id+'" draw="'+draw.id+'">';
							li += '    <div class="head">';
							li += '        <div class="icon">';
							li += '            <a href="#" class="select_date" onclick="select_date($(this)); return false;">';
							li += '                <i class="glyphicon glyphicon-menu-right"></i>';
							li += '            </a>';
							li += '        </div>';
							li += '        <span>'+ dia_completo +'</span>';
							li += '        <div class="remove">';
							li += '            <a href="#" class="remove_date" onclick="remove_date($(this)); return false;" date="'+date+'">';
							li += '                <i class="glyphicon glyphicon-remove"></i>';
							li += '            </a>';
							li += '        </div>';
							li += '    </div>';
							li += '    <div class="cont">';
							li += '        <ul class="horas">';

							for (var i = start_time.getHours(); i < end_time.getHours()+1; i++) {

								var hora_completa = new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true });
								// console.log(hora_completa);
								var timeDB = '';
								for (var i_times = 0; i_times < day_draw.time_draws.length; i_times++) {
										var time_draw = day_draw.time_draws[i_times];
										if (hora_completa == new Date(date+' '+time_draw.time).toLocaleString('en-US', { hour: 'numeric', hour12: true })) {
												timeDB = 'checked';
										}
										// console.log(new Date(date+' '+time_draw.time).toLocaleString('en-US', { hour: 'numeric', hour12: true }), hora_completa);
										// console.log(timeDB);
										// console.log('----');
								}

								// console.log(timeDB);
								var hora = new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }).split(' ')[0];
								var meridiano = new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }).split(' ')[1];
								// console.log(day_draw.time_draws.indexOf());
								li += '            <li class="li_hour '+timeDB+'" date="'+date+'">';
								li += '                <div class="hour">';
								li += '                <a href="#" class="select_hour" onclick="select_hour($(this)); return false;" date="'+date+'">';
								li += '                     <i class="glyphicon glyphicon-menu-right"></i>';
								li += '                </a>';
								li += '                <span> <input type="checkbox" onclick="select_desselect_min($(this));" class="select_desselect_min">'+ new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }) +'</span>';
								li += '                <div class="icon_checked">';
								li += '                    <i class="glyphicon glyphicon-ok"></i>';
								li += '                </div>';

								li += '                </div>';
								li += '                <div class="checkbox_">';
								for (var min = 0; min < 6; min++) {
										var minute;
										var iiii = i+'';
										// console.log(prueba.length);

										if (iiii.length == 1) {
												minute = '0'+i+':'+min+'0:00';

										}else {
												minute = i+':'+min+'0:00';
										}
										// console.log(minutes);
										// var minDB;
										minDB = '';
										var time_draw_id = '';

										for (var i_time = 0; i_time < day_draw.time_draws.length; i_time++) {
												var time_draw_ = day_draw.time_draws[i_time];
												if (minute == time_draw_.time) {
														minDB = 'checked';
														time_draw_id = time_draw_.id;

												}


										}
										// console.log(minDB);


										// var checked =

										li += '                  <div class="check">';
										li += '                      <input type="checkbox" name="hour[]" value="'+minute+'" '+minDB+' date="'+date+'" onclick="select_min($(this));" time_draw="'+time_draw_id+'" class="select_min">';
										li += '                      <span>'+hora+':'+min+'0 '+meridiano+'</span>';
										li += '                  </div>';
								}

								li += '                </div>';
								li += '            </li>';
							}
							// for (var i_times = 0; i_times < day_draw.time_draws.length; i_times++) {
							//     var time_draw = day_draw.time_draws[i_times];
							//     console.log(new Date(date+' '+time_draw.time).toLocaleString('en-US', { hour: 'numeric', hour12: true }));
							// }
							li += '        </ul>';
							li += '    </div>';
							li += '</li>';

						}
						$('.dias').append(li);
						// console.log(draw.draw_date);
						datepicker_draw(draw.draw_date);

				}
		}
		// $('.drawing ul.draws li').removeClass();
		$li.addClass('active');
		$('.mes_draws').removeClass('active');
		$('#form_draw').attr('action', ruta);
		$('#form_draw').attr('estado', 1);
		$('#add_method_put').html($('#method_put').html());
		$('#destroy_draw').attr('ruta', $(this).attr('destroy'));
		$('#destroy_draw').fadeIn();
		$('#sortearConfirm').fadeIn();
		// parametros = [];
		parametros_array();
		// console.log(date_select);
		// console.log(id, draws[0]);


		return false;
});
var ijs = 0;
$('.select_min').click(function(){
		ijs++;
		//console.log(ijs);
});

$('#cancelar_draw').click(function(){
	cancelar_draw();
});
function cancelar_draw() {
	if (parseInt($('#form_draw').attr('estado')) === 0) {
		$('#start_date_draw').val("");
		$('#end_date_draw').val("");
		$('#end_date_draw').datepicker("destroy");
		$( ".drawing_days_calendar" ).datepicker("destroy");
		$('.drawing ul.dias').html("");
	}else {
		$('#start_date_draw').val("");
		$('#end_date_draw').val("");
		$('#end_date_draw').datepicker("destroy");
		$( ".drawing_days_calendar" ).datepicker("destroy");
		$('.drawing ul.dias').html("");
		// $('.drawing ul.draws li').removeClass();
		$('#form_draw').attr('action', $('#form_draw').attr('store'));
		$('#form_draw').attr('estado', 0);
		$('#destroy_draw').attr('ruta', '');
		$('#destroy_draw').fadeOut();
		$('#sortearConfirm').fadeOut();
		$('#add_method_put').html("");
		parametros_array();
	}
	$('#tit_calendar').fadeOut(0);
}
// $('.select_hour')


$('.draws_month').click(function(){
		var li = $(this).parent();

		if (li.filter(".active").length <= 0) {
				$('.mes_draws').removeClass('active');
				li.addClass("active");
		}else {
				li.removeClass("active");
		}
		// console.log(li);
		return false;
});

var time_draws = [];

/******************************************************/
/************ Datepicker y time Reservation ***********/
/******************************************************/
var date_selected;
// var times_draw;
$('#date_reservation').on('change', function(){
		if (validar_formato_fecha($('#date_reservation').val()) === true ) {
			setTimeout(function(){
				if ($('.type_member').length >= 1) {
					validar_member_reservation($('#date_reservation').val());
				}else {
					time_reservation();
				}
			});
		}else if (validar_formato_fecha($('#date_reservation').val()) === 1) {
			$('.time_reservation').timepicker('remove');
			$('.consultar .mensaje p span').html("Debe seleccionar un formato de fecha correcto ej: 12-12-2017");
			$('.consultar .mensaje p span').addClass('warning');
			$('.consultar .mensaje').addClass('mostrar');
			setTimeout(function(){
				$('.consultar .mensaje').removeClass('mostrar');
			},6000);
		}else if (validar_formato_fecha($('#date_reservation').val()) === 2) {
			$('.time_reservation').timepicker('remove');
			$('.consultar .mensaje p span').html("Debes seleccionar una fecha real");
			$('.consultar .mensaje p span').addClass('warning');
			$('.consultar .mensaje').addClass('mostrar');
			setTimeout(function(){
				$('.consultar .mensaje').removeClass('mostrar');
			},6000);
		}
});
function this_remove(element) {
	element.remove();
}
function mostrar_minutos(date) {
	$( ".date_reservation" ).datepicker({
		minDate: new Date(),
		dateFormat: 'dd-mm-yy',
		beforeShowDay: function( date ) {
			var day = jQuery.datepicker.formatDate('yy-mm-dd', date);
			date_selected = day.replace(/-/g, '/');
			var sorteable = (window.dias_de_sorteo[day]) ? dias_de_sorteo[day] : '';
			var tournament = (window.torneos[day]) ? torneos[day] : '';
			// console.log(tournament);

			var no_disponible = not_available[day];
			if( sorteable ) {
				 return [true, "sorteable", 'Dia Sorteable'];
			}else if ( no_disponible ) {
				 return [true, "no_disponible", 'La Reserva no esta disponible'];
			}else if (tournament) {
				return [true, "torneo", 'Dia de torneo'];
			}else {
				 return [true, '', ''];
			}
		},
		onSelect: function(date){
			mostrar_minutos(date);
		}
	});
	$($('#date_reservacion').val(date));
	$.ajax({
		type: "GET",
		url: (window.url_consultar_reserv) ? url_consultar_reserv : '',
		data: {date: date},
		autoSize: true,
		beforeSend: function (){
			// $('.edit.reserv .form .carga').removeClass('none');
			// setTimeout(function(){
			// 	$('.edit.reserv .form').addClass('cargando');
			// }, 10);
		},
		success: function(respuesta) {
			var status = respuesta.response;
			var mensaje = respuesta.message;
			var code = respuesta.code;
			if (status) {
				var reservations = respuesta.reservations;
				var tournament = respuesta.tournaments[0];
				var reservation_in_day = respuesta.reservation_in_day;
				var time_draws = (respuesta.day_draws.length !== 0 && (respuesta.day_draws[0].day_draw_status === 0  )) ? respuesta.day_draws[0].time_draws : '';
				var date_format = date.split('-')[2]+'/'+date.split('-')[1]+'/'+date.split('-')[0];
				var objeto_fecha = new Date(date_format);

				var hora_inicio = new Date(date_format);
				var hora_final = new Date(date_format);
				hora_inicio.setHours(06,00,00);
				hora_final.setHours(17,00,00);
				// console.log(respuesta, reservation_in_day);


				var horas_totales = hora_final.getHours() - hora_inicio.getHours();
				li = '';
				li += '<li>';
				li += '	<div class="horas tiempo">';
				li += '		<div class="hora">';
				li += '			<h4>Salidas</h4>';
				li += '		</div>';
				li += '	</div>';
				li += '	<div class="minutos tiempo">';
				li += '		<ul>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>1°</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>2°</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>3°</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>4°</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>5°</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>6°</span>';
				li += '				</div>';
				li += '			</li>';
				li += '		</ul>';
				li += '	</div>';
				li += '</li>';
				for (var i = 0; i < horas_totales+1; i++) {
					if (i !== 0) {
						hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
					}
					var hora = hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true });
					li += '<li>';
					li += '    <div class="horas tiempo">';
					li += '        <div class="hora"> <span> '+hora+' </span> </div>';
					li += '    </div>';
					li += '    <div class="minutos tiempo">';
					li += '        <ul>';
					for (var im = 0; im < 6; im++) {
						var hora_pasada = false;
						var reservacion = null;
						if (im !== 0) {
							hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
						}
						var minuto = hora_inicio;
						minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
						if (minuto.split(':')[0].length < 2) {
							minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
						}

						if (hora_inicio.getTime() < hoy.getTime()) {
							hora_pasada = true;
						}
						for (var ir = 0; ir < reservations.length; ir++) {
							var reservation = reservations[ir];
							if (reservation.start_time == minuto) {
								reservacion = reservation;
							}
						}
						var torneo = null;
						var tournament_start_time = (tournament !== undefined && tournament !== null) ? new Date(date_format+' '+tournament.start_time) : '';
						var tournament_end_time = (tournament !== undefined && tournament !== null) ? new Date(date_format+' '+tournament.end_time) : '';
						if (hora_inicio.getTime() >= tournament_start_time.getTime() && hora_inicio.getTime() <= tournament_end_time.getTime()) {
							torneo = tournament;
						}
						li += '            <li class="">';
						li += '                <div class="icon '+ ( (hora_pasada) ? 'filter' : '' ) +'"> ';
						if (!hora_pasada) {
							if (torneo === null) {
								if (reservacion === null) {
									li += '                    <input type="checkbox" name="" value="'+minuto+'" onload="" onclick="'+ ( (hora_pasada) ? '' : 'select_hour_reservation($(this))' ) +'"> ';
								}
							}
						}

						li += '                    <img class="disponible" src="http://localhost:8000/img/icons/disponible.png" alt="" onload="' + ( (reservacion !== null || torneo !== null) ? 'this_remove($(this));' : '' ) + '"> ';
						li += '                    <img class="selected" src="http://localhost:8000/img/icons/selected.png" alt="" onload="' + ( (reservacion !== null || torneo !== null) ? 'this_remove($(this));' : '' ) + '">';
						li += '    				   <img class="no_disponible" src="'+ public_asset +'img/icons/torneo.png" alt="" onload="' + ( (torneo === null) ? 'this_remove($(this));' : '' ) + '">';
						li += '				   	   <img class="no_disponible" src="'+ public_asset +'img/icons/no_disponible.png" alt="" onload="' + ( (reservacion === null) ? 'this_remove($(this));' : '' ) + '">';
						// li += '    				   <img class="disponible" src="'+ public_asset +'img/icons/sorteable.png" alt="" onload="">';
						// li += '    				   <img class="selected" src="'+ public_asset +'img/icons/selected_sorteable.png" alt="" onload="">';
						li += '                    <div class="ayuda">';
						li += '                        <h6>'+minuto+'</h6>';
						if (torneo !== null) {
							li += '                        <p>'+torneo.name+'</p>';
						}else if (hora_pasada || reservacion !== null) {
							li += '                        <p>Hora no disponible</p>';
						}else if (reservacion === null && torneo !== null) {
							li += '                        <p>'+torneo.name+'</p>';
						}else if (reservacion === null) {
							li += '                        <p>Disponible</p>';
						}
						li += '                    </div>';
						li += '                </div>';
						li += '            </li>';
					}
					li += '        </ul>';
					li += '    </div>';
					li += '</li>';
				}
				// console.log(reservation_in_day);
				if (reservation_in_day) {
					// $('.add_').fadeOut();
					// $('.procesar .boton button').fadeOut();
					// $('.add_ input').prop('disabled', true);
				}else {
					// $('.add_').fadeIn();
					// $('.procesar .boton button').fadeIn();
					// $('.add_ input').prop('disabled', false);
				}
				$('.add_ ul').html('');
				partners_agregados = [];
				members_agregados = [];
				$('.edit .horas_reservacion ul').html(li);
				$('.edit.reserv .form.cargando').removeClass('cargando');
				setTimeout(function(){
					$('.edit.reserv .form .carga').addClass('none');
				},700);
				// $('.procesar').addClass('active');
			}else {
				// $('.procesar').removeClass('active');
				$('.consultar .mensaje p span').removeClass('warning');
				$('.consultar .mensaje p span').html(mensaje);
				$('.consultar .mensaje').addClass('mostrar');
				setTimeout(function(){
						$('.consultar .mensaje').removeClass('mostrar');
				},5000);
			}
		}
	});
	// console.log(date);

}
var hoy = new Date();
// hoy = ;
// console.log(hoy);
mostrar_minutos(hoy.toLocaleString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' }).replace(/\//g, '-'));

// function date_reservation() {
// 	$( ".date_reservation" ).datepicker({
// 	    minDate: new Date(),
// 	    dateFormat: 'dd-mm-yy',
// 	    beforeShowDay: function( date ) {
// 	        var day = jQuery.datepicker.formatDate('yy-mm-dd', date);
// 	        date_selected = day.replace(/-/g, '/');
// 	        var sorteable = (window.dias_de_sorteo[day]) ? dias_de_sorteo[day] : '';
// 			var tournament = (window.torneos[day]) ? torneos[day] : '';
// 			// console.log(tournament);
//
// 	        var no_disponible = not_available[day];
// 	        if( sorteable ) {
// 	             return [true, "sorteable", 'Dia Sorteable'];
// 	        }else if ( no_disponible ) {
// 	             return [true, "no_disponible", 'La Reserva no esta disponible'];
// 	        }else if (tournament) {
// 				return [true, "torneo", 'Dia de torneo'];
// 	        }else {
// 	             return [true, '', ''];
// 	        }
// 	    },
// 		onSelect: function(date){
// 			mostrar_minutos(date);
// 		}
// 	});
// }
// date_reservation();
function validar_member_reservation(date) {
	$.ajax({
		type: "GET",
		url: '/socios/'+user_id+'/validar_reservacion_socio',
		data: {
			member_id: member_id,
			date: date,
		},
		success: function(respuesta) {
			// console.log(respuesta);
			var validation = respuesta.validation;
			if (validation === true) {
				time_reservation();
			}else {
				$('.time_reservation').timepicker('remove');
				$('.consultar .mensaje p span').html("Solo puede realizar una reservación por dia");
				$('.consultar .mensaje p span').addClass('error');
				$('.consultar .mensaje').addClass('mostrar');
				setTimeout(function(){
						$('.consultar .mensaje').removeClass('mostrar');
				},6000);
			}
		}
	});
}
function time_reservation() {
	var val = $('#date_reservation').val();
	var date = val.split('-')[2]+'-'+val.split('-')[1]+'-'+val.split('-')[0];
	// var date
	// console.log(date);
	// if (typeof torneos[date] == "undefined") {
	// 	console.log('si');
	// }else {
	// 	console.log('no');
	// }
	$('.time_reservation').timepicker('remove');
	$('.time_reservation').timepicker({
		step: 10,
		timeFormat: 'h:i A',
		minTime: '7:00 AM',
		maxTime: '5:00 PM',
		// disableTimeRanges: (window.torneos[date]) ? torneos[date].rango_horas : [],
		// disableTimeRanges: [
		// 	['7:00 AM', '7:10 AM']
		// ]
	}).on( "showTimepicker", function() {
		$('.ui-timepicker-am, .ui-timepicker-pm').removeClass('sorteable').removeClass('torneo');
		$('.ui-timepicker-am, .ui-timepicker-pm').each(function(){
			if (window.dias_de_sorteo[date]) {
				if (dias_de_sorteo[date].indexOf($(this).html()) !== -1) {
					$(this).addClass('sorteable');
				}
			}
			if (window.torneos[date]) {
				if (torneos[date].times.indexOf($(this).html()) !== -1) {
					$(this).addClass('torneo');
				}
			}

		});
	});
}






/* Clima wheater */
function simpleWeather() {
	$.simpleWeather({
		zipcode: '709',
		unit: 'c',
			location: 'Caracas Ca',
		success: function(weather) {
					// var city, country, currently, image;

					// console.log(weather);
					html = '<div class="city">';
					html += '    <h1>'+weather.city+', '+weather.country+'</h1>';
					html += '</div>';
					html += '<div class="country">';
					html += '    <h3>'+weather.temp+'°</h3>';
					html += '</div>';
					html += '<div class="estado">';
					html += '    <img src="'+weather.image+'" alt="">';
					html += '</div>';



			$(".clime").html(html);
					// console.log(weather);
		},
		error: function(error) {
			simpleWeather();
		}
	});

}
$(document).ready(simpleWeather);

/* Crear Categoria */

$('#form_category').submit(function(){
	var ruta = $(this).attr('ruta');
	// console.log($('#form_category').serialize(), ruta);
	$.ajax({
		type: "POST",
		url: ruta,
		data: $('#form_category').serialize(),
		success: function(respuesta) {
			// console.log(respuesta);
			if (respuesta.type == 'success') {
				var option = '<option value="'+respuesta.category.id+'" selected> '+ respuesta.category.name +'</option>';
				$('#category').append(option);
				$('#myModal').modal('hide');
				$('#form_category')[0].reset();
			}
		}
	});
	return false;
});



/***********************************************************/
/******************* Condiciones Torneos *******************/
/***********************************************************/
// $("#condicion_torneo").fileinput({
// 	initialPreview: [
// 		"http://localhost:8000/assets/tournaments/files/170223040702-Conditions_rules-Torneo.pdf",
// 	],
// 	initialPreviewAsData: true,
// 	initialPreviewConfig: [
// 		{type: "pdf", size: 8000, caption: "170223040702-Conditions_rules-Torneo.pdf"},
// 	],
//
// 	showUpload: false,
// 	showRemove: false,
// 	// 'previewFileType': 'any'
// 	maxFileCount: 1,
// 	browseLabel: 'Buscar',
// 	msgZoomModalHeading: 'Vista previa',
// 	allowedFileExtensions: ['pdf', 'docx'],
// 	previewFileIconSettings: {
// 		'doc': '<i class="fa fa-file-word-o text-primary"></i>',
// 		'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
// 	},
// 	// indicatorNew: '<i class="fa fa-file-word-o text-primary"></i>',
// 	// indicatorSuccess: '<i class="fa fa-file-pdf-o text-danger"></i>',
// 	msgInvalidFileExtension: 'Error, solo puede agregar documentos "pdf" o "docx"',
// 	msgNoFilesSelected: 'No hay archivo seleccionado'
// 	// showBrowse: false
// });

// console.log($('.home_torneos .owl-item:not(".cloned") .condicion_torneo').length);


/* Torneos en el home */
var home_torneos = $('#home_torneos');
$('.ver_detalle_torneo').click(function(){
	var item = $(this).attr('item');
	var numero = 0;
	var item_numero;

	// console.log($('.home_torneos .owl-carousel .item').length);
	$('.home_torneos .owl-carousel .item').each(function(){
		if ($(this).attr('id') == item) {
			item_numero = numero;
		}
		numero++;
	});

	$('.home_torneos .owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		// singleItem: true,
		dots: false,
		items:1,
		navClass: ['anterior direcciones', 'siguiente direcciones'],
		navText: ['<i class="glyphicon glyphicon-menu-left"></i>', '<i class="glyphicon glyphicon-menu-right"></i>'],
		navContainerClass: 'navegacion',
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		startPosition: item_numero,
		reponsive: {
			0: {
				items: 1
			}
		}
	});

	setTimeout(function(){
		home_torneos.removeClass('none');
		setTimeout(function(){
			home_torneos.addClass('active');
		});
	}, 500);


});
$('.home_torneos .owl-carousel .item').each(function(){
	// console.log($('.condicion_torneo', this).attr('value'));
	$('.condicion_torneo', this).fileinput({
		initialPreview: [
			$('.condicion_torneo', this).attr('value')
		],
		initialPreviewAsData: true,
		initialPreviewConfig: [
			{type: "pdf", caption: $('.titulo h1', this).html(), showDelete: false, width: "250px"},
		],

		showUpload: false,
		showRemove: false,
		showBrowse: false,
		showClose: false,
		showCaption: false,
		// showCancel: fasle,
		// showUploadedThumbs: false,
		// 'previewFileType': 'any'
		maxFileCount: 1,
		browseLabel: 'Buscar',
		msgZoomModalHeading: 'Vista previa',
		allowedFileExtensions: ['pdf', 'docx'],
		previewFileIconSettings: {
			'doc': '<i class="fa fa-file-word-o text-primary"></i>',
			'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
		},
		msgInvalidFileExtension: 'Error, solo puede agregar documentos "pdf" o "docx"',
		msgNoFilesSelected: 'No hay archivo seleccionado'
		// showBrowse: false
	});
});

$('.home_torneos .cerrar_home_torneos').click(function(){
	home_torneos.removeClass('active');
	setTimeout(function(){
		home_torneos.addClass('none');
		$('.home_torneos .owl-carousel').trigger('destroy.owl.carousel');
	}, 700);
});


/****************************************************/
/*******************   Starters   *******************/
/****************************************************/

/* Mostrar Informacion de reservacion */

$('.activar_starter').click(function(){

});

function activar_starter($this) {
	var li = $this.parent().parent();
	var id_li = '#'+li.attr('id');
	var ul_cont = $this.parent().parent().parent();

	if (li.filter('.active').length <= 0) {
		// $('li:not('+id_li+')', ul_cont).removeClass('active');
		li.addClass('active');
	}else {
		li.removeClass('active');
	}
	return false;
}


$(function() {
	/* Lista original de reservacion */
	$( "#reservacion .lista" ).not('.no_connect').sortable({
		connectWith: "#orden_salida .conexion",
		helper: 'clone',
		forcePlaceholderSize: true,
		start:function(event,ui){
			$(ui.item).show();
			clone = $(ui.item).clone();
			before = $(ui.item).next();
		},
		stop:function(event, ui){
			n_li = $('li', this).length;
			if (n_li !== 1) {
				$(this).append(clone);
			}
		},
	}).disableSelection();



	/* Lista Ordenable */
	$( "#orden_salida .conexion" ).sortable({
		connectWith: ".conexion",
		helper: 'clone',
		forcePlaceholderSize: true,
		start: function(event, ui){
			$(ui.item).show();
		},
		receive: function(event, ui){
			var li_1 = $(this).parent().parent();
			var sender = ui.sender;
			var recibido = ui.item;
			var item_copiado = $('li', this).not(recibido).clone();
			console.log(item_copiado);

			var item = $('li', this).not(recibido);
			if (sender.attr('id') == 'reserv') {
				if ( item.attr('id') === recibido.attr('id') ) {
					ui.item.remove();
					console.log('error');
				}else {
					item.remove();
					$('#orden_salida .datos_reservacion').each(function(){
						if ($('li', this).not(recibido).attr('id') == recibido.attr('id')) {
							var cont = $(this);
							var li_2 = cont.parent().parent();
							$('li', this).remove();
							cont.append(item_copiado);
							recibido.addClass('movido');
							item_copiado.addClass('movido');
							$('#orden_salida .li_principal').not(recibido).not(item_copiado).removeClass('active');

							li_1.addClass('active');
							li_2.addClass('active');
							setTimeout(function(){
								recibido.removeClass('movido');
								item_copiado.removeClass('movido');

							}, 3000);
						}
					});
				}
			}else if (sender.attr('id') == 'ordenable') {
				item.remove();
				$('#orden_salida .datos_reservacion').each(function(){
					if ($('li', this).attr('id') === undefined) {
						var cont = $(this);
						var li_2 = cont.parent().parent();

						cont.append(item_copiado);
						recibido.addClass('movido');
						item_copiado.addClass('movido');
						$('#orden_salida .li_principal').not(recibido).not(item_copiado).removeClass('active');

						li_1.addClass('active');
						li_2.addClass('active');

						setTimeout(function(){
							recibido.removeClass('movido');
							item_copiado.removeClass('movido');

						}, 3000);
					}
				});
			}
		}
	}).disableSelection();

	// $( ".conexion" ).sortable( "option", "helper", "clone" );
});
// $('.maletero li a').not('.accion').click(function(e){
// 	e.preventDefault();
// });

$('.maletero li a').click(function(e){
	e.preventDefault();
	e.stopPropagation();
	// console.log();
	if ($(this).filter('.accion').length > 0) {
		maletero($(this));
	}
	// return false;
});
var capa_maletero = $('.capa_maletero');
function maletero($this) {
	var li = $this.parent();
	var locker = parseInt($this.attr('href').split('#')[1]);
	$('#form_maletero_prestamo').attr('locker_id', locker);
	// console.log(locker);
	var disponible = (li.filter('.Prestamo').length <= 0) ? true : false;
	for (var i = 0; i < lockers.length; i++) {
		var lock = lockers[i];
		if (lock.id === locker) {
			locker = lockers[i];
		}
	}
	var observation_name = (!disponible) ? 'observations_out' : 'observations_in';
	var mensaje = (disponible) ? 'Dar salida a equipo' : 'Dar entrada a equipo';
	var input = '';
	if (!disponible) {
		input += '<input type="hidden" name="_method" value="PUT">';
	}
	input += '		<input type="hidden" name="locker_id" value="'+locker.id+'" id="">';
	input += '		<div class="titulo">';
	input += '			<h1> '+mensaje+': </h1>';
	input += '		</div>';
	input += '		<div class="hora">';
	input += '			<h1> '+ hora() +'</h1>';
	input += '		</div>';
	input += '		<div class="add_socios add_">';
	if (disponible) {
		input += '			<div class="form-group">';
		input += '				<label class="col-4 col-md-center control-label" for="receipt_payment">Buscar Socios</label>';
		input += '				<div class="col-6 col-md-center" >';
		input += '					<input id="add_members_maletero" type="text" class="search form-control input-md" placeholder="Buscar Socios" name="search_socio" ruta="'+autocomplete_socios+'">';
		input += '				</div>';
		input += '				<div class="mensaje">';
		input += '				</div>';
		input += '			</div>';
	}
	input += '			<ul class="">';
	if (!disponible) {
		console.log(locker);
		var member = locker.locker_loans[0].member;
		input += '<li id="member_'+member.id+'" class="member_reservation" id-="'+member.id+'">';
		input += '	<input type="hidden" class="member_id" name="member_id" value="'+member.id+'">';
		input += '	<div class="nombre">'+member.name+' '+ member.last_name+'</div>';
		input += '	<div>C.I: '+ number_format(member.identity_card, 0, ',', '.') +'</div><br>';
		input += '	<div>N° Acción: '+ member.number_action+'</div>';
		input += '</li>';
	}
	input += '			</ul>';
	input += '		</div>';
	input += '		<div class="form-group textarea">';
	input += '			<label class="col-4 col-md-center control-label" for="receipt_payment">Observaciones</label>';
	input += '			<div class="col-6 col-md-center" >';
	input += '				<textarea name="'+observation_name+'" rows="8" cols="80"></textarea>';
	input += '			</div>';
	input += '		</div>';
	input += '		<div class="form-group boton">';
	input += '			<button id="guardar" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>';
	input += '			<button type="button" onclick="cerrar_capa_maletero();" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>';
	input += '		</div>';
	input = $.parseHTML(input);

	setInterval(function(){
		$('.capa_maletero .datos .hora h1').html(hora());
	}, 1000);

	if (!disponible) {
		$('.capa_maletero .add_ .form-group').remove();
		capa_maletero.addClass('Prestamo');
	}else{
		capa_maletero.removeClass('Prestamo');
	}
	$('.datos', capa_maletero).html(input);
	capa_maletero.removeClass('none');
	setTimeout(function(){
		capa_maletero.addClass('active');
	});

	$( "#add_members_maletero" ).autocomplete({
		source: $('#add_members_maletero').attr('ruta'),
		minLength: 1,
		select: function(event, ui) {
			var mensaje = $('.mensaje', $('#add_members_maletero').parent().parent());
			var li, text;
			li =	'<li id="member_'+ui.item.id+'" class="member_reservation" id-="'+ui.item.id+'">';
			li +=		'<input type="hidden" class="member_id" name="member_id" value="'+ui.item.id+'">';
			li +=		'<div class="nombre">';
			li +=			ui.item.value;
			li +=		'</div>';
			li +=		'<div>';
			li +=			'C.I: '+ number_format(ui.item.identity_card, 0, ',', '.');
			li +=		'</div>';
			li +=		'<div>';
			li +=			'N° Acción: '+ ui.item.number_action;
			li +=		'</div>';
			li +=		'<div class="cerrar" onclick="cerrar_add_maletero(\'#member_'+ui.item.id+'\'); return false;">';
			li +=			'<a href="#" class="cerrar_add">';
			li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			li +=			'</a>';
			li +=		'</div>';
			li +=	'</li>';
			if ($('#member_'+ui.item.id).length <= 0) {
				$('#socio_pagador').append('<option id="option_member_'+ui.item.id+'" value="'+ui.item.id+'">'+ui.item.value+'</option>');
				$('#member_id_prestamo').val();
				$('.add_socios ul').append(li);
			}else {
				$( "#add_members_maletero" ).val('');
				text = '<p> El socio '+ui.item.value+' esta en la agregado  </p>';
				mensaje.html(text);
				mensaje.addClass('mostrar');
				$('#member_'+ui.item.id).addClass('marcar');

				setTimeout(function(){
					$('#member_'+ui.item.id).removeClass('marcar');
					mensaje.removeClass('mostrar');
				}, 5000);
			}
			setTimeout(function(){
				$( "#add_members" ).val('');
				if ($('.capa_maletero .add_ ul li').length !== 0) {
					$('#add_members_maletero').prop('disabled', true);
				}
			},20);
		}
	});
	$('.cerrar_capa_maletero',  capa_maletero).click(function(){
		if (cerrar_capa_maletero()) {
			return false;
		}
	});
}

function remove_locker(id) {
	var locker;
	for (var i = 0; i < lockers.length; i++) {
		var lock = lockers[i];
		if (lock.id === id) {
			locker = lockers[i];
			indice_locker = i;
		}
	}
	var action = locker.remove;
	console.log(locker);
	$('#modaldelete form').attr('action', action );
	$("#modaldelete").modal();
}
function edit_locker(id) {
	var locker;
	for (var i = 0; i < lockers.length; i++) {
		var lock = lockers[i];
		if (lock.id === id) {
			locker = lockers[i];
			indice_locker = i;
		}
	}

	var input = '';
	input += '<div class="form-group">';
	input += '	<label class="col-md-4 control-label" for="textinput">Número</label>';
	input += '	<div class="col-md-6">';
	input += '		<input id="textinput" name="name" type="text" placeholder="Número" class="form-control input-md" value="'+locker.name+'" required>';
	input += '	</div>';
	input += '</div>';
	input += '<div class="form-group">';
	input += '	<label class="col-md-4 control-label" for="name">Estado</label>';
	input += '	<div class="col-md-6">';
	input += '		<select required name="status" class="form-control input-md" required>';
	input += '			<option value="Disponible" selected>Disponible</option>';
	input += '			<option value="Indisponible">No Disponible</option>';
	input += '		</select>';
	input += '	</div>';
	input += '</div>';
	input = $.parseHTML(input);
	var action = locker.remove;
	$('select option', input).each(function(){
		if ($(this).attr('value') == locker.status) {
			$(this).prop('selected', true);
		}else {
			$(this).prop('selected', false);
		}
	});
	$('#modaledit fieldset').html(input);
	$('#modaledit form').attr('action', action );
	$("#modaledit").modal();

}

$('#form_locker_delete').submit(function(e){
	e.preventDefault();
	var action = $(this).attr('action');
	var form_serialize = $(this).serialize();
	$.ajax({
		type: "POST",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			var locker = respuesta.response;
			$('#locker_'+locker.id).fadeOut();
			$('#modaldelete').modal('hide');
		}
	});
});


$('#form_lockers_edit').submit(function(e){
	e.preventDefault();
	var action = $(this).attr('action');
	var form_serialize = $(this).serialize();
	$.ajax({
		type: "POST",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			var locker = respuesta.response;
			for (var i = 0; i < lockers.length; i++) {
				var lock = lockers[i];
				if (lock.id === locker.id) {
					lockers[i] = locker;
				}
			}

			var status = (locker.status == 'Indisponible') ? 'No Disponible' : locker.status;
			$('#locker_'+locker.id).removeClass('Prestamo').removeClass('Disponible').removeClass('Indisponible').addClass(locker.status);
			if (locker.status == 'Indisponible') {
				$('#locker_'+locker.id+' .aaa').removeClass('accion');
			}else {
				$('#locker_'+locker.id+' .aaa').addClass('accion');

			}
			$('#locker_'+locker.id+' h1').html(locker.name);
			$('#locker_'+locker.id+' .text span').html(status);

			$('#modaledit').modal('hide');
		}
	});
});


$('#form_maletero_prestamo').submit(function(e){
	e.preventDefault();
	var form_serialize = $(this).serialize();
	var locker = parseInt($(this).attr('locker_id'));
	var indice_locker = 0;
	for (var i = 0; i < lockers.length; i++) {
		var lock = lockers[i];
		if (lock.id === locker) {
			locker = lockers[i];
			indice_locker = i;
		}
	}
	var disponible = (locker.status == 'Disponible') ? true : false;
	var action = (disponible) ? locker.in : locker.out;
	$.ajax({
		type: "POST",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			console.log(respuesta);
			var locker_response = respuesta.response;
			var li_locker = $('#locker_'+locker.id);

			lockers[indice_locker] = locker_response;

			if (disponible) {
				li_locker.removeClass('Disponible').addClass(locker_response.status);
				$('.header', li_locker).fadeOut();
			}else {
				li_locker.removeClass('Prestamo').addClass(locker_response.status);
				$('.header', li_locker).fadeIn();
			}


			$('.text span', li_locker).html(locker_response.status);
			cerrar_capa_maletero();

		}
	});
});


function cerrar_capa_maletero() {
	var retorno = true;
	try {
		capa_maletero.removeClass('active');
		setTimeout(function(){
			capa_maletero.addClass('none');
		}, 600);
	} catch (e) {
		retorno = false;
	}
	return retorno;
}

// function autocomplete_socios() {
// }
function cerrar_add_maletero(cerrar) {
		$(cerrar).fadeOut(400);
		setTimeout(function(){
				$(cerrar).remove();
				if ($('.capa_maletero .add_ ul li').length === 0) {
					$('#add_members_maletero').prop('disabled', false);
				}
				$('#add_members_maletero').val('');

		},400);

}
$('#form_lockers_create').submit(function(e){
	e.preventDefault();

	var form_serialize = $(this).serialize();
	var action = $(this).attr('action');
	$.ajax({
		type: "POST",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			console.log(respuesta);
			var locker = respuesta.response.locker;
			var success = (respuesta.type == 'success' ? true : false);
			var status = (locker.status == 'Indisponible') ? 'No Disponible' : locker.status;
			if (success) {
				lockers.push(locker);
				var li = '';
				li += '<li class="'+locker.status+'" id="locker_'+locker.id+'">';
				li += '<ul class="header">';
				li += '	<li>';
				li += '		<a href="" onclick="edit_locker('+locker.id+'); return false;">';
				li += '			<span class="glyphicon glyphicon-edit"></span>';
				li += '		</a>';
				li += '	</li>';
				li += '	<li>';
				li += '		<a href="" onclick="remove_locker('+locker.id+'); return false;">';
				li += '			<span class="glyphicon glyphicon-remove"></span>';
				li += '		</a>';
				li += '	</li>';
				li += '</ul>';
				li += '	<a href="#'+locker.id+'" class="accion aaa" onclick="maletero($(this)); return false;">';
				li += '		<div class="text">';
				li += '			<h1> '+locker.name+' </h1>';
				li += '			<span>'+status+'</span>';
				li += '		</div>';
				li += '	</a>';
				li += '</li>';
				li = $.parseHTML(li);
				$('.maletero').append(li);
			}
			$('#modalcreate').modal('hide');
		}
	});
	//
});

function hora() {
	var fecha_ = new Date();
	var hora = fecha_.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit', second: '2-digit' });

	// console.log(hora);
	return hora;
}



var black_capa = $('.black_capa');
$('.starter_accion').click(function(){
	var datos = '';
	var tipo = $(this).attr('type');
	var id = parseInt($('#ordenable li', $(this).parent().parent()).attr('data-id'));
	var hora = $(this).attr('hora');
	var mensaje = ( tipo == 'salida' ) ? 'Dar salida a la reservación' : 'Cancelar la reservación';
	var status = ( tipo == 'salida' ) ? "Finalizada" : "Cancelada";
	var clone = '#'+$(this).parent().parent().parent().attr('id');
	for (var i = 0; i < reservation_day.length; i++) {
		var reservation = reservation_day[i];
		if (reservation.id == id) {
			datos += '<input type="hidden" name="reservation_id" value="'+reservation.id+'" id="reservation_id">';
			datos += '<input type="hidden" name="status" value="'+status+'" id="reservation_id">';
			datos += '<input type="hidden" name="time" value="'+hora+'" id="reservation_id">';
			datos += '<div class="titulo '+tipo+'">';
			datos += '	<h1> '+mensaje+': </h1>';
			datos += '</div>';
			datos += '<div class="hora">';
			datos += '	<h1> '+hora+' </h1>';
			datos += '</div>';
			datos += '<div class="personas">';
			for (var im = 0; im < reservation.members.length; im++) {
				var member = reservation.members[im];
				datos += '	<h3>'+member.name+' '+member.last_name+'</h3>';
			}
			for (var ip = 0; ip < reservation.partners.length; ip++) {
				var partner = reservation.partners[ip];
				datos += '	<h3>'+partner.name+' '+partner.last_name+'</h3>';
			}
			datos += '</div>';
			// console.log(clone);
			datos += '<div class="form-group boton">';
			datos += '	<button id="guardar" name="singlebutton" type="button" class="btn btn-primary singlebutton1" onclick="accion_starter(\''+tipo+'\', \''+clone+'\'); return false;">Enviar</button>';
			datos += '	<button type="button" onclick="cerrar_black_capa();" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>';
			datos += '</div>';
		}
	}
	$('.datos', black_capa).html(datos);

	black_capa.removeClass('none');
	setTimeout(function(){
		black_capa.addClass('active');
	});
	return false;
});

function accion_starter(tipo, id, hora) {
	var clone = $(id).clone();
	var id_reservation = '#'+$(id+' #ordenable li').attr('id');
	var li_reservation = $('#reservacion '+id_reservation).parent().parent().parent();


	$('#reserv', li_reservation).sortable( "destroy" );
	$('#ordenable', $(id)).sortable( "destroy" );

	$(id).addClass('disabled');
	li_reservation.addClass('disabled');

	$('span.check, span.cancel, span.draggable', $(id)).remove();
	$('span.check, span.cancel, span.draggable', clone).remove();
	$('span.draggable', li_reservation).remove();



	if (tipo == 'salida') {
		$.ajax({
			type: "POST",
			url: $('#form_starter').attr('action'),
			data: $('#form_starter').serialize(),
			success: function(respuesta) {
				clone.addClass('check');
				$('#salidas').append(clone);
				$('#salidas '+id+' #ordenable li').addClass('movido');
				cerrar_black_capa();
			}
		});

	}else {
		$.ajax({
			type: "POST",
			url: $('#form_starter').attr('action'),
			data: $('#form_starter').serialize(),
			success: function(respuesta) {
				clone.addClass('cancel');
				$('#cancelados').append(clone);
				$('#cancelados '+id+' #ordenable li').addClass('movido');
				cerrar_black_capa();
			}
		});
	}
	setTimeout(function(){
		$('#salidas '+id+' #ordenable li, #cancelados '+id+' #ordenable li').removeClass('movido');
	}, 2000);
}
function cerrar_black_capa() {
	black_capa.removeClass('active');
	setTimeout(function(){
		black_capa.addClass('none');
		$('.datos', black_capa).html('');
	}, 600);
}
$('.cerrar_black_capa',  black_capa).click(function(){
	cerrar_black_capa();
});

function validar_formato_fecha(dateString)
{
    // revisar el patrón
    if(!/^\d{1,2}\-\d{1,2}\-\d{4}$/.test(dateString))
        return 1; //formato malo : formato permitido dd-mm-yyyy

    // convertir los numeros a enteros
    var parts = dateString.split("-");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);
	// console.log(day, month, year);

    // Revisar los rangos de año y mes
    if( (year < 1000) || (year > 3000) || (month === 0) || (month > 12) ){
		return 2; //fecha no real ejemplo 40-03-1200

	}

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Ajustar para los años bisiestos
    if(year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)){
		monthLength[1] = 29;
	}

    // Revisar el rango del dia
    return day > 0 && day <= monthLength[month - 1]; //formato correcto


}

$('.chequear_cedula').on('keyup change blur', function(){
	var value = $(this).val();
	var ruta = $(this).attr('checkear');
	var $this = $(this);
	var data = {
		identity_card: value,
		// if (window.socio_id) {
		// 	member_id: value
		// }
	};
	if (window.socio_id) {
		data.member_id = socio_id;
	}
	if (window.partner_id) {
		data.partner_id = partner_id;
	}
	// if (window.user_id) {
	// 	data.user_id = partner_id;
	// }
	// console.log(daa);
	var form = $('.form_validation');
	var chequear = $('.chequear', $this.parent());
	if (value.length > 4) {
		validate_datos(ruta, data, value, form, $this, chequear);
	}else {
		$('.error .mensaje p', chequear).html('La cedula ingresada debe ser mayor a 5 caracteres');
		$('.icon', chequear).not('.error').removeClass('active');
		$('.error', chequear).addClass('active');
		$this.addClass('border_error');
		form.addClass('form_error');
	}
});

$('.chequear_correo').on('keyup change blur', function(){
	var value = $(this).val();
	var ruta = $(this).attr('checkear');
	var $this = $(this);
	var data = {
		email: value
	};
	if (window.socio_id) {
		data.member_id = socio_id;
	}
	if (window.partner_id) {
		data.partner_id = partner_id;
	}
	var form = $('.form_validation');
	var chequear = $('.chequear', $this.parent());

	var formato = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	if (formato.test(value)) {
		validate_datos(ruta, data, value, form, $this, chequear);
	}else {
		$('.error .mensaje p', chequear).html('Debe ingresar un formato de correo real. Ej: lagunita@golf.com');
		$('.icon', chequear).not('.error').removeClass('active');
		$('.error', chequear).addClass('active');
		$this.addClass('border_error');
		form.addClass('form_error');
	}
});

function validate_datos(ruta, data, value, form, $this, chequear) {
	$.ajax({
		type: "GET",
		url: ruta,
		data: data,
		beforeSend: function(){
			$('.icon', chequear).not('.cargando').removeClass('active');
			$('.cargando', chequear).addClass('active');
			$this.removeClass('border_error');
			form.addClass('form_error');

		},
		success: function(validacion) {
			// console.log(ruta, data, value, form, $this, chequear);
			// console.log(validacion);
			if (validacion.code == 200) {
				$('.icon', chequear).not('.check').removeClass('active');
				$('.check', chequear).addClass('active');
				$this.removeClass('border_error');
				if ($('.chequear .error.active').length === 0) {
					form.removeClass('form_error');
				}
			}else {
				// console.log(validacion);
				// console.log(validacion.message);
				$('.error .mensaje p', chequear).html(validacion.status);
				$('.icon', chequear).not('.error').removeClass('active');
				$('.error', chequear).addClass('active');
				$this.addClass('border_error');
				form.addClass('form_error');
			}
		}
	});
}
$('.form_validation').submit(function(){
	if ($(this).filter('.form_error').length <= 0) {
		$(this)[0].submit();
	}else {
		return false;
	}
});



// var fecha_inscripcion = new Date('2017/04/07');
// console.log(fecha_inscripcion.toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' }));

var detalles_torneo = $('#detalles_torneo');
$('.btn_detalles_torneo').click(function(){
	var ruta = $(this).attr('href');
	$.ajax({
		type: "GET",
		url: ruta,
		// data: {id: id,exonerated: exonerated},
		success: function(respuesta) {
			var tournament = respuesta.tournament;
			// console.log(tournament);

			$('#detalles_torneo #nombre').html(tournament.name);
			var fecha_inscripcion = new Date(tournament.start_date_inscription.replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			var fecha_tope_inscripcion = new Date(tournament.end_date_inscription.replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			$('#detalles_torneo #fecha_inscripcion span').html(fecha_inscripcion);
			$('#detalles_torneo #fecha_tope_inscripcion span').html(fecha_tope_inscripcion);

			$('#detalles_torneo #modalidad').html(tournament.modality.name);

			var categorias = '';
			for (var i = 0; i < tournament.categories.length; i++) {
				categorias += '<h4>'+tournament.categories[i].name+'</h4>';
			}
			$('#detalles_torneo #categorias').html(categorias);


			var fecha_inicio = new Date(tournament.start_date.replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			var fecha_fin = new Date(tournament.end_date.replace('-', '/')).toLocaleString('es-ES', { weekday: 'long', day: '2-digit', month: 'long', year: 'numeric' });
			$('#detalles_torneo #fecha_inicio span').html(fecha_inicio);
			$('#detalles_torneo #fecha_fin span').html(fecha_fin);

			//console.log(public_directory+tournament.conditions_file);
			$('#detalles_torneo #condicion_torneo').fileinput('destroy');
			if (tournament.conditions_file != null) {
				$('#detalles_torneo #condicion_torneo').fileinput({
					initialPreview: [
						public_directory+tournament.conditions_file
					],
					initialPreviewAsData: true,
					initialPreviewConfig: [
						{type: "pdf", caption: tournament.name, showDelete: false, width: "250px"},
					],

					showUpload: false,
					showRemove: false,
					showBrowse: false,
					showClose: false,
					showCaption: false,
					// showCancel: fasle,
					// showUploadedThumbs: false,
					// 'previewFileType': 'any'
					maxFileCount: 1,
					browseLabel: 'Buscar',
					msgZoomModalHeading: 'Vista previa',
					allowedFileExtensions: ['pdf', 'docx'],
					previewFileIconSettings: {
						'doc': '<i class="fa fa-file-word-o text-primary"></i>',
						'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
					},
					msgInvalidFileExtension: 'Error, solo puede agregar documentos "pdf" o "docx"',
					msgNoFilesSelected: 'No hay archivo seleccionado'
					// showBrowse: false
				});
			}
			else{
				$('#detalles_torneo #condicion_torneo').addClass('hidden');
			}



			setTimeout(function(){
				detalles_torneo.removeClass('none');
				setTimeout(function(){
					detalles_torneo.addClass('active');
				});
			}, 500);
		}
	});
	return false;
});

var modal_lista_espera = $('#modal_lista_espera');
$('.btn_lista_espera').click(function(){
	var ruta = $(this).attr('href');
	$.ajax({
		type: "GET",
		url: ruta,
		// data: {id: id,exonerated: exonerated},
		success: function(respuesta) {
			$('#modal_lista_espera #content').html("");
			var categories = respuesta.categories;
			console.log(categories);

			var table = '';
			if (categories.length > 0) {
				for (var i = 0; i < categories.length; i++) {
					var category = categories[i];
					table += '<thead>';
					table += '	<tr>';
					table += '		<th colspan="5"><h4>Categoria: '+category.name+'</h4></th>';
					table += '	</tr>';
					table += '	<tr>';
					table += '		<th class="th_nombre">Nombre</th>';
					table += '		<th class="th_apellido">Apellido</th>';
					table += '		<th class="th_ci">C.I.</th>';
					table += '		<th class="th_n_accion">Nª accion</th>';
					table += '		<th class="th_telefono">Teléfono</th>';
					table += '	</tr>';
					table += '</thead>';
					table += '<tbody>';
					if (category.members.length > 0) {
						for (var im = 0; im < category.members.length; im++) {
							var member = category.members[im];
							table += '	<tr>';
							table += '		<td>'+member.name+'</td>';
							table += '		<td>'+member.last_name+'</td>';
							table += '		<td>'+member.identity_card+'</td>';
							table += '		<td>'+member.number_action+'</td>';
							table += '		<td>'+member.phone+'</td>';
							table += '	</tr>';
						}
					}else {
						table += '<tr>';
						table += '	  <td colspan="5" class="no_inscrito">No hay usuarios en lista de espera</td>';
						table += '</tr>';
					}
					table += '</tbody>';
				}

				// table = "<table>";
				//
				// for (var i = categories.length - 1; i >= 0; i--) {
				// 	table += "<thead>";
				// 	table += "	  <tr>";
				// 	table += "	  	  <th> Categoria: "+ categories[i].name+"</th>";
				// 	table += "	  </tr>";
				// 	table += "</thead>";
				// 	table += "    <tr>";
				// 	table += "    <td>";
				// 	table += "    Nombre";
				// 	table += "    </td>";
				// 	table += "    <td>";
				// 	table += "    Apellido";
				// 	table += "    </td>";
				// 	table += "    <td>";
				// 	table += "    C.I.";
				// 	table += "    </td>";
				// 	table += "    <td>";
				// 	table += "    Nª accion";
				// 	table += "    </td>";
				// 	table += "    <td>";
				// 	table += "    Teléfono";
				// 	table += "    </td>";
				// 	table += "	  </tr>";
				// 	console.log(categories[i].members);
				// 	members = categories[i].members;
				// 	if (members.length > 0) {
				// 		for (var j = members.length - 1; j >= 0; j--) {
				// 			table += "<tr>";
				// 			table += "<td>";
				// 			table += members[j].name;
				// 			table += "</td>";
				// 			table += "<td>";
				// 			table += members[j].last_name;
				// 			table += "</td>";
				// 			table += "<td>";
				// 			table += members[j].identity_card;
				// 			table += "</td>";
				// 			table += "<td>";
				// 			table += members[j].number_action;
				// 			table += "</td>";
				// 			table += "<td>";
				// 			table += members[j].phone;
				// 			table += "</td>";
				// 			table += "</tr>";
				// 		}
				// 	}
				//
				// }
				// table += "</table>";


			}
			// console.log(table);
			// console.log($('#modal_lista_espera #content'));
			// console.log($('#modal_lista_espera'));
			$('#modal_lista_espera #content_listado_espera table').html(table);
			$('#modal_lista_espera').modal();
		}
	});





	return false;
});

$('.detalles_torneo .cerrar_detalles_torneo').click(function(){
	detalles_torneo.removeClass('active');
	setTimeout(function(){
		detalles_torneo.addClass('none');
		$('.home_torneos .owl-carousel').trigger('destroy.owl.carousel');
	}, 700);
});


$('checkbox_payment_plan').click(function(e){
		e.preventDefault();

});


$('#create_student').click(function(e){
		e.preventDefault();
		$('#modal_create_student').modal();
});
$('#create_group').click(function(e){
		e.preventDefault();
		$('#modal_create_group').modal();
});



$('#form_create_student').submit(function(e){
	e.preventDefault();
	var action = $(this).attr('action');
	var form_serialize = $(this).serialize();
	$.ajax({
		type: "GET",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			var student = respuesta.response;
			var option = '<option value="'+student.id+'" selected>'+student.name+' '+student.last_name+'</option>';
			option = $.parseHTML(option);

			$('#student_select').append(option);
			$('#modal_create_student').modal('hide');
			$('#form_create_student')[0].reset();

		}
	});
});

$('#form_create_group').submit(function(e){
	e.preventDefault();
	var action = $(this).attr('action');
	var form_serialize = $(this).serialize();
	console.log(action, form_serialize);
	$.ajax({
		type: "GET",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			console.log(respuesta);
			var group = respuesta.response;
			var option = '<option value="'+group.id+'" selected>'+group.name+'</option>';
			option = $.parseHTML(option);

			$('#group_select').append(option);
			$('#modal_create_group').modal('hide');
			$('#form_create_group')[0].reset();

		}
	});
});


$( "#add_member_registration" ).autocomplete({
	source: $('#add_member_registration').attr('ruta'),
	minLength: 1,
	select: function(event, ui) {
		$('#member_id_student').val(ui.item.id);
		$('#student_select, #create_student').prop('disabled', false);
		// var action = $( "#add_member_registration" ).attr('ruta2');
		var action = 'http://localhost:8000/alumnos/member/'+ui.item.id;
		// var action = 'http://dementecreativo.com/lagunita/golf/alumnos/member/'+ui.item.id;

		$.ajax({
			type: "GET",
			url: action,
			data: { member_id: ui.item.id },
			success: function(respuesta) {
				if (respuesta.type == 'success') {

					var students = respuesta.response;
					// foreach
					var options = '';
					for (var i = 0; i < students.length; i++) {
						var student = students[i];
						options += '<option value="'+student.id+'">'+student.name+' '+student.last_name+'</option>';
					}
					$('#student_select').html(options);
					// console.log(options);
				}

			}
		});
	}
});
// Change status of members and partners
$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');
    if ($(this).find('.btn-primary').size()>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    $(this).find('.btn').toggleClass('btn-default');
});

function change_status( id, ruta )
{
	$.ajax({
		type: "PUT",
		url: ruta+'/change_status/'+id,
		success: function(validacion) {
			if ( validacion.changed ) // ojo negar la validacion para que se muestre solo por error
			{
				alert( "Negro acomoda este menaje: " + validacion.message );
			}
		}
	});
	return false;

}
/*--------------------------------------------change status with toggle------------------*/
$('.toggle-event').change(function() {
  $.ajax({
    type: "PUT",
    url: $(this).attr('data-route') + $(this).attr('data-item'),
    success: function(validacion) {
    }
  });


})
/*--------------------------------------------change status with toggle------------------*/

/*--------------------------------------- change status court Home ----------------------*/

// Cambia el estado de las canchas y los carrito_estado
$('#img_estado_cancha').on('click',function(e){
	e.preventDefault();
	$.ajax({
		type: "PUT",
		url: 'campo_golf/disponibilidad/court',
		success: function(validacion) {
			if ( validacion.changed ) {
				if (validacion.status == 1)	{
					$('#img_estado_cancha').attr("src","img/icons/cancha.png");
				}
				else {
					$('#img_estado_cancha').attr("src","img/icons/nocancha.png");
				}
			}
			else
			{
				console.log('Fallo en Respuesta');
			}
		}
	});
});

$('#img_estado_carrito').on('click',function(e){
	e.preventDefault();
	$.ajax({
		type: "PUT",
		url: 'campo_golf/disponibilidad/cars',
		success: function(validacion) {
			if ( validacion.changed )	{
				if (validacion.status == 1)	{
					$('#img_estado_carrito').attr("src","img/icons/carrito.png");
				}
				else {
					$('#img_estado_carrito').attr("src","img/icons/nocarrito.png");
				}
			}
			else
			{
				console.log('Fallo en Respuesta');
			}
		}
	});
});


/**/

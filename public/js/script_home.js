var home_torneos = $('#home_torneos');


$(document).ready(function(){
	simpleWeather();
});


$('.ver_detalle_torneo').click(function(){
	var item = $(this).attr('item');
	var numero = 0;
	var item_numero;

	$('.home_torneos .owl-carousel .item').each(function(){
		if ($(this).attr('id') == item) {
			item_numero = numero;
		}
		numero++;
	});

	$('.home_torneos .owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		// singleItem: true,
		dots: false,
		items:1,
		navClass: ['anterior direcciones', 'siguiente direcciones'],
		navText: ['<i class="glyphicon glyphicon-menu-left"></i>', '<i class="glyphicon glyphicon-menu-right"></i>'],
		navContainerClass: 'navegacion',
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		startPosition: item_numero,
		reponsive: {
			0: {
				items: 1
			}
		}
	});

	setTimeout(function(){
		home_torneos.removeClass('none');
		setTimeout(function(){
			home_torneos.addClass('active');
		});
	}, 500);


});
$('.home_torneos .owl-carousel .item').each(function(){
	$('.condicion_torneo', this).fileinput({
		initialPreview: [
			$('.condicion_torneo', this).attr('value')
		],
		initialPreviewAsData: true,
		initialPreviewConfig: [
			{type: "pdf", caption: $('.titulo h1', this).html(), showDelete: false, width: "250px"},
		],

		showUpload: false,
		showRemove: false,
		showBrowse: false,
		showClose: false,
		showCaption: false,
		// showCancel: fasle,
		// showUploadedThumbs: false,
		// 'previewFileType': 'any'
		maxFileCount: 1,
		browseLabel: 'Buscar',
		msgZoomModalHeading: 'Vista previa',
		allowedFileExtensions: ['pdf', 'docx'],
		previewFileIconSettings: {
			'doc': '<i class="fa fa-file-word-o text-primary"></i>',
			'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
		},
		msgInvalidFileExtension: 'Error, solo puede agregar documentos "pdf" o "docx"',
		msgNoFilesSelected: 'No hay archivo seleccionado'
		// showBrowse: false
	});
});

$('.home_torneos .cerrar_home_torneos').click(function(){
	home_torneos.removeClass('active');
	setTimeout(function(){
		home_torneos.addClass('none');
		$('.home_torneos .owl-carousel').trigger('destroy.owl.carousel');
	}, 700);
});

// Cambia el estado de las canchas y los carrito_estado
$('#img_estado_cancha').on('click',function(e){
	e.preventDefault();
	$.ajax({
		type: "PUT",
		url: 'campo_golf/disponibilidad/court',
		success: function(validacion) {
			if ( validacion.changed ) {
				if (validacion.status == 1)	{
					$('#img_estado_cancha').attr("src","img/icons/cancha.png");
				}
				else {
					$('#img_estado_cancha').attr("src","img/icons/nocancha.png");
				}
			}
		}
	});
});

$('#img_estado_carrito').on('click',function(e){
	e.preventDefault();
	$.ajax({
		type: "PUT",
		url: 'campo_golf/disponibilidad/cars',
		success: function(validacion) {
			if ( validacion.changed )	{
				if (validacion.status == 1)	{
					$('#img_estado_carrito').attr("src","img/icons/carrito.png");
				}
				else {
					$('#img_estado_carrito').attr("src","img/icons/nocarrito.png");
				}
			}
		}
	});
});


/* Clima wheater */
function simpleWeather() {
	$.simpleWeather({
		zipcode: '709',
		unit: 'c',
			location: 'Caracas Ca',
		success: function(weather) {
					// var city, country, currently, image;

					html = '<div class="city">';
					html += '    <h1>'+weather.city+', '+weather.country+'</h1>';
					html += '</div>';
					html += '<div class="country">';
					html += '    <h3>'+weather.temp+'°</h3>';
					html += '</div>';
					html += '<div class="estado">';
					html += '    <img src="'+weather.image+'" alt="">';
					html += '</div>';



			$(".clime").html(html);
		},
		error: function(error) {
			simpleWeather();
		}
	});

}

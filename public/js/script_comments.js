$(document).ready(function(){
	comments();
	remove_comments();
	edit_comments();
});


function edit_comments() {
	$('.edit_comment').off('click');
	$('.edit_comment').click(function(e){
		var id = parseInt($(this).attr('id'));
		var reservation_id = parseInt($(this).attr('reservation'));
		function encontrar_reservation(reservation) {
			return reservation.id === reservation_id;
		}
		var reservation = reservations.find(encontrar_reservation);
		var comment = reservation.comments.filter(function (comment) {
	        return comment.id === id
      	});
		$('#comment_id').val(id);
		$('#comment').val(comment[0].comment);
		$('#editar_comentario').modal();
	});
}



function remove_comments() {
	$('#listar_comentarios ul li button.remove, .remove_comment').off('click');
	$('#listar_comentarios ul li button.remove, .remove_comment').click(function(e){
		var id = parseInt($(this).attr('id'));
		var reservation_id = parseInt($(this).attr('reservation'));
		var table = $(this).hasClass('remove_comment') ? true : false;
		var li = (table) ? $(this).parent().parent() : $(this).parent();
		$.ajax({
			type: "POST",
			url: '/rangers/comentario/'+id,
			data: {
				comment_id: id,
				'_method': 'DELETE',
				'_token': tokem
			},
			success: function(respuesta) {
				if (respuesta) {
					function encontrar_reservation(reservation) {
						return reservation.id === reservation_id;
					}
					var reservation = reservations.find(encontrar_reservation);
					reservation.comments.forEach(function(comment, index){
						if (id === comment.id) {
							reservation.comments.splice(index, 1);
							li.remove();
						}
					});

				}
			}
		});
	});
}


$('#form_agregar_comentario').submit(function(e){
	e.preventDefault();
	$.ajax({
		type: "POST",
		url: '/rangers/comentario',
		data: $('#form_agregar_comentario').serialize(),
		success: function(respuesta) {
			if (respuesta.status) {
				function encontrar_reservation(reservation) {
					return reservation.id === parseInt(respuesta.comment.reservation_id);
				}
				var reservation = reservations.find(encontrar_reservation);
				reservation.comments.push(respuesta.comment);
				$('#agregar_comentario').modal('hide');
				$('#form_agregar_comentario')[0].reset();
			}
		}
	});
});

function comments() {
	$('.agregar_comentario').off('click');
	$('.agregar_comentario').click(function(e){
		e.preventDefault();
		var now = new Date();
		var hora = now.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
		var fecha = now.toLocaleString('es-Es', { day: "2-digit", year: "2-digit", month: "2-digit"}).split('/').join('-');
		$('#time').val(hora);
		$('#date').val(fecha);
		$('#reservation_id').val($(this).attr('reservation'));
		$('#agregar_comentario').modal();
	});
	$('.listar_comentario').off('click');
	$('.listar_comentario').click(function(e){
		e.preventDefault();
		var id = parseInt($(this).attr('reservation'));
		function encontrar_reservation(reservation) {
			return reservation.id === id;
		}
		var reservation = reservations.find(encontrar_reservation);
		li = '';
		if (reservation.comments.length > 0) {
			reservation.comments.forEach(function(comment, index){
				li += '<li>';
				li += '	<div class="tiempo">';
				li += '		<div class="item fecha">';
				if (index === 0) {
					li += '			<span><strong>Fecha:</strong> '+comment.date.split('-').reverse().join('-')+'</span>';
				}
				li += '		</div>';
				li += '		<div class="item hora">';
				li += '			<span><strong>Hora:</strong> '+comment.time+'</span>';
				li += '		</div>';
				li += '		<div class="clear"></div>';
				li += '	</div>';
				li += '	<div class="comentario">';
				li += '		<p><strong>Comentario:</strong> '+comment.comment+'</p>';
				li += '	</div>';
				li += '	<button type="button" name="button" class="remove" id="'+comment.id+'" reservation="'+comment.reservation_id+'">&times;</button>';
				li += '</li>';
			});
		}else {
			li += '<li><h4>No hay comentarios para esta reservación</h4></li>';
		}
		$('#listar_comentarios ul').html(li);
		$('#listar_comentarios').modal();
		remove_comments();
	});

}

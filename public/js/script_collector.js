var members = null, partners = null, date_reservation_selected = '', intervalo_selected = []
var members_agregados = [];
var partners_agregados = [];
var partners_green_fee = [];
var green_fee_mounts = [];
var date_selected;
var hoy = new Date();
$(document).ready(function(){
    buscar_invitados_reservacion();
    buscar_socios_reservacion();
    date_reservation();

    mostrar_minutos(hoy.toLocaleString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' }).replace(/\//g, '-'));
});
/* Funcion que consulta todos los members de la DB y lo pasa a la variable members*/
function buscar_socios_reservacion() {
	$.ajax({
		type: "GET",
		url: '/socios/alls',
		success: function(respuesta) {
			if (respuesta.status === true) {
				members = respuesta.response;
			}
		}
	});
}
/* Funcion que consulta todos los Partners de la DB y lo pasa a la variable partners*/
function buscar_invitados_reservacion() {
	var url = ($('.type_member').length < 1) ? '/invitados/alls' : '/s_invitados/alls';
	var data = ($('.type_member').length < 1) ? {} : {user_id: 17};
	$.ajax({
		type: "GET",
		url: url,
		data: data,
		success: function(respuesta) {
			if (respuesta.status === true) {
				partners = respuesta.response;
			}
		}
	});
}
// Funcion que agrega todos los members y partners a select2 de reservacion y inicializa el mismo
function select2() {
	var option_partner = '';
	option_partner = '<option value="" disabled selected>Seleccionar Invitado</option>';
	for (var i = 0; i < partners.length; i++) {
		var partner = partners[i];
		// var nombre = partner.name + ' ' + partner.last_name;
		var id = partner.id;
		option_partner += '<option value="'+id+'">C.I.: '+(number_format(partner.identity_card, 0, ',', '.'))+' '+ partner.nombre +' </option>';
	}
	$('#add_partners').html(option_partner);
	var option_members = '';
	option_members = '<option value="selected" disabled selected>Seleccionar Socio</option>';
	for (var i = 0; i < members.length; i++) {
		var member = members[i];
		// var nombre = member.name + ' ' + member.last_name;
		var id = member.id;
		option_members += '<option value="'+id+'">C.I.: '+(number_format(member.identity_card, 0, ',', '.'))+' '+ member.nombre +' </option>';
	}
	$('#add_members').html(option_members);
	$(".select2_").select2();
}
// Funcion que muestra todos los consaje de reservacion
function mensaje_reservation($this, mensaje_, tipo, usuario) {
	var mensaje = $('.mensaje', $this.parent().parent());
	text = '<p> '+ mensaje_ +' </p>';
	mensaje.html(text);
	mensaje.addClass('mostrar');
	$('#'+tipo+'_'+usuario.id).addClass('marcar');
	setTimeout(function(){
		$('#'+tipo+'_'+usuario.id).removeClass('marcar');
		mensaje.removeClass('mostrar');
	}, 5000);

}

// Funcion que seleciona las horas de reservacion
function select_hour_reservation() {
	$('.select_hour_reservation').off('click');
	$('.select_hour_reservation').click(function(){
		if ($(this).hasClass('intervalo')) {
			var check = $(this).prop('checked');
			var id = parseInt($(this).attr('id'));
			var checks = $('.intervalo_'+id);
			var icons = checks.parent();
			var icons_not = $('.edit .horas_reservacion ul li .minutos ul li .icon').not(icons);
			var no_checks = $('input', icons_not).prop('checked', false);
            if (intervalo_selected.indexOf(id) === -1) {
                intervalo_selected.push(id);
                icons.addClass('selected');
            }else {
                intervalo_selected.splice(intervalo_selected.indexOf(id), 1);
                icons.removeClass('selected');
            }
            $('#time_reservacion').val(JSON.stringify(intervalo_selected));
            input_error($('.horas_reservacion ul'), null, false);
		}else {
            $('#time_reservacion').val($(this).val());
			var check = $(this).prop('checked');
			var icon = $(this).parent();
			var icon_not = $('.edit .horas_reservacion ul li .minutos ul li .icon').not(icon);
			if (check === true) {
				$('input', icon_not).prop('checked', false);
				$(this).prop('checked', true);
				icon_not.removeClass('selected');
				icon.addClass('selected');
				// dias_de_sorteo_select = null;
			}else {
				icon.removeClass('selected');
				$(this).prop('checked', false);
			}
			input_error($('.horas_reservacion ul'), null, false);

		}
	});
}
// Funcion que exonera a partners durante reeservacion
function exonerar(id_selector) {
	var id = parseInt(id_selector.split('_')[1]);
	if (partners_green_fee.indexOf(id) == -1) {
		partners_green_fee.push(id);
		$(id_selector).addClass('select_green_fee');
		if ($('.green_fee select', $(id_selector)).val() !== '') {
			green_fee_mounts.push({ id: id, id_green_fee: parseInt($('.green_fee select', $(id_selector)).val()) });
		}
	}else {
		partners_green_fee.splice(partners_green_fee.indexOf(id), 1);
		$(id_selector).removeClass('select_green_fee');
		for (var i = 0; i < green_fee_mounts.length; i++) {
			var green_fee_mount = green_fee_mounts[i];
			if (green_fee_mount.id == id) {
				green_fee_mounts.splice(i, 1);
			}
		}
	}
	if (partners_green_fee.length < 1) {
		$('.reserv .inputs .boton').removeClass('pagar');
	}else {
		$('.reserv .inputs .boton').addClass('pagar');
	}
}

/* Funcion que remueve participantes de lista */
function cerrar_add(cerrar) {
	var tipo = cerrar.split('_')[0].split('#')[1];
	var id = parseInt(cerrar.split('_')[1]);
	if (tipo == 'partner') {
		if (partners_green_fee.indexOf(id) != -1) {
			partners_green_fee.splice(partners_green_fee.indexOf(id), 1);
		}
		partners_agregados.splice(partners_agregados.indexOf(id), 1);
	}else {
		var pagador = '#option_member_'+cerrar.split('_')[1];
		$(pagador).remove();
		members_agregados.splice(members_agregados.indexOf(id), 1);
	}
	for (var i = 0; i < green_fee_mounts.length; i++) {
		var green_fee_mount = green_fee_mounts[i];
		if (green_fee_mount.id == id) {
			green_fee_mounts.splice(i, 1);
		}
	}
	if (partners_green_fee.length < 1) {
		$('.reserv .inputs .boton').removeClass('pagar');
	}
	$(cerrar).fadeOut(400);
	setTimeout(function(){
		$(cerrar).remove();
	},400);
}

/* Funcion que seleciona el green fee de cada partner*/
function select_green_fee(selector, id) {
	input_error(selector, null, false);
	var object = { id: id, id_green_fee: parseInt(selector.val()) };
	var index;
	if (green_fee_mounts.length > 0) {
		for (var i = 0; i < green_fee_mounts.length; i++) {
			var green_fee_mount = green_fee_mounts[i];
			if (green_fee_mount.id == id) {
				index = i;
			}
		}
		if (index === undefined) {
			green_fee_mounts.push(object);
		}else {
			green_fee_mounts.splice(index, 1, object);
		}
	}else {
		green_fee_mounts.push(object);
	}
}

/* Funcion que abre el modal de procesar pago */



$('#add_members').on('select2:select', function (evt) {
	var $this = $(this);
	var id = parseInt(evt.params.data.id);
	var member;
	for (var i = 0; i < members.length; i++) {
		if (members[i].id == id) {
			member = members[i];
		}
	}
	var li, text;
	li =	'<li id="member_'+member.id+'" class="member_reservation" id-="'+member.id+'">';
	li +=		'<input type="hidden" class="member_id" name="member_id[]" value="'+member.id+'">';
	li +=		'<div class="nombre">';
	li +=			member.nombre;
	li +=		'</div>';
	li +=		'<div>';
	if (member.user == 2) {
		li +=			'C.I: '+ number_format(member.identity_card, 0, ',', '.');
	}else{
		li +=			'N° Accion: '+ member.number_action;
	}
	li +=		'</div>';
	li +=		'<div class="cerrar" onclick="cerrar_add(\'#member_'+member.id+'\'); return false;">';
	li +=			'<a href="#" class="cerrar_add">';
	li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
	li +=			'</a>';
	li +=		'</div>';
	li +=	'</li>';
	li = $.parseHTML(li);
	if ($('.type_member').length <= 0) { //Verifico si es tipo admin
		if (members_agregados.indexOf(member.id) == -1) {
			$.ajax({
				type: "GET",
				url: '/socios/'+user_id+'/validar_reservacion_socio',
				data: {
					member_id: member.id,
					date: $('#date_reservation').val(),
				},
				success: function(respuesta) {
					if (respuesta.validation === true) {
						$('.participante_dia label span', $this.parent()).html('');
						reservacion_en_el_dia($this, false);

						$('#socio_pagador').append('<option id="option_member_'+member.id+'" value="'+member.id+'">'+member.nombre+'</option>');
						$('.add_socios ul').not('.socio_auth').append(li);
						input_error($('#add_members'), null, false);
						members_agregados.push(member.id);
					} else {
						input_error($('#add_members'), null, false);
						$('.participante_dia label span', $this.parent()).html(respuesta.message);
						reservacion_en_el_dia($this, true);
						$('.participante_dia button', $this.parent()).off('click');
						/* Si Hace clicl en continuar*/
						$('.participante_dia button', $this.parent()).filter('.continuar').click(function(){
							$('#socio_pagador').append('<option id="option_member_'+member.id+'" value="'+member.id+'">'+member.nombre+'</option>');
							$('.add_socios ul').not('.socio_auth').append(li);
							members_agregados.push(member.id);
							reservacion_en_el_dia($this, false);
						});

						/* Si Hace clicl en cancelar*/
						$('.participante_dia button', $this.parent()).filter('.cancelar').click(function(){
							$('.participante_dia label span', $this.parent()).html('');
							reservacion_en_el_dia($this, false);
						});


					}
				}
			});
		}else {
			mensaje_reservation($this, 'El socio '+member.nombre+' esta en la reservacion', 'member', member);
		}
	}else { //sino se agregan validaciones para tipos miembros
		$.ajax({
			type: "GET",
			url: '/socios/'+user_id+'/validar_reservacion_socio',
			data: {
				member_id: member.id,
				date: $('#date_reservation').val(),
			},
			success: function(respuesta) {
                if (respuesta.validation === true) {
					var total = partners_agregados.length + members_agregados.length;
					if (total < 3) {
						if ($('#member_'+member.id).length <= 0) {
							$('.add_socios ul').not('.socio_auth').append(li);
							members_agregados.push(member.id);
						}else {
							mensaje_reservation($this, 'El socio '+member.nombre+' esta en la reservacion', 'member', member);
						}
					}else {
						mensaje_reservation($this,'Ya la reservacion esta completa', 'member', member);
					}
				} else {
					mensaje_reservation($this, respuesta.message, 'member', member);

				}
			}
		});
	}
	select2();
});


$('#add_partners').on('select2:select', function (evt) {
	var $this = $(this);
	var id = parseInt(evt.params.data.id);
	var partner;
	for (var i = 0; i < partners.length; i++) {
		if (partners[i].id == id) {
			partner = partners[i];
		}
	}
	 var mensaje = $('.mensaje', $('#add_partners').parent().parent());
	 var li, text;
	 li =	'<li id="partner_'+partner.id+'" exonerado="" class="'+( ((partner.exonerated === null)) ? 'select_green_fee' : '' )+'">';
	 li +=		'<input type="hidden" class="partner_id" name="partner_id[]" value="'+partner.id+'">';
	 li +=		'<div>';
	 li +=			partner.nombre;
	 li +=		'</div>';
	 li +=		'<div>';
	 li +=			'C.I: '+ number_format(partner.identity_card, 0, ',', '.');
	 li +=		'</div>';
	 li += 		'<div class="green_fee">';
	 li += 			'<select name="reservation_definition_id[]" id="" class="form-control input-md" onchange="select_green_fee($(this), '+partner.id+');">';
	 li += 			'<option value="" selected disabled>Seleccionar gren fee</option>';
					for (var igf = 0; igf < green_fees.length; igf++) {
						var green_fee = green_fees[igf];
						li += '<option value="'+green_fee.id+'">'+green_fee.name+'</option>';
					}
	 li += 			'</select>';
	 li += 			'<div class="input_error none">';
	 li += 			'	<p></p>';
	 li += 			'</div>';
	 li += 		'</div>';
	 li +=		'<div class="cerrar" onclick="cerrar_add(\'#partner_'+partner.id+'\'); return false;">';
	 li +=			'<a href="#" class="cerrar_add">';
	 li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
	 li +=			'</a>';
	 li +=		'</div>';

	 li +=	'</li>';
	//  li = $.parseHTML(li);


	if ($('.type_member').length <= 0) { //Verifico si es tipo admin

		if (members_agregados.length > 0) {
			if (partners_agregados.indexOf(partner.id) == -1) {

				var exonerated =	'<div class="check_exonerated">';
				exonerated +=		'<input type="checkbox" name="exonerated[]" value="'+partner.id+'" onclick="exonerar(\'#partner_'+partner.id+'\')">';
				exonerated +=	'</div>';
				var exo = $.parseHTML(exonerated);
				var html = $.parseHTML(li);
				$.ajax({
					type: "GET",
					url: '/socios/'+user_id+'/validar_reservacion_invitado',
					// url: 'http://dementecreativo.com/lagunita/golf/socios/'+user_id+'/validar_reservacion_invitado',
					data: {
						partner_id: partner.id,
						date: $('#date_reservation').val(),
					},
					success: function(respuesta) {
						if (respuesta.validation === true) {
							$('.participante_dia label span', $this.parent()).html('');
							reservacion_en_el_dia($this, false);


							$('.cerrar', html).prev().after(exo);
							$('.add_invitados ul').append(html);
							partners_agregados.push(partner.id);

							if (partner.exonerated === null) {
								$('.reserv .inputs .boton').addClass('pagar');
								partners_green_fee.push(partner.id);
							}else {
								$('.check_exonerated input', html).prop('checked', true);
								mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
							}

						}else {
							input_error($('#add_members'), null, false);
							$('.participante_dia label span', $this.parent()).html(respuesta.message);
							reservacion_en_el_dia($this, true);
							$('.participante_dia button', $this.parent()).off('click');
							/* Si Hace clicl en continuar*/
							$('.participante_dia button', $this.parent()).filter('.continuar').click(function(){
								$('.cerrar', html).prev().after(exo);
								$('.add_invitados ul').append(html);
								partners_agregados.push(partner.id);

								if (partner.exonerated === null) {
									$('.reserv .inputs .boton').addClass('pagar');
									partners_green_fee.push(partner.id);
								}else {
									$('.check_exonerated input', html).prop('checked', true);
									mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
								}

								reservacion_en_el_dia($this, false);
							});

							/* Si Hace clicl en cancelar*/
							$('.participante_dia button', $this.parent()).filter('.cancelar').click(function(){
								$('.participante_dia label span', $this.parent()).html('');
								reservacion_en_el_dia($this, false);
							});

							// mensaje_reservation($this, respuesta.message, partner);
						}
					}
				});

			}else {
					mensaje_reservation($this, 'El invitado '+partner.nombre+' esta en la reservacion', 'partner', partner);
			}
		}else {
			mensaje_reservation($this, 'Debes seleccionar un socio para la reservacion', 'partner', partner);
		}
	}else { //sino se agregan validaciones para tipos miembros
		$.ajax({
			type: "GET",
			url: '/socios/'+user_id+'/validar_reservacion_invitado',
			// url: 'http://dementecreativo.com/lagunita/golf/socios/'+user_id+'/validar_reservacion_invitado',
			data: {
				partner_id: partner.id,
				date: $('#date_reservation').val(),
			},
			success: function(respuesta) {
                if (respuesta.validation === true) {
					var total = partners_agregados.length + members_agregados.length;
					 if (total < 3) {
							 if (partners_agregados.length < 2) {
									 if ($('#partner_'+partner.id).length <= 0) {
											 $('.add_invitados ul').append(li);
											 partners_agregados.push(partner.id);
											if (partner.exonerated === null) {
												$('.reserv .inputs .boton').addClass('pagar');
												partners_green_fee.push(partner.id);
											}else {
												mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
											}
									 }else {
										 mensaje_reservation($this, 'El invitado '+partner.nombre+' esta en la reservacion', 'partner', partner);
									 }
							 }else {
									 mensaje_reservation($this, 'Ya hay 2 invitados en la reservacion', 'partner', partner);
							 }
					 }else {
							 mensaje_reservation($this, 'Ya la reservacion esta completa', 'partner', partner);
					 }
				}else {
					mensaje_reservation($this, respuesta.message, partner);
				}
			}
		});
	}
	select2();
});

function reservacion_en_el_dia($this, abrir) {
	var participante_dia = $('.participante_dia', $this.parent());
	if (abrir) {
		participante_dia.removeClass('none');
		setTimeout(function(){
			participante_dia.addClass('active');
		}, 10);
	}else {
		participante_dia.removeClass('active');
		setTimeout(function(){
			participante_dia.addClass('none');
		}, 500);
	}
}

$('.reserv .inputs .boton a').click(function(event){
	event.preventDefault();
	var form = $('#form_reservacion');
	var num_members = members_agregados.length;
	var num_times = $('input', $('.edit .horas_reservacion ul li .minutos ul li .icon.selected')).length;
	var estado = false;
	var green_fee = ($('.green_fee').filter('.active').length > 0) ? true : false;
	var procesar = $('.procesar', form);


	if ($('.type_member').length < 1) {
		if (num_members < 1) {
			input_error($('#add_members'), 'Debes agregar un socio', true);
		}
	}
	if (num_times < 1) {
		input_error($('.horas_reservacion ul'), 'Debes seleccionar una hora para la reserva', true);
	}

	if ($('.type_member').length < 1) {
		if (num_members > 0 && num_times > 0) {
			estado = true;
		}
	}else {
		if (num_times > 0) {
			estado = true;
		}

	}
	if (estado === true) {
		var date = $(".date_reservation").datepicker( "getDate" ).toLocaleString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' }).replace(/\//g, '-');
		var time = $('input', $('.edit .horas_reservacion ul li .minutos ul li .icon.selected')).val();
		$($('#date_reservacion').val(date));
		$($('#time_reservacion').val(time));
		transaction.date = date.split('-').reverse().join('-');
		transaction.start_time = time;
		transaction.participants_members = [];
		transaction.participants_partners = [];
		// members.forEach(function(member){
		members_agregados.forEach(function(member_id){
			function encontrar_participant(participant) {
				return participant.id === member_id;
			}
			var member_reservation = members.find(encontrar_participant);
			transaction.participants_members.push(member_reservation);
		});
		partners_agregados.forEach(function(partner_id){
			function encontrar_participant(participant) {
				return participant.id === partner_id;
			}
			var partner_reservation = partners.find(encontrar_participant);
			transaction.participants_partners.push(partner_reservation);
		});
		green_fee_amount = 0;
		green_fee_mounts.forEach(function(green_fee_mount){
			function encontrar_green_fee(green_fee) {
				return green_fee.id === green_fee_mount.id_green_fee;
			}
			var green_fee = green_fees.find(encontrar_green_fee);
			green_fee_amount += green_fee.green_fee;

		});
		transaction.amount = green_fee_amount;

		// });

		var num = 0;
		$('.add_.add_invitados ul li.select_green_fee select').each(function(index, element){
			if (num === 0) {
				if ($(this).val() === '' || $(this).val() === null) {
					input_error($(this), 'Debes seleccionar el green fee de los invitados selecionados', true);
					num++;
				}
			}
		});
		if (num === 0) {
			mostrar_procesar(transaction);
		}
	}
});
// var mostrar_procesar_num = 0;
function mostrar_procesar(transaction) {
	var li_members = li_participantes(transaction.participants_members, 1);
	var li_partners = li_participantes(transaction.participants_partners, 2);

	var start_time = new Date(transaction.date.replace(/-/g, '/')+' '+transaction.start_time);
	start_time = start_time.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
	if (start_time.split(':')[0].length < 2) {
		start_time = 0+start_time.split(':')[0]+':'+start_time.split(':')[1];
	}
	$('#fecha').html(transaction.date.split('-').reverse().join('-'));
	$('#hora').html(start_time);
	$('#transaction_id').val(transaction.id);
	$('.modal_transaccion .participants .socios ul').html(li_members);
	$('.modal_transaccion .participants .invitados ul').html(li_partners);
	$('.datepicker_transaccion').val(transaction.date.split('-').reverse().join('-'));
	$('.modal_transaccion .total span.monto').html(number_format(transaction.amount, 2, ',', '.'));
	$('#method_pago').select2();
	$('#modaldetalles').modal();
}
function this_remove(element) {
	element.remove();
}
function mostrar_minutos(date) {
	$($('#date_reservacion').val(date));
	$.ajax({
		type: "GET",
		url: (window.url_consultar_reserv) ? url_consultar_reserv : '',
		data: {date: date},
		autoSize: true,
		beforeSend: function (){
			$('.edit.reserv .form .carga').removeClass('none');
			setTimeout(function(){
				$('.edit.reserv .form').addClass('cargando');
			}, 10);
		},
		success: function(respuesta) {
			console.log(respuesta)
			var status = respuesta.response;
			var mensaje = respuesta.message;
			var code = respuesta.code;
			if (status) {
				var reservations = respuesta.reservations;
				var tournament = respuesta.tournaments[0];
                var reservation_in_day = respuesta.reservation_in_day;

				var time_draws = (respuesta.day_draws.length !== 0 && (respuesta.day_draws[0].day_draw_status === 0  )) ? respuesta.day_draws[0].time_draws : '';
				var date_format = date.split('-')[2]+'/'+date.split('-')[1]+'/'+date.split('-')[0];
				var objeto_fecha = new Date(date_format);

				var hora_inicio = new Date(date_format);
				var hora_final = new Date(date_format);
				hora_inicio.setHours(06,00,00);
				hora_final.setHours(17,00,00);
				var horas_totales = hora_final.getHours() - hora_inicio.getHours();
				li = '';
				li += '<li>';
				li += '	<div class="horas tiempo">';
				li += '		<div class="hora">';
				li += '			<h4>Salidas</h4>';
				li += '		</div>';
				li += '	</div>';
				li += '	<div class="minutos tiempo">';
				li += '		<ul>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>1era</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>2da</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>3era</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>4ta</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>5ta</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>6ta</span>';
				li += '				</div>';
				li += '			</li>';
				li += '		</ul>';
				li += '	</div>';
				li += '</li>';
				for (var i = 0; i < horas_totales+1; i++) {
					if (i !== 0) {
						hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
					}
					var hora = hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true });
					li += '<li>';
					li += '    <div class="horas tiempo">';
					li += '        <div class="hora"> <span> '+hora+' </span> </div>';
					li += '    </div>';
					li += '    <div class="minutos tiempo">';
					li += '        <ul>';
					for (var im = 0; im < 6; im++) {
						var hora_pasada = false;
						var reservacion = null;
						if (im !== 0) {
							hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
						}
						var minuto = hora_inicio;
						minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
						if (minuto.split(':')[0].length < 2) {
							minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
						}

						if (hora_inicio.getTime() < hoy.getTime()) {
							hora_pasada = true;
						}
						for (var ir = 0; ir < reservations.length; ir++) {
							var reservation = reservations[ir];
							if (reservation.start_time == minuto) {
								reservacion = reservation;
							}
						}
						var torneo = null;
						if (tournament !== undefined && tournament !== null) {
							var tournament_start_time = new Date(date_format+' '+tournament.start_time);
							var tournament_end_time = new Date(date_format+' '+tournament.end_time);
							if (hora_inicio.getTime() >= tournament_start_time.getTime() && hora_inicio.getTime() <= tournament_end_time.getTime()) {
								torneo = tournament;
							}
						}
                        var date_format2 = date.split('-').reverse().join('-');

						var sorteo = false;
						var intervalo_id = null;
						var draw_id = null;
						draws.forEach(function(draw){
							draw.day_draws.forEach(function(day_draw){
								if (date_format2 == day_draw.date) {
									draw_id = day_draw.draw_id;
									day_draw.time_draws.forEach(function(time_draw){
										var draw_start_time = new Date(date_format+' '+time_draw.start_time);
										var draw_end_time = new Date(date_format+' '+time_draw.end_time);
										if (hora_inicio.getTime() >= draw_start_time.getTime() && hora_inicio.getTime() <= draw_end_time.getTime()) {
											sorteo = true;
											intervalo_id = time_draw.id;
										}
									});
								}
							});
						});
                        var hora_bloqueada = false;
                        function encontrar_dia_bloqueado(day_blocked) {
                            return day_blocked.date === date_format2;
                        }
                        var day_blocked = day_blockeds.find(encontrar_dia_bloqueado);
                        if (day_blocked !== undefined) {
                            day_blocked.time_blockeds.forEach(function(time_blocked){
                                var start_time = new Date(date_format+' '+time_blocked.start_time);
                                var end_time = new Date(date_format+' '+time_blocked.end_time);
                                if (hora_inicio.getTime() >= start_time.getTime() && hora_inicio.getTime() <= end_time.getTime()) {
                                    hora_bloqueada = true;
                                }
                            });
                        }
						li += '            <li class="">';
                        li += '                <div class="icon '+ ( (hora_pasada || reservation_in_day) ? 'filter' : '' ) +'"> ';
						if (!reservation_in_day) {
							if (!hora_pasada) {
                                if (!hora_bloqueada) {
                                    if (torneo === null) {
                                        if ((reservacion === null || sorteo) || (reservacion.id == reservation_id)) {
                                            li += '                    <input type="checkbox" class="select_hour_reservation '+( (sorteo) ? 'intervalo intervalo_'+intervalo_id : '' )+'" name="time" value="'+minuto+'" id="'+( (sorteo) ? intervalo_id : '' )+'" draw-id="'+( (sorteo) ? draw_id : '' )+'"> ';
                                        }
                                    }
                                }
							}
						}
                        if (!hora_bloqueada) {
                            if (reservacion === null || torneo !== null) {
                                if (torneo !== null) {
                                    li += '    				   <img class="no_disponible" src="'+ public_asset +'img/icons/torneo.png" alt="" >';
                                }else{
                                    if (sorteo) {
                                        li += '                    <img class="disponible" src="'+ public_asset +'img/icons/sorteable.png" alt="" > ';
                                        li += '                    <img class="selected" src="'+ public_asset +'img/icons/selected_sorteable.png" alt="" >';
                                    }else {
                                        li += '                    <img class="disponible" src="'+ public_asset +'img/icons/disponible.png" alt="" > ';
                                        li += '                    <img class="selected" src="'+ public_asset +'img/icons/selected.png" alt="" >';
                                    }
                                }
                            }else {
                                if (sorteo) {
                                    li += '                    <img class="disponible" src="'+ public_asset +'img/icons/sorteable.png" alt="" > ';
                                    li += '                    <img class="selected" src="'+ public_asset +'img/icons/selected_sorteable.png" alt="" >';
                                }else {
                                    if ((reservacion.id == reservation_id)) {
                                        li += '                    <img class="disponible" src="'+ public_asset +'img/icons/disponible.png" alt="" > ';
                                        li += '                    <img class="selected" src="'+ public_asset +'img/icons/selected.png" alt="" >';

                                    }else {
                                        li += '				   	   <img class="no_disponible" src="'+ public_asset +'img/icons/no_disponible.png" alt="" >';
                                    }
                                }
                            }
                        }else {
                            li += '                    <img class="no_disponible" src="'+ public_asset +'img/icons/hora_bloqueada.png" alt="" style="height: 27.5px;"> ';
                        }
						li += '                    <div class="ayuda">';
						li += '                        <h6>'+minuto+'</h6>';
                        if (hora_bloqueada) {
							li += '                        <p>Hora Bloqueada</p>';
						}else if (torneo !== null) {
							li += '                        <p>'+torneo.name+'</p>';
						}else if (hora_pasada || reservacion !== null) {
							if (hora_pasada) {
								li += '                        <p>Hora no disponible</p>';
							}else if (sorteo) {
								li += '                        <p>Hora de sorteo</p>';
							}else {
								if ((reservacion.id == reservation_id)) {
									li += '                        <p>Disponible</p>';
								}else {
									li += '                        <p>Hora no disponible</p>';
								}
							}
						}else if (reservacion === null && torneo !== null) {
							li += '                        <p>'+torneo.name+'</p>';
						}else if (sorteo) {
							li += '                        <p>Hora de sorteo</p>';
						}else if (reservacion === null || (reservacion.id == reservation_id)) {
							li += '                        <p>Disponible</p>';
						}
						li += '                    </div>';
						li += '                </div>';
						li += '            </li>';
					}
					li += '        </ul>';
					li += '    </div>';
					li += '</li>';
				}
				if (reservation_in_day) {
					$('.reserv .inputs').fadeOut();
					$('.reserv .reservacion_en_el_dia').fadeIn();
				}else {
					$('.reserv .inputs').fadeIn();
					$('.reserv .reservacion_en_el_dia').fadeOut();
				}


				$('.add_ ul').not('.socio_auth').html('');
				partners_agregados = [];
				members_agregados = [];
				partners_green_fee = [];
				green_fee_mounts = [];

				if (partners_green_fee.length < 1) {
					$('.reserv .inputs .boton').removeClass('pagar');
				}else {
					$('.reserv .inputs .boton').addClass('pagar');
				}


				$('.edit .horas_reservacion ul').html(li);
                // console.log(.length);
				select_hour_reservation();
				$('.edit.reserv .form.cargando').removeClass('cargando');
				setTimeout(function(){
					$('.edit.reserv .form .carga').addClass('none');
				},700);
				// $('.procesar').addClass('active');
			}else {
				// $('.procesar').removeClass('active');
				$('.consultar .mensaje p span').removeClass('warning');
				$('.consultar .mensaje p span').html(mensaje);
				$('.consultar .mensaje').addClass('mostrar');
				setTimeout(function(){
						$('.consultar .mensaje').removeClass('mostrar');
				},5000);
			}
            select2();


            // console.log('/******************************/');

		}
	});

}
/* Datepicker Reservacion */
function date_reservation() {
	$( ".date_reservation" ).datepicker({
	    minDate: new Date(),
	    dateFormat: 'dd-mm-yy',
        defaultDate: new Date(),
	    beforeShowDay: function( date ) {
			var tipo_dia = beforeShowDay(date);
            if( tipo_dia.dias_bloqueados ) {
				 return [true, "dia_bloqueado", 'Dia Bloqueado'];
			}else if( tipo_dia.sorteable ) {
				 return [true, "sorteable", 'Dia Sorteable'];
			}else if (tipo_dia.torneo) {
				return [true, "torneo", 'Dia de torneo'];
			}else {
				return [true, '', ''];
			}
	    },
		onSelect: function(date){
			input_error($('.horas_reservacion ul'), null, false);
			input_error($('#add_members'), null, false);
			mostrar_minutos(date);
		}
	});
}



/****************************************************************************************************************************/
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/****************************************************************************************************************************/
// transaction = null;
$('.pay').click(function(e){
	e.preventDefault();
	var id = parseInt($(this).attr('id'));
	function encontrar_transaction(transaction) {
		return transaction.id === id;
	}
	transaction = transactions.find(encontrar_transaction);
	var li_members = li_participantes(transaction.participants_members, 1);
	var li_partners = li_participantes(transaction.participants_partners, 2);

	var start_time = new Date(transaction.date.replace(/-/g, '/')+' '+transaction.start_time);
	start_time = start_time.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
	if (start_time.split(':')[0].length < 2) {
		start_time = 0+start_time.split(':')[0]+':'+start_time.split(':')[1];
	}
	$('#fecha').html(transaction.date.split('-').reverse().join('-'));
	$('#hora').html(start_time);
	// $('#transaction_id').val(transaction.id);
	$('.modal_transaccion .participants .socios ul').html(li_members);
	$('.modal_transaccion .participants .invitados table tbody').html(li_partners);
    $('.modal_transaccion .participants .invitados table tbody tr td select').select2();
	$('.datepicker_transaccion').val(transaction.date.split('-').reverse().join('-'));
	$('.modal_transaccion .total span.monto').html(number_format(transaction.amount, 2, ',', '.'));
	$('.modal_transaccion .pago select').select2();
	$('#modaldetalles').modal();
});
function li_participantes(participants, tipo) {
	var participantes = '';
	if (tipo === 1) {
		participants.forEach(function(member){
			participantes += '<li class="member" id="member_'+member.id+'" participante-id="'+member.id+'">';
			participantes += '	<div class="name">';
			participantes += '	'+member.name+' '+member.last_name;
			participantes += '	</div>';
			participantes += '	<div class="clear"></div>';
			participantes += '</li>';
		});
	}else {
		participants.forEach(function(partner){
            participantes += '<tr id="'+partner.p_reservation+'" partnerid="'+partner.id+'">';
            participantes += '    <td><input type="checkbox"></td>';
            participantes += '    <td>'+partner.name+' '+partner.last_name+'</td>';
            participantes += '    <td>'+((partner.exonerated === 0) ? partner.green+' BsF' : '--' )+'</td>';
            participantes += '    <td>';
            if (partner.payed === 1) {
                participantes += '    <span>Pagado</span>';
            }else {
                participantes += '        <select class="" name="payment_method_id[]">';
                participantes += '            <option selected value="0">Metodo de pago</option>';
                payment_methods.forEach(function(payment_method){
                    participantes += '            <option value="'+payment_method.id+'">'+payment_method.name+'</option>';
                });
                participantes += '        </select>';
            }
            participantes += '    </td>';
            participantes += '</tr>';
			// participantes += '<li class="partner" id="partner_'+partner.id+'" participante-id="'+partner.id+'">';
			// participantes += '	<div class="name">';
			// participantes += '	'+partner.name+' '+partner.last_name;
			// participantes += '	</div>';
			// participantes += '	<div class="clear"></div>';
			// participantes += '</li>';
		});
	}
	return participantes;
}

$('#form_transaction').submit(function(e) {
    e.preventDefault();
    transaction.payment = [];
    // var num_0 = $('.modal_transaccion .participants .invitados table tbody tr td select').val();
    // var error = true;
    var num = $('.modal_transaccion .participants .invitados table tbody tr td select').length;
    $('.modal_transaccion .participants .invitados table tbody tr').each(function() {
        // if (parseInt($('td select', $(this)).val()) !== 0) {
        //     error = false;
        // }
        var partner_id = parseInt($(this).attr('partnerid'));
        var partner = transaction.participants_partners.find(function(partner) {
            return partner.id === partner_id;
        });
        if ($('input', $(this)).prop('checked')) {
            partner.isDelete = true;
        }else {
            delete partner.isDelete;
        }
        transaction.payment.push({
            p_reservation: parseInt($(this).attr('id')),
            method_payment: $('td select', $(this)).val()
        });
    });

    // if (!error) {
        $('.modal_transaccion .error').removeClass('active');
        $('#transaction').val(JSON.stringify(transaction));
        $('#form_transaction')[0].submit();
    // }else {
    //     $('.modal_transaccion .error').addClass('active');
    // }
});

$(document).ready(function() {
    $('#agregar_participantes select').select2();
    $('#tipo_participante').on('select2:select', function (evt) {
    	if (parseInt(evt.params.data.id) === 1) {
            var members_reservation = [];
            transaction.participants_members.forEach(function(member) {
                members_reservation.push(member.id);
            });
            // console.log(members_reservation);
            function llenar_members() {
                var option = '<option disabled selected>Selecionar socio</option>';
                members.forEach(function(member){
                    if (members_reservation.indexOf(member.id) === -1) {
                        option += '<option value="'+member.id+'">'+member.name+' '+member.last_name+'</option>';
                        // option += '<option value="'+member.id+'">'+member.name+' '+member.last_name+'</option>';
                    }
                });
                $('#participante_id').html(option);
                $('#select_participant').css('display', 'inline-block');
                $('#green_fee').css('display', 'none');
            }
			llenar_members();
    	}else {
            var partners_reservation = [];
            transaction.participants_partners.forEach(function(partner) {
                partners_reservation.push(partner.id);
            });
    		function llenar_partners() {
    			var option = '<option disabled selected>Selecionar invitado</option>';
                partners.forEach(function(partner){
                    if (partners_reservation.indexOf(partner.id) === -1) {
                        option += '<option value="'+partner.id+'">'+partner.name+' '+partner.last_name+'</option>';
                    }
                });
    			$('#participante_id').html(option);
                $('#select_participant').css('display', 'inline-block');
                $('#green_fee').css('display', 'inline-block');
    		}
			llenar_partners();
    	}
    });
});

$('#form_agregar_participante').submit(function(e) {
    e.preventDefault();
    if ($('#tipo_participante').val() === null || $('#participante_id').val() === null) {
        $('#form_agregar_participante .error').addClass('active');
    }else {
        if (parseInt($('#tipo_participante').val()) === 2 && $('#select_green_fee').val() === null) {
            $('#form_agregar_participante .error').addClass('active');
        }else {
            $('#form_agregar_participante .error').removeClass('active');
            $('#form_agregar_participante')[0].submit();
        }
    }
    // console.log($('#tipo_participante').val(), $('#participante_id').val());
});
$('.agregar_participantes').click(function(e){
	e.preventDefault();
	var id = parseInt($(this).attr('id'));
	function encontrar_transaction(transaction) {
		return transaction.id === id;
	}
	transaction = transactions.find(encontrar_transaction);
    $('#tipo_participante').html($('#tipo_participante').html());
    $('#select_participant').css('display', 'none');
    $('#green_fee').css('display', 'none');
    $('#agregar_participantes #reservation_id').val(transaction.reservation_id);

	// var li_members = li_participantes(transaction.participants_members, 1);
	// var li_partners = li_participantes(transaction.participants_partners, 2);
    //
	// var start_time = new Date(transaction.date.replace(/-/g, '/')+' '+transaction.start_time);
	// start_time = start_time.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
	// if (start_time.split(':')[0].length < 2) {
	// 	start_time = 0+start_time.split(':')[0]+':'+start_time.split(':')[1];
	// }
	// $('#fecha').html(transaction.date.split('-').reverse().join('-'));
	// $('#hora').html(start_time);
	// // $('#transaction_id').val(transaction.id);
	// $('.modal_transaccion .participants .socios ul').html(li_members);
	// $('.modal_transaccion .participants .invitados table tbody').html(li_partners);
    // $('.modal_transaccion .participants .invitados table tbody tr td select').select2();
	// $('.datepicker_transaccion').val(transaction.date.split('-').reverse().join('-'));
	// $('.modal_transaccion .total span.monto').html(number_format(transaction.amount, 2, ',', '.'));
	// $('.modal_transaccion .pago select').select2();
	$('#agregar_participantes').modal();
});

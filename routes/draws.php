<?php

//Draws

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Draws
Route::get('dias_bloqueados','DayBlockedController@index')->name('dias_bloqueados.index');

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('admin/{user_id}/sorteos', 'DrawController',
		['except' => [ 'show', 'create' ]],
		['parameters' =>['sorteos' =>'draw_id']]
	);

	Route::get('sorteos/busqueda','DrawController@search')->name('sorteos.busqueda');

	Route::get('admin/{user_id}/sorteos/historial','DrawController@history')->name('sorteos.history');

	Route::post('admin/{user_id}/sorteos/sortear_horarios','DrawController@draw')->name('sorteos.horarios');

});

<?php

// Financier

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Financier
Route::group([ 'middleware' => ['auth'], 'prefix' => 'finanzas/{user_id}'], function () {

	Route::get('listado_de_salida','FinancierController@departure_list')->name('finanzas.departure_list');

});


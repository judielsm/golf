<?php

//Cobrador

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Cobrador

Route::group(['prefix' => 'cobranza/' ], function () {
   Route::delete('/{id}', 'CollectorController@destroy')->name('cobranza.delete');
   Route::resource('/', 'CollectorController');
   Route::get('list', 'CollectorController@listPa' );
   Route::get('/{user_id}/editar/{transaction_id}', 'CollectorController@edit_reservation')->name('cobranza.editar_reservacion'); 
   Route::post('/cobranza/store', 'CollectorController@store')->name('cobranza.store');
   Route::post('/cobranza/update', 'CollectorController@update')->name('cobranza.update');
    Route::post('/cobranza_participant', 'CollectorController@add_participant')->name('cobranza.add_participant');
 


});
Route::post('/cobranza_participant', 'CollectorController@add_participant')->name('cobranza.add_participant');
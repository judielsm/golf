<?php

//Reports

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Reports

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::get('reportes/prueba','ReportController@prueba')->name('reportes.prueba');

	Route::get('reportes','ReportController@index')->name('reportes.index');

	Route::get('reportes/pdf','ReportController@pdf_report_create')->name('reportes.crear_reporte_pdf');
	
	Route::get('reportes/xls','ReportController@xls_report_create')->name('reportes.crear_reporte_xls');

});
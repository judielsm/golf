<?php

//Categories

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Categories

Route::group(['prefix' => 'torneos/{torneo_id}'], function () {

	Route::resource('categorias', 'CategoryController',
		['except' => [ 'show', 'create', 'edit' ]],
		['parameters' =>['categorias' =>'category_id']]
	);

	// 	Route::post('/almacenar','CategoryController@store')->name('categorias.almacenar');

	// 	Route::get('/{reservation_id}','CategoryController@edit')
	// 			->name('categorias.editar')->where('reservation_id', '[0-9]+');

	// 	Route::put('/{reservation_id}','CategoryController@update')
	// 			->name('categorias.actualizar')->where('reservation_id', '[0-9]+');

	// 	Route::delete('/{reservation_id}','CategoryController@delete')
	// 			->name('categorias.eliminar')->where('reservation_id', '[0-9]+');

});

Route::group(['prefix' => 'categoria/'], function(){

		Route::get('/','CategoryController@index')->name('categorias');
		Route::post('/', 'CategoryController@storeMain')->name('categorias.store');
		Route::post  ('/{id}', 'CategoryController@updateMain');
		Route::delete('/{id}', 'CategoryController@destroyMain');
	 	Route::get('/crear','CategoryController@create')->name('categorias.crear');
		Route::get('/{user_id}/{id}','CategoryController@show');
		Route::put('/{id}', 'CategoryController@changeStatus');
});
<?php

//Students

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//MonthlyPayment

	Route::group([ 'middleware' => ['auth'], 'prefix' => 'finanzas/{user_id}'], function () {
		
		Route::resource('mensualidades', 'MonthlyPaymentController',
				['except' => [ 'destroy' ]],
				['parameters' =>['mensualidades' =>'monthly_payment_id']]
				);

	});

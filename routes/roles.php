<?php

//Roles

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Roles

Route::group([ 'middleware' => ['auth'], 'prefix' => 'roles'], function () {

	Route::resource('roles', 'RoleController', 
		['except' => [ 'show', 'create' ]],
		['parameters' =>['role' =>'role_id']]
	);

	// Route::get('/','RoleController@index')->name('all_roles');

	// // confirmar uso
	// Route::get('/create','RoleController@create_role')->name('create_role');

	// // confirmar uso
	// Route::post('/','RoleController@store_role')->name('store_role');

	// // confirmar uso
	// Route::get('/{role}','RoleController@edit_role')->name('edit_role');

	// // confirmar uso
	// Route::put('/{role}','RoleController@update_role')->name('update_role');

	// // confirmar uso
	// Route::delete('/{role}','RoleController@delete_role')->name('destroy_role');

});

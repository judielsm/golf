<?php

//Tournaments

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Tournaments
Route::get('torneos/all_tournaments', 'TournamentController@all_tournaments')->name('torneos.all_tournaments');
Route::group(['middleware' => ['auth']], function () {

	Route::resource('torneos', 'TournamentController',
		['except' => [ 'create' ]],
		['parameters' =>['torneos' =>'tournament_id']]
	);

	Route::get('torneos/{tournament_id}/lista_espera', 'TournamentController@waiting_list')
		->name('torneos.waiting_list');


	Route::get('torneos/detalles/{tournament_id}','TournamentController@detalles')
		->name('torneos.detalles');

});

<?php

//Storages

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Storages

Route::group([ 'middleware' => ['auth']], function () {

	Route::resource('almacenes', 'StorageController', 
		['except' => [ 'show', 'create' ]],
		['parameters' =>['almacenes' =>'storage_id']]
	);

	// Route::get('/','StorageController@index')->name('all_storages');

	// // confirmar uso
	// Route::get('/create','StorageController@create_storage')->name('create_storage');

	// // confirmar uso
	// Route::post('/','StorageController@store_storage')->name('store_storage');

	// // confirmar uso
	// Route::get('/{storage}','StorageController@edit_storage')->name('edit_storage');

	// // confirmar uso
	// Route::put('/{storage}','StorageController@update_storage')->name('update_storage');

	// // confirmar uso
	// Route::delete('/{storage}','StorageController@delete_storage')->name('destroy_storage');

});

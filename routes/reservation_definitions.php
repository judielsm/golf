<?php

//ReservationDefinitions

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ReservationDefinitions

Route::group([ 'middleware' => ['auth']], function () {

	Route::resource('admin/{user_id}/definicion_reservas', 'ReservationDefinitionController',
		['except' => [ 'show', 'create', 'destroy' ]],
		['parameters' =>['definicion_reservas' =>'reservation_definition_id']]
	);
	Route::put('/green/{id}','ReservationDefinitionController@update_reservation_definition_status');
	Route::get('/green/{id}','ReservationDefinitionController@delete_green');
	Route::get('/green/{user}/{id}','ReservationDefinitionController@edit');

	// Route::get('/','ReservationDefinitionController@index')->name('all_reservation_definitions');

	// // confirmar uso
	// Route::get('/create','ReservationDefinitionController@create_reservation_definition')->name('create_reservation_definition');

	// // confirmar uso
	// Route::post('/','ReservationDefinitionController@store_reservation_definition')->name('store_reservation_definition');

	// // confirmar uso
	// Route::get('/{reservation_definition_id}','ReservationDefinitionController@edit_reservation_definition')
	// 		->name('edit_reservation_definition')->where('reservation_definition_id', '[0-9]+');

	// // confirmar uso
	// Route::put('/{reservation_definition_id}','ReservationDefinitionController@update_reservation_definition')
	// 		->name('update_reservation_definition')->where('reservation_definition_id', '[0-9]+');;

	// // confirmar uso
	// Route::post('/{reservation_definition_id}','ReservationDefinitionController@delete_reservation_definition')
	// 		->name('destroy_reservation_definition')->where('reservation_definition_id', '[0-9]+');;

});

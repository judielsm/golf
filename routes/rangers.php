<?php

//Starters

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Starters

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('rangers/{id}/lista_control', 'RangerController',
		['only' => [ 'index' ]],
		['parameters' => ['ranger' =>'id']]
	);

	Route::post('starters/{start_id}/cambiar_estado','StarterController@status')->name('reservaciones_del_dia.status');

	Route::get('rangers/{id}/listado_de_salida','RangerController@departure_list')->name('rangers.departure_list');
	Route::get('rangers/reservacion/{name}/','RangerController@get_by_name')->name('rangers.get_by_name');
	Route::post('rangers/comentario','RangerController@add_comment');
	Route::delete('rangers/comentario/{id}','RangerController@remove_comment');
});

<?php

//MemberReservations

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//MemberReservations

Route::group([ 'middleware' => ['auth'], 'prefix' => 'socios/{user_id}' ], function () {

	// 	Route::get('/','MemberReservationController@index')->name('all_reservations');

	Route::resource('s_reservaciones', 'MemberReservationController',
		['except' => [ 'show' ]],
		['parameters' =>['s_reservaciones' =>'reservation_id']]
	);


	Route::get('check_reservation','MemberReservationController@check_reservation')->name('s_check_reservation');

	Route::get('s_reservaciones/listado_de_salida','MemberReservationController@departure_list')->name('s_reservaciones.departure_list');


	Route::get('validar_reservacion_socio','MemberReservationController@validate_member_reservation')->name('s_reservaciones.validate_member_reservation')->where('user_id', '[0-9]+');

	Route::get('validar_reservacion_invitado','MemberReservationController@validate_partner_reservation')->name('s_reservaciones.validate_partner_reservation')->where('user_id', '[0-9]+');

	Route::get('s_reservaciones/obtener_horas_disponibles','MemberReservationController@get_reservations')->name('s_reservaciones.get_reservations')->where('user_id', '[0-9]+');
});

// Route::group(['prefix' => 'user/id/reservations'], function () {

// 	Route::get('/create','MemberReservationController@create_reservation_admin')->name('create_reservation_admin');

// 	Route::post('/','MemberReservationController@store_reservation_admin')->name('store_reservation_admin');

// });

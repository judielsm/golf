<?php

//Homes

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Homes


Route::group([ 'middleware' => ['auth'], 'prefix' => 'home'], function () {

	Route::get('/','HomeController@index')->name('home');

	Route::get('root/dashboard','HomeController@root')->name('root.dashboard');

	Route::get('administrador_reservaciones/dashboard','HomeController@administrador_reservaciones')
			->name('administrador_reservaciones.dashboard');

	Route::get('administrador_finanzas/dashboard','HomeController@administrador_finanzas')
			->name('administrador_finanzas.dashboard');

	Route::get('administrador_storage/dashboard','HomeController@administrador_storage')
			->name('administrador_storage.dashboard');

	Route::get('starter/dashboard','HomeController@starter')
			->name('starter.dashboard');

	Route::get('socios/dashboard','HomeController@socios')
			->name('socios.dashboard');

});

// Route::group([ 'middleware' => ['auth','root'], 'prefix' => 'usuarios'], function () {

// 	Route::get('/','HomeController@root')->name('root');

// });

// Route::group([ 'middleware' => ['auth','root'], 'prefix' => 'admin'], function () {

// 	Route::get('/','HomeController@admin')->name('admin');

// });

// Route::group([ 'middleware' => ['auth','root'], 'prefix' => 'admin_reservacion'], function () {

// 	Route::get('/','HomeController@admin_reservation')->name('admin_reservation');

// });

// Route::group([ 'middleware' => ['auth','root'], 'prefix' => 'admin_financiero'], function () {

// 	Route::get('/','HomeController@admin_finance')->name('admin_finance');

// });

// Route::group([ 'middleware' => ['auth','root'], 'prefix' => 'starter'], function () {

// 	Route::get('/','HomeController@starter')->name('starter');

// });

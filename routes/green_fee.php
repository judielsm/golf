<?php

//Green fee

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Green fee

Route::group(['prefix' => 'green'], function () {

	Route::resource('/', 'GreenFeeController');
});

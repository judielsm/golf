<?php

//Registrations

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Registrations

Route::group(['middleware' => ['auth'], 'prefix' => 'finanzas/{user_id}'], function () {

	Route::resource('inscripciones', 'RegistrationController', 
		['except' =>['destroy']],
		['parameters' =>['inscripciones' =>'registration_id']]
	);
});

Route::group(['middleware' => ['auth'], 'prefix' => 'escuela/{user_id}'], function () {

	Route::get('validar_estudiante','RegistrationController@validate_student')
		->name('inscripciones.validate_student');

	//reporte de inscripcion
	Route::get('inscripciones/listado','RegistrationController@list_registrations')
		->name('alumnos.list_registrations');


	//reporte de inscripcion
	Route::get('inscripciones/pdf','RegistrationController@pdf_report_create')
		->name('alumnos.pdf_report_create_registration');
	
	//reporte de inscripcion
	Route::get('inscripciones/xls','RegistrationController@xls_report_create')
		->name('alumnos.xls_report_create_registration');

});

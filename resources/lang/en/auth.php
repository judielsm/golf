<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Uno de los datos suministrados no es válido',
    'throttle' => 'Intento fallido. Intente nuevamente dentro de :seconds segundos.',

];

<style type="text/css">
h2 {
	text-align: center; 
	font-family: inherit;
	font-weight: 500;
	line-height: 1.1;
	color: inherit;
	}

	table{
		width: 100%;
		max-width: 100%;

	}
	.table{
		margin-top: 50px;
	}
	.table:after{
		margin-top: 150px;
	}

	/*ESTILOS PARA EL HEADER*/
	@page {
	 	margin-top: 5cm;
	 	margin-bottom: 2cm;
	 	margin-left: 1cm;
	 	margin-right: 1cm;
	}

	#header { 
		position: fixed; 
		left: 0px; 
		top: -180px; 
		right: 0px; 
		background-color: white; 
		text-align: center; 
	}
	#footer { 
		position: fixed; 
		left: 0px; 
		bottom: -100px; 
		right: 0px; 
		height: 100px; 
		background-color: white; 
	}
	#footer .page:after { 
		content: counter(page); 

		background-color: white; 

	}


</style>

<div id="header">
	 <table>
		<tr>
			<td></td>
			<td align="center">
				<img src="{{public_path('img/logo.png')}}" style="width: 110px;" img align=center alt="">
				<!-- <br>
				<p>Sistema automatizado para la Gerencia de Golf</p>
				<p>Listado de Estudiantes Inscritos</p> -->
			</td>
			<td></td>
		</tr>
	</table> 
</div>

<h2 class="title" align="center">
	Sistema para Alquiler de Canchas de Golf <br>
	Lagunita Country Club <br>
	Listado de reservas<br>
	<?php
		if (!empty($start_date) && !empty($end_date) ) {
			if ($start_date == $end_date) {
				echo "Fecha : ".$start_date;
			} else {
				echo "Desde : ".$start_date." Hasta : ".$end_date;
			}
		} else {
			if (!empty($start_date)) {
				echo "Desde : ".$start_date;
			}
			if (!empty($end_date)) {
				echo "Hasta : ".$end_date;
			}
		}
	?><br>
	<?php 
		if (!empty($status) && $status != "null") {
			echo "Estado : ".$status;
		}
	?>
</h2>
<table class="table table-striped table-bordered" border="1">
	<thead style='background-color: rgb(27, 60, 123); color:#ffffff; text-align: center;'>
		<tr>
			<th style="height: 30px;">Fecha</th>
			<th style="height: 30px;">Hora</th>
			<th style="height: 30px;">Estado</th>
			<th style="height: 30px;">Participantes</th>
		</tr>
	</thead>
	<tbody style="text-align: left;">
		@foreach($reservations as $reservation)
			<tr>
				<td style="height: 40px;">{{$reservation->date}}</td>
				<td style="height: 40px;">{{$reservation->start_time}}</td>
				<td style="height: 40px;">{{$reservation->status}}</td>
				<td style="height: 40px;">
					<?php 
						$invitados = '';
						foreach($reservation->members as $member){

							$invitados .= '<strong>'.$member->name.' '.$member->last_name.' C.I: '.$member->identity_card.' (S)<br></strong>';
						}

						foreach($reservation->partners as $partner){
							$invitados .= $partner->name.' '.$partner->last_name.' C.I: '.$partner->identity_card.' (I)<br>';
						}
						echo($invitados);
					?>
				</td>
			</tr>
		@endforeach       
	</tbody>
</table>

<div id="estadisticas">
	<p>
		<span>Nº de Reservaciones totales {{$totales}}</span><br>
		<span>Nº de Reservaciones en proceso {{$proceso}}</span><br>
		<span>Nº de Reservaciones sorteables {{$sorteables}}</span><br>
		<span>Nº de Reservaciones aprobadas {{$aprobadas}}</span><br>
		<span>Nº de Reservaciones canceladas {{$canceladas}}</span><br>
		<span>Nº de Reservaciones cerradas {{$cerradas}}</span><br>
		<span>Nº de Reservaciones finalizadas {{$finalizadas}}</span><br>
	</p>
</div>

<div id="footer" class="page">

	<span class="page"></span>
	
</div>


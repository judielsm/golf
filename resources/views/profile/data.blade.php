@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Actualizar Datos</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="row">
				<div class="col-md-4">
					<label class=" control-label" for="old"> Nro. Accion</label>
					<input id="name"  type="text" value="{{$member->number_action}}" class="form-control input-md" readonly="readonly">
				</div>
				<div class="col-md-4">
					<label class="control-label" for="old">Correo</label>
					<input id="name"  type="text" value="{{$member->user->email}}" class="form-control input-md" readonly="readonly">
				</div>
				<div class="col-md-4">
					<label class="control-label" for="old">Handicap</label>
					<input id="name" type="text" value="{{$member->handicap}}" class="form-control input-md" readonly="readonly">
				</div>
			</div>
			<div class="row" style="height:25px;"></div>
			<div class="form">
				<form class="form-horizontal" action="/perfil/datos" method="POST">
					{{ csrf_field() }}
					{{ method_field('POST') }}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<input id="name" name="name" type="text" value="{{ $member->name }}" placeholder="Nombre" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('last_name'))
							<div class="input_error">
								<span>{{ $errors->first('last_name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="last_name">Apellido</label>
							<div class="col-md-6">
								<input id="last_name" name="last_name" type="text" value="{{ $member->last_name }}" placeholder="Apellido" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('identity_card'))
							<div class="input_error">
								<span>{{ $errors->first('identity_card') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="identity_card">Cédula</label>
							<div class="col-md-6 inpt">
								<input id="identity_card2" name="identity_card" type="text" value="{{ $member->identity_card }}" placeholder="Cedula" class="cedula form-control input-md " required checkear="{{ route('socios.validate_identity_card_member') }}">
								<div class="chequear">
									<div class="icon check">
										<img src="{{ asset('img/icons/check.png') }}" alt="">
									</div>
									<div class="icon error">
										<img src="{{ asset('img/icons/error.png') }}" alt="">
										<div class="mensaje">
											<p>

											</p>
										</div>
									</div>
									<div class="icon cargando">
										<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
									</div>
								</div>
							</div>
						</div>

						@if ($errors->has('gender'))
						<div class="input_error">
							<span>{{ $errors->first('gender') }}</span>
						</div>
						@endif


						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Sexo</label>
							<div class="col-md-6">
								<select required name="sex" class="form-control input-md" id="sex">
									<option value="" disabled>Seleccione</option>
									<option value="1" {{ ($member->sex == 'Femenino') ? 'selected' : '' }}>Femenino</option>
									<option value="2" {{ ($member->sex == 'Masculino') ? 'selected' : '' }}>Masculino</option>
								</select>
							</div>
						</div>
						
						@if ($errors->has('phone'))
							<div class="input_error">
								<span>{{ $errors->first('phone') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="phone">Teléfono</label>
							<div class="col-md-6">
								<input id="phone" name="phone" type="text" value="{{ $member->phone }}" placeholder="Téfonono" class="form-control input-md" required>
							</div>
						</div>




						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('socios.index', array('user_id' => Auth::user()->id) )}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection

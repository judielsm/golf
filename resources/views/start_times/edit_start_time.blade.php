@extends('layouts.app')
@section('content')
<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Horario de salida</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('horario_salidas.update', $start_time->id) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<fieldset>
					@if ($errors->has('start_time'))
						<div class="input_error">
							<span>{{ $errors->first('start_time') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Hora de inicio</label>
						<div class="col-md-6">
							<input type="text" class="timepicker form-control input-md" placeholder="Hora de inicio:" name="start_time" id="start_time_startTime" date="" value="07:50 AM">
						</div>
					</div>

					@if ($errors->has('end_time'))
						<div class="input_error">
							<span>{{ $errors->first('end_time') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Hora de salida</label>
						<div class="col-md-6">
							<input type="text" class="timepicker form-control input-md" placeholder="Hora de salida:" name="end_time" id="end_time_startTime" date="" value="{{$start_time->end_time}}">
						</div>
					</div>


					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

						<a href="{{route('horario_salidas.index')}}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
 @endsection

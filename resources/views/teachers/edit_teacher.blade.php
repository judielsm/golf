@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Profesor</h3>
			</div>
			<div class="form">
				<form class="form-horizontal form_validation" action="{{ route('profesores.update', array('user_id' => Auth::user()->id, 'teacher_id' => $teacher->id ) ) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Nombre</label>
							<div class="col-md-6">
								<input id="textinput" required name="name" type="text" value="{{ $teacher->name }}" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
							</div>
						</div>

						@if ($errors->has('last_name'))
							<div class="input_error">
								<span>{{ $errors->first('last_name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Apellido</label>
							<div class="col-md-6">
								<input id="textinput" required name="last_name" type="text" value="{{ $teacher->last_name }}" placeholder="Apellido" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
							</div>
						</div>

						@if ($errors->has('phone'))
							<div class="input_error">
								<span>{{ $errors->first('phone') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Teléfono</label>
							<div class="col-md-6">
								<input id="phone" name="phone" type="text" value="{{ $teacher->phone }}" placeholder="Teléfono" class="phone form-control input-md" required title="Formato: 0414 888-88-88" pattern=".{5,14}" title="de 5 a 14 caracteres">
							</div>
						</div>

						@if ($errors->has('email'))
							<div class="input_error">
								<span>{{ $errors->first('email') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="email">Correo</label>
							<div class="col-md-6 inpt">
								<input id="email" name="email" type="email" value="{{ $teacher->email }}" placeholder="Correo" class="form-control input-md chequear_correo" checkear="{{ route('teachers.validate_email_teacher') }}" required pattern=".{3,50}" title="de 3 a 50 caracteres">
								<div class="chequear">
									<div class="icon check">
										<img src="{{ asset('img/icons/check.png') }}" alt="">
									</div>
									<div class="icon error">
										<img src="{{ asset('img/icons/error.png') }}" alt="">
										<div class="mensaje">
											<p>

											</p>
										</div>
									</div>
									<div class="icon cargando">
										<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
									</div>
								</div>
							</div>
						</div>

						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('profesores.index',array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
<script type="text/javascript">
	var teacher_id = {{ $teacher->id }};
</script>
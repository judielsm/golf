@php
use Carbon\Carbon;
$fecha = new Carbon(date('Y-m-d'));
@endphp
@extends('layouts.app')

@section('content')

<div class="container" id="starters">
	<div class="tabla_">
		<div class="titulo">
			<h1 style="text-transform: capitalize;" id="fecha">{{ $fecha->formatLocalized('%A, %d de %B del %Y') }}</h1>
			<h2 class="">Listado de Salida</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="table-responsive success tabla {{ (Auth::user()->role_id == 7) ? 'menos_2' : '' }}" style="overflow: visible;">
			<div class="form-group input_date">
				<label class="control-label" for="textinput">Buscar Participante</label>
				<div class="">
					<input type="text" class="form-control input-md" placeholder="Buscar Participantes:" name="" id="search_participant">
				</div>
			</div>
            <div class="starter">
				<ul id="orden_salida" class="listas_starter">
					<h3>Partidas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Aprobada')
								<li class="li_principal active" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="">
												<option value="" selected>{{$reservation->start_time}}</option>
											</select>
										</div>
										<a class="icon check starter_accion agregar_comentario" type="salida" reservation="{{ $reservation->id }}">
											<i class="fa fa-commenting-o" aria-hidden="true"></i>
										</a>
										<a class="icon cancel starter_accion listar_comentario" type="cancelar" reservation="{{ $reservation->id }}">
											<i class="fa fa-comments"></i>
										</a>

										<div class="clear"></div>
									</div>
									<ul class="">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<?php
												$num_participantes = count($reservation->members) +  count($reservation->partners);
												$height = ($num_participantes * 26) + 10;
												?>
												<ul class="participantes" id="{{$reservation->id}}">
													@foreach( $reservation->members as $member )
														<li id="{{$member->id}}" tipo="member">
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li id="{{$partner->id}}" tipo="partner">
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
							<?php //break; ?>
						@endforeach

					</div>
				</ul>
				<ul id="orden_finalizada" class="listas_starter">
					<h3>Salidas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Finalizada')
								<li class="li_principal" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="" >
												<option value="">{{ $reservation->start_time }}</option>
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<a class="icon check starter_accion agregar_comentario" type="salida" reservation="{{ $reservation->id }}">
											<i class="fa fa-commenting-o" aria-hidden="true"></i>
										</a>
										<a class="icon cancel starter_accion listar_comentario" type="cancelar" reservation="{{ $reservation->id }}">
											<i class="fa fa-comments"></i>
										</a>
										<div class="clear"></div>
									</div>
									<ul class="conexion">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<ul class="participantes conexion_participantes">
													@foreach( $reservation->members as $member )
														<li>
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li>
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
						@endforeach

					</div>
				</ul>
				<ul id="orden_canceladas" class="listas_starter">
					<h3>Canceladas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Cancelada')
								<li class="li_principal" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="" >
												<option value="">{{ $reservation->start_time }}</option>
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<a class="icon check starter_accion agregar_comentario" type="salida" reservation="{{ $reservation->id }}">
											<i class="fa fa-commenting-o" aria-hidden="true"></i>
										</a>
										<a class="icon cancel starter_accion listar_comentario" type="cancelar" reservation="{{ $reservation->id }}">
											<i class="fa fa-comments"></i>
										</a>
										<div class="clear"></div>
									</div>
									<ul class="conexion">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<!-- <div class="drop">
													<i class="fa fa-arrows" aria-hidden="true"></i>
												</div> -->
												<ul class="participantes conexion_participantes">
													@foreach( $reservation->members as $member )
														<li>
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li>
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
						@endforeach

					</div>
				</ul>
				<div class="clear"></div>
				<div class="mensaje" style="display: {{ (count($reservations) == 0) ? 'block' : '' }}">
					<p>{{ (count($reservations) == 0) ? 'No hay reservaciones para este dia' : '' }}</p>
				</div>
            </div>
		</div>
	</div>
</div>
<!-- <div class="agregar_comentarios active">
	<div class="form">
		<div class="inpt">
			<label for="">Comentario</label>
			<textarea name="name" rows="8" cols="80"></textarea>
		</div>
	</div>
</div> -->

<div class="modal fade" id="agregar_comentario" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Agregar Comentario</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal" action="" method="POST" id="form_agregar_comentario">
					{{ csrf_field() }}
					<input type="hidden" name="reservation_id" value="" id="reservation_id">
					<input type="hidden" name="time" value="" id="time">
					<input type="hidden" name="date" value="" id="date">
					<fieldset>
						@if ($errors->has('comment'))
							<div class="input_error">
								<span>{{ $errors->first('comment') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Comentario</label>
							<div class="col-md-6">
								<textarea name="comment"  class="form-control input-md" rows="8" cols="80" placeholder="Comentario"></textarea>
							</div>
						</div>
						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="listar_comentarios" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Comentarios</h4>
			</div>
			<div class="modal-body form">
				<fieldset>
					<ul>

					</ul>
					<div class="form-group boton">
						<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cerrar</button>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>
<div class="error_draw">
	<p>sajidhasd</p>
</div>

@endsection
@section('script')
	<script type="text/javascript">
	var reservations = {!! $reservations !!};
	</script>
	<script src="{{ asset('js/script_rangers.js') }}" charset="utf-8"></script>
@endsection

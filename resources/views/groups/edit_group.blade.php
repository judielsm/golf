@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Grupo</h3>
			</div>
			<div class="form">
				<form class="form-horizontal inhabilitar" action="{{route('grupos.update', array('user_id' => Auth::user()->id, $group->id))}}" method="POST">
					{{csrf_field()}}
					{{method_field('PUT')}}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{$errors->first('name')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<input id="name" required name="name" type="text" value="{{$group->name}}" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
							</div>
						</div>

						@if ($errors->has('teacher_id'))
							<div class="input_error">
								<span>{{$errors->first('teacher_id')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="teacher">Profesor</label>
							<div class="col-md-6">
								<select name="teacher_id" required class="form-control input-md">

                                 	 <option value="{{$group->teacher_id}}" disabled selected>Seleccione Profesor</option>
                                        @foreach ($teachers as $teacher)
                                             <option value="{{$teacher->id}}" {{($group->teacher_id == $teacher->id) ? 'selected' : ''}}>{{$teacher->name.' '.$teacher->last_name}}</option>
                                        @endforeach
                                </select>
                            </div>
						</div>



						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('grupos.index', array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection

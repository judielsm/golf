@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Comentarios reservaciones</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="table-responsive success tabla {{ (Auth::user()->role_id == 6) ? 'menos_2' : '' }} margin_none">
			<table class="table">
				<thead>
					<tr>
						<th width="100px">Fecha</th>
						<th width="120px">Hora</th>
						<th>Creado por</th>
						<th>Comentario</th>
						<th class="opciones">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($reservations as $reservation)
						@foreach($reservation->comments as $comment)
							<tr>
								<td>{{$comment->date}}</td>
								<td>{{$comment->time}}</td>
								<td>{{$comment->user->name}}</td>
								<td style="text-align: justify">{{$comment->comment}}</td>
								<td>
									<button reservation="{{$reservation->id}}" name="" type="button" class="btn btn-primary singlebutton1 listar_comentario" ruta="" style="padding-right: 10px;">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
									</button>
									<button id="{{$comment->id}}" reservation="{{$reservation->id}}" name="" type="button" class="btn btn-primary singlebutton1 edit_comment" style="padding-right: 10px;">
										<span class="glyphicon glyphicon-pencil " aria-hidden="true"></span>
									</button>
									<button id="{{$comment->id}}" reservation="{{$reservation->id}}" name="" table="1" type="button" class="btn btn-primary singlebutton1 remove_comment" style="padding-right: 10px;">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								</td>
							</tr>
						@endforeach
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@if( Auth::user()->role_id == 2 )
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar esta reservación?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
@endif
<div class="modal fade" id="listar_comentarios" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Comentarios</h4>
			</div>
			<div class="modal-body form">
				<fieldset>
					<ul>

					</ul>
					<!-- Button -->
					<div class="form-group boton">
						<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cerrar</button>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editar_comentario" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar Comentario</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal" action="{{route('comment.update', array('user_id' => Auth::user()->id))}}" method="POST" id="form_editar_comentario">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="comment_id" value="" id="comment_id">
					<fieldset>
						@if ($errors->has('comment'))
							<div class="input_error">
								<span>{{ $errors->first('comment') }}</span>a
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Comentario</label>
							<div class="col-md-6">
								<textarea name="comment" id="comment" class="form-control input-md" rows="8" cols="80" placeholder="Comentario"></textarea>
							</div>
						</div>
						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
	<script type="text/javascript">
	var reservations = {!! $reservations !!};
	</script>
	<script src="{{ asset('js/script_comments.js') }}" charset="utf-8"></script>
@endsection

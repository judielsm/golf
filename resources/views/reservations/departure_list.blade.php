@php
use Carbon\Carbon;
$fecha = new Carbon(date('Y-m-d'));
@endphp
@extends('layouts.app')

@section('content')

<div class="container" id="starters">
	<div class="tabla_">
		<div class="titulo">
			<h1 style="text-transform: capitalize;" id="fecha">{{ $fecha->formatLocalized('%A, %d de %B del %Y') }}</h1>
			<h2 class="">Listado de Salida</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif

		<div class="table-responsive success tabla {{ (Auth::user()->role_id == 6) ? 'menos_2' : '' }}" style="overflow: visible;">
			<div class="form-group input_date">
				<label class="control-label" for="textinput">Buscar Fecha</label>
				<div class="">
					<input type="text" class="date form-control input-md" placeholder="Buscar Fecha:" name="" id="date_reservaciones_del_dia" date="">
				</div>
			</div>
            <div class="starter">
				<ul id="orden_salida" class="listas_starter">
					<h3>Partidas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Aprobada')
								<li class="li_principal active" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="">
												<option value="" selected>{{$reservation->start_time}}</option>
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<div class="clear"></div>
									</div>
									<ul class="">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<?php
												$num_participantes = count($reservation->members) +  count($reservation->partners);
												$height = ($num_participantes * 26) + 10;
												?>
												<ul class="participantes" id="{{$reservation->id}}">
													@foreach( $reservation->members as $member )
														<li id="{{$member->id}}" tipo="member">
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li id="{{$partner->id}}" tipo="partner">
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
							<?php //break; ?>
						@endforeach

					</div>
				</ul>
				<ul id="orden_finalizada" class="listas_starter">
					<h3>Salidas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Finalizada')
								<li class="li_principal" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="" >
												<option value="">{{ $reservation->start_time }}</option>
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<div class="clear"></div>
									</div>
									<ul class="conexion">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<ul class="participantes conexion_participantes">
													@foreach( $reservation->members as $member )
														<li>
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li>
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
						@endforeach

					</div>
				</ul>
				<ul id="orden_canceladas" class="listas_starter">
					<h3>Canceladas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Cancelada')
								<li class="li_principal" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="" >
												<option value="">{{ $reservation->start_time }}</option>
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<div class="clear"></div>
									</div>
									<ul class="conexion">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<!-- <div class="drop">
													<i class="fa fa-arrows" aria-hidden="true"></i>
												</div> -->
												<ul class="participantes conexion_participantes">
													@foreach( $reservation->members as $member )
														<li>
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li>
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
						@endforeach

					</div>
				</ul>

				<div class="clear"></div>
				<div class="mensaje" style="display: {{ (count($reservations) == 0) ? 'block' : '' }}">
					<p>{{ (count($reservations) == 0) ? 'No hay reservaciones para este dia' : '' }}</p>
				</div>
            </div>
		</div>
	</div>

</div>

@endsection
@section('script')
	<script src="{{ asset('js/script_horario_salida.js') }}" charset="utf-8"></script>
@endsection

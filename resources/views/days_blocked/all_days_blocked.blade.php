<script type="text/javascript">
	var draws = {!! $draws !!};
	var tournaments = {!! $tournaments !!};
	var day_blockeds = {!! $day_blockeds !!};
	var day_draws = {!! $day_draws !!};
	var user_id = {{ Auth::user()->id }};

</script>
@extends('layouts.app')

@section('content')
<div class="error_draw">
	<p></p>
</div>
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Dias bloqueados</h2>
        </div>
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
        <div class="table-responsive success tabla sorteo">
            <p id="method_put">{{ method_field('PUT') }}</p>
            <form class="form-horizontal" action="{{ route('dias_bloqueados.store', array('user_id' => Auth::user()->id)) }}" method="POST" enctype="multipart/form-data" store="#" id="form_dias_bloqueados" estado="0">
				{{ csrf_field() }}
                <p id="add_method"></p>
				<!-- <p></p> -->
                <input type="hidden" name="days_blocked" value="" id="days_blocked">
                <fieldset>
                    <div class="drawing" style="">
                        <div class="divider">
                            <div class="calendar">
                                <h4 style="display: none" id="tit_calendar">Días a Sortear</h4>
                                <div class="dias_bloqueados_calendario calendario_medio"></div>
								<div class="input_error none">
									<p></p>
								</div>
                            </div>
                        </div>
                        <div class="divider diass">
							<h3>Días a bloquear</h3>
							<div class="input_error none">
								<p></p>
							</div>
                            <ul class="dias">

                            </ul>
                        </div>
                    </div>
                    <!-- Button -->
                    <div class="form-group boton" id="boton_draw" >
                        <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
						<button type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="" id="destroy" style="display: none">Eliminar</button>
                        <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal" id="cancelar_day_blocked">Cancelar</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
	<div class="modal fade" id="modalsorteo" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="confirmacion">
					<div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <h4 class="modal-title">¿Está seguro que desea realizar este sorteo?</h4>
	                </div>
					<div class="modal-body form sorteos">
						<div class="modal-body form">
                            <div class="form-group boton">
                                <button type="button" class="btn btn-primary singlebutton1" id="sortear" ruta="{{ route('sorteos.horarios', array( 'user_id' => Auth::user()->id )) }}">Aceptar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal" id="">Cancelar</button>
                            </div>
		                </div>
					</div>
				</div>
				<div class="resultado">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Sorteo</h4>
					</div>
					<div class="modal-body form sorteos">
						<div class="resultado_sorteo">
						</div>
					</div>
				</div>
				<div class="cargando">
					<h1></h1>
				</div>
			</div>
		</div>
	</div>


    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Sorteo</h4>
                </div>
                <div class="modal-body form">

                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar este día bloqueado?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
	<script src="{{ asset('js/script_dias_bloqueados.js') }}" charset="utf-8"></script>
@endsection

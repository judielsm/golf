@extends('layouts.app')
@section('content')
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Períodos</h2>
        </div>
        @if (session('validations'))
            <div class="alerta">
                <div class="alert alert-{{ session('type') }}">
                    @foreach (session('validations') as $m)
                        {{$m}}<br/>
                    @endforeach
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
        @endif
        @if (session('status'))
            <div class="alerta">
                <div class="alert alert-{{ session('type') }}">
                    {{session('status')}}
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
        @endif


        <div class="table-responsive success tabla {{ (Auth::user()->role_id == 3 ) ? 'menos_2' : '' }}">
            <div class="agregar_registro">
                <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Fecha Inicial</th>
                        <th>Fecha Final</th>
                        <th>Grupo</th>
                        <th class="opciones">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $periods as $period)
                        <tr>
                            <td>{{ $period->name }}</td>
                            <td>{{ $period->date_start }}</td>
                            <td>{{ $period->date_end }}</td>
                            <td>{{ $period->group->name }}</td>
                            <td>
                                <a href="{{ route('periodos.show', array('user_id' =>  Auth::user()->id, 'period_id' =>  $period->id)) }}" class="btn btn-primary singlebutton1 period_show" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                </a>
                                <a href="{{route('periodos.edit', array('user_id' => Auth::user()->id, 'period_id' => $period->id))}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">

                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>

    </div>

    @if(Auth::user()->role_id == 3)

        <!-- MODAL AGREGAR PERIODO -->
        <div class="modal fade" id="modalcreate" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Agregar Periodo</h4>
                    </div>
                    <div class="modal-body form">
                        <form class="form-horizontal inhabilitar" action="{{ route('periodos.store', array('user_id' => Auth::user()->id)) }}" method="POST">
                            {{ csrf_field() }}
                            <fieldset>
                                <!-- Form Name -->


                                @if ($errors->has('name'))
                                <div class="input_error">
                                    <span>{{ $errors->first('name') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Nombre</label>
                                    <div class="col-md-6">
                                        <input id="textinput" required name="name" type="text" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
                                    </div>
                                </div>

                                @if($errors->has('date_start'))
                                <div class="input_error">
                                    <span>{{ $errors->first('date_start') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Fecha Inicial
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Fecha Inicial" class="date form-control input-md datepicker_min" name="date_start" id="start_date_periods" required>
                                    </div>
                                </div>

                                @if($errors->has('date_end'))
                                <div class="input_error">
                                    <span>{{ $errors->first('date_end') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Fecha Final
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Fecha Final" class="end_datepicker_min date form-control input-md" name="date_end" id="end_date_periods" date=" " required>
                                    </div>
                                </div>

                                @if ($errors->has('group_id'))
                                <div class="input_error">
                                    <span>{{ $errors->first('group_id') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Grupo</label>
                                    <div class="col-md-6">
                                        <select required name="group_id" class="form-control input-md">
                                            <option value="" disabled selected>Seleccione Grupo</option>
                                            @foreach ($groups as $group)
                                            <option value="{{$group->id}}" >{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="form-group boton">
                                    <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                    <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="modaldelete" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">¿Seguro que desea eliminar este Periodo?</h4>
                    </div>
                    <div class="modal-body form">
                        <form class="form-horizontal inhabilitar" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <fieldset>
                                <div class="form-group boton">
                                    <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                    <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL DETALLE ESTUDIANTE-->
		<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Per&iacute;odo</h4>
				</div>
					<div class="modal-body form">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<p id="d_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Fecha Inicial</label>
								<div class="col-md-6">
									<p id="d_start_date"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Fecha Final</label>
								<div class="col-md-6">
									<p id="d_end_date"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Grupo</label>
								<div class="col-md-6">
									<p id="d_group"></p>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
    @endif

@endsection

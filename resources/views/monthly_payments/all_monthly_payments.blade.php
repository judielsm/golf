@extends('layouts.app')
@section('content')
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Mensualidades</h2>
        </div>
            @if (session('validations'))
                <div class="alerta">
                    <div class="alert alert-{{ session('type') }}">
                        @foreach (session('validations') as $m)
                            {{$m}}<br/>
                        @endforeach
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                </div>
            @endif
            @if (session('status'))
                <div class="alerta">
                    <div class="alert alert-{{ session('type') }}">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                </div>
            @endif
            <div class="table-responsive success tabla">
                <div class="agregar_registro">
                    <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                </div>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Estudiante</th>
                            <th>Socio</th>
                            <th>Monto</th>
                            <th>Periodo</th>
                            <th>Fecha de Pago</th>
                            <th class="opciones">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $monthly_payments as $monthly_payment)
                            <tr>
                                <td>{{ (isset($monthly_payment->student->name)) || (isset($monthly_payment->student->last_name))?$monthly_payment->student->name.' '.$monthly_payment->student->last_name:'-' }}</td>
                                <td>{{ (isset($monthly_payment->member->name)) || (isset($monthly_payment->member->last_name))?$monthly_payment->member->name.' '.$monthly_payment->member->last_name:'-' }}</td>
                                <td>{{ number_format($monthly_payment->amount, 2, ',', '.') }} BsF</td>
                                <td>{{ (isset($monthly_payment->period->name))?$monthly_payment->period->name:'-' }}</td>
                                <td>{{ $monthly_payment->date_payment }}</td>
                                <td>
                                    <a href="{{ route('mensualidades.show', array('user_id' =>  Auth::user()->id, 'monthly_payment_id' =>  $monthly_payment->id)) }}" class="btn btn-primary singlebutton1 monthly_payment_show" style="padding-right: 10px;">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{route('mensualidades.edit', array( 'user_id' => Auth::user()->id, 'monthly_payment_id' => $monthly_payment->id ))}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">

                            </td>
                        </tr>
                    </tfoot>
                </table>

            </div>

        </div>


        @if(Auth::user()->role_id == 3)
        <!--MODAL AGREGAR MENSUALIDAD-->
        <div class="modal fade" id="modalcreate" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Agregar Mensualidad</h4>
                    </div>
                    <div class="modal-body form">
                        <form class="form-horizontal" action="{{ route('mensualidades.store', array('user_id' => Auth::user()->id )) }}" method="POST">
                            {{ csrf_field() }}
                            <fieldset>
                                <!-- Form Name -->
                                

                                @if ($errors->has('number_receipt'))
                                    <div class="input_error">
                                        <span>{{ $errors->first('number_receipt') }}</span>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="textinput">Número de Recibo</label>
                                        <div class="col-md-6">
                                            <input id="textinput" required name="number_receipt" type="number" placeholder="Número de Recibo" class="form-control input-md">
                                        </div>
                                    </div>

                                @if ($errors->has('amount'))
                                    <div class="input_error">
                                        <span>{{ $errors->first('amount') }}</span>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Monto</label>
                                    <div class="col-md-6">
                                        <input id="textinput" pattern=".{1,9}" title="máximo 9 caracteres" name="amount" type="text" placeholder="Monto" class="form-control money input-md">
                                    </div>
                                </div>

                                @if ($errors->has('observation'))
                                    <div class="input_error">
                                        <span>{{ $errors->first('observation') }}</span>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Observación</label>
                                    <div class="col-md-6">
                                        <textarea style="resize: none;" rows="3" cols="1" id="observation" name="observation" type="text" placeholder="Observación" pattern=".{3,255}" title="de 3 a 25 caracteres" class="form-control input-md"></textarea>
                                    </div>
                                </div>

                                @if($errors->has('date_payment'))
                                <div class="input_error">
                                    <span>{{ $errors->first('date_payment') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Fecha de Pago
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text"  placeholder="Fecha de Pago" class="date form-control input-md datepicker_min" name="date_payment" id="start_date_periods" date=" " required>
                                    </div>
                                </div>

                                @if ($errors->has('method_payment'))
                                    <div class="input_error">
                                        <span>{{ $errors->first('method_payment') }}</span>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Método de Pago</label>
                                    <div class="col-md-6">
                                        <input id="textinput" name="method_payment" type="text" placeholder="Método de Pago" class="form-control input-md" pattern=".{3,50}" title="de 3 a 50 caracteres" required>
                                    </div>
                                </div>



                                @if ($errors->has('period_id'))
                                <div class="input_error">
                                    <span>{{ $errors->first('period_id') }}</span>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Período</label>
                                    <div class="col-md-6">
                                        <select name="period_id" class="form-control input-md" required>
                                            <option value="" disabled selected>Seleccione Período</option>
                                                @foreach ($periods as $period)
                                                    <option value="{{$period->id}}" >{{$period->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                @if ($errors->has('student_id'))
                                <div class="input_error">
                                    <span>{{ $errors->first('student_id') }}</span>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Estudiante</label>
                                    <div class="col-md-6">
                                        <select name="student_id" class="form-control input-md" required>
                                            <option value="" disabled selected>Seleccione Estudiante</option>
                                                @foreach ($students as $student)
                                                    <option value="{{$student->id}}" >{{$student->name.' '.$student->last_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                @if ($errors->has('member_id'))
                                <div class="input_error">
                                    <span>{{ $errors->first('member_id') }}</span>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Socio</label>
                                    <div class="col-md-6">
                                        <select name="member_id" class="form-control input-md" required>
                                            <option value="" disabled selected>Seleccione Socio</option>
                                                @foreach ($members as $member)
                                                    <option value="{{$member->id}}" >{{$member->name.' '.$member->last_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>


                                <!-- Button -->
                                <div class="form-group boton">
                                    <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                    <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle de Per&iacute;odo</h4>
                </div>
                    <div class="modal-body form">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Número de Recibo</label>
                                <div class="col-md-6">
                                    <p id="d_number_receipt"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Monto</label>
                                <div class="col-md-6">
                                    <p id="d_amount"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Observación</label>
                                <div class="col-md-6">
                                    <p id="d_observation"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Fecha de Pago</label>
                                <div class="col-md-6">
                                    <p id="d_date_payment"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Método de Pago</label>
                                <div class="col-md-6">
                                    <p id="d_method_payment"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Período</label>
                                <div class="col-md-6">
                                    <p id="d_period"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Estudiante</label>
                                <div class="col-md-6">
                                    <p id="d_student"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Socio</label>
                                <div class="col-md-6">
                                    <p id="d_member"></p>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
@endsection

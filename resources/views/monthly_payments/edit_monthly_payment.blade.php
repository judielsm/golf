@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Mensualidad</h3>
			</div>
			<div class="form">
				<form class="form-horizontal form_validation" action="{{ route('mensualidades.update', array('user_id' => Auth::user()->id, 'monthly_payment_id' => $monthly_payment->id ) ) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('number_receipt'))
							<div class="input_error">
								<span>{{ $errors->first('number_receipt') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Número de Recibo</label>
							<div class="col-md-6">
								<input id="textinput" name="number_receipt" type="number" value="{{ $monthly_payment->number_receipt}}" placeholder="Número de Recibo" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('amount'))
							<div class="input_error">
								<span>{{ $errors->first('amount') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Monto</label>
							<div class="col-md-6">
								<input id="textinput" pattern=".{1,9}" title="máximo 9 caracteres" name="amount" type="text" value="{{ $monthly_payment->amount }}" placeholder="Monto" class="form-control money input-md" required> 
							</div>
						</div>

						@if ($errors->has('observation'))
							<div class="input_error">
								<span>{{ $errors->first('observation') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Observacion</label>
							<div class="col-md-6">
								<textarea style="resize: none;" rows="3" cols="1" id="observation" name="observation" type="text" placeholder="Observación" pattern=".{3,255}" title="de 3 a 25 caracteres" class="form-control input-md">{{ $monthly_payment->observation }}</textarea>
							</div>
						</div>

						@if ($errors->has('date_payment'))
							<div class="input_error">
								<span>{{ $errors->first('date_payment') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Fecha de Pago</label>
							<div class="col-md-6">
								<input type="text" placeholder="Fecha de Pago" class="date form-control input-md datepicker_min" name="date_payment" value="{{$monthly_payment->date_payment}}" id="start_date_periods" required>
							</div>
						</div>

						@if ($errors->has('method_payment'))
							<div class="input_error">
								<span>{{ $errors->first('method_payment') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Método de Pago</label>
							<div class="col-md-6">
                                <input id="textinput" name="method_payment" type="text" value="{{ $monthly_payment->method_payment }}" placeholder="Método de Pago"  class="form-control input-md" pattern=".{3,50}" title="de 3 a 50 caracteres" required>
							</div>
						</div>

						@if ($errors->has('period_id'))
							<div class="input_error">
								<span>{{ $errors->first('period_id') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Período</label>
							<div class="col-md-6">
								<select name="period_id" class="form-control input-md" required>
                                 	 <option value="{{ $monthly_payment->period_id}}" disabled selected>Seleccione Período</option>
                                        @foreach ($periods as $period)
                                             <option value="{{$period->id}}" {{ ($monthly_payment->period_id == $period->id) ? 'selected' : '' }}>{{$period->name}}</option>
                                        @endforeach
                                </select>
                            </div>
						</div>

						@if ($errors->has('student_id'))
                            <div class="input_error">
                                <span>{{ $errors->first('student_id') }}</span>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Estudiante</label>
                            <div class="col-md-6">
                                <select name="student_id" class="form-control input-md" required>
                                    <option value="{{ $monthly_payment->student_id}}" disabled selected>Seleccione Estudiante</option>
                                        @foreach ($students as $student)
            								<option value="{{$student->id}}" {{ ($monthly_payment->student_id == $student->id) ? 'selected' : '' }}>{{$student->name.' '.$student->last_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

						@if ($errors->has('member_id'))
                            <div class="input_error">
                                <span>{{ $errors->first('member_id') }}</span>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Socio</label>
                            <div class="col-md-6">
                                <select name="member_id" class="form-control input-md" required>
                                    <option value="{{ $monthly_payment->member_id}}" disabled selected>Seleccione Socio</option>
                                        @foreach ($members as $member)
            								<option value="{{$member->id}}" {{ ($monthly_payment->member_id == $member->id) ? 'selected' : '' }}>{{$member->name.' '.$member->last_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>


						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
							<a href="{{route('mensualidades.index', array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection

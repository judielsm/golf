@extends('layouts.app')
@section('content')
<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Casillero</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('casilleros.update', array('user_id' => Auth::user()->id,'locker_id' => $locker->id)) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<fieldset>
                    @if ($errors->has('name'))
                        <div class="input_error">
                            <span>{{ $errors->first('name') }}</span>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Nombre</label>
                        <div class="col-md-6">
                            <input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" value="{{$locker->name}}">
                        </div>
                    </div>

					@if ($errors->has('status'))
					<div class="input_error">
						<span>{{ $errors->first('status') }}</span>
					</div>
					@endif

					<div class="form-group">
						<label class="col-md-4 control-label" for="status">Estado</label>
						<div class="col-md-6">
							<select required name="status" class="form-control input-md" required>
								<option value="Disponible" @if($locker->status == 'Disponible') {{ 'selected' }} @endif >Disponible</option>
								<option value="Prestamo" @if($locker->status == 'Prestamo') {{ 'selected' }} @endif >Prestamo</option>
								<option value="Indisponible" @if($locker->status == 'Indisponible') {{ 'selected' }} @endif >Indisponible</option>
							</select>
						</div>
					</div>
					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

						<a href="{{route('casilleros.index', array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
 @endsection

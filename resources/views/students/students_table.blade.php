<style type="text/css">

.header{
	width: 100%;
}
.header h2{
	margin: 0;
}
.fechas{
	width: 100%;
	/*background: rgb(69, 224, 45);*/
	text-align: center;
	position: relative;
}
.fecha{
	display: inline-block;
	font-size: 18px;
	margin-right: 15px;
	position: relative;
	/*left: 50%;*/
	/*background: rgb(142, 246, 121);*/
}
.fecha:nth-child(2){
	margin: 0 15px;
}

.table_reportes{
	width: 100%;
	border-spacing: 0px;
	border: none;
}
.table_reportes thead tr th,
.table_reportes tbody tr td{
	border: none;
	border-collapse: none;
}
.table_reportes thead tr th{
	border-left: solid 1px #fff;
	padding-left: 10px;
	text-align: left;
	border-top: solid 1px #000;

}
.table_reportes thead tr th.th_fecha{
	width: 120px;
}

.table_reportes thead tr th.th_hora{
	width: 100px;
}
.table_reportes thead tr th.th_estado{
	width: 140px;
}
.table_reportes thead tr th:first-child{
	border-left: solid 1px #000;
}
.table_reportes thead tr th:last-child{
	border-right: solid 1px #000;
}

.table_reportes tbody tr{
	border-bottom: solid 1px #000;
}
.table_reportes tbody tr td{
	border-left: solid 1px #000;
	padding: 5px 0 5px 10px;
    border-bottom: 1px solid #000;
}
.table_reportes tbody tr td:last-child{
	border-right: solid 1px #000;
}
.table_reportes tbody tr td {
	/*border-left: solid 1px #000;*/
}

#estadisticas{
	position: absolute;
	bottom: 0;
}
#footer {
	position: fixed;
	left: 0px;
	bottom: -100px;
	right: 0px;
	height: 100px;
	background-color: white;
	text-align: left;
}
#footer .page:after {
	content: counter(page);

	background-color: white;

}
@page {
	margin-top: 2cm;
	margin-bottom: 2cm;
	margin-left: 1cm;
	margin-right: 1cm;
}
</style>

<table class="header">
	<tr>
		<td>
			<h2>
				Sistema para Alquiler de Canchas de Golf <br>
				Lagunita Country Club <br>
				Listado de Alumnos<br>
			</h2>
		</td>
		<td align="center">
			<img src="{{public_path('img/logo.png')}}" style="width: 110px;" img align=center alt="">
		</td>
	</tr>
</table>

<table class="table table-striped table-bordered table_reportes" border="1">
	<thead style='background-color: rgb(27, 60, 123); color:#ffffff; text-align: center;'>
		<tr>
			<th style="height: 30px;">ESTUDIANTES</th>
			<th style="height: 30px;">TEL&Eacute;FONOS</th>
			<th style="height: 30px;">CUMPLEANOS</th>
			<th style="height: 30px;">SOCIOS</th>
		</tr>
	</thead>
	<tbody style="text-align: left;">
		@foreach($students as $student)
			<tr>
				<td style="height: 40px;">
					<?php
						$alumno = '';
						if(isset($student->name)){

							$alumno .= '<strong>'.strtoupper($student->name).' '.strtoupper($student->last_name).'<br> C.I: '.number_format($student->identity_card, 0,',','.').'<br></strong>';
						}
						echo($alumno);
					?>
				</td>
				<td style="height: 40px;">
					<?php
						$telefono = '';
						if(isset($student->phone)){

							$telefono .= '<strong>'.strtoupper($student->phone).'</strong>';
						}
						echo($telefono);
					?>
				</td>
				<td style="height: 40px;">
					<?php
						$cumpleanos = '';
						if(isset($student->birthdate)){

							$cumpleanos .= '<strong>'.strtoupper($student->birthdate).'</strong>';
						}
						echo($cumpleanos);
					?>
				</td>
				<td style="height: 40px;">
					<?php
						$socio = '';
						if(isset($student->member->name) || isset($student->member->last_name)){

							$socio .= '<strong>'.strtoupper($student->member->name.' '.$student->member->last_name).'<br> C.I: '.number_format($student->member->identity_card, 0,',','.').'<br></strong>';
						}
						echo($socio);
					?>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

<div id="footer" class="page">

	<span class="page"></span>

</div>

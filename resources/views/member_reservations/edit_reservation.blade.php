<script type="text/javascript">
	var draws = {!! $draws !!};
	var tournaments = {!! $tournaments !!};
	var user_id = {{ Auth::user()->id }};
	var member_id = {{ Auth::user()->member->id }}
	var role_id = {{ Auth::user()->role_id }};

	var url_consultar_reserv = '{{route('s_check_reservation', array( 'user_id', Auth::user()->id ) )}}';
	var green_fees = {!! $reservation_definition !!};
	var day_blockeds = {!! $day_blockeds !!};
	var reservation_edit = {!! $reservation !!};
	// reservation_edit
	// console.log(reservation);


</script>
@extends('layouts.app')

@section('content')

<!-- {{route('check_reservation', array( 'user_id', Auth::user()->id ) )}} -->
<div class="container type_member" id="reservacion">
	<div class="edit reserv">
		<div class="form">
			<div class="modal-header">
				<h3 class="modal-title">Editar Reservación </h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="carga none">
				<div class="icon">
					<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
				</div>
			</div>
			<form class="form-horizontal" method="POST" action="{{ route('s_reservaciones.update', array( 'user_id' => Auth::user()->id, 'reservation_id' => $reservation->id ) ) }}" id="form_reservacion">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<fieldset>
                    @if ($errors->has('receipt_payment'))
					<div class="input_error">
						<span>{{ $errors->first('receipt_payment') }}</span>
					</div>
					@endif
					<div class="form-group consultar divider">
						<div class="fecha_reservacion" >
							<input type="text" name="date" value="" id="date_reservacion" class="input_tiempo_reserv" required>
                            <div class="date_reservation" id="date_reservation" date="">
							</div>
							<div class="leyenda">
								<div class="item torneo">
									<div class="icon"></div>
									<div class="text">
										<p>
											Dia de torneo
										</p>
									</div>
								</div>
								<div class="item sorteo">
									<div class="icon"></div>
									<div class="text">
										<p>
											Dia de sorteos
										</p>
									</div>
								</div>
								<div class="item bloqueado">
									<div class="icon"></div>
									<div class="text">
										<p>
											Dia bloqueado
										</p>
									</div>
								</div>

							</div>
						</div>
						<div class="mensaje">
							<p>
								<span>

								</span>
							</p>

						</div>
						<div class="inputs">
							<h2>Participantes</h2>

							<div class="add_socios add_">
								<div class="inpt">
									<!-- <label class="" for="receipt_payment">Buscar Socios</label> -->
									<select class="js-example-basic-single select2 form-control input-md" id="add_members">
										<option value="" disabled selected>Selecionar Socio</option>
									</select>
									<div class="mensaje"></div>
									<div class="input_error none">
										<p></p>
									</div>
								</div>
								<ul class="socio_auth">
									<li id="" class="member_reservation">
										<!-- <input type="hidden" class="member_id" name="member_id[]" value="2"> -->
										<div class="nombre">{{ Auth::user()->member->name }} {{ Auth::user()->member->last_name }}</div>
										<div>N° Accion: {{ Auth::user()->member->number_action }}</div>
										<!-- <div class="cerrar" onclick="cerrar_add('#member_2'); return false;">
											<a href="#" class="cerrar_add">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
											</a>
										</div> -->
									</li>
								</ul>
								<ul class="">
								</ul>
							</div>
							<div class="add_invitados add_">
								<div class="inpt">
									<!-- <label class="" for="receipt_payment">Buscar Invitados</label> -->
									<div class="" >
										<select class="js-example-basic-single select2 form-control input-md" id="add_partners">
											<option value="" disabled selected>Selecionar Invitado</option>
										</select>
									</div>
									<div class="mensaje">
									</div>
								</div>

								<ul class="">
								</ul>
							</div>
							<div class="boton">
								<div class="avanzar division">
									<div class="item">
										<a href="#">
											<button type="button" class="btn btn-primary singlebutton1">Procesar</button>
										</a>
									</div>
								</div>
								<div class="tipo_pago division">
									<div class="sub_titulo">
										<label for="">Seleciones tipo de pago</label>
									</div>
									@foreach( $type_payments as $type_payment )
									<div class="item">
										<a href="#" tipo-pago="{{$type_payment->name}}-{{$type_payment->id}}" title="Jaja">
											<img src="{{asset('img/icons/'.$type_payment->name.'.png')}}" alt="">
										</a>
										<span style="display: block;">{{$type_payment->label}}</span>
									</div>
									@endforeach
								</div>
							</div>

						</div>
						<div class="reservacion_en_el_dia">
							<p>Ya tienes una reservación para este día</p>
						</div>
					</div>


					<div class="divider">
						<input type="text" name="start_time" value="" id="time_reservacion" class="input_tiempo_reserv">
						<div class="muestra interaction">
							<div class="horas_reservacion">
								<ul>
								</ul>
								<div class="input_error none">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
					</div>


				</fieldset>
				<div class="procesar none">
					<div class="form">
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

@endsection
<!-- Seccion De Scripts -->
@section('script')
<script src="{{asset('js/script_reservations.js')}}"></script>
@endsection

@php
use Carbon\Carbon;
@endphp
@extends('layouts.app')

@section('content')

<div class="container vista_home">
	<div class="inicio">
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif

		<div class="clime">
			<!-- Via javascript se carga los datos del clima -->
		</div>
		@if (count($tournaments) > 0)
			<ul class="tournaments">
				<li class="titulo">
					<h3>Próximos Torneos</h3>
				</li>
				<?php $num = 0; ?>
				@foreach( $tournaments as $tournament )

				<?php $num++;
							$start_date = Carbon::createFromTimeStamp(strtotime($tournament->start_date));
							$end_date = Carbon::createFromTimeStamp(strtotime($tournament->end_date));
							$start_date_inscription = Carbon::createFromTimeStamp(strtotime($tournament->start_date_inscription));
							$end_date_inscription = Carbon::createFromTimeStamp(strtotime($tournament->end_date_inscription));
				?>
					@if($num < 6)
						<li class="">
							{{ $tournament->name }} -- {{ $start_date->formatLocalized('%A, %d de %B') }}

							<button type="button" name="button" class="{{ ($day->between($start_date_inscription, $end_date_inscription, true) and Auth::user()->role_id == 6) ? 'active' : ''  }} btn btn-primary singlebutton1 ver_detalle_torneo" item="torneo_{{ $tournament->id }}"><span class="text">Ver detalles</span></button>
						</li>
					@endif

				@endforeach
			</ul>
		@endif
		<div class="cancha_estado {{ ($golf_course->status != 1) ? 'closed' : ''  }}">
			<div class="text">
				@if( Auth::user()->role_id != 2 ) <!-- Validamos el status de la cancha para mostrar la imagen -->
					@if( $golf_course->status == 0 )
						<div class="statusCancha">
							<img class="noDisponibleCancha " src="{{asset('img/icons/nocancha.png')}}" alt="No hay disponibilidad de Cancha" title="Disponibilidad de Canchas">
						</div>
					@endif
					@if( $golf_course->cars_status == 0 )
						<div class="statusCarrito">
							<img class="noDisponibleCarrito" src="{{asset('img/icons/nocarrito.png')}}" alt="" title="Disponibilidad de Cancha">
						</div>
					@endif

				@endif
			</div>

			@if( Auth::user()->role_id == 2 ) <!-- Cambia el status de la info del home si es admin -->
				@if($golf_course->status == 1)
					<div class="statusCancha">
						<img id="img_estado_cancha" src="{{asset('img/icons/cancha.png')}}" alt="Hay Disponibilidad de Cancha" title="Disponibilidad de Cancha">
					</div>
				@else
					<div class="statusCancha">
						<img id="img_estado_cancha" src="{{asset('img/icons/nocancha.png')}}" alt="No hay disponibilidad de Canchas" title="Disponibilidad de Cancha">
					</div>
				@endif
				@if($golf_course->cars_status == 1)
					<div class="statusCarrito">
						<img id="img_estado_carrito" src="{{asset('img/icons/carrito.png')}}" alt="Hay disponibilidad de Carritos" data-param="cars" title="Disponibilidad de Carritos">
					</div>
				@else
					<div class="statusCarrito">
						<img id="img_estado_carrito" src="{{asset('img/icons/nocarrito.png')}}" alt="No hay disponibilidad de Carritos" data-param="cars" title="Disponibilidad de Carritos">
					</div>
				@endif
			@endif
		</div>
	</div>
</div>

<div class="home_torneos none" id="home_torneos">
	<div class="container">
		<div class="owl-carousel owl-theme">
			@foreach( $tournaments as $tournament )

			<?php
						$start_date = Carbon::createFromTimeStamp(strtotime($tournament->start_date));
						$end_date = Carbon::createFromTimeStamp(strtotime($tournament->end_date));
						$start_date_inscription = Carbon::createFromTimeStamp(strtotime($tournament->start_date_inscription));
						$end_date_inscription = Carbon::createFromTimeStamp(strtotime($tournament->end_date_inscription));
			 ?>
				<div class="item" id="torneo_{{ $tournament->id }}">

					<div class="division datos">
						<div class="datos_torneo">
							<div class="titulo">
								<h1>
									{{ $tournament->name }}
								</h1>
							</div>
							<div class="inscripcion">
								<h3>Inscripciones</h3>
								@if(($day->between($start_date_inscription, $end_date_inscription, true) and Auth::user()->role_id == 6))
									@if($tournament->inscripcion == 0)
										<div class="boton">
											<!-- <a href="#" class="btn btn-primary singlebutton1"> -->
											<a href="{{ route('inscripcion_torneos.create', array('tournament_id' => $tournament->id) ) }}" class="btn btn-primary singlebutton1">
												<span class="icon">
													<img src="{{ asset('img/icons/inscribirse.png') }}" alt="incribirse">
												</span>
												<span class="text">
													Inscribirse
												</span>
											</a>
										</div>
									@else
										<h1 style="color: green;">¡YA ESTAS INSCRITO!</h1>

									@endif
								@else
									<div class="fechas">
										@if($start_date_inscription == $end_date_inscription)
											Fecha de inscripcion: {{ $start_date_inscription->formatLocalized('%A, %d de %B del %Y') }}
										@else
											<h4>Desde: {{ $start_date_inscription->formatLocalized('%A, %d de %B del %Y') }}</h4>
											<h4>Hasta: {{ $end_date_inscription->formatLocalized('%A, %d de %B del %Y') }}</h4>
										@endif
									</div>
								@endif
								<div class="mod_cat">

									<div >
										<h3>Categorías</h3>
										@foreach($tournament->categories as $category)
										<h4>
											{{$category->name}}
										</h4>
										@endforeach
									</div>
								</div>
								<div class="fechas">
									@if($start_date == $end_date)
										<h3>Fecha del torneo</h3>
										<h4>{{ $start_date->formatLocalized('%A, %d de %B del %Y') }}</h4>
									@else
										<h3>Fechas del torneo</h3>
										<h4>Desde: {{ $start_date->formatLocalized('%A, %d de %B del %Y') }}</h4>
										<h4>Hasta: {{ $end_date->formatLocalized('%A, %d de %B del %Y') }}</h4>
									@endif
								</div>
							</div>
						</div>
					</div>
					<div class="division condiciones">
						<input id="condicion_torneo" type="file" class="file condicion_torneo" name="conditions_file" value="{{asset(''.$tournament->conditions_file.'')}}">
					</div>

				</div>
			@endforeach
		</div>
	</div>
	<div class="cerrar_home_torneos">
		<i class="glyphicon glyphicon-remove"></i>
	</div>
</div>
@endsection

@section('script')
	<script src="{{ asset('js/script_home.js') }}" charset="utf-8"></script>
@endsection

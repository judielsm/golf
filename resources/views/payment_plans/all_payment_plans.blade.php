@extends('layouts.app')
@section('content')
<style media="screen">
	table th:first-child {
		min-width: 250px;
	}
	table th:nth-last-child(3){
		min-width: 200px;
	}
</style>
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Planes de Pagos</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<!-- <div class="tabla_head">
			<div class="tabla_divider add">
				@if(Auth::user()->role_id == 3)
					<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
				@endif
			</div>
			<div class="tabla_divider search">
				<div class="buscar"></div>
			</div>
		</div> -->
		<div class="table-responsive success tabla {{(Auth::user()->role_id == 3) ? 'menos_2' : ''}}">
			<div class="agregar_registro">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			</div>

			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th class="descripcion">Descripción</th>
						<th class="th_tarifa">Tarifa</th>
						<th class="opciones">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $payment_plans as $payment_plan)
						<tr>
							<td>{{ $payment_plan->name }}</td>
							<td class="descripcion">{{ $payment_plan->description }}</td>
							<td>{{ number_format($payment_plan->rate, 2, ',', '.') }} BsF</td>
							<td>

								<a href="{{ route('planes.show', array('user_id' =>  Auth::user()->id, 'payment_plan_id' =>  $payment_plan->id)) }}" class="btn btn-primary singlebutton1 payment_plan_show" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
								<a href="{{ route('planes.edit', array('user_id' => Auth::user()->id, 'payment_plan_id' => $payment_plan->id)) }}" class="btn btn-primary singlebutton1">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('planes.destroy', array('user_id' => Auth::user()->id, 'payment_plan_id' => $payment_plan->id))}}" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4">

						</td>
					</tr>
				</tfoot>

			</table>

		</div>

	</div>

	@if(Auth::user()->role_id == 3)

<!--MODAL AGREGAR PAGO-->
	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Plan Pago</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" action="{{route('planes.store', array('user_id' => Auth::user()->id))}}" method="POST">
						{{csrf_field()}}
						<fieldset>
							<!-- Form Name -->
							

								@if ($errors->has('name'))
								<div class="input_error">
									<span>{{$errors->first('name')}}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Nombre</label>
									<div class="col-md-6">
										<input id="name" name="name" type="text" placeholder="Nombre" pattern=".{3,50}" title="de 3 a 50 caracteres" class="form-control input-md" required>
									</div>
								</div>

								@if ($errors->has('description'))
								<div class="input_error">
									<span>{{$errors->first('description')}}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="description">Descripción</label>
									<div class="col-md-6">
										<textarea style="resize: none;" rows="3" cols="1" id="description" name="description" type="text" placeholder="Descripción" class="form-control input-md" pattern=".{3,500}" title="de 3 a 500 caracteres" required></textarea>
									</div>
								</div>

								@if ($errors->has('rate'))
								<div class="input_error">
									<span>{{$errors->first('rate')}}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="rate">Tarifa</label>
									<div class="col-md-6">
										<input id="rate" pattern=".{1,9}" title="máximo 9 caracteres" name="rate" type="text" placeholder="Tarifa" class="form-control input-md money" required>
									</div>
								</div>

								<!-- Button -->
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>


		<!--MODAL PARA ELIMINAR-->
		<div class="modal" id="modaldelete" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">¿Seguro que desea eliminar este Plan?</h4>
					</div>
					<div class="modal-body form">
						<form class="form-horizontal" method="POST">
							{{csrf_field()}}
							{{method_field('DELETE')}}
							<fieldset>
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Plan de Pago</h4>
				</div>
				<div class="modal-body form">
					<fieldset>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<p id="d_name"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Descripción</label>
							<div class="col-md-6">
								<p id="d_description"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Tarifa</label>
							<div class="col-md-6">
								<p id="d_rate"></p>
							</div>
						</div>
					</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	@endif

@endsection

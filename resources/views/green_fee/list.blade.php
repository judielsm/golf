@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Green Fee</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
    <div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
		</div>
		<div class="table-responsive success tabla menos_1">
			{!! $table_green_fees->render() !!}
		</div>
	</div>

</div>

<div class="modal fade" id="modalcreate" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Agregar Green Fee</h4>
            </div>
            <div class="modal-body form">
                <form class="form-horizontal" action="/green" method="POST">
                    {{ csrf_field() }}
                    <fieldset>
                        <!-- Form Name -->
                        
                        @if ($errors->has('club'))
                            <div class="input_error">
                                <span>{{ $errors->first('club') }}</span>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Club</label>
                            <div class="col-md-6">
                                <select class="form-control" name="club" required="required">
                                    <option value="">Seleccione</option>
                                  @foreach ($clubs as $club)
                                    <option value="{{$club->id}}">{{$club->name}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                        @if ($errors->has('description'))
                            <div class="input_error">
                                <span>{{ $errors->first('description') }}</span>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Descripcion</label>
                            <div class="col-md-6">
                                <input id="textinput" name="name" type="text" placeholder="Nombre" class="form-control input-md" required="required">
                            </div>
                        </div>

                        @if ($errors->has('green_fee'))
                            <div class="input_error">
                                <span>{{ $errors->first('green_fee') }}</span>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Monto</label>
                            <div class="col-md-6">
                                <input id="textinput" name="green_fee" type="text" placeholder="Monto" class="form-control input-md" required="required" >
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group boton">
                            <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                            <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modaldelete" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">¿Seguro que desea eliminar esta reservacion?</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<fieldset>
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Compañía</h3>
			</div>
			<div class="form">
				<form class="form-horizontal inhabilitar" action="{{ route('update_company', $company->id) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Nombre</label>
							<div class="col-md-6">
								<input id="textinput" required name="name" type="text" value="{{ $company->name }}" placeholder="Nombre" class="form-control input-md">
							</div>
						</div>

						@if ($errors->has('phone'))
							<div class="input_error">
								<span>{{ $errors->first('phone') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Teléfono</label>
							<div class="col-md-6">
								<input id="textinput" required name="phone" type="text" value="{{ $company->phone }}" placeholder="Teléfono"  class="form-control input-md phone" min="11">
							</div>
						</div>

						@if ($errors->has('rif'))
							<div class="input_error">
								<span>{{ $errors->first('rif') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Rif</label>
							<div class="col-md-6">
								<input id="textinput" required name="rif" type="text" value="{{ $company->rif }}"  min="7" class="form-control input-md cedula">
							</div>
						</div>


						@if ($errors->has('address'))
							<div class="input_error">
								<span>{{ $errors->first('address') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Dirección</label>
							<div class="col-md-6">
								<textarea required name="address" class="form-control input-md">{{$company->address}}</textarea>
							</div>
						</div>

						
						@if ($errors->has('tax'))
							<div class="input_error">
								<span>{{ $errors->first('tax') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Impuesto</label>
							<div class="col-md-6">
								<input id="textinput" required name="tax" type="text" value="{{ $company->tax }}" placeholder="Impuesto" class="form-control input-md">
							</div>
						</div>


						<!-- Button -->
						<div class="form-group boton">
							<button id="" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('all_company')}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection

@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Socios</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<!-- <div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
		</div> -->
		<div class="table-responsive success tabla">
			<div class="agregar_registro">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Cédula</th>
						<th>Nº acción</th>
						<th>Teléfono</th>
						<th class="opciones">Opciones</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $members as $member)
						@if(isset($member->user))
							@if($member->user->is_subscripted)
								<tr>
										<td>{{ $member->name }}</td>
										<td>{{ $member->last_name }}</td>
										<td>{{ $member->identity_card }}</td>
										<td>{{ $member->number_action }}</td>
										<td>{{ $member->phone }}</td>
										<td>
											<a href="{{route('socios.show', array('user_id' =>  Auth::user()->id, 'member_id' =>  $member->id))}}" class="btn btn-primary singlebutton1 member_show" style="padding-right: 10px;">
												<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
											</a>
											<a href="{{route('socios.edit', array('user_id' =>  Auth::user()->id, 'member_id' =>  $member->id))}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
												<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
											</a>
											<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('socios.destroy', array('user_id' =>  Auth::user()->id, 'member_id' =>  $member->id))}}" style="padding-right: 10px;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
											</button>
										</td>
										<td>
											<input type="checkbox" {{ ($member->enabled == 1) ? 'checked' : '' }} class="toggle-event" data-item="{{$member->id}}"  data-toggle="toggle" data-route="/socios/change_status/" data-on="Activo" data-off="Inactivo">
										</td>
								</tr>
							@endif
						@endif

					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7">

						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Socio</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal form_validation" action="{{ route('socios.store', array('user_id' => Auth::user()->id)) }}" method="POST">
						{{ csrf_field() }}
						<fieldset>
							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
								</div>
							</div>
							@if ($errors->has('last_name'))
								<div class="input_error">
									<span>{{ $errors->first('last_name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="last_name">Apellido</label>
								<div class="col-md-6">
									<input id="last_name" name="last_name" type="text" placeholder="Apellido" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
								</div>
							</div>
							@if ($errors->has('identity_card'))
								<div class="input_error">
									<span>{{ $errors->first('identity_card') }}</span>
								</div>
							@endif
							<div class="form-group">
							<label class="col-md-4 control-label" for="identity_card">Cédula</label>
								<div class="col-md-6 inpt">
									<input id="identity_card" name="identity_card" type="text" placeholder="Cédula" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('socios.validate_identity_card_member') }}" pattern=".{6,8}" title="de 6 a 8 caracteres">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p></p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>
							@if ($errors->has('gender'))
							<div class="input_error">
								<span>{{ $errors->first('gender') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Sexo</label>
								<div class="col-md-6">
									<select required name="sex" class="form-control input-md" id="sex">
										<option value="" disabled selected>Seleccione</option>
										<option value="1">Femenino</option>
										<option value="2">Masculino</option>
									</select>
								</div>
							</div>
							@if ($errors->has('number_action'))
								<div class="input_error">
									<span>{{ $errors->first('number_action') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="number_action">Nº acción</label>
								<div class="col-md-6">
									<input id="number_action" name="number_action" type="text" placeholder="Nº acción" class="number_action form-control input-md" pattern=".{5,20}" title="de 5 a 20 caracteres" required>
								</div>
							</div>
							@if ($errors->has('handicap'))
								<div class="input_error">
									<span>{{ $errors->first('handicap') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="handicap">Handicap</label>
								<div class="col-md-6">
									<input id="handicap" name="handicap" type="text" placeholder="Handicap" class="handican form-control input-md" pattern=".{2,9}" title="" required>
								</div>
							</div>
							@if ($errors->has('phone'))
								<div class="input_error">
									<span>{{ $errors->first('phone') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="phone">Teléfono</label>
								<div class="col-md-6">
									<input id="phone" name="phone" type="text" placeholder="Teléfono" class="phone form-control input-md" required title="Formato: 0414 888-88-88" pattern=".{5,14}" title="de 5 a 14 caracteres">
								</div>
							</div>
							@if ($errors->has('email'))
								<div class="input_error">
									<span>{{ $errors->first('email') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="email">Correo</label>
								<div class="col-md-6 inpt">
									<input id="email" name="email" type="email" placeholder="Correo" class="form-control input-md chequear_correo" checkear="{{ route('socios.validate_email_member') }}" required pattern=".{3,50}" title="de 3 a 50 caracteres">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p>
												</p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>

							@if ($errors->has('password'))
								<div class="input_error">
									<span>{{ $errors->first('password') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="password">Contraseña</label>
								<div class="col-md-6">
									<input id="password" name="password" type="password" placeholder="Contraseña" class="form-control input-md" pattern=".{6,25}" title="de 6 a 25 caracteres" required>
								</div>
							</div>
							@if ($errors->has('password_confirmation'))
								<div class="input_error">
									<span>{{ $errors->first('password_confirmation') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="password_confirmation">Confirmar Contraseña</label>
								<div class="col-md-6">
									<input id="password_confirmation" name="password_confirmation" type="password" placeholder="Confirmar Contraseña" class="form-control input-md" required>
								</div>
							</div>
							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar este socio?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Socio</h4>
				</div>
				<div class="modal-body form">
					<fieldset>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<p id="d_name"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Apellido</label>
							<div class="col-md-6">
								<p id="d_last_name"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Cédula</label>
							<div class="col-md-6">
								<p id="d_identity_card"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Sexo</label>
							<div class="col-md-6">
								<p id="d_sex"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nº acción</label>
							<div class="col-md-6">
								<p id="d_number_action"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Handicap</label>
							<div class="col-md-6">
								<p id="d_handicap"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Teléfono</label>
							<div class="col-md-6">
								<p id="d_phone"></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Correo</label>
							<div class="col-md-6">
								<p id="d_email"></p>
							</div>
						</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var user_id = {{ Auth::user()->id }};
</script>
@endsection

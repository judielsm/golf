@extends('layouts.app')
@section('content')

<div class="contenedor">
	<div>
		<form class="form-horizontal white">
			<legend style="color: white;">Datos de socio</legend>
			{{ csrf_field() }}
			<fieldset>

			@if ($errors->has('name'))
			<div class="input_error">
				<span>{{ $errors->first('name') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="textinput">Nombre</label>	
				<div class="col-md-4">
					<input id="textinput" name="name" type="text" placeholder="Nombre" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('last_name'))
			<div class="input_error">
				<span>{{ $errors->first('last_name') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="textinput">Apellido</label>	
				<div class="col-md-4">
					<input id="textinput" name="last_name" type="text" placeholder="Apellido" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('identity_card'))
			<div class="input_error">
				<span>{{ $errors->first('identity_card') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="textinput">Cedula</label>	
				<div class="col-md-4">
					<input id="textinput" name="identity_card" type="text" placeholder="Cedula" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('phone'))
			<div class="input_error">
				<span>{{ $errors->first('phone') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="textinput">Telefono</label>	
					<div class="col-md-4">
				<input id="textinput" name="phone" type="text" placeholder="Telefono" class="form-control input-md">
				</div>
			</div>
		</div>
		<div class="agregar regresar">
			<center>
				<a href="{{ route('/') }}" class="waves-effect">
					<div class="">
						<span class="text">Regresar</span>
					</div>
				</a>
			</center>
		</div>
	</div>
</div>

@if ($errors->has('type'))
<div class="input_error">
	<span>{{ $errors->first('type') }}</span>
</div>
@endif
<div class="form-group">
	<label class="col-md-4 control-label" for="textinput"></label>	
	<div class="col-md-4">
		<select	name="type">
		<option value='F' selected>Familiar</option>
		<option value='I'>Invitado</option>
	</select>
	</div>
</div>

<div id="eliminar_partner" class="modal modal_">
	<div class="titulo">
		<h3>
			Esta seguro que desea eliminar estes contenido
		</h3>
	</div>
	<div class="form">
		<form class="form-horizontal" role="form" method="POST">
			{{ csrf_field() }}
			{{ method_field('DELETE') }}
			<div class="button">
				<center>
					<button type="submit" name="button">
						<span>Si</span>
					</button>
					<a href="#" class="" onclick="$('#eliminar_partner').modal('close'); return false;">
						<span>No</span>
					</a>
				</center>
			</div>
		</form>
	</div>
</div>

@endsection
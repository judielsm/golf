@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Pagos</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<!-- <div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
		</div> -->
		<div class="table-responsive success tabla">
			
			<table class="table">
				<thead>
					<tr>
						
						<th>Monto</th>
						<th>Fecha del Pago</th>
						<th>Fecha de Reservacion</th>
						<th>Medio de pago</th>
                        
						
						
					</tr>
				</thead>
				<tbody>
					@foreach( $payments as $payment)
							
								<tr>
                                		<td>Bs. {{ $payment->transaction->amount }}</td>
										<td>{{ date('Y-m-d'),strtotime($payment->created_at) }}</td>
										<td>{{ date('Y-m-d'),strtotime($payment->transaction->created_at) }}</td>
										<td>{{ $payment->payment->name }}</td>

										
										
								</tr>


					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4">

						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea cancelar este Reserva?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Reserva</h4>
				</div>
				<div class="modal-body form">
					<fieldset>
						<div class="col-xs-4">
							Fecha <input type="date" name="" id="reservation_date" value="">
						</div>
						<div class="col-xs-4">
							Hora <input type="time" >
						</div>
						<div class="col-xs-4"></div>
					</fieldset>
					<fieldset>
						<div id="dmember">
							<h3>Socios</h3>
							<ul id="m_members" style="list-style:none">
								
							</ul>
							<select class="js-example-basic-single select2 form-control input-md" id="add_members">
										<option value="" disabled selected>Selecionar Socio</option>
							</select>
						</div>
						<div id="dpartner">
							<h3>Invitados</h3>
							<ul id="m_partners"  style="list-style:none">
								
							</ul>
							<select class="js-example-basic-single select2 form-control input-md" id="add_partners">
										<option value="" disabled selected>Selecionar Invitado</option>
							</select>
						</div>
					</fieldset>
					<fieldset>
						<div class="col-xs-4">
							<h3>Total</h3>
						</div>
						<div class="col-xs-8">
							<h3 id="m_total"></h3>
						</div>
					</fieldset>
					<fieldset>
						<div class=" boton">
								<button type="button" id="pay_button" class="btn btn-primary ">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

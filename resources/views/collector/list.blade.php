@php
use Carbon\Carbon;
@endphp
@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Reservaciones</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<!-- <div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
		</div> -->
		<div class="table-responsive success tabla">
			<table class="table">
				<thead>
					<tr>
						<th>Participantes</th>
						<th>Monto</th>
						<th>Fecha</th>
						<th class="opciones">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $transactions as $transaction)
					@if(isset($transaction->payer))
					<tr id="{{$transaction->payer}}">
						<td>
							<ul style="list-style: none">
								<li>
									<ul style="list-style: none">
										<p style="font-weight: bold;">Socios</p>
										@foreach($transaction->participants_members as $member)
										<li>{{$member->name}} {{$member->last_name}} ({{$member->identity_card}})</li>
										@endforeach
									</ul>
								</li>
								@if($transaction->participants_partners)
								<ul style="list-style: none">
									<p style="font-weight: bold;">Invitados</p>
									@foreach($transaction->participants_partners as $partner)
									@if($partner)
										<li>{{$partner->name}} {{$partner->last_name}} ({{$partner->identity_card}})</li>
									@endif
									@endforeach
								</ul>
								@endif
							</ul>
						</td>
						<td>Bs. {{ $transaction->amount }}</td>
						<td>{{ Carbon::parse($transaction->date)->format('d-m-Y') }}</td>
						<td>
							<a href="" class="btn btn-primary singlebutton1 pay" data-item="" style="padding-right: 10px;" id="{{ $transaction->id }}">
								<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
							</a>
							<a href="#" class="btn btn-primary singlebutton1 agregar_participantes" style="padding-right: 10px;" id="{{ $transaction->id }}">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</a>
							<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="/cobranza/{{$transaction->id}}" style="padding-right: 10px;">
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
							</button>
						</td>

					</tr>

					@endif

					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4">

						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea cancelar este Reserva?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Reserva</h4>
				</div>
				<div class="modal-body form">
					<fieldset>
						<div class="col-xs-4">
							Fecha <input type="date" name="" id="reservation_date" value="">
						</div>
						<div class="col-xs-4">
							Hora <input type="time" >
						</div>
						<div class="col-xs-4"></div>
					</fieldset>
					<fieldset>
						<div id="dmember">
							<h3>Socios</h3>
							<ul id="m_members" style="list-style:none">

							</ul>
							<select class="js-example-basic-single select2 form-control input-md" id="add_members">
								<option value="" disabled selected>Selecionar Socio</option>
							</select>
						</div>
						<div id="dpartner">
							<h3>Invitados</h3>
							<ul id="m_partners"  style="list-style:none">

							</ul>
							<select class="js-example-basic-single select2 form-control input-md" id="add_partners">
								<option value="" disabled selected>Selecionar Invitado</option>
							</select>
						</div>
					</fieldset>
					<fieldset>
						<div class="col-xs-4">
							<h3>Total</h3>
						</div>
						<div class="col-xs-8">
							<h3 id="m_total"></h3>
						</div>
					</fieldset>
					<fieldset>
						<div class=" boton">
							<button type="button" id="pay_button" class="btn btn-primary ">Enviar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div> -->
	<div class="modal fade modaldetalles modal_transaccion" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Pago</h4>
				</div>
				<div class="modal-body form">
					<fieldset>
						<form action="{{ route('cobranza.store') }}" method="POST" id="form_transaction">
							{{ csrf_field() }}
							<input type="hidden" name="transaction" value="" id="transaction">
							<div class="fechas">
								<div class="inpt divider">
									<label for="">Fecha</label>
									<span id="fecha">19-51-5211</span>
								</div>
								<div class="inpt divider">
									<label for="">Hora</label>
									<span id="hora">12:24 AM</span>
								</div>
								<div class="clear"></div>
							</div>
							<div class="participants">
								<div class="socios participantes divider">
									<div class="titulo">
										<h4>Socios</h4>
									</div>
									<ul>
										<li>
										</li>
									</ul>
								</div>
								<div class="invitados participantes divider">
									<div class="titulo">
										<h4>Invitados</h4>
									</div>
									<table>
										<thead>
											<tr>
												<th style="width: 75px">Eliminar</th>
												<th>Nombre</th>
												<th>Monto</th>
												<th style="width: 150px">Pagar</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input type="checkbox">df</td>
												<td>Jose Miguel</td>
												<td>10.000.000 BsF</td>
												<td>
													<select class="" name="payment_method_id[]">
														<option selected disabled>Seleccionar metodo de pago</option>
														@foreach( $payment_methods as $payment)
															<option value= {{ $payment->id}}>{{ $payment->name}}</option>
														@endforeach
													</select>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="clear"></div>
							</div>
							<div class="total">
								<div class="divider">
									<span class="label">Total:</span>
								</div>
								<div class="divider">
									<span class="monto">100.00</span> BsF
								</div>
								<div class="clear"></div>
							</div>
							<div class="error">
								<p>Debe Selecionar un metodo de pago</p>
							</div>
							<div class="boton">
								<button type="submit" class="btn btn-primary singlebutton1">
									<span>Pagar</span>
								</button>
							</div>

						</form>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade modaldetalles" id="agregar_participantes" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar participante</h4>
				</div>
				<div class="modal-body form">
					<fieldset>
						<form action="/cobranza_participant" method="POST" id="form_agregar_participante">
							{{ csrf_field() }}
							<input type="hidden" name="reservation_id" value="" id="reservation_id">
							<div class="form-group">
								<select class="" name="type" id="tipo_participante">
									<option value="" disabled selected>Tipo de participante</option>
									<option value="1">Socio</option>
									<option value="2">Invitado</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-group" id="select_participant">
								<select class="" name="participant_id" id="participante_id">
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-group" id="green_fee">
								<select class="" name="green_fee_id" id="select_green_fee">
									<option selected disabled>Selecionar green fee</option>
									<option value="0">Exonerado</option>
									@foreach($greens as $green)
										<option value="{{$green->id}}">{{$green->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-group">
								<label for="">Comentario</label>
								<textarea name="comment" rows="8" cols="80"></textarea>
							</div>
							<div class="clear"></div>
							<div class="error">
								<p>Debe selecionar un tipo y un participante</p>
							</div>
							<div class="boton">
								<button type="submit" class="btn btn-primary singlebutton1">
									<span>Agregar</span>
								</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">
									<span>Cancelar</span>
								</button>
							</div>
						</form>
					</fieldset>
				</div>
			</div>
		</div>
	</div>

</div>

</div>
<script type="text/javascript">
var user_id = {{ Auth::user()->id }};
var transactions = {!! $transactions !!};
var payment_methods = {!! $payment_methods !!};

var members = {!! $members !!};
var partners = {!! $partners !!};

// console.log(transactions);

</script>
@endsection
@section('script')
	<script type="text/javascript" src="{{ asset('js/script_collector.js') }}"></script>
@endsection

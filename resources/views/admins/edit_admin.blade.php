@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Administrador</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="form">
				<form class="form-horizontal" action="{{ route('administradores.update', $user->id) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<input id="name" name="name" type="text" value="{{ $user->name }}" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
							</div>
						</div>

						@if ($errors->has('email'))
							<div class="input_error">
								<span>{{ $errors->first('email') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="email">Correo</label>
							<div class="col-md-6 inpt">
								<input id="email" name="email" type="email" value="{{ $user->email }}" placeholder="Correo" class="form-control input-md chequear_correo" checkear="{{ route('administradores.validate_email_user') }}" required pattern=".{3,50}" title="de 3 a 50 caracteres">
								<div class="chequear">
									<div class="icon check">
										<img src="{{ asset('img/icons/check.png') }}" alt="">
									</div>
									<div class="icon error">
										<img src="{{ asset('img/icons/error.png') }}" alt="">
										<div class="mensaje">
											<p>
											</p>
										</div>
									</div>
									<div class="icon cargando">
										<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
									</div>
								</div>
							</div>
						</div>

						@if ($errors->has('role_id'))
						<div class="input_error">
							<span>{{ $errors->first('role_id') }}</span>
						</div>
						@endif

						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Roles</label>
							<div class="col-md-6">
								<select required name="role_id" class="form-control input-md" required>
									<option value="{{$user->role_id}}" disabled selected>Seleccione Rol</option>
									@foreach($roles as $role)
										<option value="{{ $role->id }}" {{ ($user->role_id == $role->id) ? 'selected' : '' }}>{{ $role->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('administradores.index')}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
<script type="text/javascript">
	var user_id = {{ $user->id }};
</script>


@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Invitados</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<div class="exonerated_mensaje">
			<div class="alert">
				<div class="text"></div>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		<!-- <div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
		</div> -->
		<div class="table-responsive success tabla">
			<div class="agregar_registro">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			</div>

			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Cédula</th>
						<th class="opciones" id="kdkd">Opciones</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $partners as $partner)
						<tr>
							<td>{{ $partner->name }}</td>
							<td>{{ $partner->last_name }}</td>
							<td>{{ $partner->identity_card }}</td>
							<td>
								<a href="{{route('invitados.show', array('user_id' =>  Auth::user()->id, 'partner_id' =>  $partner->id))}}" class="btn btn-primary singlebutton1 partner_show" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
								<a href="{{route('invitados.edit', array('user_id' =>  Auth::user()->id, 'partner_id' =>  $partner->id))}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('invitados.destroy', array('user_id' =>  Auth::user()->id, 'partner_id' =>  $partner->id))}}" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</td>
							<td>
								<input type="checkbox" {{ ($partner->enabled == 1) ? 'checked' : '' }} class="toggle-event" data-item="{{$partner->id}}"  data-toggle="toggle" data-route="/invitados/change_status/" data-on="Activo" data-off="Inactivo">
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">

						</td>
					</tr>
				</tfoot>

			</table>
		</div>
	</div>
	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Invitado</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal form_validation" action="{{ route( 'invitados.store', array( 'user_id' =>  Auth::user()->id ) ) }}" method="POST">
						{{ csrf_field() }}
						<fieldset>
							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" required pattern=".{3,25}" title="de 3 a 25 caracteres">
								</div>
							</div>
							@if ($errors->has('last_name'))
								<div class="input_error">
									<span>{{ $errors->first('last_name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="last_name">Apellido</label>
								<div class="col-md-6">
									<input id="last_name" name="last_name" type="text" placeholder="Apellido" class="form-control input-md" required pattern=".{3,25}" title="de 3 a 25 caracteres">
								</div>
							</div>
							@if ($errors->has('identity_card'))
								<div class="input_error">
									<span>{{ $errors->first('identity_card') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="identity_card">Cédula</label>
								<div class="col-md-6 inpt">
									<input id="identity_card" name="identity_card" type="text" placeholder="Cédula" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('socios.validate_identity_card_member') }}" pattern=".{6,8}" title="de 6 a 8 caracteres">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p></p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>
							@if ($errors->has('sex'))
								<div class="input_error">
									<span>{{ $errors->first('sex') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="sex">Sexo</label>
								<div class="col-md-6">
									<select name="sex" class="form-control input-md" required>
										<option value="" disabled selected>Seleccione</option>
										<option value="1">Femenino</option>
										<option value="2">Masculino</option>
									</select>
								</div>
							</div>
							@if ($errors->has('phone'))
								<div class="input_error">
									<span>{{ $errors->first('phone') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="phone">Teléfono</label>
								<div class="col-md-6">
									<input id="phone" name="phone" type="text" placeholder="Teléfono" class="phone form-control input-md" required title="Formato: 0414 999-99-88" pattern=".{5,14}" title="de 5 a 14 caracteres">
								</div>
							</div>
							@if ($errors->has('email'))
								<div class="input_error">
									<span>{{ $errors->first('email') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="email">Correo</label>
								<div class="col-md-6 inpt">
									<input id="email" name="email" type="email" placeholder="Correo" class="form-control input-md chequear_correo" required checkear="{{ route('socios.validate_email_member') }}" pattern=".{3,50}" title="de 3 a 50 caracteres" >
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p></p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>
							@if ($errors->has('exonerated'))
								<div class="input_error">
									<span>{{ $errors->first('exonerated') }}</span>
								</div>
							@endif
							<!-- Solo si es administrador -->
							@if(Auth::user()->role_id == 2)
								<div class="form-group">
									<label class="col-md-4 control-label" for="exonerated">Exonerado</label>
									<div class="col-md-6">
										<select name="exonerated" id="exonerated" class="form-control input-md" required>
											<option value="" disabled selected>Seleccione</option>
											<option value="0">No Exonerado</option>
											<option value="1">Parcial</option>
											<option value="2">Total</option>
											<option value="3">Por reservación</option>
										</select>
									</div>
								</div>
								@if ($errors->has('club_id'))
									<div class="input_error">
										<span>{{ $errors->first('club_id') }}</span>
									</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="club_id">Club Origen</label>
									<div class="col-md-6">
										<select name="club_id" id="club_id" class="form-control input-md" required>
											<option value="" disabled selected>Seleccione Club</option>
											@foreach($clubes as $club)
												<option value="{{ $club->id }}">{{ $club->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							@endif
							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar este invitado?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de invitado</h4>
				</div>
				<div class="modal-body form">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<p id="d_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Apellido</label>
								<div class="col-md-6">
									<p id="d_last_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Cedula</label>
								<div class="col-md-6">
									<p id="d_identity_card"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Sexo</label>
								<div class="col-md-6">
									<p id="d_sex"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Teléfono</label>
								<div class="col-md-6">
									<p id="d_phone"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Correo</label>
								<div class="col-md-6">
									<p id="d_email"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Exonerado</label>
								<div class="col-md-6">
									<p id="d_exonerated"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Club</label>
								<div class="col-md-6">
									<p id="d_club"></p>
								</div>
							</div>
						</fieldset>
				</div>
			</div>
		</div>
	</div>


</div>
<script type="text/javascript">
var user_id = {{ Auth::user()->id }};
</script>
@endsection

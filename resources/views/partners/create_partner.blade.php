@extends('layouts.app')

@section('content')
	<div id="contenido">
		<form class="form-horizontal" action="{{ route( 'invitados.store' ) }}" method="POST">
   			
			{{ csrf_field() }}
			<fieldset>

			<!-- Form Name -->
			<legend>Jugador</legend>

			@if ($errors->has('name'))
			<div class="input_error">
				<span>{{ $errors->first('name') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="name">Nombre</label>
				<div class="col-md-4">
					<input id="name" name="name" value="{{ old('name') }}" type="text" placeholder="Nombre" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('last_name'))
			<div class="input_error">
				<span>{{ $errors->first('last_name') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="last_name">Apellido</label>
				<div class="col-md-4">
					<input id="last_name" name="last_name" value="{{ old('last_name') }}" type="text" placeholder="Apellido" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('identity_card'))
			<div class="input_error">
				<span>{{ $errors->first('identity_card') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="identity_card">Cedula</label>
				<div class="col-md-4">
					<input id="identity_card" name="identity_card" value="{{ old('identity_card') }}" type="text" placeholder="Cedula" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('sex'))
				<div class="input_error">
					<span>{{ $errors->first('sex') }}</span>
				</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="sex">Sexo</label>
				<div class="col-md-6">
					<select name="sex" class="form-control input-md" required>
						<option value="Femenino">Femenino</option>
						<option value="Masculino">Masculino</option>
					</select>
				</div>
			</div>

			@if ($errors->has('phone'))
			<div class="input_error">
				<span>{{ $errors->first('phone') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="phonetextinput">Telefono</label>
				<div class="col-md-4">
					<input id="phone" name="phone" value="{{ old('phone') }}" type="text" placeholder="Telefono" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('email'))
			<div class="input_error">
				<span>{{ $errors->first('email') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="email">Email</label>
				<div class="col-md-4">
					<input id="email" name="email" value="{{ old('email') }}" type="text" placeholder="Email" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('type'))
			<div class="input_error">
				<span>{{ $errors->first('type') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="type">Tipo</label>
				<div class="col-md-4">
					<input id="type" name="type" value="{{ old('type') }}" type="text" placeholder="Tipo" class="form-control input-md">
				</div>
			</div>

			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="singlebutton">Enviar</label>
				<div class="col-md-4">
					<button id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
				</div>
			</div>

			</fieldset>
		</form>
	</div>
@endsection
@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Socio</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="form">
				<form class="form-horizontal" action="{{ route('usuarios.update', $user->id) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<input id="name" name="name" type="text" value="{{ $user->name }}" placeholder="Nombre" class="form-control input-md">
							</div>
						</div>

						@if ($errors->has('email'))
							<div class="input_error">
								<span>{{ $errors->first('email') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="email">Correo</label>
							<div class="col-md-6">
								<input id="email" name="email" type="email" value="{{ $user->email }}" placeholder="Email" class="form-control input-md">
							</div>
						</div>

						@if ($errors->has('role_id'))
							<div class="input_error">
								<span>{{ $errors->first('role_id') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-4 col-md-center control-label" for="role_id">Rol</label>
							<div class="col-6 col-md-center">
								<select required name="role_id" class="form-control input-md">
									<option value="" disabled selected>Seleccione Rol</option>
									@foreach($roles as $role)
										<option value="{{ $role->id }}" {{ ($role->id == $user->role_id) ? 'selected' : '' }}>{{ $role->name }}</option>
									@endforeach
								</select>
							</div>
						</div>


						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('usuarios.index')}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection

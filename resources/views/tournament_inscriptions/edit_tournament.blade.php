@extends('layouts.app')

@section('content')

<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Torneo</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('torneo_inscription.store', $tournament->id) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}

				<fieldset>
					@if ($errors->has('name'))
						<div class="input_error">
							<span>{{ $errors->first('name') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="name">Nombre</label>
						<div class="col-md-6">
							<input id="textinput" name="name" type="text" placeholder="Nombre" class="form-control input-md" value="{{$tournament->name}}">
						</div>
					</div>


					@if ($errors->has('start_date'))
					<div class="input_error">
						<span>{{ $errors->first('start_date') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="start_date">Fecha de inicio</label>
						<div class="col-md-6">
							<input type="date" class="date_tournament form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="start_date_tournament" value="{{ $tournament->start_date }}">
						</div>
					</div>

					@if ($errors->has('end_date'))
					<div class="input_error">
						<span>{{ $errors->first('end_date') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="end_date">Fecha final</label>
						<div class="col-md-6">
							<input type="date" class="form-control input-md" placeholder="Fecha final:" name="end_date" id="end_date_tournament" value="{{ $tournament->end_date }}">
						</div>
					</div>

					@if ($errors->has('inscription_fee'))
						<div class="input_error">
							<span>{{ $errors->first('inscription_fee') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="inscription_fee">Cuota de inscripción</label>
						<div class="col-md-6">
							<input id="" name="inscription_fee" type="text" placeholder="Cuota de inscripción" class="form-control input-md money" value="{{$tournament->inscription_fee}}">
						</div>
					</div>


					@if ($errors->has('modality_id'))
					<div class="input_error">
						<span>{{ $errors->first('modality_id') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="modality_id">Modalidad</label>
						<div class="col-md-6">
							<select required name="modality_id" class="form-control input-md">
								<option value="" selected disabled>Seleccione modalidad</option>
								@foreach($modalities as $modality)
									<option value="{{ $modality->id }}" {{ ($tournament->modality_id == $modality->id) ? 'selected' : '' }}>{{ $modality->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					@if ($errors->has('category_id'))
					<div class="input_error">
						<span>{{ $errors->first('category_id') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="category_id">Categoria</label>
						<div class="col-md-4">
							<select required multiple name="category_id[]" class="form-control input-md select_categories" id="category">
								@foreach($tournament->categories as $category)
									<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<button type="button" id="myBtn" class="btn btn-primary singlebutton1">Crear</button>
						</div>
					</div>

					@if ($errors->has('start_time'))
						<div class="input_error">
							<span>{{ $errors->first('start_time') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="start_time">Hora de Inicio</label>
						<div class="col-md-6">
							<input required id="start_time" name="start_time" type="time" placeholder="Hora de Inicio" class="form-control input-md" value="{{$tournament->start_time}}">
						</div>
					</div>

					@if ($errors->has('end_time'))
						<div class="input_error">
							<span>{{ $errors->first('end_time') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="end_time">Hora de salida</label>
						<div class="col-md-6">
							<input required id="end_time" name="end_time" type="time" placeholder="Hora de Inicio" class="form-control input-md" value="{{$tournament->end_time}}">
						</div>
					</div>

					@if ($errors->has('condition_file'))
						<div class="input_error">
							<span>{{ $errors->first('condition_file') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="condition_file">Condiciones de campeonatos</label>
						<div class="col-md-6">
							<!-- <input id="textinput" name="condition_file" type="text" placeholder="Numero de participantes" class="form-control input-md numParticiped"> -->
							<input id="file_torunament" type="file" class="file" name="conditions_file" value="{{asset(''.$tournament->conditions_file.'')}}">
						</div>
					</div>
					<input type="hidden" name="modified" value="false" id="modified" class="form-control inpud-md">

					@if ($errors->has('start_date_inscription'))
					<div class="input_error">
						<span>{{ $errors->first('start_date_inscription') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="start_date_inscription">Fecha de inscripcion</label>
						<div class="col-md-6">
							<input type="date"	placeholder="Fecha de inicio:" value="{{ $tournament->start_date_inscription }}" class="form-control input-md date_inscription" name="start_date_inscription" id="start_date_inscription" required>
						</div>
					</div>

					@if ($errors->has('end_date_inscription'))
					<div class="input_error">
						<span>{{ $errors->first('end_date_inscription') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="end_date_inscription">Fecha Tope inscripcion</label>
						<div class="col-md-6">
							<input type="date"	placeholder="Fecha final:" value="{{ $tournament->end_date_inscription }}" class="form-control input-md" name="end_date_inscription" id="end_date_inscription" date=" " required>
						</div>
					</div>

					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

						<a href="{{route('torneos.index')}}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Agregar Categoría</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal" id="form_category" method="POST" enctype="multipart/form-data" ruta="{{ route('categorias.store', array( 'tournament_id' => $tournament->id )) }}">
					{{ csrf_field() }}
					<input type="hidden" name="tournament_id" value="{{ $tournament->id }}">
					<fieldset>
						<!-- <div class="mensaje">
							<div class="text">
								<p>Error</p>
							</div>
						</div> -->
						@if ($errors->has('name'))
						<div class="input_error">
							<span>{{ $errors->first('name') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Nombre</label>
							<div class="col-md-6">
								<input id="name" name="name" type="text" placeholder="Nombre de la Categoría" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('number_participants'))
						<div class="input_error">
							<span>{{ $errors->first('number_participants') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Cantidad de Participantes</label>
							<div class="col-md-6">
								<input id="number_participants" name="number_participants" type="text" placeholder="Número de Participantes" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('sex'))
						<div class="input_error">
							<span>{{ $errors->first('sex') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Género</label>
							<div class="col-md-6">
								<select required class="form-control input-md" name="sex" id="sex">
									<option value="" disabled selected>Sexo</option>
									<option value="Masculino" >Masculino</option>
									<option value="Femenino" >Femenino</option>
									<option value="Mixto" >Mixto</option>
								</select>
							</div>
						</div>

						@if ($errors->has('handicap_min'))
						<div class="input_error">
							<span>{{ $errors->first('handicap_min') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Handicap Mínimo</label>
							<div class="col-md-6">
								<input id="handicap_min" name="handicap_min" type="text" placeholder="Rango Mínimo" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
							</div>
						</div>

						@if ($errors->has('handicap_max'))
						<div class="input_error">
							<span>{{ $errors->first('handicap_max') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Handicap Máximo</label>
							<div class="col-md-6">
								<input id="handicap_max" name="handicap_max" type="text" placeholder="Rango Máximo" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
							</div>
						</div>

						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1" id="create_category">Enviar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>


@endsection

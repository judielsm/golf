@extends('layouts.app')

@section('content')


<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Green fee</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('definicion_reservas.update', array('user_id' => Auth::user()->id, 'reservation_definition_id' => $reservation_definition->id)) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}


				<fieldset>

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Nombre
							</label>
							<div class="col-md-6">
								<input type="text" name="name" value="{{ $reservation_definition->name }}" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('green_fee'))
							<div class="input_error">
								<span>{{ $errors->first('green_fee') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Monto
							</label>
							<div class="col-md-6">
								<input type="text" name="green_fee" value="{{ $reservation_definition->green_fee }}" class="form-control input-md money_green_feed" required>
							</div>
						</div>

						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Procesar</button>
							<a href="{{ route('definicion_reservas.index', array( 'user_id' => Auth::user()->id ) ) }}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>



				</fieldset>
			</form>
		</div>
	</div>
</div>@endsection

@extends('layouts.app')

@section('content')

	<div id="inscripcion" class="container">
		<div class="edit reserv">
		<div class="modal-header">
			<h3 class="modal-title">Inscribir Alumno </h3>
			@if (session('validations'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						@foreach (session('validations') as $m)
							{{$m}}<br/>
						@endforeach
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
			@endif
			@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
			@endif
		</div>
				<form class="form-horizontal inhabilitar" action="{{route('inscripciones.store', array('user_id' => Auth::user()->id))}}" method="POST">
			{{csrf_field()}}
			<fieldset>
			<!-- Form Name -->
			

				<div class="panel-group acordion_planes" id="accordion">
					@foreach ($payment_plans as $payment_plan)
						<div class="panel panel-default">
							<div class="panel-heading head">
								<div class="input">
									<input type="radio" name="payment_plan_id" value="{{ $payment_plan->id }}" class="checkbox_payment_plan" required="">
								</div>
								<a data-toggle="collapse" data-parent="#accordion" href="#acordion_planes_{{ $payment_plan->id }}">
									<div class="titulo">
										<h5>{{ $payment_plan->name }}</h5>
									</div>
									<div class="precio">
										<span>
											{{ $payment_plan->rate }} BsF
										</span>
									</div>
								</a>

							</div>
							<div id="acordion_planes_{{ $payment_plan->id }}" class="panel-collapse collapse">
								<div class="panel-body descripcion">{{ $payment_plan->description }}</div>
							</div>
						</div>
					@endforeach
				</div>


				@if(Auth::user()->role_id == 3)

					@if ($errors->has('member_id'))
						<div class="input_error">
							<span>{{$errors->first('member_id')}}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="receipt_payment">Buscar Socio</label>
						<div class="col-md-6" >
							<input id="add_member_registration" required type="text" class="form-control input-md" placeholder="Buscar Socio" name="search_socio" ruta="{{ route('socios.autocomplete') }}" ruta2="{{ route('alumnos.student_for_member', array( 'member_id' => 0 ))  }}">
						</div>
					</div>
				@endif

				@if ($errors->has('student_id'))
				<div class="input_error">
					<span>{{ $errors->first('student_id') }}</span>
				</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="name">Estudiante</label>
					<div class="col-md-6">
						<select required {{ (Auth::user()->role_id == 3) ? 'disabled' : '' }} name="student_id" class="form-control input-md select_categories" id="student_select">
							<option value="" disabled selected>Busque primero al Socio</option>
								@foreach( $students as $student )
									<option value="{{$student->id}}">{{$student->name.' '.$student->last_name}}</option>
								@endforeach
						</select>
					</div>
					<div class="col-md-2">
						<button type="button" id="create_student" class="btn btn-primary singlebutton1" {{ (Auth::user()->role_id == 3) ? 'disabled' : '' }} style="float: left;">+</button>
					</div>
				</div>


				@if ($errors->has('group_id'))
					<div class="input_error">
						<span>{{$errors->first('group_id')}}</span>
					</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Grupo</label>
					<div class="col-md-6">
						<select name="group_id" class="form-control input-md" id="group_select" required>
							<option value="" disabled selected>Seleccione Grupo</option>
								@foreach ($groups as $group)
							<option value="{{$group->id}}">{{$group->name}}</option>
								@endforeach
						</select>
					</div>
					@if(Auth::user()->role_id == 3)

						<div class="col-md-2">
							<button type="button" id="create_group" class="btn btn-primary singlebutton1" style="float: left;">+</button>
						</div>
					@endif

				</div>



				@if ($errors->has('type_payment_id'))
					<div class="input_error">
						<span>{{$errors->first('type_payment_id')}}</span>
					</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="student_id">Tipo de Pago
					</label>
					<div class="col-md-6">
						<select name="type_payment_id" class="form-control input-md" id="type_payment_id" required>
							<option value="" disabled selected>Seleccione Tipo Pago</option>
							@foreach( $type_payments as $type_payment )
								<option value="{{ $type_payment->id }}"> {{ $type_payment->label }} </option>
							@endforeach
						</select>
					</div>
				</div>


				<!-- Button -->
				<div class="form-group boton">
					<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
					<a href="{{route('inscripciones.index', array('user_id' => Auth::user()->id) )}}" class="btn btn-primary singlebutton1">
						Cancelar
					</a>
				</div>
			</fieldset>
		</form>
	</div>


</div>

<div class="modal fade" id="modal_create_student" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Agregar Alumno</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal" action="{{route('alumnos.store_ajax', array('user_id' => Auth::user()->id))}}" method="POST" id="form_create_student">
					{{csrf_field()}}
					<input type="hidden" name="member_id" value="{{ (Auth::user()->role_id == 6) ? Auth::user()->member->id : '' }}" id="member_id_student">
					<fieldset>
						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<input id="name" name="name" type="text" placeholder="Nombre" pattern=".{3,25}" title="de 3 a 25 caracteres" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('last_name'))
							<div class="input_error">
								<span>{{ $errors->first('last_name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="last_name">Apellido</label>
							<div class="col-md-6">
								<input id="last_name" name="last_name" type="text" placeholder="Apellido" pattern=".{3,25}" title="de 3 a 25 caracteres" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('identity_card'))
							<div class="input_error">
								<span>{{ $errors->first('identity_card') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="identity_card">Cédula</label>
								<div class="col-md-6 inpt">
									<input id="identity_card" name="identity_card" type="text" placeholder="Cédula" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('alumnos.validate_identity_card_student') }}" pattern=".{6,8}" title="de 6 a 8 caracteres">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p></p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>


						@if ($errors->has('phone'))
							<div class="input_error">
								<span>{{ $errors->first('phone') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="phone">Teléfono</label>
							<div class="col-md-6">
								<input id="phone" name="phone" type="text" placeholder="Teléfono"  pattern=".{5,14}" title="de 5 a 14 caracteres" class="phone form-control input-md" required title="Formato: 0414 888-88-88" required>
							</div>
						</div>

						@if ($errors->has('birthdate'))
							<div class="input_error">
								<span>{{ $errors->first('birthdate') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="birthdate">Fecha de Nacimiento</label>
							<div class="col-md-6">
								<input type="date" id="datetimepicker" pattern="Y-m-d" required class="datetimepicker3 form-control input-md" name="birthdate" >
							</div>
						</div>
							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal_create_group" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Agregar Grupo</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal inhabilitar" action="{{route('grupos.store_ajax', array('user_id' => Auth::user()->id))}}" method="POST" id="form_create_group">
						{{csrf_field()}}
						<fieldset>
									<!-- Form Name -->
									

							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{$errors->first('name')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Nombre</label>
								<div class="col-md-6">
									<input id="name" required name="name" type="text" placeholder="Nombre" pattern=".{3,25}" title="de 3 a 25 caracteres" class="form-control input-md">
								</div>
							</div>

							@if ($errors->has('teacher_id'))
							<div class="input_error">
								<span>{{$errors->first('teacher_id')}}</span>
							</div>
							@endif

							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Profesor</label>
								<div class="col-md-6">
									<select required name="teacher_id" class="form-control input-md">
										<option value="" disabled selected>Seleccione Profesor</option>
											@foreach ($teachers as $teacher)
												<option value="{{$teacher->id}}" >{{$teacher->name.' '.$teacher->last_name}}</option>
											@endforeach
									</select>
								</div>
							</div>

								<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>


@endsection

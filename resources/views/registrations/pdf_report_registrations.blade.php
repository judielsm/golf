<style type="text/css">

.header{
	width: 100%;
}
.header h2{
	margin: 0;
}
.fechas{
	width: 100%;
	/*background: rgb(69, 224, 45);*/
	text-align: center;
	position: relative;
}
.fecha{
	display: inline-block;
	font-size: 18px;
	margin-right: 15px;
	position: relative;
	/*left: 50%;*/
	/*background: rgb(142, 246, 121);*/
}
.fecha:nth-child(2){
	margin: 0 15px;
}



.table_reportes{
	width: 100%;
	border-spacing: 0px;
	border: none;
}
.table_reportes thead tr th,
.table_reportes tbody tr td{
	border: none;
	border-collapse: none;
}
.table_reportes thead tr th{
	border-left: solid 1px #fff;
	padding-left: 10px;
	text-align: left;
	border-top: solid 1px #000;

}
.table_reportes thead tr th.th_fecha{
	width: 120px;
}

.table_reportes thead tr th.th_hora{
	width: 100px;
}
.table_reportes thead tr th.th_estado{
	width: 140px;
}
.table_reportes thead tr th:first-child{
	border-left: solid 1px #000;
}
.table_reportes thead tr th:last-child{
	border-right: solid 1px #000;
}

.table_reportes tbody tr{
	border-bottom: solid 1px #000;
}
.table_reportes tbody tr td{
	border-left: solid 1px #000;
	padding: 5px 0 5px 10px;
    border-bottom: 1px solid #000;
}
.table_reportes tbody tr td:last-child{
	border-right: solid 1px #000;
}
.table_reportes tbody tr td {
	/*border-left: solid 1px #000;*/
}

#estadisticas{
	position: absolute;
	bottom: 0;
}
#footer {
	position: fixed;
	left: 0px;
	bottom: -100px;
	right: 0px;
	height: 100px;
	background-color: white;
	text-align: left;
}
#footer .page:after {
	content: counter(page);

	background-color: white;

}
@page {
	margin-top: 2cm;
	margin-bottom: 2cm;
	margin-left: 1cm;
	margin-right: 1cm;
}
</style>

<table class="header">
	<tr>
		<td>
			<h2>
				Sistema para Alquiler de Canchas de Golf <br>
				Lagunita Country Club <br>
				Listado de Inscripciones de Alumnos<br>
				Grupo {{$group->name}}<br>
			</h2>
		</td>
		<td align="center">
			<img src="{{public_path('img/logo.png')}}" style="width: 110px;" img align=center alt="">
		</td>
	</tr>
</table>

<table class="fechas">
	<?php if (!empty($start_date) OR !empty($end_date) ): ?>
		<tr>
			<td align="center">
				<?php if (!empty($start_date) && !empty($end_date) ): ?>
					<?php if ($start_date == $end_date): ?>
						<div class="fecha">
							<strong>Fecha:</strong> <?php echo $start_date ?>
						</div>
					<?php else: ?>
						<div class="fecha">
							<strong>Desde:</strong> <?php echo $start_date ?>
						</div>
						<div class="fecha">
							<strong>Hasta:</strong> <?php echo $end_date ?>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<?php if (!empty($start_date)): ?>
						<div class="fecha">
							<strong>Desde:</strong> <?php echo $start_date ?>
						</div>
					<?php else: ?>
						<div class="fecha">
							<strong>Hasta:</strong> <?php echo $end_date ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</td>

		</tr>
	<?php endif; ?>
	<?php if (!empty($status) && $status != "null"): ?>
		<tr>
			<td>
				<div class="fecha">
					<strong>Estado:</strong> <?php echo $status ?>
				</div>
			</td>
		</tr>
	<?php endif; ?>
</table>


<table class="table table-striped table-bordered table_reportes" border="1">
	<thead style='background-color: rgb(27, 60, 123); color:#ffffff; text-align: center;'>
		<tr>
			<th style="height: 30px;">FECHA</th>
			<th style="height: 30px;">ESTADO</th>
			<th style="height: 30px;">ALUMNO</th>
			<th style="height: 30px;">SOCIO</th>
		</tr>
	</thead>
	<tbody style="text-align: left;">
		@foreach($registrations as $registration)
			<tr>
				<td style="height: 40px;">{{$registration->created_at}}</td>
				<td style="height: 40px;">{{strtoupper($registration->status)}}</td>
				<td style="height: 40px;">
					<?php
						$invitado = '';
						if(isset($registration->student)){

							$invitado .= '<strong>'.strtoupper($registration->student->name).' '.strtoupper($registration->student->last_name).'<br> C.I: '.number_format($registration->student->identity_card, 0,',','.').'<br></strong>';
						}
						echo($invitado);
					?>
				</td>
				<td style="height: 40px;">
					<?php
						$socio = '';
						if(isset($registration->student->member)){

							$socio .= '<strong>'.strtoupper($registration->student->member->name).' '.strtoupper($registration->student->member->last_name).'<br> C.I: '.number_format($registration->student->member->identity_card, 0,',','.').'<br></strong>';
						}
						echo($socio);
					?>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

<div id="estadisticas">
	<p>
		<span>Nº DE RESERVACIONES TOTALES: {{$totales}}</span><br>
		<span>Nº DE RESERVACIONES EN PROCESO: {{$en_proceso}}</span><br>
		<span>Nº DE RESERVACIONES CANCELADAS: {{$canceladas}}</span><br>
		<span>Nº DE RESERVACIONES PROCESADAS: {{$procesadas}}</span><br>
		<span>Nº DE RESERVACIONES FINALIZADAS: {{$finalizadas}}</span><br>
	</p>
</div>

<div id="footer" class="page">

	<span class="page"></span>

</div>

@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Listado de Inscripción de Alumnos</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="tabla_head head_reportes">
			<form class="" action="{{route('alumnos.list_registrations', array('user_id' => Auth::user()->id ))}}" method="get">
				<div class="inputs">
					<label for="group_id">Grupo</label>
					<select id="group_id" name="group_id" class="form-control input-md" required>
						@foreach($groups as $group)
							<option value="{{$group->id}}" {{ (isset($input) && (isset($input['group_id'])) && ($input['group_id'] == $group->id)) ? 'selected' : '' }}>{{$group->name}}</option>
						@endforeach
					</select>
				</div>

				@if ($errors->has('start_date'))
				<div class="input_error">
					<span>{{ $errors->first('start_date') }}</span>
				</div>
				@endif
				<div class="inputs">
					<label class="" for="start_date">Fecha de inicio</label>
					<input type="text" class="datepicker_alls date report form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="start_date" date="" value="{{(isset($input['start_date'])) ? $input['start_date'] : ''}}">

				</div>

				@if ($errors->has('end_date'))
				<div class="input_error">
					<span>{{ $errors->first('end_date') }}</span>
				</div>
				@endif
				<div class="inputs">
					<label class="" for="end_date">Fecha de cierre</label>
					<input type="text" class="end_datepciker_alls date report form-control input-md" placeholder="Fecha de cierre:" name="end_date" id="end_date" date="" value="{{(isset($input['end_date'])) ? $input['end_date'] : ''}}">
				</div>

				<div class="boton">
					<button type="submit" class="btn btn-primary singlebutton1">
						<span>Buscar</span>
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					</button>
				</div>


			</form>
		</div>
		<div class="table-responsive success tabla">
			<table class="table">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Estudiante</th>
						<th>Socio</th>
						<th>Estatus</th>
					</tr>
				</thead>
				<tbody>
					@foreach($registrations as $registration)
						<tr>
							<td>{{ $registration->created_at }}</td>
							<td>{{ $registration->student->name.' '.$registration->student->last_name }}</td>
							<td>{{ $registration->student->member->name.' '.$registration->student->member->last_name }}</td>
							<td>{{ $registration->status }}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4">

						</td>
					</tr>
				</tfoot>

			</table>
		</div>

		@if(count($registrations) >= 1)
			<div class="bajar_reporte">
				<a id="crear_reporte_pdf" type="button" href="{{route('alumnos.pdf_report_create_registration',['user_id' => Auth::user()->id])}}">
					<span class="icon">
						<img src="{{asset('img/icons/pdf.png')}}" alt="">
					</span>
				</a>
				<a id="crear_reporte_xls" type="button" href="{{route('alumnos.xls_report_create_registration',['user_id' => Auth::user()->id])}}">
					<span class="icon">
						<img src="{{asset('img/icons/xls.png')}}" alt="">
					</span>
				</a>
			</div>
		@endif
	</div>

</div>

@if( Auth::user()->role_id == 2 )
@endif


@endsection

@extends('layouts.app')
@section('content')


<div class="contenedor">
	<div>
		<form class="form-horizontal white" action="{{route('delete_timeframe', array('timeframe_id' => $timeframe->id))}}" method="POST">
   
			{{ csrf_field() }}
			{{ method_field('DELETE') }}
			<fieldset>
			@if ($errors->has('name'))
			<div class="input_error">
				<span>{{ $errors->first('name') }}</span>
			</div>
			@endif
			<div class="form-group">
			  <label class="col-md-4 control-label" for="textinput">Nombre</label>  
			  <div class="col-md-4">
			  <input id="textinput" name="name" type="text" placeholder="Nombre" class="form-control input-md">
			  </div>
			</div>

			<div class="form-group for">
			  <label class="col-md-4 control-label" name="available_days" for="textinput">Timeframe</label>  
			  <div class="col-md-4">
			  	<select multiple="" class="timeframe" name="type1" style="color: black;">
					<option style="color: black;" value='F' selected>aaaa</option>
					<option style="color: black;" value='I'>aaaa</option>
					<option style="color: black;" value='F'></option>
					<option style="color: black;" value='I'>aaaa</option>
					<option style="color: black;" value='F'></option>
					<option style="color: black;" value='I'>aaaa</option>
				</select>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton">Enviar</label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
			  </div>
			</div>
			
		</div>
	</div>
</div>


@endsection
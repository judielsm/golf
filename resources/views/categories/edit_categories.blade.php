@extends('layouts.app')
@section('content')

<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Categoría</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{'/categoria/'.$category->id}}" method="POST">
				{{ csrf_field() }}
				<fieldset>
							@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" required pattern="[A-Za-z0-9 ./*,-]{3,50}" maxlength="50" title="de 3 a 50 caracteres" value="{{$category->name}}"
                                     maxlength="20">
								</div>
							</div>

							@if ($errors->has('number_participants'))
							<div class="input_error">
								<span>{{ $errors->first('number_participants') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Cantidad de Participantes</label>
								<div class="col-md-6">
									<input id="number_participants" value="{{$category->number_participants}}" name="number_participants" type="text" placeholder="Cantidad de Participantes" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3" title="de 1 a 3 caracteres">
								</div>
							</div>

							@if ($errors->has('genre'))
							<div class="input_error">
								<span>{{ $errors->first('genre') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Género</label>
								<div class="col-md-6">
									<select required class="form-control input-md" name="genre" id="sex" value="{{$category->sex}}">
										<option value="" disabled selected>Seleccione Género</option>
										<option value="Masculino" {{ ($category->sex == 'Masculino') ? 'selected' : '' }} >Masculino</option>
										<option value="Femenino" {{ ($category->sex == 'Femenino') ? 'selected' : '' }}>Femenino</option>
										<option value="Mixto" >Mixto</option>
									</select>
								</div>
							</div>

							@if ($errors->has('handicap_min'))
							<div class="input_error">
								<span>{{ $errors->first('handicap_min') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Handicap Mínimo</label>
								<div class="col-md-6">
									<input id="handicap_min" value="{{$category->handicap_min}}" name="handicap_min" type="text" placeholder="Handicap Mínimo" class="form-control input-md handican" required maxlength="3">
								</div>
							</div>

							@if ($errors->has('handicap_max'))
							<div class="input_error">
								<span>{{ $errors->first('handicap_max') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Handicap Máximo</label>
								<div class="col-md-6">
									<input id="handicap_max" value="{{$category->handicap_max}}" name="handicap_max" type="text" placeholder="Handicap Máximo" class="form-control input-md handican" required maxlength="3">
								</div>
							</div>

							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary ">Enviar</button>
								<a href="{{route('categorias')}}" class="btn btn-primary singlebutton1">
									Cancelar
								</a>
							</div>
						</fieldset>
			</form>
		</div>
	</div>
</div>
@endsection

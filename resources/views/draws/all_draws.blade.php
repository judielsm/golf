<script type="text/javascript">
	var draws = {!! $draws !!};
	var tournaments = {!! $tournaments !!};
	var day_blockeds = {!! $day_blockeds !!};
</script>
@extends('layouts.app')

@section('content')
<div class="error_draw">
	<p></p>
</div>
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Sorteos</h2>
        </div>
        @if (session('status'))
        <div class="alerta">
            <div class="alert alert-{{ session('type') }}">
                {{session('status')}}.
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        </div>
        @endif
        <div class="table-responsive success tabla sorteo">
            <p id="method_put">{{ method_field('PUT') }}</p>
            <form class="form-horizontal" action="{{ route( 'sorteos.store', array('user_id' => Auth::user()->id ) ) }}" method="POST" enctype="multipart/form-data" store="{{route( 'sorteos.store', array('user_id' => Auth::user()->id ) )}}" id="form_draw" estado="0">
				{{ csrf_field() }}
                <p id="add_method"></p>
				<!-- <p></p> -->
                <input type="hidden" name="draw" value="" id="day_draws">
                <fieldset>
                    <div class="drawing" style="">
                        <div class="drawing_head" style="">
							<h3 style="float: right; margin-top: 0; width: 100%; text-align: right;">Sorteos Creados</h3>
                            <ul class="draws">
                                <?php
                                $meses = [];
                                $mes_esp = [
                                    '01' => 'Enero',
                                    '02' => 'Febrero',
                                    '03' => 'Marzo',
                                    '04' => 'Abril',
                                    '05' => 'Mayo',
                                    '06' => 'Junio',
                                    '07' => 'Julio',
                                    '08' => 'Agosto',
                                    '09' => 'Septiembre',
                                    '10' => 'Octubre',
                                    '11' => 'Noviembre',
                                    '12' => 'Diciembre',
                                ]
                                ?>
                                @foreach($draws as $mes)
                                    @if(!in_array(date("m", strtotime($mes->draw_date)), $meses))
                                        <?php array_push($meses, date("m", strtotime($mes->draw_date))); ?>

                                        <li class="mes_draws">
                                            <a href="#" class="draws_month">
                                                {{  $mes_esp[date("m", strtotime($mes->draw_date))] }}
                                            </a>
                                            <ul class="fechas_draw">
                                                @foreach($draws as $draw)
                                                    @if(date("m", strtotime($mes->draw_date)) == date("m", strtotime($draw->draw_date)))
                                                        <li id="draw_{{$draw->id}}">
															<a href="#" class="mostrar_draw" draw="{{ $draw->id }}" ruta=" {{ route('sorteos.update', array( 'user_id' => Auth::user()->id, 'draw_id' => $draw->id )) }} " destroy="{{ route('sorteos.destroy', array( 'user_id' => Auth::user()->id, 'draw_id' => $draw->id )) }}" route-draw="route()" li-id="#draw_{{$draw->id}}">{{Carbon\Carbon::parse($draw->draw_date)->format('d-m-Y')}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>


                                    @endif
                                @endforeach
                                <!-- <li>
                                    <input type="text" class="date date_draw form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="" date="" readonly>
                                </li> -->
                            </ul>
                        </div>
                        <div class="divider">
                            @if ($errors->has('start_date'))
                            <div class="input_error">
                                <span>{{ $errors->first('start_date') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-6 control-label" for="textinput">Fecha de inicio</label>
                                <div class="col-md-6">
                                    <input type="text" class="date date_draw form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="start_date_draw" date="">
                                </div>
                            </div>

                            @if ($errors->has('draw_date'))
                            <div class="input_error">
                                <span>{{ $errors->first('draw_date') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-6 control-label" for="textinput">Fecha de sorteo</label>
                                <div class="col-md-6">
                                    <input type="text" class="date form-control input-md" placeholder="Fecha sorteo:" name="draw_date" id="end_date_draw" date="">
                                </div>
                            </div>
                            <div class="calendar">
                                <h4 style="display: none" id="tit_calendar">Días a Sortear</h4>
                                <div class="drawing_days_calendar calendario_medio"></div>
								<div class="input_error none">
									<p></p>
								</div>

                            </div>

                        </div>
                        <div class="divider diass">
							<h3>Días a sortear</h3>
							<div class="input_error none">
								<p></p>
							</div>
                            <ul class="dias">
                            </ul>
                        </div>

                    </div>


                    <!-- Button -->
                    <div class="form-group boton" id="boton_draw" >
                        <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                        <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal" id="cancelar_draw">Cancelar</button>
                        <button type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="" id="destroy_draw" style="display: none">Eliminar</button>
						<button type="button" class="btn btn-primary singlebutton1 modalsortear" ruta="{{ route('sorteos.horarios', array( 'user_id' => Auth::user()->id )) }}" id="sortearConfirm" style="">Sortear</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

	<div class="modal fade" id="modalsorteo" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="confirmacion">
					<div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <h4 class="modal-title">¿Está seguro que desea realizar este sorteo?</h4>
	                </div>
					<div class="modal-body form sorteos">
						<div class="modal-body form">
                            <div class="form-group boton">
                                <button type="button" class="btn btn-primary singlebutton1" id="sortear" ruta="{{ route('sorteos.horarios', array( 'user_id' => Auth::user()->id )) }}">Aceptar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
		                </div>
					</div>
				</div>
				<div class="resultado">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Sorteo</h4>
					</div>
					<div class="modal-body form sorteos">
						<div class="resultado_sorteo">
						</div>
					</div>
				</div>
				<div class="cargando">
					<h1></h1>
				</div>
			</div>
		</div>
	</div>


    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Sorteo</h4>
                </div>
                <div class="modal-body form">

                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar este sorteo?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
	<script src="{{ asset('js/script_draws.js') }}" charset="utf-8"></script>
@endsection

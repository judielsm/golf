
@extends('layouts.app')
@section('content')

<div class="container">



	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Torneos</h2>
		</div>
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif

		<div class="table-responsive success tabla tournament {{ (Auth::user()->role_id == 6) ? 'menos_2' : '' }}">
			<div class="agregar_registro">
				<button type="button" onclick="" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			</div>

			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Fecha de Inicio</th>
						<th>Fecha Final</th>
						<th>Lista de Espera</th>
						<th class="opciones">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $tournaments as $tournament)
						<tr>
							<td>{{ $tournament->name }}</td>
							<td>{{ $tournament->start_date }}</td>
							<td>{{ $tournament->end_date }}</td>
							<td>
								<a href="{{route('torneos.waiting_list', $tournament->id)}}" class="btn btn-primary singlebutton1 btn_lista_espera">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
							</td>
							<td>
								<a href="{{route('torneos.show', $tournament->id)}}" class="btn btn-primary singlebutton1 btn_detalles_torneo">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
								<a href="{{route('torneos.edit', $tournament->id)}}" class="btn btn-primary singlebutton1">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('torneos.destroy', $tournament->id)}}">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>

							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">

						</td>
					</tr>
				</tfoot>

			</table>

		</div>
	</div>

	@if( Auth::user()->role_id == 2 )
		<div class="modal fade" id="modalcreate" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Agregar Torneo</h4>
					</div>
					<div class="modal-body form">
						<form class="form-horizontal" action="{{ route('torneos.store') }}" method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}
							<fieldset>
								@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Nombre</label>
									<div class="col-md-6">

										<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" required pattern="[A-Za-z0-9./,*' ]{3,100}" maxlength="100">

									</div>
								</div>

								@if ($errors->has('start_date'))
								<div class="input_error">
									<span>{{ $errors->first('start_date') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Fecha de inicio</label>
									<div class="col-md-6">
										<input type="text"  placeholder="Fecha de inicio" class="datepicker_mark_disabled date form-control input-md date_tournament" name="start_date" id="start_date_tournament" required>
									</div>
								</div>

								@if ($errors->has('end_date'))
								<div class="input_error">
									<span>{{ $errors->first('end_date') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Fecha final</label>
									<div class="col-md-6">
										<input type="text" placeholder="Fecha final" class="end_datepicker_mark_disabled date form-control input-md" name="end_date" id="end_date_tournament" date=" " required>
									</div>
								</div>

								@if ($errors->has('inscription_fee'))
								<div class="input_error">
									<span>{{ $errors->first('inscription_fee') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Cuota de inscripción</label>
									<div class="col-md-6">
										<input id="inscription_fee" name="inscription_fee" type="text" placeholder="Cuota de inscripción" class="form-control input-md money" required>

									</div>
								</div>

								@if ($errors->has('category_id'))
								<div class="input_error">
									<span>{{ $errors->first('category_id') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Categoría</label>
									<div class="col-md-4">
										<select required multiple name="category_id[]" class="form-control input-md select_categories" id="category">
											<!-- <option value="" disabled selected>Seleccione</option> -->
											@foreach($categories as $category)
												<option value="{{$category->id}}">{{$category->name}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-2">
										<button type="button" id="myBtn" class="btn btn-primary singlebutton1">Crear</button>
									</div>
								</div>

								@if ($errors->has('start_time'))
								<div class="input_error">
									<span>{{ $errors->first('start_time') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Hora de Inicio</label>
									<div class="col-md-6">
										<!-- <input required id="start_time" name="start_time" type="text" placeholder="Hora de Inicio"  class="form-control input-md input_time"> -->
										<select class="js-example-basic-single select_tournament form-control input-md" id="start_time" name="start_time">
											<!-- <option value="" disabled selected>Selecionar Socio</option> -->
										</select>

									</div>
								</div>

								@if ($errors->has('end_time'))
								<div class="input_error">
									<span>{{ $errors->first('end_time') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Hora Final</label>
									<div class="col-md-6">
										<!-- <input required id="end_time" name="end_time" type="text" placeholder="Hora Final" class="form-control input-md input_time"> -->
										<select class="js-example-basic-single select_tournament form-control input-md" id="end_time" name="end_time">
											<!-- <option value="" disabled selected>Selecionar Socio</option> -->
										</select>

									</div>
								</div>

								@if ($errors->has('condition_file'))
								<div class="input_error">
									<span>{{ $errors->first('condition_file') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Condiciones de campeonatos</label>
									<div class="col-md-6">
										<input id="file_torunament" type="file" class="file" name="conditions_file">
									</div>
								</div>

							@if ($errors->has('start_date_inscription'))
							<div class="input_error">
								<span>{{ $errors->first('start_date_inscription') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Fecha de inscripción</label>
								<div class="col-md-6">
									<input type="text"  placeholder="Fecha de inicio" class="date form-control input-md datepicker_mark_disabled_2" name="start_date_inscription" id="start_date_inscription" required>
								</div>
							</div>

							@if ($errors->has('end_date_inscription'))
							<div class="input_error">
								<span>{{ $errors->first('end_date_inscription') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Fecha Tope inscripción</label>
								<div class="col-md-6">
									<input type="text"  placeholder="Fecha final" class="date form-control input-md end_datepicker_mark_disabled_2" name="end_date_inscription" id="end_date_inscription" date=" " required>
								</div>
							</div>


								<!-- Button -->
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" id="modaldelete" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">¿Seguro que desea eliminar este torneo?</h4>
					</div>
					<div class="modal-body form">
						<form class="form-horizontal" method="POST">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<fieldset>
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endif
	<br>
</div>

<!-- Modal para crear Nueva Categoría -->



		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Agregar Categoría</h4>
					</div>
					<div class="modal-body form">
						<form class="form-horizontal" id="capture"	method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}
							<fieldset>

								@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Nombre</label>
									<div class="col-md-6">
										<input id="name" name="name" type="text" placeholder="Nombre de la Categoría" class="form-control input-md" required pattern="[A-Za-z0-9 ./*,-]{3,20}" maxlength="20">
									</div>
								</div>

								@if ($errors->has('number_participants'))
								<div class="input_error">
									<span>{{ $errors->first('number_participants') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Cantidad de Participantes</label>
									<div class="col-md-6">
										<input id="number_participants" name="number_participants" type="text" placeholder="Número de Participantes" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
									</div>
								</div>

								@if ($errors->has('sex'))
								<div class="input_error">
									<span>{{ $errors->first('sex') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Género</label>
									<div class="col-md-6">
										<select required class="form-control input-md" name="sex" id="sex">
											<option value="" disabled selected>Sexo</option>
											<option value="Masculino" >Masculino</option>
											<option value="Femenino" >Femenino</option>
											<option value="Mixto" >Mixto</option>
										</select>
									</div>
								</div>

								@if ($errors->has('handicap_min'))
								<div class="input_error">
									<span>{{ $errors->first('handicap_min') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Handicap Mínimo</label>
									<div class="col-md-6">
										<input id="handicap_min" name="handicap_min" type="text" placeholder="Rango Mínimo" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
									</div>
								</div>

								@if ($errors->has('handicap_max'))
								<div class="input_error">
									<span>{{ $errors->first('handicap_max') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Handicap Máximo</label>
									<div class="col-md-6">
										<input id="handicap_max" name="handicap_max" type="text" placeholder="Rango Máximo" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
									</div>
								</div>

								<!-- Button -->
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="modal fade lista_espera" id="modal_lista_espera" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lista de espera</h4>
				</div>
				<div id="content_listado_espera">
					<table>
						<thead>
							<tr>
								<th colspan="5"><h4>Categoria: categoria 1</h4></th>
							</tr>
							<tr>
								<th class="th_nombre">Nombre</th>
								<th class="th_apellido">Apellido</th>
								<th class="th_ci">C.I.</th>
								<th class="th_n_accion">Nª accion</th>
								<th class="th_telefono">Teléfono</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jose</td>
								<td>Apellido</td>
								<td>22.534.111</td>
								<td>5244555</td>
								<td>0416-241-5211</td>
							</tr>
							<tr>
								<td colspan="5" class="no_inscrito">No hay usuarios en lista de espera</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="detalles_torneo none" id="detalles_torneo">
		<div class="container">
			<div class="item" id="torneo_(id)">
				<div class="division datos">
					<div class="datos_torneo">
						<div class="titulo">
							<h1 id="nombre">

							</h1>
						</div>
						<div class="inscripcion">
							<h3>Inscripciones</h3>

								<div class="fechas">
									<h4 id="fecha_inscripcion">Desde: <span></span></h4>
									<h4 id="fecha_tope_inscripcion">Hasta: <span></span></h4>

									<h3>Torneo en Proceso de Inscripcion</h3>
								</div>
							<div class="mod_cat">

								<div class="">
									<h3>Categorías</h3>
									<div id="categorias">

									</div>
								</div>
							</div>
							<div class="fechas">
								<h3>Fechas del torneo</h3>
								<h4 id="fecha_inicio">Desde: <span></span></h4>
								<h4 id="fecha_fin">Hasta: <span></span></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="division condiciones">
					<input id="condicion_torneo" type="file" class="file condicion_torneo" name="conditions_file" value="Archivo de condicion">
				</div>
			</div>
		</div>
		<div class="cerrar_detalles_torneo">
			<i class="glyphicon glyphicon-remove"></i>
		</div>
	</div>




	<script type="text/javascript">

		var draws = {!! $draws !!};
		var tournaments = {!! $tournaments !!};
		var day_blockeds = {!! $day_blockeds !!};

		var value_start_time = null;
		var value_end_time = null;

	</script>
@endsection

@section('script')
	<script src="{{ asset('js/script_tournament.js') }}" charset="utf-8"></script>
@endsection

<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class MonthlyPayment extends Model
{

	use SoftDeletes;
	use Sortable;


   protected $fillable = ['number_receipt','amount','observation','student_id','date_payment','method_payment','period_id', 'member_id'];

   public function period()
	{
		return $this->hasOne('Golf\Period', 'id', 'period_id');
	}

	public function student()
	{
		return $this->hasOne('Golf\Student', 'id', 'student_id');
	}

	public function member()
	{
		return $this->hasOne('Golf\Member', 'id', 'member_id');
	}
}

<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Transaction extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'type_payment','amount','type_product','enabled','product_id','payer_id',
	];
	protected $sortable = [
		'type_payment','amount','type_product','enabled','product_id','payer_id',
	];

	public function payer()
	{
		return $this->belongsTo('Golf\Member', 'payer_id','id'); //->where('type_payer','1');
	}
}

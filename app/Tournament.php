<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Tournament extends Model
{
	use SoftDeletes;
	use Sortable;

	protected $dates = [
        'start_date',
		'end_date',
		'start_date_inscription',
		'end_date_inscription',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

	protected $fillable = [
		'name', 'start_date', 'end_date', 'start_date_inscription', 'end_date_inscription', 'inscription_fee', 'number_participants', 'modality_id', 'conditions_file', 'enabled',
	];
	protected $sortable = [
		'name', 'start_date', 'end_date', 'start_date_inscription', 'end_date_inscription', 'inscription_fee', 'number_participants', 'modality_id', 'enabled',
	];


	public function setDateAttribute($value)
	{
		$this->attributes['date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function setStartDateInscriptionAttribute($value)
	{
		$this->attributes['start_date_inscription'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getStartDateInscriptionAttribute()
	{
		return date( 'd-m-Y', strtotime($this->attributes['start_date_inscription']) );
	}


	public function setEndDateInscriptionAttribute($value)
	{
		$this->attributes['end_date_inscription'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getEndDateInscriptionAttribute()
	{
		return date( 'd-m-Y', strtotime($this->attributes['end_date_inscription']) );
	}


	public function setStartDateAttribute($value)
	{
		$this->attributes['start_date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function setEndDateAttribute($value)
	{
		$this->attributes['end_date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getStartDateAttribute()
	{
		return date( 'd-m-Y', strtotime($this->attributes['start_date']) );
	}

	public function getEndDateAttribute()
	{
		return date( 'd-m-Y', strtotime($this->attributes['end_date']) );
	}

	public function getDateAttribute()
	{
		return date( 'd-m-Y', strtotime($this->attributes['date']) );
	}

	public function setStartTimeAttribute($value)
	{
		$this->attributes['start_time'] = date( 'H:i', strtotime($value) );
	}

	public function getStartTimeAttribute()
	{
		return date( 'h:i A', strtotime($this->attributes['start_time']) );
	}
	public function setEndTimeAttribute($value)
	{
		$this->attributes['end_time'] = date( 'H:i', strtotime($value) );
	}

	public function getEndTimeAttribute()
	{
		return date( 'h:i A', strtotime($this->attributes['end_time']) );
	}


	public function categories()
	{
			return $this->hasMany('Golf\Category', 'tournament_id', 'id');
	}
	public function tournament_category()
	{
		return $this->hasOne('Golf\TournamentCategory', 'tournament_id', 'id');
	}
	public function tournament_inscriptions()
	{
		return $this->hasMany('Golf\TournamentInscriptionMembers', 'tournament_id', 'id');
	}

	public function start_times()
	{
		return $this->belongsToMany('Golf\StartTime','tournament_categories')
			->withPivot('category_id','enabled');
	}
	public function modality()
	{
			return $this->belongsTo('Golf\Modality', 'modality_id', 'id');
	}

}

<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class PartnerReservation extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'enabled', 'reservation_id', 'partner_id',
	];
	protected $sortable = [
		'enabled', 'reservation_id', 'partner_id',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function partners()
	{
		return $this->belongsTo('Golf\Partner', 'partner_id', 'id');
	}

	public function reservations()
	{
		return $this->belongsTo('Golf\Reservation', 'reservation_id', 'id');
	}
	public function green()
	{
		return $this->belongsTo('Golf\ReservationDefinition', 'green_id', 'id')->withTrashed();
	}

}

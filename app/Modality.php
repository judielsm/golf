<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    protected $table = 'modalities';
      protected $primaryKey= 'id';
}

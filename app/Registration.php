<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Registration extends Model
{

	use SoftDeletes;
	use Sortable;


	protected $fillable = [
		'payment_plan_id','group_id','student_id'
	];

	protected $sortable = [
		'payment_plan_id','group_id','student_id'
	];
	
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function student()
	{
		return $this->belongsTo('Golf\Student', 'student_id', 'id');
	}

	public function group()
	{
		return $this->belongsTo('Golf\Group', 'group_id', 'id');
	}

	public function payment_plan()
	{
		return $this->belongsTo('Golf\PaymentPlan', 'payment_plan_id', 'id');
	}

	public function getCreatedAtAttribute()
	{
		return date( 'd-m-Y H:i', strtotime($this->attributes['created_at']) );
	}
}

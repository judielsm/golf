<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class TournamentInscriptionMembers extends Model
{

	use SoftDeletes;
	use Sortable;

protected $table = 'tournament_inscription_members';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'member_id', 'enabled', 'category_id', 'waiting'
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $sortable = [
		'member_id', 'enabled', 'category_id', 'waiting'
	];
	
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function member()
	{
		return $this->belongsTo('Golf\Member', 'member_id', 'id');
	}

	public function tournament()
	{
		return $this->belongsTo('Golf\Tournament', 'tournament_id', 'id');
	}

	public function category()
	{
		return $this->belongsTo('Golf\Category', 'category_id', 'id');
	}

	public function getCreatedAtAttribute()
	{
		return date( 'd-m-Y H:i', strtotime($this->attributes['created_at']) );
	}

}

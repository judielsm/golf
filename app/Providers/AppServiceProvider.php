<?php

namespace Golf\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Route::resourceVerbs([
			'index' => 'todos',
			'create' => 'crear',
			'store' => 'almacenar',
			'edit' => 'editar',
			'update' => 'actualizar',
			'destroy' => 'eliminar',
		]);
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}

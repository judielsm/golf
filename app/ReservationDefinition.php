<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class ReservationDefinition extends Model
{

	use SoftDeletes;
	use Sortable;


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'green_fee', 'enabled'
	];
	protected $sortable = [
		'name', 'green_fee', 'enabled'
	];
	protected $dates = ['deleted_at'];
	public function reservations()
	{
		return $this->hasMany('Golf\Reservation', 'id', 'reservation_id');
	}
	
	
}

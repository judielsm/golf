<?php

namespace Golf\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Golf\EmailJob;
use Mail;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       \Golf\Console\Commands\DayUnblocked::class,
       \Golf\Console\Commands\DrawUnblocked::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function(){

            
                $tasks = EmailJob::where('intents','<',5)->limit(10)->get();
                foreach($tasks as $task){
                    try{
                        $parametros = json_decode($task->data , true);
                        $email = $task->to;
                        $nombre_destinatario =$task->recipient;
                        $subject = $task->subject;
                         Mail::send($task->template, $parametros, function ( $m ) use ( $email, $nombre_destinatario,$subject  ) {

                                $m->from(env('MAIL_USERNAME'), 'Reservación Lagunita');
                                $m->to($email, $nombre_destinatario)->subject($subject);
                            });
                           

                        $task->delete();
                       
                    }catch(\Exception $e){
                        $task->intents +=1 ;
                        $task->save();
                    }
                }
                
        })->everyTenMinutes();

        $schedule->command('desbloquear:dia')->fridays()->at('06:00:00');  
        $schedule->command('desbloquear:sorteo')->thursdays()->at('16:00:00');     
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

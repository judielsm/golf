<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Group extends Model
{

	use SoftDeletes;
	use Sortable;


	protected $fillable = [
		'name','price','teacher_id'
	];
	protected $sortable = [
		'name','price','teacher_id'
	];



	public function teacher()
	{
		return $this->hasOne('Golf\Teacher', 'id', 'teacher_id');
	}
	
	public function registrations()
	{
		return $this->hasMany('Golf\Registration', 'group_id', 'id');
	}

}

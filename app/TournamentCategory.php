<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class TournamentCategory extends Model
{

	use SoftDeletes;
	use Sortable;


	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	
	protected $fillable = [
		'enabled', 'category_id', 'tournament_id',
	];
	public function categories()
	{
		return $this->hasMany('Golf\Category', 'category_id', 'id');
	}
	public function tournament()
	{
		return $this->hasOne('Golf\Tournament', 'tournament_id', 'id');
	}
}

<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Gbrock\Table\Traits\Sortable;

class TimeBlocked extends Model
{
    use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'start_time','end_time','day_blocked_id'
	];
	protected $sortable = [
		'start_time','end_time','day_blocked_id'
	];

	public function day_blocked()
	{
		return $this->hasOne('Golf\DayBlocked', 'id', 'day_blocked_id');
	}

}

<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Reservation extends Model
{
	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'receipt_payment', 'detail', 'date', 'start_time', 'total_green_fee', 'status', 'enabled', 'member_id', 'reservation_definition_id'
	];
	protected $sortable = [
		'date', 'start_time','status', 'created_at'
	];

	public function setDateAttribute($value)
	{
		$this->attributes['date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getDateAttribute()
	{
		return date( 'd-m-Y', strtotime($this->attributes['date']) );
	}

	public function setStartTimeAttribute($value)
	{
		$this->attributes['start_time'] = date( 'H:i', strtotime($value) );
	}

	public function getStartTimeAttribute()
	{
		return date( 'h:i A', strtotime($this->attributes['start_time']) );
	}

	public function members()
	{
		return $this->belongsToMany('Golf\Member', 'member_reservations')->withTimestamps();
	}

	public function user()
	{
		return $this->belongsTo('Golf\User', 'user_id', 'id');
	}

	public function reservation_definition()
	{
		return $this->belongsTo('Golf\ReservationDefinition', 'reservation_definition_id', 'id');
	}

	public function partners()
	{
		return $this->belongsToMany('Golf\Partner', 'partner_reservations')->withTimestamps();
	}

	public function day_draws()
	{
		return $this->belongsToMany('Golf\DayDraw', 'draw_reservations');
	}

	public function draw_reservation()
	{
		return $this->hasOne('Golf\DrawReservation', 'reservation_id', 'id');
	}
	public function starter_process()
	{
		return $this->belongsTo('Golf\StarterProcess', 'id', 'reservation_id');
	}
	public function comments()
	{
		return $this->hasMany('Golf\CommentsReservation');
	}

}

<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Gbrock\Table\Traits\Sortable;

class DayBlocked extends Model
{
    use SoftDeletes;
	use Sortable;

    protected $fillable = [
		'date', 'observation'
	];
	protected $sortable = [
		'date', 'observation'
	];

    public function time_blockeds()
	{
		return $this->hasMany('Golf\TimeBlocked', 'day_blocked_id', 'id');
	}
}

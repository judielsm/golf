<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EmailJob extends Model
{
  	use SoftDeletes;
    
protected $dates = ['deleted_at'];
}

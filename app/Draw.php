<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Draw extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'start_date', 'draw_date', 'enabled'
	];

	protected $sortable = [
		'start_date', 'draw_date',
	];

	public function setStartDateAttribute($value)
	{
		$this->attributes['start_date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getStartDateAttribute()
	{
		return date( 'Y-m-d', strtotime($this->attributes['start_date']) );
	}

	public function setDrawDateAttribute($value)
	{
		$this->attributes['draw_date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getDrawDateAttribute()
	{
		return date( 'Y-m-d', strtotime($this->attributes['draw_date']) );
	}

	public function day_draws()
	{
		return $this->hasMany('Golf\DayDraw', 'draw_id', 'id');
	}
}

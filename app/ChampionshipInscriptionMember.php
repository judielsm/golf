<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class ChampionshipInscriptionMember extends Model
{

	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'id', 'member_id', 'championship_id', 'enabled',
	];

	public function member()
	{
		return $this->belongsTo('Golf\Member', 'id', 'member_id');
	}

	public function championship()
	{
		return $this->belongsTo('Golf\Championship', 'id', 'championship_id');
	}
}

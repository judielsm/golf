<?php  

namespace Golf\Http\Util;
use Mail;
use Golf\Member;
/**
* 
*/
class MailToMember 
{
	
	public static function send($members,$reservation)
	{

		foreach ($members as $member) {
			$memb = Member::find($member);

			Mail::send('emails.reservation', ['member' => $memb, 'reservation' =>$reservation], function ($m) use ($memb) {
	            $m->from(env('MAIL_USERNAME'), 'Reservacion Lagunita');

	            $m->to($memb->user->email, $memb->name)->subject('Reservacion');
	        });
		}//$memb->user->email
		
	}
}
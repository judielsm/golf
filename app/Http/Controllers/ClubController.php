<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClubController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$clubs = Club::where([])->sorted()->get();
		$table_clubs = Table::create($clubs, ['name' => 'Nombre']);
		$table_clubs->addColumn('edit', 'Editar', function($club) {
			$ruta = route('clubes.edit', $club->id);
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_clubs->addColumn('delete', 'Eliminar', function($club) {
			$ruta = route('clubes.destroy', $club->id);
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});



		return view('clubs.all_clubs', ['clubs' => $clubs, 'table_clubs' => $table_clubs] );
	}

	public function post()
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$clubs = ['clubs' => Club::get()];

		response()->json($clubs, 200);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store ( Request $request )
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$club = new Club();

			$club->name = $request->name;
			$club->green_fee = $request->green_fee;


			if ( !$club->save() ) {
				return redirect()->route( 'clubes.index' )
								->with( [ 'status' => 'Error al registrar el club ', 'type' => 'error' ] )
								->withInput();
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);
			return redirect()->route( 'clubes.index', array( 'user_id' => Auth::user()->id ) )
							->with( [ 'status' => 'Error al registrar sorteo 4', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();
		return redirect()->route( 'clubes.index' )
						->with( [ 'status' => 'El club se ha creado exitosamente', 'type' => 'success' ] );
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ( $club_id )
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$club = Club::where([
							[ 'id', '=', $club_id ]
						])->first();

		return view('clubs.edit_club', ['club' => $club]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $club_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $club_id)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$club = Club::where([
								[ 'id', '=', $club_id ]
							])->first();

			$club->name = $request->name;
			$club->green_fee = $request->green_fee;

			if ( !$club->save() ) {
				return redirect()->route('clubes.index')
					->with(['status' => 'Se ha actualizado satisfactoriamente el club', 'type' => 'success']);
			} else {
				return redirect()->route('edit_club')
					->with(['status' => 'Error al actualizar los datos el club', 'type' => 'error'])
					->withInput();
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);
			return redirect()->route( 'clubes.index', array( 'user_id' => Auth::user()->id ) )
							->with( [ 'status' => 'Error al registrar sorteo 4', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();
		return redirect()->route('clubes.index')
				->with(['status' => 'Se ha actualizado satisfactoriamente el club', 'type' => 'success']);
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ( $club_id )
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$club = Club::where([
							[ 'id', '=', $club_id ]
						])->first();

		$name = $club->name.' '.$club->last_name;

		if ($club->delete()) {
			return redirect()->route('clubes.index')
				->with(['status' => 'Se ha eliminado satisfactoriamente', 'type' => 'success']);
		} else {
			return redirect()->route('clubes.index')
				->with(['status' => 'Error al eliminar el club', 'type' => 'error']);
		}
	}
}

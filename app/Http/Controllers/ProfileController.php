<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Golf\User;
use Golf\Member;
use Auth;
use Hash;
use DB;
class ProfileController extends Controller
{

	/**
	 * return view with form.
	 *
	 * @param  null
	 * @return View
	 */
    public function index(){

    	return view('profile.update');
    }
    public function profile(){
    	$user= Member::where('user_id',Auth::user()->id)->first();
    	
    	return view('profile.data')->with('member',$user);
    }

    /**
	 * Update .
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
    public function update_pass(Request $request){
    	if ($request->new != $request->conf) {
    		return redirect('perfil/password')->with('status', 'Las confirmaciones no coinciden')->with('type', 'danger');
    	}
    	$new  = $request->new;
    	$conf = $request->conf;
    	$user = Auth::user()->password;

    	if (Hash::check($request->old, $user)) {
    		try{
    			$update = User::where('id',Auth::user()->id)
    				->update(['password' => Hash::make($request->new)]);
    			return redirect('perfil/password')->with('status', 'Contraseña actualizada correctamente')->with('type', 'success');
    		}catch(\Exception $e){

    		}
    		

    	}else{
    		return redirect('perfil/password')->with('status',  ' Contraseña actual incorrecta')->with('type', 'danger');
    	}

    }
    public function update_profile(Request $request){
		DB::beginTransaction();
		try {
			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:255',
				'last_name' => 'required|min:3|max:255',
				'identity_card' => 'required|numeric|min:6',
				'phone' => 'required|min:5',
				// 'password' => 'required|min:6|confirmed',
			]);
			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}
			$member = Member::where([
								[ 'user_id', '=', Auth::user()->id]
							])->first();
			if (empty($request->name)) {
				$member->name = $member->name;
			}
			else{
				$member->name = ucwords(strtolower($request->name));
			}
			if (empty($request->last_name)) {
				$member->last_name = $member->last_name;
			}
			else{
				$member->last_name = ucwords(strtolower($request->last_name));
			}
			if (empty($request->identity_card)) {
				$member->identity_card = $member->identity_card;
			}
			else{
				$member->identity_card = $request->identity_card;
			}
			if (empty($request->phone)) {
				$member->phone = $member->phone;
			}
			else{
				$member->phone = $request->phone;
			}
			$member->user->name= $member->name.' '.$member->last_name;
    		
			if ($member->user->save()) {
				if (!$member->save()) {
					throw new \Exception('Error al actualizar los datos del usuario', 400);
				}
			} else {
				throw new \Exception('Error al actualizar los datos del socio', 400);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect('perfil/datos')
							->with(['status' => $e->getMessage(),
								'line' => $e->getLine(),
								'code' => $e->getCode(),
								'input' => $request->input(),
								'type' => 'error']);
		}
		DB::commit();
		return redirect('perfil/datos')
						->with([ 'status' => 'Datos actualizados correctamente ', 'type' => 'success' ]);
    }
}

<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use Log;
use Golf\Category;
use Golf\DayBlocked;
use Golf\Member;
use Golf\Modality;
use Golf\StartTime;
use Golf\Draw;
use Golf\Tournament;
use Golf\TournamentCategory;
use Golf\TournamentInscription;
setlocale(LC_TIME, 'es_ES.UTF-8');


class TournamentController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		$draws = Draw::where([
			['draw_date', '>=', Carbon::now()->format('Y-m-d')],
			['draw_status', '=', 0]
		])->get();
		foreach ($draws as $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $day_draw) {
				$day_draw->time_draws;
			}
		}
		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();
		$categories = Category::get();
		$day_blockeds = DayBlocked::with('time_blockeds')->get();
		//$modalities = Modality::get();
		/* Creando Tabla */

		return view('tournaments.all_tournaments', [
			'tournaments' => $tournaments,
			'categories' => $categories ,
			'draws' => $draws,
			'day_blockeds' => $day_blockeds,
		]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//dd($request->request);
		$category_ids = array();
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		DB::beginTransaction();
		try {
			//TODO: validar si la fecha final es mayor o igual que la inicial
			foreach($request->category_id as $cat){
				if (gettype(json_decode($cat)) == "object") {
					$category= new Category();
					$cat_decode=json_decode($cat);

					$category->name=$cat_decode->name;
					$category->sex=$cat_decode->sex;
					$category->number_participants=$cat_decode->number_participants;
					$category->handicap_min=$cat_decode->handicap_min;
					$category->handicap_max=$cat_decode->handicap_max;
					$category->departure_order =0;
					if(!$category->save()) {
						throw new \Exception('Error al almacenar torneo.', 400);
					}
					array_push($category_ids, $category->id);
				}else{
					array_push($category_ids,  $cat);
				}
			}

			$tournament = new Tournament();
			$tournament->name = $request->name;
			$tournament->start_date = $request->start_date;
			$tournament->end_date = $request->end_date;
			$tournament->inscription_fee = floatval($request->inscription_fee);
			$tournament->start_time = $request->start_time;
			$tournament->end_time = $request->end_time;

			if($file = $request->hasFile('conditions_file')) {

				$file = $request->file('conditions_file');
				$file_name = $file->getClientOriginalName();
				$file_name = date('ymdhis').'-Conditions_rules-'.$tournament->name.'.'.$file->getClientOriginalExtension();
				$destination_path = public_path().'/assets/tournaments/files/';
				if ($file->move($destination_path,$file_name)) {
					$tournament->conditions_file = 'assets/tournaments/files/'.$file_name;
				} else {
					throw new Exception('Error al subir el archivo', 400);
				}
			}
			$tournament->start_date_inscription= $request->start_date_inscription;
			$tournament->end_date_inscription= $request->end_date_inscription;


			if ($tournament->save()) {
				foreach($category_ids as $category){
					$tournament_category = new TournamentCategory();
					$tournament_category->category_id = gettype($category) == "object" ? $category->id : $category;
					$tournament_category->tournament_id = $tournament->id;
					if (!$tournament_category->save()) {
						throw new \Exception('Error al almacenar torneo.', 400);
					}
				}
			}
		} catch (\Exception $e) {
			DB::rollback();

			Log::info("Exception: ".$e->getMessage().
						" File: ".$e->getFile()." Line: ".$e->getLine().
						" Trace: ".$e->getTraceAsString()." Line: ".$e->getLine());
			$response['response'] = false;
			$response['type'] = 'error';
			$response['status'] = $e->getMessage();
			$response['code'] = $e->getCode();
			$response['line'] = $e->getLine();
			$response['file'] = $e->getFile();
			return redirect()->route('torneos.index')
							->with(['status' => $e->getMessage(),
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}
		DB::commit();
		return redirect()->route('torneos.index')
			->with(['status' => 'Se ha agregado satisfactoriamente el torneo', 'type' => 'success']);
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($tournament_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		$tournament = Tournament::where([
							[ 'id', '=', $tournament_id ]
						])->first();

		$tournament->categories = Category::where('tournament_categories.tournament_id', $tournament_id)
									->where('tournament_categories.deleted_at', null)
									->join('tournament_categories','tournament_categories.category_id','=','categories.id')
									->get();

		//$modalities = Modality::get();
		$categories = Category::all();
		// dd($tournament);

		// $tournaments = Tournament::where([])->sorted()->orderBy('start_date', 'ASC')->paginate(10);
		// $tournament_dias_habiles = [];
		// foreach ($tournaments as $tournament_) {
		// 	$start_date = Carbon::parse($tournament_->start_date);
		// 	$end_date = Carbon::parse($tournament_->end_date);
		// 	for ($i=0; $i < $start_date->diffInDays($end_date)+1; $i++) {
		// 		array_push($tournament_dias_habiles, $start_date->addDay($i)->format('Y-m-d'));
		// 	}
		// }


		$draws = Draw::where([
			['draw_date', '>=', Carbon::now()->format('Y-m-d')],
			['draw_status', '=', 0]
		])->get();
		foreach ($draws as $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $day_draw) {
				$day_draw->time_draws;
			}
		}
		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();
		$day_blockeds = DayBlocked::with('time_blockeds')->get();





		// dd($tournament_dias_habiles);
		return view('tournaments.edit_tournament', [
								//'modalities' => $modalities,
								'tournament' => $tournament,
								'categoris' => $categories,
								'tournaments' => $tournaments,
								'draws' => $draws,
								'day_blockeds' => $day_blockeds,


							]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $tournament_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $tournament_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		//dd($request->input());
		$category_ids = array();
		DB::beginTransaction();
		try {
			foreach($request->category_id as $cat){
				if (gettype(json_decode($cat)) == "object") {
					$category= new Category();
					$cat_decode=json_decode($cat);

					$category->name=$cat_decode->name;
					$category->sex=$cat_decode->sex;
					$category->number_participants=$cat_decode->number_participants;
					$category->handicap_min=$cat_decode->handicap_min;
					$category->handicap_max=$cat_decode->handicap_max;
					$category->departure_order =0;
					if(!$category->save()) {
						throw new \Exception('Error al almacenar torneo.', 400);
					}
					array_push($category_ids, $category->id);
				}else{
					array_push($category_ids,  $cat);
				}
			}
			$tournament = Tournament::where([
								[ 'id', '=', $tournament_id ]
							])->first();

			$conditions_file = $tournament->conditions_file;
			$tournament->name = $request->name;
			// dd($tournament->start_date);
			$tournament->start_date = $request->start_date;
			$tournament->end_date = $request->end_date;
			$tournament->inscription_fee = floatval($request->inscription_fee);
		//	$tournament->modality_id = $request->modality_id;
			$tournament->start_time = $request->start_time;
			$tournament->end_time = $request->end_time;

			if($file = $request->hasFile('conditions_file')) {

				$file = $request->file('conditions_file');
				//	dd($request->file('conditions_file'));
				$file_name = $file->getClientOriginalName();
				$file_name = date('ymdhis').'-Conditions_rules-'.$tournament->name.'.'.$file->getClientOriginalExtension();
				$destination_path = public_path().'/assets/tournaments/files/';
				if ($file->move($destination_path,$file_name)) {
					$tournament->conditions_file = 'assets/tournaments/files/'.$file_name;
				} else {
					throw new \Exception('Error al subir el archivo.', 400);
				}
			}
			$tournament->start_date_inscription= $request->start_date_inscription;
			$tournament->end_date_inscription= $request->end_date_inscription;

		/*	foreach ($tournament->categories as $key => $category) {
				$category->delete();
			}*/

			if ($tournament->save()) {

				$tournament->where('id', '=', $tournament->id)->first();

				//validar que envia categorias
				if (empty($request->category_id)) {
					throw new \Exception('Error: no se encontró las categorias del torneo.', 400);
				}
				else
				{
					/*foreach ($request->category_id as $category_id) {

						$category_decode=json_decode($category_id);

						$category = Category::where([
										['tournament_id', '=', $tournament->id ],
										['id', '=', $category_decode ]
									])->withTrashed()->first();
						if ($category) {
							if (!$category->restore()) {
								throw new \Exception('Error al actualizar las categorias del torneo.', 400);
							}
						} else {
							throw new \Exception('Error: al obtener las categorias del torneo.', 400);
						}
					}*/
					$tournament_categories = TournamentCategory::where('tournament_id',$tournament->id )->get();
					foreach ($tournament_categories as $key => $tournament_category) {
						$tournament_category->delete();
					}
					foreach ($category_ids as $key => $categoria) {
						if (gettype($categoria)=="object") {
							$tc = new TournamentCategory();
							$tc->category_id = $categoria->id;
							$tc->tournament_id = $tournament->id;
							if (!$tc->save()) {
								throw new \Exception('Error al actualizar el torneo', 400);
							}
						}
						else{
							$t =TournamentCategory::where([
								['category_id','=' ,$categoria],
								['tournament_id','=' ,$tournament->id]
								] )->withTrashed()->first();
 							$t->restore();
						}
					}
					if (empty($conditions_file)) {
						if (!File::delete($conditions_file)) {
							throw new \Exception('Error al actualizar las condiciones del torneo.', 400);
						}

					}
				}
			}else {
				throw new \Exception('Error al actualizar el torneo', 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			// dd($e);
			return redirect()->route('torneos.edit', array('tournament_id' => $tournament_id))
							->with(['status' => $e->getMessage(),
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}

		DB::commit();
		return redirect()->route('torneos.index')
			->with(['status' => 'Se ha actualizado satisfactoriamente el torneo', 'type' => 'success']);

	}
	public function show($tournament_id)
	{
		try {
			$tournament = Tournament::find($tournament_id);
			$tournament->categories = Category::where('tournament_categories.tournament_id', $tournament_id)
									->where('tournament_categories.deleted_at', null)
									->join('tournament_categories','tournament_categories.category_id','=','categories.id')
									->get();
			//$tournament->modality;
			//$tournament->categories;
			if (empty($tournament)) {
				throw new \Exception("Error al obtener el recurso", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getCode();
			return response()->json($response);
		}

		return response()->json(['status' => 'Exito!',
								'tournament' => $tournament,
								'type' => 'success']);
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($tournament_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$tournament = Tournament::where([
								[ 'id', '=', $tournament_id ]
							])->first();

			$delete = true;

			foreach ($tournament->categories as $key => $category) {
				if (!$category->delete()) {
					throw new \Exception('Error al eliminar el el torneo.', 400);
				}
			}
			if (!$tournament->delete()) {
				throw new \Exception('Error al eliminar el el torneo.', 400);
			}
		} catch(\Exception $e)
		{
			DB::rollback();

			return redirect()->route('torneos.index')
							->with(['status' => $e->getMessage(),
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}

		DB::commit();

		return redirect()->route('torneos.index')
			->with(['status' => 'Se ha eliminado satisfactoriamente el torneo', 'type' => 'success']);
	}

	public function detalles(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		// $request->id;
		$tournament = Tournament::find($request->tournament_id);
		$tournament->categories =  Category::where('tournament_categories.tournament_id', $request->tournament_id)
									->where('tournament_categories.deleted_at', null)
									->join('tournament_categories','tournament_categories.category_id','=','categories.id')
									->get();
		//$tournament->modality;

		return response()->json($tournament);
	}

	public function check_reservation(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		try {
			$date = Carbon::createFromFormat('d-m-Y', $request->date);

			$tournament = Tournament::whereDate('date', $date->toDateString())
						->first();

			if (!empty($tournament)) {
				throw new \Exception("Ya existe un torneo para la fecha seleccionada", 400);
			}

		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getCode();
			return response()->json($response);

		}

		return response()->json($response);
		// $request->id;
		$tournament = Tournament::find($request->tournament_id);
		$tournament->categories;
		$tournament->modality;

		return response()->json($tournament);
	}

	public function waiting_list($tournament_id)
	{
		$user = Auth::user();
		if ($user->role_id != 2) {
			return redirect()->route('home');
		}
		try {

			$categories = Category::where('tournament_categories.tournament_id', $tournament_id)
									->where('tournament_categories.deleted_at', null)
									->join('tournament_categories','tournament_categories.category_id','=','categories.id')
									->get();
			foreach ($categories as $key => $category) {
				$count = Member::where('category_id', $category->id)
								->join('tournament_inscription_members', 'tournament_inscription_members.member_id', '=', 'members.id')
								->count();
				$category->members = Member::where('category_id', $category->id)
								->join('tournament_inscription_members', 'tournament_inscription_members.member_id', '=', 'members.id')
								->take($count)
								->skip($category->number_participants)
								->get();
								//var_dump($category->members);
			}
		} catch (\Exception $e) {
			return response()->json(['status' => $e->getMessage(),
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}
		return response()->json(['status' => true,
								'categories' => $categories,
								'code' => 200,
								'type' => 'success']);
	}


	public function all_tournaments()
	{
		// dd('');
		try {
			$tournaments = Tournament::where('end_date', '>=', Carbon::now()->format('Y-m-d') )->get();
		} catch (\Exception $e) {
			return response()->json(['status' => $e->getMessage(),
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}

		return response()->json(['status' => true,
								'tournaments' => $tournaments,
								'code' => 200,
								'type' => 'success']);
	}
}

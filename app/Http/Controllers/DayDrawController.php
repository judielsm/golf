<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Gbrock\Table\Facades\Table;
use Golf\DayDraw;
use Golf\DrawReservation;
use Golf\TimeDraw;
use Golf\User;

class DayDrawController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		// $day_draws = DayDraw::where([])->with('time_draws')->sorted('date','asc')->sorted('start_time','asc')->paginate(10);;
		$day_draws = DayDraw::where([])->with('time_draws')->sorted()->orderBy('id', 'desc')->get();

		$table_day_draws = Table::create($day_draws, ['id' => 'Id', 'date' => 'Fecha']);

		$table_day_draws->addColumn('edit', 'Editar', function($day_draw) {
			$ruta = route('dias_sorteables.edit', array( 'user_id' => Auth::user()->id, 'day_draw_id' => $day_draw->id ));
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_day_draws->addColumn('delete', 'Eliminar', function($day_draw) {
			$ruta = route('dias_sorteables.destroy', array( 'user_id' => Auth::user()->id, 'day_draw_id' => $day_draw->id ));
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});

		//dd($day_draws);

		return view('day_draws.all_day_draws', [
				'day_draws' => $day_draws,
				'table_day_draws' => $table_day_draws,
			] );
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}
		// dd($request->array);

		DB::beginTransaction();

		try {

			$day_draws = json_decode($request->day_draws);

			//	dd($day_draws);

			foreach ($day_draws as $key => $value) {

				$day_draw = new DayDraw();
				$day_draw->date = $value->date;

				if ($day_draw->save()) {

					foreach ($value->times as $key => $time) {
						$time_draw = new TimeDraw();
						$time_draw->time = $time;
						$time_draw->day_draw_id = $day_draw->id;

						if ( !$time_draw->save() ) {

							DB::rollback();

							return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
											->with( [ 'status' => 'Error al registrar dia de sorteo 1', 'type' => 'error' ] )
											->withInput();
						}
					}

				}else {
					return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
									->with( [ 'status' => 'Error al registrar dia de sorteo 2', 'type' => 'error' ] )
									->withInput();
				}
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);

			return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
							->with( [ 'status' => 'Error al registrar dia de sorteo 4', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();
		return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
						->with( [ 'status' => 'El dia de sorteo se ha creado exitosamente', 'type' => 'success' ] )
						->withInput();
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $user_id, $day_draw_id )
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$day_draw = DayDraw::where([
							[ 'id', '=', $day_draw_id ]
						])->with('time_draws')->first();

		// dd($day_draw);

		return view('day_draws.edit_day_draw', ['day_draw' => $day_draw]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $day_draw_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $user_id, $day_draw_id)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$request->day_draw = ["date" => "07-03-2017", "times" => ["9:00", "10:10", "9:20", "10:30"] ];
			//dd($prueba_day_draw);

			$day_draw = DayDraw::where([
								[ 'id', '=', $day_draw_id ]
							])->first();

			$request_day_draw = json_decode($request->day_draw);

			$day_draw->date = $request_day_draw->date;

			if ($day_draw->save()) {

				foreach ($request_day_draw->times as $key => $time) {

					$time_draw = TimeDraw::where([
									["time", "=", $time ],
									["day_draw_id", "=", $day_draw->id ]
								])->withTrashed()->first();

					if ( $time_draw ) {
						if ( !$time_draw->restore() ) {

							DB::rollback();

							return redirect()->route( 'dias_sorteables.edit', array( 'user_id' => Auth::user()->id, 'day_draw_id' => $day_draw_id ) )
											->with( [ 'status' => 'Error al actualizar el dia de sorteo 5', 'type' => 'error' ] )
											->withInput();

						}
					}
					else {

						$time_draw = new TimeDraw();
						$time_draw->time = $time;
						$time_draw->day_draw_id = $day_draw->id;

						if ( !$time_draw->save() ) {

							DB::rollback();

							return redirect()->route( 'dias_sorteables.edit', array( 'user_id' => Auth::user()->id, 'day_draw_id' => $day_draw_id ) )
											->with( [ 'status' => 'Error al actualizar el dia de sorteo 1', 'type' => 'error' ] )
											->withInput();
						}

					}
				}

			}
			else {
				return redirect()->route( 'dias_sorteables.edit', array( 'user_id' => Auth::user()->id, 'day_draw_id' => $day_draw_id ) )
								->with( [ 'status' => 'Error al actualizar el dia de sorteo 2', 'type' => 'error' ] )
								->withInput();
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);

			return redirect()->route( 'dias_sorteables.edit', array( 'user_id' => Auth::user()->id, 'day_draw_id' => $day_draw_id ) )
							->with( [ 'status' => 'Error al actualizar dia de sorteo 4', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();
		return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
						->with( [ 'status' => 'El dia de sorteo se ha actualizado exitosamente', 'type' => 'success' ] )
						->withInput();
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $user_id, $day_draw_id )
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$day_draw = DayDraw::where([
								[ 'id', '=', $day_draw_id ]
							])->with('time_draws')->first();

			foreach ($day_draw->time_draws as $key => $time_draw) {

				if ( !$time_draw->delete() ) {
					DB::rollback();
					return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
									->with( [ 'status' => 'Error al eliminar el dia sorteable 1', 'type' => 'error' ] )
									->withInput();
				}

			}

			if (!$day_draw->delete()) {
				DB::rollback();
				return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
								->with( [ 'status' => 'Error al eliminar el dia sorteable 2', 'type' => 'error' ] )
								->withInput();
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);
			return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
							->with( [ 'status' => 'Error al eliminar el dia sorteable 4', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
			->with(['status' => 'Se ha eliminado satisfactoriamente el dia sorteable', 'type' => 'success']);
	}

	/**
	 * Enable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function enable( $user_id, $day_draw_id )
	{

		DB::beginTransaction();

		try {
			if (Auth::user()->role_id != 2 ) {
				return redirect()->route('home');
			}

			$day_draw = DayDraw::where([
								[ 'id', '=', $day_draw_id ]
							])->withTrashed()->first();

			$day_draw->enable = 1;

			$day_draw->deleted_at = NULL;

			if (!$day_draw->save()) {
				return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
					->with(['status' => 'Error al habilitar el dia sorteable 1', 'type' => 'error']);
			} else {
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//	dd($e);

			return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
							->with( [ 'status' => 'Error al habilitar el dia sorteable 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
			->with(['status' => 'Se ha habilitado satisfactoriamente el dia sorteable 4', 'type' => 'success']);
	}

	/**
	 * Disable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function disable( $user_id, $day_draw_id )
	{

		DB::beginTransaction();

		try {
			if (Auth::user()->role_id != 2 ) {
				return redirect()->route('home');
			}

			$day_draw = DayDraw::where([
								[ 'id', '=', $day_draw_id ]
							])->withTrashed()->withTrashed()->first();

			$day_draw->enable = 0;

			if (!$day_draw->save()) {
				return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
					->with(['status' => 'Error al deshabilitar el dia sorteable', 'type' => 'error']);
			} else {
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//	dd($e);

			return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
							->with( [ 'status' => 'Error al deshabilitar el dia sorteable', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route( 'dias_sorteables.index', array( 'user_id' => Auth::user()->id ) )
			->with(['status' => 'Se ha deshabilitado satisfactoriamente el dia sorteable', 'type' => 'success']);
	}
}

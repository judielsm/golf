<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Golf\GolfCourse;
use Golf\Member;
use Golf\ReservationDefinition;
use Golf\User;
use Golf\Tournament;
use Golf\Category;
use Carbon\Carbon;
use DB;
use Golf\TournamentInscriptionMembers;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		$golf_course = GolfCourse::first();
		// $tournaments= Tournament::whereDate('start_date', '>=', Carbon::now()->format('Y-m-d'))->get();
		setlocale(LC_TIME, 'es_ES.UTF-8');
		//

		$tournaments = Tournament::whereDate('start_date', '>=', Carbon::now()->format('Y-m-d'))->orderBy('start_date_inscription','asc')->orderBy('end_date_inscription','asc')->orderBy('end_date','asc')->orderBy('end_date','asc')->take(5)->get();

		foreach ($tournaments as $key => $tournament) {
				$tournaments[$key]->categories = Category::where('tournament_categories.tournament_id',  $tournament->id)
									->where('tournament_categories.deleted_at', null)
									->join('tournament_categories','tournament_categories.category_id','=','categories.id')
									->get();
				$tournament_inscripcion = TournamentInscriptionMembers::where([
													['tournament_id', '=', $tournament->id],
													['user_id', '=', Auth::user()->id]
												])->first();
				$tournament->inscripcion = ($tournament_inscripcion == null) ? 0 : 1 ;


		}
		// dd($tournaments);

		// dd($tournaments);

		return view('home', [
								'golf_course' => $golf_course,
								'tournaments' => $tournaments,
								'day' => Carbon::now(),
							]);
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function root()
	{
		if (Auth::user()->role_id != 1 ) {
			return redirect()->route('home');
		}

		return view('home');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function administrador_reservaciones()
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		return view('admin');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function administrador_finanzas()
	{
		if (Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		return view('admin_reservation');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function administrador_storage()
	{
		if (Auth::user()->role_id != 4 ) {
			return redirect()->route('home');
		}

		return view('admin_finance');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function starter()
	{
		if (Auth::user()->role_id != 5 ) {
			return redirect()->route('home');
		}

		return view('starter');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function socios()
	{
		if (Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}

		return view('member');
	}
}

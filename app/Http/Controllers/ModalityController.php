<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Respons;
use DB as DB;
use Golf\Modality;

class ModalityController extends Controller
{
  public function post_modalities()
  {
    $mod = DB::table('modalities')->where('id','>','0')->orderBy('name', 'asc')->get();
    return response()->json($mod);
  }
}

<?php

namespace Golf\Http\Controllers;

use Auth;
use DB;
use Golf\Http\Controllers\Controller;
use Golf\Member as Member;
use Golf\User as User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create()
    {
    	if (Auth::guest()) {
    		// Crear nuevo usuario
    		$user = new User();
    		
    		// Retornar a la vista de registro
    		return view('auth.register')
    			->with('user', $user)
    		;
    		
    	} else {
    		// Retornar a la vista principal
    		return view('home');
    	}
    }
    
    public function store(Request $request)
    {
    	// Comienza la transacci�n
    	DB::beginTransaction();
    	
    	try {
    		// Validamos los campos del formulario
	    	$validator = Validator::make($request->all(), [
    			'name' => 'required|min:3|max:25',
    			'last_name' => 'required|min:3|max:25',
    			'identity_card' => 'required|numeric|unique:members',
    			'number_action' => 'required|numeric|min:5',
    			'phone' => 'required|min:5|max:14',
    			'email' => 'required|email|min:3|max:50|unique:users',
    			'password' => 'required|min:6|max:25|confirmed',
    			'sex' => 'required|min:1|max:1',
				'password' => 'required|min:6|max:25|confirmed'
	    	]);
	    	
	    	// Condici�n si falla la prueba de validaci�n
	    	if($validator->fails()){
	    		return back()
		    		->withErrors($validator);
	    	}
	    	
	    	// Se introduce los datos del nuevo usuario
	    	$user = new User();
	    	$user->name = $request->email;
	    	$user->email = $request->email;
	    	$user->password = bcrypt($request->password);
	    	$user->role_id = 6;
			$user->is_subscripted=false;
	    	
	    	// Condici�n en el caso que el usuario se guard� para agregar los datos de socio
	    	if( $user->save() ) {
	    		$member = new Member();
	    		$member->name = ucwords(strtolower($request->name));
	    		$member->enabled = false;
	    		$member->last_name = ucwords(strtolower($request->last_name));
	    		$member->identity_card = $request->identity_card;
	    		$member->number_action = $request->number_action;
	    		$member->phone = $request->phone;
	    		$member->handicap = $request->handicap;
	    		$member->sex = $request->sex;
	    		$member->user_id = $user->id;
	    		$name = $user->name;
	    		
	    		// Condici�n en el caso que el socio se guard� para proceder a enviar el correo
	    		if ( $member->save() )
	    		{
	    			$user->delete();
	    			
	    			// Salvamos la transacci�n en caso de �xito
	    			DB::commit();
	    			
	    			// Se construye la informaci�n que ir� en el cuerpo y from del correo
	    			$data = array('email' => $request->email, 'password' => $request->password);
	    			$fromEmail	=	$user->email; 
	    			$fromName	=	$member->name;
	    			
	    			Mail::send('emails.registry', $data, function($message) use ($fromName, $fromEmail)
	    			{
	    				$message->to($fromEmail, $fromName); 
	    				$message->from('no-reply@dementecreativo.com', 'No Reply');
	    				$message->subject('Creación de Cuenta');
	    			});
	    			
	    			// Despu�s del correo se retorna a la vista del login
	    			return redirect()->route('login')
						->with(['status' => 'Su cuenta ha sido registrada exitosamente. Se le ha enviado un correo con sus datos e información adicional que deberá revisar']);
	    		}
	    		else
	    		{
	    			// La transacci�n retrocede en caso de fallo
	    			DB::rollback();

	    			// Redirecciona al login con mensaje de error
	    			return redirect()->back()->with('status', 'Error al almacenar el socio');
	    		}	    			
	    	}	    	
	    	
	    } catch(\Exception $e) {
	    	// La transacci�n retrocede en caso de fallo
	    	DB::rollback();

	    	// Redirecciona al login con mensaje de error general
	    	return redirect()->route('login')
	    		->with(['status' => $e->getMessage()]);
	    }
    		
    }
	
}

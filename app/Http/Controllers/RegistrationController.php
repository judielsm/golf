<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Facades\Golf\Facades\Payment;
use Facades\Golf\Facades\ValidateRecurringData;
use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use Auth;
use DB;
use Excel;
use Log;
use PDF;

use Golf\Group;
use Golf\Member;
use Golf\PaymentPlan;
use Golf\Registration;
use Golf\Student;
use Golf\Teacher;
use Golf\TypePayment;

class RegistrationController extends Controller
{
	public function index()
	{
		if ((Auth::user()->role_id != 3) && (Auth::user()->role_id != 6)) {
			return redirect()->route('home');
		}


		$registrations = Registration::where([])->orderBy('created_at','asc')->get();

		$data = array();

		// if (Auth::user()->role_id==3) {
		// 	$data['members'] = Member::with('students')->get();
		// } else {
		// 	$data['students'] = Student::get();
		// }
		$data['type_payments'] = TypePayment::get();
		$data['members'] = Member::with('students')->get();
		$data['students'] = Student::get();
		$data['registrations'] = $registrations;

		$data['groups'] = Group::get();
		$data['payment_plans'] = PaymentPlan::get();
		$data['payment_plans'] = PaymentPlan::get();
		// $data['students'] = Student::get();
		// dd($data['students']);
		//dd($data);


		return view('registrations.all_registrations', $data);
	}

	public function show(Request $request, $user_id, $registration_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$registration = Registration::find( $registration_id );

			if (!empty($registration->student->member->name) || !empty($registration->student->member->last_name))
			{
				$registration->student->member = $registration->student->member->name.' '.$registration->student->member->last_name;
			}

			if (!empty($registration->payment_plan->name))
			{
				$registration->payment_plan = $registration->payment_plan->name;
			}

			if (!empty($registration->group->name))
			{
				$registration->group = $registration->group->name;
			}

			if (empty($registration)) {
				throw new \Exception("Error al obtener el registro", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['registration' => $registration,
				'type' => 'success']);
	}

	public function create($user_id)
	{
		if ((Auth::user()->role_id != 3) &&  (Auth::user()->role_id != 6)) {
			return redirect()->route('home');
		}

		// if (Auth::user()->role_id==3) {
		// 	$data['members'] = Member::with('students')->get();
		// } else {
		// 	$data['students'] = Student::where('member_id',Auth::user()->member->id)->get();
		// }

		$data['groups'] = Group::get();
		$data['members'] = Member::with('students')->get();
		$data['payment_plans'] = PaymentPlan::get();
		$data['students'] = Student::get();
		$data['teachers'] = Teacher::get();
		$data['type_payments'] = TypePayment::get();

		return view('registrations.create_registration', $data);

	}

	public function store(Request $request)
	{
		try {
			if ((Auth::user()->role_id != 3) && (Auth::user()->role_id != 6)) {
				return redirect()->route('home');
			}

			DB::beginTransaction();

			// dd($request->input());
			$validation = Validator::make($request->input(), [
				'group_id' => 'required|numeric',
				'payment_plan_id' => 'required|numeric',
				'student_id' => 'required|numeric',
				'type_payment_id' => 'required|numeric',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea un nuevo registro
			$registration = new Registration();
			$registration->student_id = $request->student_id;
			$registration->payment_plan_id = $request->payment_plan_id;
			$registration->group_id = $request->group_id;

			// Se condiciona si el registro se guard�
			if ($registration->save()) {

				//registro de transaccion de pago de greenfee
				$transaction = array();
				$transaction['amount'] = $registration->payment_plan->rate;
				//1 para socio
				$transaction['type_payer'] = 1;
				//1 para socio
				$transaction['payer_id'] = $registration->student->member->id;
				$transaction['product_id'] = $registration->id;
				//1 para reservacion
				$transaction['type_product_id'] = 3;
				$transaction['transaction_status_id'] = 2;
				$transaction['type_payment_id'] = $request->type_payment_id;

				$transaction_response = Payment::transaction($transaction);

				if ($transaction_response['code'] != 1) {
					throw new \Exception($transaction['message'], 400);
				}
			} else {
				// Arroja el mensaje de error si el registro no guard�
				throw new Exception('Error al registrar inscripción', 400);
			}

			DB::commit();
			$response['response'] = $registration;
			$response['status'] = 'Se ha registrado satisfactoriamente la inscripción';
			$response['type'] = 'success';
			return redirect()->route('inscripciones.index', array('user_id' => Auth::user()->id))
							->with($response);

			} catch(\Exception $e) {
				// Creamos un switch para que nos instancie los casos de excepciones
				switch ($e){
					// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
					case ($e instanceof \PDOException):
						return redirect()->route('inscripciones.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
						break;

					default:
						// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
						$excepciones = $e->validator->messages()->getMessages();
						$resultados = array();

						foreach ($excepciones as $excepcion){
							foreach ($excepcion as $ex){
								array_push($resultados, $ex);
							}
						}

						// Se retorna al listado de inscripciones con los mensajes de diferentes errores
						DB::rollback();
						return redirect()->route('inscripciones.index', array('user_id' => Auth::user()->id))
						->with(['validations' => $resultados,
								'line'  => $e->getLine(),
								'code'  => $e->getCode(),
								'input' => $request->input(),
								'type'  => 'error']);

				}
			}
	}

	public function edit($user_id, $registration_id)
	{
		if ((Auth::user()->role_id != 3) &&  (Auth::user()->role_id != 6)) {
			return redirect()->route('home');
		}

		$registration = Registration::where([
						['id', '=', $registration_id]
						])->first();

		return view('registrations.edit_registration', ['registration' => $registration ]);

	}

	public function update(Request $request, $user_id, $registration_id)
	{
		try {
			if (Auth::user()->role_id != 3) {
				return redirect()->route('home');
			}

			DB::beginTransaction();

			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:50',
				'description' => 'required|min:3',
				'rate' => 'required',
			]);

			//dd($validation->errors());

			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}

			$registration = Registration::where([
								['id', '=', $registration_id]
							])->first();

			$registration->name = $request->name;
			$registration->description = $request->description;
			$registration->rate = $request->rate;

			$name = $registration->name;

			if (!$registration->save()) {
				throw new Exception('Error al actualizar inscripción', 400);
			}
		} catch (\Exception $e) {
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';

			return redirect()->route('inscripciones.index', array('user_id' => Auth::user()->id))
							->with($response);
		}
		DB::commit();
		$response['response'] = $registration;
		$response['status'] = 'Se ha actualizado satisfactoriamente inscripción';
		$response['type'] = 'success';
		return redirect()->route('inscripciones.index', array('user_id' => Auth::user()->id))
						->with($response);
	}

	/*public function destroy($user_id, $registration_id)
	{
		try {
			if (Auth::user()->role_id != 3) {
				return redirect()->route('home');
			}

			$registration = Registration::where([
								['id', '=', $registration_id]
							])->first();

			if (!$registration->delete()) {
				throw new Exception('Error al eliminar inscripción', 400);
			}
		} catch (\Exception $e) {
			DB::rollback();
			//dd($e);
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';

			return redirect()->route('inscripciones.index', array('user_id' => Auth::user()->id))
							->with($response);
		}
		DB::commit();
		$response['response'] = $registration;
		$response['status'] = 'Se ha eliminado satisfactoriamente la inscripción';
		$response['type'] = 'success';
		return redirect()->route('inscripciones.index', array('user_id' => Auth::user()->id))
						->with($response);
	}
*/
	public function validate_student(Request $request)
	{
		try {
			$validacion = ValidateRecurringData::student_is_registered($request->student_id);
		} catch (\Exception $e) {
			DB::rollback();
			//dd($e);
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';
			return response()->json($response);
		}
		DB::commit();
		$response['response'] = !$validacion;
		$response['status'] = '';
		$response['type'] = 'success';
		return response()->json($response);
	}

	public function list_registrations(Request $request)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		$groups = Group::has('registrations')->get();

		if(count($groups)!= 0 ) {

			$registrations = Registration::select(['registrations.*']);
			if (!is_null($request->start_date)) {
				$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
				if (!is_null($request->end_date)) {
					$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
					$registrations->join('groups', function ($join) use($start_date,$end_date) {
						$join->on('groups.id', '=', 'registrations.group_id')
							->whereDate('registrations.created_at', '>=', $start_date->toDateString())
							->whereDate('registrations.created_at', '<=', $end_date->toDateString());
					});
				} else {
					$registrations->join('groups', function ($join) use($start_date) {
						$join->on('groups.id', '=', 'registrations.group_id')
							->whereDate('registrations.created_at', '>=', $start_date->toDateString());
					});
				}
			} else{
				if (!is_null($request->end_date)) {
					$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
					$registrations->join('groups', function ($join) use($end_date) {
						$join->on('groups.id', '=', 'registrations.group_id')
							->whereDate('registrations.created_at', '<=', $end_date->toDateString());
					});
				}
			}
			//seleccion del grupo a mostrar
			if ($request->group_id != "null" && !is_null($request->group_id)) {
				$registrations->where('group_id', $request->group_id);
				$group = Group::find($request->group_id);
			} else {
				$registrations->where('group_id', $groups[0]->id);
			}
		
		$registrations = $registrations->sorted()->orderBy('registrations.created_at','asc')->paginate(10);
		//dd($registrations);

		foreach ($registrations as $key => $registration) {
			$registration->partners;
			$registration->members;
		}
		
		$data['registrations'] = $registrations;
		$data['group'] = (isset($group)) ? $group :$groups[0];
		$data['groups'] = $groups;
		$data['input'] = $request->input();

		return view('registrations.list_report', $data);
		} else {
			$registrations = [];
			$data['registrations'] = $registrations;
			$data['group'] = null;
			$data['groups'] = $groups;
			$data['input'] = $request->input();

		return view('registrations.list_report', $data);
		}
	}

	public function pdf_report_create(Request $request)
	{
		try {

			$groups = Group::has('registrations')->get();

			$vista_url = 'registrations.pdf_report_registrations';

			$registrations = Registration::select(['registrations.*']);

			if (!is_null($request->start_date)) {
				$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
				$registrations->whereDate('created_at', '>=', $start_date->toDateString());
			}

			if (!is_null($request->end_date)) {
				$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
				$registrations->whereDate('created_at', '<=', $end_date->toDateString());
			}else{
				if (!is_null($request->start_date)) {
					$registrations->whereDate('created_at', '<=', $start_date->toDateString());
				}
			}
			//seleccion del grupo a mostrar
			if ($request->group_id != "null" && !is_null($request->group_id)) {
				$registrations->where('group_id', $request->group_id);
			} else {
				$registrations->where('group_id', $groups[0]->id);
			}
			$registrations = $registrations->sorted()->orderBy('created_at','asc')->paginate(999999);

			$data = array();
			$data['totales'] = $registrations->count();
			$data['canceladas'] = $registrations->where('status','Cancelada')->count();
			$data['finalizadas'] = $registrations->where('status','Finalizada')->count();
			$data['en_proceso'] = $registrations->where('status','En proceso')->count();
			$data['procesadas'] = $registrations->where('status','Procesada')->count();
			$data['registrations'] = $registrations;
			$data['group'] = $groups[0];
			$data['start_date'] = $request->start_date;
			$data['end_date'] = $request->end_date;
			$data['status'] = $request->status;
			$data['title'] = "Reporte de Inscripciones de Alumnos";



			//return view('registrations.pdf_report_registrations', $data);


			$pdf = PDF::loadView($vista_url, $data);

			return $pdf->download('Inscripciones Alumnos	.pdf');

		} catch (\Exception $e) {
			Log::info('Error al crear pdf'. $e);
			dd($e);
		}
	}

	public function xls_report_create(Request $request)
	{
		try {
			//dd($request->input());

			$groups = Group::has('registrations')->get();

			$vista_url = 'registrations.pdf_report_registrations';

			$registrations = Registration::select(['registrations.*']);

			if (!is_null($request->start_date)) {
				$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
				$registrations->whereDate('created_at', '>=', $start_date->toDateString());
			}

			if (!is_null($request->end_date)) {
				$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
				$registrations->whereDate('created_at', '<=', $end_date->toDateString());
			}else{
				if (!is_null($request->start_date)) {
					$registrations->whereDate('created_at', '<=', $start_date->toDateString());
				}
			}
			//seleccion del grupo a mostrar
			if ($request->group_id != "null" && !is_null($request->group_id)) {
				$group = Group::find($request->group_id);
				$registrations->where('group_id', $request->group_id);
			} else {
				$registrations->where('group_id', $groups[0]->id);
			}
			$registrations = $registrations->sorted()->orderBy('created_at','asc')->paginate(999999);
			foreach ($registrations as $key => $registration) {
				$participantes = '';
				if (isset($registration->student)) {
					$participantes = strtoupper($registration->student->name)." ".strtoupper($registration->student->last_name)." ".$registration->student->identity_card;
				}
				$registrations_array[] = array(
					"FECHA" => $registration->created_at,
					"ESTADO" => strtoupper($registration->status),
					"ALUMNOS" => $participantes,
					"SOCIO" => strtoupper($registration->student->member->name)." ".strtoupper($registration->student->member->last_name)." ".$registration->student->member->identity_card,
				);
			}

			$data = array();
			$data['titulo'] = "REPORTE DE INSCRIPCIONES DE ALUMNOS";
			$data['totales'] = $registrations->count();
			$data['registrations'] = $registrations;
			$data['group'] = (isset($group)) ? $group :$groups[0];
			$data['start_date'] = $request->start_date;
			$data['end_date'] = $request->end_date;
			$data['status'] = $request->status;

			$excel = Excel::create('ListadoInscripciones'.date('Ymdhis'), function($excel) use($registrations_array, $registrations, $data) {

				// Set the title
				$excel->setTitle('Listado de reeservas '.date('Y-m-d'));

				// Chain the setters
				$excel->setCreator('Lagunita Country Club')->setCompany('Lagunita Country Club');

				$excel->setDescription('Listado de las reservas consultadas en el Sistema para Alquiler de Canchas de Golf Lagunita Country Club');

				$excel->sheet('Listado', function ($sheet) use ($registrations_array, $registrations, $data) {
					$sheet->setOrientation('landscape');
					$row = 3;
					$sheet->row($row, array($data['titulo']));
					++$row;
					$sheet->row($row, array("GRUPO: ".strtoupper($data['group']->name)));
					if (!empty($start_date) OR !empty($end_date) ){
						if (!empty($start_date) && !empty($end_date) ){
							if ($start_date == $end_date) {
								++$row;
								$sheet->row($row, array("Fecha: 1 end_date".$data['start_date']));
							} else {
								++$row;
								$sheet->row($row, array("Desde: ".$data['start_date']." Hasta: ".$data['end_date']));
							}
						} else {
							if (!empty($start_date)){
								++$row;
								$sheet->row($row, array("Fecha: 1 start_date".$data['start_date']));
							} else{
								++$row;
								$sheet->row($row, array("Fecha: 2 end_date".$data['end_date']));
							}
						}
					}
					if (!empty($status) && $status != "null"){
						$sheet->row($row, array("Estatus: ".$data['status']));
					}
					++$row;
					$sheet->fromArray($registrations_array, NULL, 'A'.(1+$row));


					$data = array();
					$data['totales']['titulo'] = "Nº TOTAL DE INSCRIPCIONES";
					$data['totales']['cantidad'] = $registrations->count();

					$data['procesadas']['titulo'] = "Nº TOTAL DE INSCRIPCIONES PROCESADAS:";
					$data['procesadas']['cantidad'] = $registrations->where('status','Procesada')->count();
					$data['canceladas']['titulo'] = "Nº TOTAL DE INSCRIPCIONES CANCELADAS:";
					$data['canceladas']['cantidad'] = $registrations->where('status','Cancelada')->count();

					$data['finalizadas']['titulo'] = "Nº TOTAL DE INSCRIPCIONES FINALIZADAS:";
					$data['finalizadas']['cantidad'] = $registrations->where('status','Finalizada')->count();
					$data['proceso']['titulo'] = "Nº TOTAL DE INSCRIPCIONES EN PROCESO:";
					$data['proceso']['cantidad'] = $registrations->where('status','En proceso')->count();

					$row = count($registrations_array)+$row+3;
					foreach ($data as $key => $value) {
						$row++;
						$sheet->row($row, array($value['titulo']." ".$value['cantidad']));
						// $row++;
						// $sheet->row($row, array($value['cantidad']));
					}
				});
			});
			$excel->export('xls');

		} catch (\Exception $e) {
			Log::info('Error al crear pdf'. $e);
			dd($e);
		}
	}
}

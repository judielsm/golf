<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\DrawingDay;
use Golf\User;

class DrawingDaysController extends Controller
{

	public function index()
	{
		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$drawign_days = DrawingDay::where([
		//					['user_id', '=', $user->user_id],
						])->orderBy('name', 'asc')->get();

		// echo "<pre>";  var_dump($drawign_days); echo "</pre>";
		// return;

		return view('drawign_days.all_drawign_days', ['drawign_days' => $drawign_days] );
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create_drawing_day()
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$drawing_day = DrawingDay::where([
		//					[ 'user_id', '=', $user->user_id ]
						])->first();

		return view('drawign_days.create_drawing_day', ['drawing_day' => $drawing_day]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store_drawing_day( Request $request )
	{

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$drawing_day = new DrawingDay();
		//$drawing_day->user_id = $user->user_id;
		$drawing_day->user_id = 1;
		$drawing_day->receipt_payment = $request->receipt_payment;
		$drawing_day->processed = $request->processed;
		$drawing_day->detail = $request->detail;
		$drawing_day->date = $request->date;
		$drawing_day->enabled = 1;


		if ($drawing_day->save()) {
			return redirect()->route( 'all_drawign_days' )
							->with( [ 'status' => 'El dia de sorteo se ha creado exitosamente', 'type' => 'success' ] );
		}else {
			return redirect()->route( 'create_drawing_day' )
							->with( [ 'status' => 'Error al registrar el dia de sorteo ', 'type' => 'success' ] )
							->withInput();
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit_drawing_day( $drawing_day_id )
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$drawing_day = DrawingDay::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $drawing_day_id ]
						])->first();

		return view('drawign_days.edit_drawing_day', ['drawing_day' => $drawing_day]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $drawing_day_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update_drawing_day(Request $request, $drawing_day_id)
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$drawing_day = DrawingDay::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $drawing_day_id ]
						])->first();

		$drawing_day->receipt_payment = $request->receipt_payment;
		$drawing_day->processed = $request->processed;
		$drawing_day->detail = $request->detail;
		$drawing_day->date = $request->date;

		if ($drawing_day->save()) {
			return redirect()->route('all_drawign_days')
				->with(['status' => 'Se ha actualizado satisfactoriamente el dia de sorteo', 'type' => 'success']);
		} else {
			return redirect()->route('edit_drawing_day')
				->with(['status' => 'Error al actualizar los datos el dia de sorteo', 'type' => 'error'])
				->withInput();
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function delete_drawing_day( $drawing_day_id )
	{

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$drawing_day = DrawingDay::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $drawing_day_id ]
						])->first();

		$name = $drawing_day->name.' '.$drawing_day->last_name;

		if ($drawing_day->delete()) {
			return redirect()->route('all_drawign_days')
				->with(['status' => 'Se ha eliminado satisfactoriamente', 'type' => 'success']);
		} else {
			return redirect()->route('all_drawign_days')
				->with(['status' => 'Error al eliminar el el dia de sorteo', 'type' => 'error']);
		}
	}
}

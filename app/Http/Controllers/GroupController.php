<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use Golf\Group;
use Golf\Teacher;
use Golf\User;

use Gbrock\Table\Facades\Table;



class GroupController extends Controller
{
	public $response = array();

    public function index()
	{
		if (Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		$groups = Group::where([])->get();

		$teachers = Teacher::where([])->get();

		return view('groups.all_groups', [
			'groups' => $groups,
			'teachers' => $teachers,
		]);
	}

	public function show(Request $request, $user_id, $group_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$group = Group::find( $group_id );

			if (!empty($group->teacher->name) || !empty($group->teacher->last_name))
			{
				$group->teacher = $group->teacher->name.' '.$group->teacher->last_name;
			}

			if (!empty($group->teacher->email))
			{
				$group->teacher = $group->teacher->email;
			}

			if (empty($group)) {
				throw new \Exception("Error al obtener el grupo", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['group' => $group,
				'type' => 'success']);
	}

	public function store(Request $request)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
					'name' => 'required|min:3|max:25',
					'teacher_id' => 'required',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea un nuevo grupo
			$group = new Group();
			$group->name = $request->name;
			$group->teacher_id = $request->teacher_id;

			$name = $group->name;

			// Se condiciona si el profesor se guard�
			if (!$group->save()) {
				throw new \Exception("Error al almacenar grupo", 400);
			}

			// Se retorna al listado de grupos con los valores guardados
			DB::commit();
			$response['response'] = $group;
			$response['status'] = 'Grupo se ha creado exitosamente de:'.$name;
			$response['type'] = 'success';

			if (isset($request->mas)) {
				return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
								->with($response);
			} else {
				return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
								->with($response);
			}
		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				case ($e instanceof \PDOException):
					return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de grupos con los mensajes de diferentes errores
					DB::rollback();
					$response['response'] = false;
					$response['validations'] = $resultados;
					$response['line'] = $e->getLine();
					$response['code'] = $e->getCode();
					$response['input'] = $request->input();
					$response['type'] = 'error';

					return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
					->with($response);
			}
		}
	}

	public function store_ajax(Request $request)
	{
		try {
			if (Auth::user()->role_id != 3 ) {
				return redirect()->route('home');
			}

			$group = new Group();
			$group->name = $request->name;
			$group->teacher_id = $request->teacher_id;

			$name = $group->name;

			if (!$group->save()) {
				throw new \Exception("Error al registrar Grupo", 400);
			}
		} catch (\Exception $e) {
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['input'] = $request->input();
			$response['type'] = 'error';

			return response()->json($response);
		}

		DB::commit();
		$response['response'] = $group;
		$response['status'] = 'Grupo se ha creado exitosamente de: '.$name;
		$response['type'] = 'success';
		return response()->json($response);
	}

	public function edit($user, $group_id)
	{
		if (Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		if ( Group::find( $group_id ) )
		{
			$group = Group::where([
						[ 'id', '=', $group_id ]
						])->first();

			$teachers = Teacher::where([])->get();

			return view('groups.edit_group', ['group' => $group, 'teachers' => $teachers]);
		}
		else
		{
			return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
				->with( [ 'status' => 'Error al modificar Grupo', 'type' => 'error' ] )
				->withInput();
		}
	}

	public function update(Request $request, $user_id, $group_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
					'name' => 'required|min:3|max:25',
					'teacher_id' => 'required',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se obtiene los grupos ordenados por ID
			$group = Group::where([
								[ 'id', '=', $group_id ]
							])->first();
			$group->name = $request->name;
			$group->teacher_id = $request->teacher_id;

			$name = $group->name;

			// Se valida si el grupo no se guardo seguido de un mensaje de error
			if (!$group->save()) {
				throw new \Exception("Error al actualizar los datos de: ".$name, 400);
			}

			// Se retorna al listado de grupos con los valores guardados
			DB::commit();
			$response['response'] = $group;
			$response['status'] = 'Se ha actualizado satisfactoriamente el grupo: '.$name;
			$response['type'] = 'success';

			if (isset($request->mas)) {
				return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
								->with($response);
			} else {
				return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
								->with($response);
			}
		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				case ($e instanceof \PDOException):
					// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
					return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de grupos con los mensajes de diferentes errores
					DB::rollback();
					$response['response'] = false;
					$response['validations'] = $resultados;
					$response['line'] = $e->getLine();
					$response['code'] = $e->getCode();
					$response['type'] = 'error';

					return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
					->with($response);
			}
		}

	}

	public function destroy($user_id, $group_id)
	{
		try {
			if (Auth::user()->role_id != 3 ) {
				return redirect()->route('home');
			}

			$group = Group::where([
								[ 'id', '=', $group_id ]
							])->first();

			$name = $group->name;

			if (!$group->delete()) {
				throw new \Exception("Error al eliminar los datos de: ".$name, 400);
			}
		} catch (\Exception $e) {
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['input'] = $request->input();
			$response['type'] = 'error';

			return redirect()->route('grupos.index')
							->with($response);
		}

		DB::commit();
		$response['response'] = $group;
		$response['status'] = 'Se ha eliminado satisfactoriamente el grupo';
		$response['type'] = 'success';
		return redirect()->route('grupos.index', array('user_id' => Auth::user()->id))
						->with($response);
	}
}

<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Golf\GreenFee;
use Golf\Club;
use Gbrock\Table\Facades\Table;
use Auth;
class GreenFeeController extends Controller
{
    public function index()
    {
      if (Auth::user()->role_id != 2 ) {
  			return redirect()->route('home');
  		}

  		$green_fees = GreenFee::all();
      $clubs =  Club::all();
  		$table_green_fees = Table::create($green_fees , ['description' => 'Descripcion', 'club->name'=>'Club']);

  		$table_green_fees->addColumn('amount', 'Monto', function($green_fee) {
  			return number_format($green_fee->amount, 2, ',', '.').' BsF';
  		});

  		$table_green_fees->addColumn('edit', 'Editar', function($green_fee) {

  			$return = '<a href="/green/"'.$green_fee->id.' class="btn btn-primary singlebutton1">';
  			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
  			$return .= '</a>';
  			return $return;
  		});

  		return view('green_fee.list' )
      ->with('table_green_fees', $table_green_fees)
      ->with('clubs', $clubs);

    }

    public function store(Request $request)
    {
      try{
        $green_fee = new GreeFee();
        $green_fee->description = $request->description;
        $green_fee->amount = $request->amount;
        $green_fee->club_id = $request->club;
        if ($green_fee->save()) {
          return redirect('/green')->with();
        }

      }catch(\Exception $e){

      }

    }
}

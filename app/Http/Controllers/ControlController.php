<?php
namespace Golf\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ControlController extends BaseController
{
	
	function __construct()
	{
	
	}

	function ingresar(){
		return view('home');
	}

	function reserva(){
		return view('timeframe/timeframe');
	}
	function data(){
		return view('reservations/create_reservation');
	}
	function pru(){
		return view('prueba');
	}
	
}
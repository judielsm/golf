<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Respons;

use Golf\Teacher;
use Golf\User;
use Golf\Group;
use Golf\Student;
use Golf\Period;
use Golf\Payment;
use Golf\Transaction;
use Gbrock\Table\Facades\Table;


class PaymentController extends Controller
{
	public function index()
	{
		if ( Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}


		$payments = Transaction::where('type_product_id',3)->sorted()->get();


		$periods = Period::where([])->get();

		$table_payments = Table::create($payments, []);
		
		return view('payments.all_payments', ['payments' => $payments, 'periods' => $periods, 'table_payments' => $table_payments] );
	}

	public function create()
	{
		if ( Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		return view('payments.create_payment');
	}

	public function store(Request $request)
	{
		if ( Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		$payment = new Payment();
		$payment->concept = $request->concept;
		$payment->period_id = $request->period_id;

		$concept = $payment->concept;


		if ($payment->save()) {
			return redirect()->route( 'pagos.index' )
							->with( [ 'status' => 'Pago se ha creado exitosamente de:'.$concept, 'type' => 'success' ] );
		}else {
			return redirect()->route( 'pagos.create' )
							->with( [ 'status' => 'Error al registrar Pago', 'type' => 'success' ] )
							->withInput();
		}
	}

	public function edit($payment_id)
	{
		if ( Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		$payment = Payment::where([
						[ 'id', '=', $payment_id ]
						])->first();

		$periods = Period::where([])->get();

		return view('payments.edit_payment', ['payment' => $payment , 'periods' => $periods]);

	}

	public function update(Request $request, $payment_id)
	{
		if ( Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		$payment = Payment::where([
							[ 'id', '=', $payment_id ]
						])->first();
		$payment->concept = $request->concept;
		$payment->period_id = $request->period_id;

		$concept = $payment->concept;

		if ($payment->save()) {
			return redirect()->route('pagos.index')
				->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$concept, 'type' => 'success']);
		} else {
			return redirect()->route('pagos.edit')
				->with(['status' => 'Error al actualizar los datos de:'.$concept, 'type' => 'error'])
				->withInput();
		}
	}

	public function destroy($payment_id)
	{
		if ( Auth::user()->role_id != 3 ) {
			return redirect()->route('home');
		}

		$payment = Payment::where([
							[ 'id', '=', $payment_id ]
						])->first();

		$concept = $payment->concept;

		if ($payment->delete()) {
			return redirect()->route('pagos.index')
				->with(['status' => 'Se ha eliminado satisfactoriamente:'.$concept, 'type' => 'success']);
		} else {
			return redirect()->route('pagos.index')
				->with(['status' => 'Error al eliminar el invitado:'.$concept, 'type' => 'error']);
		}
	}

}

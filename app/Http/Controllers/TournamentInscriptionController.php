<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Facades\Golf\Facades\Payment;
use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Excel;
use Log;
use PDF;

use Golf\Category;
use Golf\Member;
use Golf\Tournament;
use Golf\TournamentInscriptionMembers;
use Golf\TypePayment;

class TournamentInscriptionController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create($tournament_id)
	{


		$tournament = Tournament::where([
						['id', '=', $tournament_id]
					])->first();
		$tournament->categories = Category::where('tournament_categories.tournament_id', $tournament_id)
									->where('tournament_categories.deleted_at', null)
									->join('tournament_categories','tournament_categories.category_id','=','categories.id')
									->get();
		$type_payments = TypePayment::get();
		setlocale(LC_TIME, 'es_ES.UTF-8');


		return view('tournament_inscriptions.create', [
						'tournament' => $tournament,
						'type_payments' => $type_payments
					]);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(!isset($request->payment_type)){
			throw new \Exception("No selecciono metodo de pago", 400);
		}
		DB::beginTransaction();
		try {
			$tournament_inscriptions= new TournamentInscriptionMembers();
			$tournament= Tournament::find($request->tournament_id);
			$tournament->categories = Category::where('tournament_categories.tournament_id', $tournament->id)
									->where('tournament_categories.deleted_at', null)
									->join('tournament_categories','tournament_categories.category_id','=','categories.id')
									->get();
			$inscripcion= TournamentInscriptionMembers::where([
								['user_id', '=', Auth::user()->id],
								['tournament_id', '=', $request->tournament_id]
						])->first();

			$member = Auth::user()->member;

			if($inscripcion){
				throw new \Exception("Ya se encuentra inscrito en el torneo seleccionado", 400);
			}

			if (empty($tournament->categories)) {
				throw new \Exception("Error al acceder a las categorias del torneo", 400);
			} else {

				$no_sexo = true;

				foreach($tournament->categories as $category) {

					if(($category->sex == 'Mixto') || ($member->sex == $category->sex)) {
						//dd($category);
						$no_sexo = false;
						$no_handicap = true;

						if (($member->handicap >= $category->handicap_min) &&
							($member->handicap <= $category->handicap_max)) {
							$no_handicap = false;

							//dd($category->id);

							$num= TournamentInscriptionMembers::where('category_id', '=', $category->id)->count();
							$tournament_inscriptions->member_id= $member->id;
							$tournament_inscriptions->category_id= $category->category_id;
							$tournament_inscriptions->tournament_id=$request->tournament_id;
							$tournament_inscriptions->waiting= ($category->number_participants <= $num) ? 1 : 0;
							$tournament_inscriptions->user_id=Auth::user()->id;
							//dd($tournament_inscriptions);
							if ($tournament_inscriptions->save()) {

								//registro de transaccion de pago de greenfee
								$transaction = array();
								$transaction['amount'] = $tournament->inscription_fee;
								//1 para socio
								$transaction['type_payer'] = 1;
								//1 para socio
								$transaction['payer_id'] = Auth::user()->member->id;
								$transaction['product_id'] = $tournament->id;
								//2 para tournament
								$transaction['type_product_id'] = 2;
								$transaction['transaction_status_id'] = 2;
								$transaction['type_payment_id'] = $request->type_payment_id;


								$transaction_response = Payment::transaction($transaction);
								if ($transaction_response['code'] = 0) {
									throw new \Exception($transaction['message'], 400);
								}
							}else {
								throw new \Exception("No se pudo realizar la inscripción", 400);
							}
						}
					}
				}
				if ($no_sexo) {
					throw new \Exception("No cumple con el sexo", 400);
				}
				if ($no_handicap) {
					throw new \Exception("Su handicap: ".$member->handicap." no se encuentra dentro del rango de handicap :".$category->handicap_min." - ".$category->handicap_max, 400);
				}
			}
		} catch (\Exception $e) {
			DB::rollback();
			return redirect('home')
						->with(['status' => $e->getMessage(),
							'line' => $e->getLine(),
							'code' => $e->getCode(),
							'type' => 'error' ]);
		}
		DB::commit();
		if($num <= $category->number_participants) {
			return redirect('home')
						->with(['status' => "Se ha realizado la inscripción exitosamente. Ahora se encuentra en la lista de espera por un cupo disponible.",
							'type' => 'success' ]);
		}
			return redirect('home')
						->with(['status' => "Se ha realizado la inscripción exitosamente.",
							'type' => 'success' ]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$list_inscription = TournamentInscriptionMembers::join('tournaments', 'tournament_inscription_members.tournament_id', '=', 'tournaments.id')
		->join('members', 'tournament_inscription_members.member_id', '=', 'members.id')
		->join('categories', 'tournament_inscription_members.category_id', '=', 'categories.id')
		->select('members.name as member_name', 'tournaments.name as tournament_name' , 'categories.name  as category_name', 'members.handicap')->orderBy('members.handicap', 'asc')->get();

		//dd($list_inscription);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function validate_disponibility($tournament_id)
	{
		$user = Auth::user();
		if ($user->role_id != 6) {
			return redirect()->route('home');
		}
		try {
			$categories = Category::where('tournament_id', $tournament_id)
									->where('handicap_min', '<', $user->member->handicap)
									->where('handicap_max', '>', $user->member->handicap)
									->get();

			foreach ($categories as $key => $category) {
				if(($category->sex=='Mixto') || ($user->member->sex == $category->sex)) {
					$num= TournamentInscriptionMembers::where('category_id', $category->id)->count();
					if($num == $category->number_participants) {
						throw new \Exception("El torneo no tiene cupos disponibles en la categoria correspondiente. Se registrara como lista de espera", 400);
					}
				}
			}
		} catch (\Exception $e) {
			return response()->json(['status' => $e->getMessage(),
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}
		return response()->json(['status' => true,
								'code' => 200,
								'type' => 'success']);
	}

	public function list_tournament_inscriptions()
	{

		$tournaments = Tournament::all();
		$tournament_inscriptions = TournamentInscriptionMembers::with('member','tournament','category')->get();
		
		return view('tournament_inscriptions.list_report')->with('tournaments',$tournaments)->with('inscriptions', $tournament_inscriptions );
	}
	public function list_tournament_inscriptions_ajax($id)
	{
		$tournament_inscriptions = TournamentInscriptionMembers::with('member','tournament','category')->where('tournament_id',$id)->get();
		
		return response()->json(['data' => $tournament_inscriptions ]);

	}

	public function pdf_report_create(Request $request)
	{
		try {
			$vista_url = 'tournament_inscriptions.pdf_report_tournaments';
			$tournaments = Tournament::has('tournament_inscriptions')->get();
			$tournament_inscriptions = TournamentInscriptionMembers::where([])->with('member','tournament');

			//filtro rango de fecha de torneo
			if (!is_null($request->start_date)) {
				$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
				if (!is_null($request->end_date)) {
					$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
					$tournament_inscriptions->join('tournaments', function ($join) use($start_date,$end_date) {
						$join->on('tournaments.id', '=', 'tournament_inscription_members.tournament_id')
							 ->whereDate('tournaments.start_date', '>=', $start_date)
							 ->whereDate('tournaments.end_date', '<=', $end_date);
					});
				} else {
					$tournament_inscriptions->join('tournaments', function ($join) use($start_date) {
						$join->on('tournaments.id', '=', 'tournament_inscription_members.tournament_id')
							 ->whereDate('tournaments.end_date', '<=', $start_date);
					});
				}
			} else{
				if (!is_null($request->end_date)) {
					$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
					$tournament_inscriptions->join('tournaments', function ($join) use($end_date) {
						$join->on('tournaments.id', '=', 'tournament_inscription_members.tournament_id')
							 ->whereDate('tournaments.end_date', '<=', $end_date);
					});
				}
			}
			//seleccion del torneo a mostrar
			if ($request->tournament_id != "null" && !is_null($request->tournament_id)) {
				$tournament_inscriptions->where('tournament_id', $request->tournament_id);
			} else {
				$tournament_inscriptions->where('tournament_id', $tournaments[0]->id);
			}
			$tournament_inscriptions = $tournament_inscriptions->sorted()->paginate(99999);

			//dd($request->input(),$tournaments);

			$data = array();
			$data['totales'] = $tournament_inscriptions->count();
			$data['cantidad_inscritos'] = $tournament_inscriptions->where('waiting',0)->count();
			$data['cantidad_espera'] = $tournament_inscriptions->where('waiting',1)->count();

			// $tournaments = Tournament::with('tournament_inscriptions')->get();
			// //dd($tournaments);
			// foreach ($tournaments as $key => $tournament) {
			// 	$tournament->total = $tournament->tournament_inscriptions->count();
			// 	$tournament->cantidad_inscritos = $tournament->tournament_inscriptions->where('waiting',0)->count();
			// 	$tournament->cantidad_espera = $tournament->tournament_inscriptions->where('waiting',1)->count();
			// 	$data['tournaments'][]  = $tournament;
			// }
			$tournament = $tournament_inscriptions[0]->tournament;
			$tournament->total = $tournament->tournament_inscriptions->count();
			$tournament->cantidad_inscritos = $tournament->tournament_inscriptions->where('waiting',0)->count();
			$tournament->cantidad_espera = $tournament->tournament_inscriptions->where('waiting',1)->count();

			// dd($tournament_inscriptions);
			// foreach ($tournament_inscriptions as $tournament_inscription) {
			// 	echo $tournament_inscription->waiting . '</br>';
			// }

			$data['tournaments'][]  = $tournament;
			$data['tournament_inscriptions'] = $tournament_inscriptions;
			$data['start_date'] = $request->start_date;
			$data['end_date'] = $request->end_date;
			$data['tournament_id'] = $request->tournament_id;
			$data['title'] = "Reporte de Reservaciones";


			// return view('tournament_inscriptions.pdf_report_tournaments', $data);

			$pdf = PDF::loadView($vista_url, $data);


			return $pdf->download('Inscripciones en torneos.pdf');

		} catch (\Exception $e) {
			Log::info('Error al crear pdf '. $e);
			//dd($e);
		}
	}

	public function xls_report_create(Request $request)
	{
		try {
			$tournaments = Tournament::has('tournament_inscriptions')->get();
			$tournament_inscriptions = TournamentInscriptionMembers::where([])->with('member','tournament');

			//filtro rango de fecha de torneo
			if (!is_null($request->start_date)) {
				$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
				if (!is_null($request->end_date)) {
					$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
					$tournament_inscriptions->join('tournaments', function ($join) use($start_date,$end_date) {
						$join->on('tournaments.id', '=', 'tournament_inscription_members.tournament_id')
							 ->whereDate('tournaments.start_date', '>=', $start_date)
							 ->whereDate('tournaments.end_date', '<=', $end_date);
					});
				} else {
					$tournament_inscriptions->join('tournaments', function ($join) use($start_date) {
						$join->on('tournaments.id', '=', 'tournament_inscription_members.tournament_id')
							 ->whereDate('tournaments.end_date', '<=', $start_date);
					});
				}
			} else{
				if (!is_null($request->end_date)) {
					$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
					$tournament_inscriptions->join('tournaments', function ($join) use($end_date) {
						$join->on('tournaments.id', '=', 'tournament_inscription_members.tournament_id')
							 ->whereDate('tournaments.end_date', '<=', $end_date);
					});
				}
			}
			//seleccion del torneo a mostrar
			if ($request->tournament_id != "null" && !is_null($request->tournament_id)) {
				$tournament_inscriptions->where('tournament_id', $request->tournament_id);
			} else {
				$tournament_inscriptions->where('tournament_id', $tournaments[0]->id);
			}
			$tournament_inscriptions = $tournament_inscriptions->sorted()->paginate(99999);

			foreach ($tournament_inscriptions as $key => $tournament_inscriptions) {
				$participante = strtoupper($tournament_inscriptions->member->name)." ".strtoupper($tournament_inscriptions->member->last_name)." ".$tournament_inscriptions->member->identity_card;

				$tournament_inscriptions_array[] = array(
					"FECHA DE INSCRIPCIÓN" => $tournament_inscriptions->created_at,
					"SOCIO" => $participante,
					"CATEGORÍA" => strtoupper($tournament_inscriptions->category->name),
					"ESTADO DE INSCRIPCIÓN" => ($tournament_inscriptions->waiting == 0) ? "INSCRITO" : "LISTA DE ESPERA",
				);
			}

			$excel = Excel::create('ListadoTorneo'.date('Ymdhis'), function($excel) use($tournament_inscriptions_array,$tournament_inscriptions) {

				// Set the title
				$excel->setTitle('Listado de reeservas '.date('Y-m-d'));

				// Chain the setters
				$excel->setCreator('Lagunita Country Club')->setCompany('Lagunita Country Club');

				$excel->setDescription('Listado de las inscripciones de socios en un torneo consultado Sistema para Alquiler de Canchas de Golf Lagunita Country Club');

				$excel->sheet('Listado', function ($sheet) use ($tournament_inscriptions_array, $tournament_inscriptions) {
					$sheet->setOrientation('landscape');
					$sheet->fromArray($tournament_inscriptions_array, NULL, 'A3');
					$data = array();

					$data['totales']['titulo'] = "Nº TOTAL DE INSCRIPCIONES DE SOCIOS EN EL TORNEO:";
					$data['totales']['cantidad'] = $tournament_inscriptions->count();
					$data['cantidad_inscritos']['titulo'] = "Nº DE SOCIOS INSCRITOS EN EL TORNEO:";
					$data['cantidad_inscritos']['cantidad'] = $tournament_inscriptions->where('waiting',0)->count();
					$data['cantidad_espera']['titulo'] = "Nº DE SOCIOS EN LISTA DE ESPERA:";
					$data['cantidad_espera']['cantidad'] = $tournament_inscriptions->where('waiting',1)->count();
					//dd($data);

					$row = count($tournament_inscriptions_array)+6;
					foreach ($data as $key => $value) {
						$row++;
						$sheet->row($row, array($value['titulo']." ".$value['cantidad']));
						// $row++;
						// $sheet->row($row, array($value['cantidad']));
					}
				});

			});

			$excel->export('xls');

		} catch (\Exception $e) {
			Log::info('Error al crear pdf'. $e);
			dd($e);
		}
	}
}

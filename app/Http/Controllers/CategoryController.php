<?php namespace Golf\Http\Controllers;

use Golf\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Gbrock\Table\Facades\Table;


class CategoryController extends Controller
{
	public function index()
	{
		if (Auth::user()->role_id != 2 )
		{
			return redirect()->route('home');
		}
		$categories = Category::all();

		return view('categories.all_categories', [
				'categories' => $categories,
			] );
	}
	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}
		// dd($request->name);

		DB::beginTransaction();

		try {

			$validation = Validator::make($request->input(), [
				'name' => 'required|max:50',
				'number_participants' => 'required|integer',
				'sex' => 'required|in:Masculino,Femenino,Mixto',
				'handicap_min' => 'required|numeric',
				'handicap_max' => 'required|numeric',
				'departure_order' => 'integer',
				'tournament_id' => 'required|integer',
			]);

			// dd($validation->fails());
			// dd($validation->messages());
			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}

			$category = new Category();

			$category->name = $request->name;
			$category->number_participants = $request->number_participants;
			$category->sex = $request->sex;
			$category->handicap_min = $request->handicap_min;
			$category->handicap_max = $request->handicap_max;
			$category->departure_order = 0;


			if (!$category->save()) {
				throw new \Exception("Error al registrar la categoria", 400);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			return response()->json(['line' => $e->getLine(),
								'code' => $e->getCode(),
								'status' => $e->getMessage(),
								'type' => 'error',
								'inputs' => $request->input()]);
		}

		DB::commit();
		return response()->json([ 'status' => 'La categoria se ha creado exitosamente', 'type' => 'success', 'category' => $category ], 200);
	}
	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function storeMain (Request $request)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}
		// dd($request->name);

		DB::beginTransaction();

		try {

			$validation = Validator::make($request->input(), [
				'name' => 'required|max:50',
				'number_participants' => 'required|integer',
				'genre' => 'required|in:Masculino,Femenino,Mixto',
				'handicap_min' => 'required|numeric',
				'handicap_max' => 'required|numeric',
				'departure_order' => 'integer',
			]);

			// dd($validation->fails());
			// dd($validation->messages());
			if ($validation->fails()) {
				//throw new \Exception($validation->errors(), 400);
				$this->throwValidationException($request, $validation);
			}

			$category = new Category();

			$category->name = $request->name;
			$category->number_participants = $request->number_participants;
			$category->sex = $request->genre;
			$category->handicap_min = $request->handicap_min;
			$category->handicap_max = $request->handicap_max;
			$category->departure_order = 0;
			//$category->tournament_id = $request->tournament_id;

			if (!$category->save()) {
				throw new \Exception("Error al registrar la categoria", 400);
			}
		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('categorias')
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de categorías con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('categorias')
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}

		DB::commit();
		return redirect()->route('categorias')
			->with([ 'status' => 'La categoria se ha creado exitosamente', 'type' => 'success', 'category' => $category ]);
	}
	public function show($user_id, $category_id)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		try {
			$category = Category::find( $category_id );

			if (empty($category)) {
				throw new \Exception("Error al obtener la categoria", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['category' => $category,
				'type' => 'success']);
	}
	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $category_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $tournament_id, $category_id)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$category = Category::where([
						[ 'tournament_id', '=', $tournament_id ],
						[ 'id', '=', $category_id ]
					])->first();

			if (!$category->save()) {
				throw new \Exception("Error al actualizar los datos la categoria", 400);
			}
		} catch(\Exception $e)
		{
			return response()->json(['line' => $e->getLine(),
								'code' => $e->getCode(),
								'status' => $e->getMessage(),
								'type' => 'error',
								'inputs' => $request->input()]);
		}

		DB::commit();
		return response()->json([
							'status' => 'La categoria se ha creado exitosamente',
							'type' => 'success' ], 200);
	}
	public function updateMain (Request $request, $category_id)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$validation = Validator::make($request->input(), [
				'name' => 'required|max:50',
				'number_participants' => 'required|integer',
				'genre' => 'required|in:Masculino,Femenino,Mixto',
				'handicap_min' => 'required|numeric',
				'handicap_max' => 'required|numeric',
				'departure_order' => 'integer'

			]);


			if ($validation->fails()) {
				//throw new \Exception($validation->errors(), 400);
				$this->throwValidationException($request, $validation);
			}

			$category = Category::where('id', $category_id)->first();
          	$category->name = $request->name;
			$category->number_participants = $request->number_participants;
			$category->sex = $request->genre;
			$category->handicap_min = $request->handicap_min;
			$category->handicap_max = $request->handicap_max;
			$category->departure_order = 0;

			if (!$category->save()) {
				throw new \Exception("Error al actualizar los datos la categoria");
			}

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('categorias')
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de socios con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('categorias')
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}

		DB::commit();
		return redirect()->route('categorias')->with([
							'status' => 'La categoria se ha creado exitosamente',
							'type' => 'success' ]);
	}
	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($tournament_id, $category_id)
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		try {

			$category = Category::where([
						[ 'tournament_id', '=', $tournament_id ],
						[ 'id', '=', $category_id ]
					])->first();

			$name = $category->name.' '.$category->last_name;

			if (!$category->delete()) {
				throw new \Exception("Error al eliminar la categoria", 400);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			return response()->json(['line' => $e->getLine(),
								'code' => $e->getCode(),
								'status' => $e->getMessage(),
								'type' => 'error']);
		}

		DB::commit();
		return response()->json(['status' => 'La categoria se ha eliminado exitosamente', 'type' => 'success'], 200)
				->withInput();
	}
	public function destroyMain ($category_id)
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		try
		{
			if (empty( $category_id ) )
			{
				throw new Exception("No se ha indicado el ID de la categoría a eliminar", 1);
			}

			$category = Category::find( $category_id );
			if ( !$category->delete() )
			{
				throw new Exception("Error al eliminar el Definición de reservación", 1);
			}
		}
		catch(\Exception $e)
		{
			return redirect()->route('categorias')
							->with( [ 'status' => 'Error al elimina Definición de reservación', 'type' => 'error' ] )
							->withInput();
		}
		return redirect()->route('categorias')
						->with( [ 'status' => 'Definición de Reservación se ha eliminado exitosamente', 'type' => 'success' ] );
	}
	public function changeStatus($category_id)
	{
		$category = Category::find($category_id);
		if ($category->enabled == 1) {
			$category->enabled = 0;
		}
		else{
			$category->enabled = 1;
		}
		$category->save();
		return response()->json(
			[
				'changed' => true,
				'message' => 'cambio exitoso'
		]);
	}
}

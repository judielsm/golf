<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Gbrock\Table\Facades\Table;
use Golf\DrawReservation;

class DrawReservationController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$draw_reservations = DrawReservation::where([])->orderBy('date','asc')->orderBy('start_time','asc')->sorted()->get();

		$table_draw_reservations = Table::create($draw_reservations, ['date' => 'Fecha']);
		
		$table_draw_reservations->addColumn('edit', 'Editar', function($draw_reservation) {
			$ruta = route('sorteo_reservacion.edit', $draw_reservation->id);
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_draw_reservations->addColumn('delete', 'Eliminar', function($draw_reservation) {
			$ruta = route('sorteo_reservacion.destroy', $draw_reservation->id);
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});

		return view('draw_reservations.all_draw_reservations', [
				'draw_reservations' => $draw_reservations,
				'table_draw_reservations' => $table_draw_reservations,
			] );
	}

	public function show()
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$draw_reservations = DrawReservation::where([])->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->paginate(10);;

		$table_draw_reservations = Table::create($draw_reservations, ['date' => 'Fecha']);
		
		$table_draw_reservations->addColumn('edit', 'Editar', function($draw_reservation) {
			$ruta = route('sorteo_reservacion.edit', $draw_reservation->id);
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_draw_reservations->addColumn('delete', 'Eliminar', function($draw_reservation) {
			$ruta = route('sorteo_reservacion.destroy', $draw_reservation->id);
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});

		return view('draw_reservations.all_draw_reservations', [
				'draw_reservations' => $draw_reservations,
				'table_draw_reservations' => $table_draw_reservations,
			] );
	}
}

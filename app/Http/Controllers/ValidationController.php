<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Golf\Validation;
use Illuminate\Support\Facades\Validator;
class ValidationController extends Controller
{
    public function index()
    {
        if (  Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}
        $validation = Validation::all()->first();

        return view('validations.index')->with('validation', $validation);
    }

    public function store(Request $request)
    {
        if (  Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}
        try{
            $message = null;
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'max_participants_direct' => 'required|numeric|min:1|max:99',
				'max_participants_draws' => 'required|numeric|min:1|max:99',
				'min_participants_direct' => 'required|numeric|min:1|max:99',
				'min_participants_draw' => 'required|numeric|min:1|max:99',
                'min_member_reservation' => 'required|numeric|min:1|max:99',
				'max_partners_member' => 'required|numeric|min:1|max:99',
				'max_reservation_day_member' => 'required|numeric|min:1|max:99',
                'max_reservation_month_partner' => 'required|numeric|min:1|max:99',
                'enabled_reservation_days' => 'required|numeric|min:1|max:99',

			]);

			// Validación si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}
            //se valida que los maximos no sean menores que los mínimos
            if(($request->max_participants_direct < $request->min_participants_direct) || ($request->max_participants_draws < $request->min_participants_draw)){
                $message = 'Los valores máximos deben ser mayores que los mínimos';
                throw new \Exception($message);
            }
            //
            /*if(($request->min_member_reservation > $validation->max_participants_direct) || ($request->min_member_reservation > $request->max_participants_draws)){
                $message = '';
                throw new \Exception($message);  
            }
            //
            if(($request->min_member_reservation > $validation->max_participants_direct) || ($request->min_member_reservation > $request->max_participants_draws)){
                $message = '';
                throw new \Exception($message);  
            }*/
            $validation = Validation::all()->first();
            $validation->max_participants_direct = $request->max_participants_direct ;
            $validation->max_participants_draws = $request->max_participants_draws;
            $validation->min_participants_direct = $request->min_participants_direct;
            $validation->min_participants_draw = $request->min_participants_draw;
            $validation->max_partners_member = $request->max_partners_member;
            $validation->min_member_reservation = $request->min_member_reservation;
            $validation->max_reservation_day_member = $request->max_reservation_day_member;
            $validation->max_reservation_month_partner	= $request->max_reservation_month_partner;
            $validation->enabled_reservation_days = $request->enabled_reservation_days;
            $validation->save();
        }catch(\Exception $e){
            if (isset($e->validator)){
                	$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}
                    return redirect('opciones')
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);
            }else{

            return redirect('opciones')->with(['status' => ($message) ? $message : 'Hubo un error al actualizar las opciones' , 'type' => 'error']);           
            }
        }
            return redirect('opciones')->with(['status' => 'Opciones actualizadas correctamente', 'type' => 'success']);
    }
}

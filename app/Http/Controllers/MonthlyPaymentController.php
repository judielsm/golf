<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use Golf\Member;
use Golf\MonthlyPayment;
use Golf\Period;
use Golf\Student;
use Golf\User;

use Validator;

class MonthlyPaymentController extends Controller
{
    public function index()
	{
		// Listar los pagos mensuales ordenados por número de recibo
		$monthlypayments = MonthlyPayment::where([])->orderBy('number_receipt', 'asc')->get();

		// Listar los periodos
		$periods = Period::where([])->get();
		//dd($periods);

		// Listar los estudiantes
		$students = Student::where([])->get();

		// Listar los miembros
		$members = Member::where([])->get();

		// Retorna al listado de pagos mensuales
		return view('monthly_payments.all_monthly_payments')
			->with('members', $members)
			->with('monthly_payments', $monthlypayments)
			->with('periods', $periods)
			->with('students', $students)
		;
	}

	public function show(Request $request, $user_id, $monthly_payment_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$monthly_payment = MonthlyPayment::find( $monthly_payment_id );

			if (!empty($monthly_payment->member->name) || !empty($monthly_payment->member->last_name))
			{
				$monthly_payment->member = $monthly_payment->member->name.' '.$monthly_payment->member->last_name;
			}

			if (!empty($monthly_payment->student->name) || !empty($monthly_payment->student->last_name))
			{
				$monthly_payment->student = $monthly_payment->student->name.' '.$monthly_payment->student->last_name;
			}

			if (!empty($monthly_payment->period->name))
			{
				$monthly_payment->period = $monthly_payment->period->name;
			}

			if (empty($monthly_payment)) {
				throw new \Exception("Error al obtener la mensualidad", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['monthly_payment' => $monthly_payment,
				'type' => 'success']);
	}

	public function create()
	{
		return view('monthly_payments.create_monthly_payment');
	}

	public function store(Request $request)
	{
		// Se valida que los usuarios de rol 3 sólo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacción
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'amount' => 'required|numeric',
				'date_payment' => 'required|date|date_format:d-m-Y',
				'method_payment' => 'required|min:3|max:20',
				'number_receipt' => 'required|numeric',
				'observation' => 'required|min:3|max:255',
				'member_id' => 'required',
				'period_id' => 'required',
				'student_id' => 'required',
			]);
			
			// Validación si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepción de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea una nueva mensualidad
			$monthly_payment = new MonthlyPayment();
			$monthly_payment->amount = $request->amount;
			$monthly_payment->date_payment = $request->date_payment;
			$monthly_payment->method_payment = $request->method_payment;
			$monthly_payment->number_receipt = $request->number_receipt;
			$monthly_payment->observation = $request->observation;
			$monthly_payment->member_id = $request->member_id;
			$monthly_payment->period_id = $request->period_id;
			$monthly_payment->student_id = $request->student_id;
		
			$observation = $monthly_payment->observation;

			// Si mensualidad no se guarda lleva a un mensaje de error
			if (!$monthly_payment->save()) {
				throw new \Exception("Error al registrar la mensualidad", 400);
			}

			// Redirecciona al listado de mensualidades con el dato guardado
			DB::commit();
			$response['response'] = $monthly_payment;
			$response['status'] = 'Se ha registrado satisfactoriamente la mensualidad';
			$response['type'] = 'success';
			return redirect()->route('mensualidades.index', array('user_id' => Auth::user()->id))
				->with($response);

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('mensualidades.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ])
						;
					break;
						
				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a través de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();
						
					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}
						
					// Se retorna al listado de mensualidades con los mensajes de diferentes errores
					DB::rollback();
					$response['response'] = false;
					$response['validations'] = $resultados;
					$response['line'] = $e->getLine();
					$response['code'] = $e->getCode();
					$response['input'] = $request->input();
					$response['type'] = 'error';

					return redirect()->route('mensualidades.index', array('user_id' => Auth::user()->id))
						->with($response);
			}
		}

	}

	public function edit($user_id, $monthly_payment_id)
	{
		// Listar los pagos mensuales ordenados por ID
		$monthly_payment = MonthlyPayment::where([
						[ 'id', '=', $monthly_payment_id ]
						])->first();

		// Listar los periodos
		$periods = Period::where([])->get();

		// Listar los estudiantes
		$students = Student::where([])->get();

		// Listar los miembros
		$members = Member::where([])->get();

		// Retorna a la vista de editar mensualidad
		return view('monthly_payments.edit_monthly_payment')
			->with('monthly_payment', $monthly_payment)
			->with('periods', $periods)
			->with('students', $students)
			->with('members', $members)
		;
	}

	public function update(Request $request, $user_id, $monthly_payment_id)
	{
		// Se valida que los usuarios de rol 3 sólo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}
		
		// Empieza la transacción
		DB::beginTransaction();
		try {

			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->all(), [
				'amount' => 'required|numeric',
				'date_payment' => 'required',
				'method_payment' => 'required|min:3|max:20',
				'number_receipt' => 'required|numeric',
				'observation' => 'required|min:3|max:255',
				'member_id' => 'required',
				'period_id' => 'required',
				'student_id' => 'required',
			]);
			
			// Validación si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepción de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se obtienen las mensualidades ordenados por ID
			$monthly_payment = MonthlyPayment::where([
				[ 'id', '=', $monthly_payment_id ]
			])->first();

			$monthly_payment->amount = $request->amount;
			$monthly_payment->date_payment = $request->date_payment;
			$monthly_payment->method_payment = $request->method_payment;
			$monthly_payment->number_receipt = $request->number_receipt;
			$monthly_payment->observation = $request->observation;
			$monthly_payment->member_id = $request->member_id;
			$monthly_payment->period_id = $request->period_id;
			$monthly_payment->student_id = $request->student_id;

			// Si la mensualidad no se guardó se envía un mensaje de error
			if (!$monthly_payment->save()) {
				throw new \Exception("Error al actualizar Mensualidad", 400);
			}

			// Se retorna al listado de mensualidades con los valores guardados
			DB::commit();
			$response['response'] = $monthly_payment;
			$response['status'] = 'Se ha actualizado satisfactoriamente la mensualidad';
			$response['type'] = 'success';
			return redirect()->route('mensualidades.index', array('user_id' => Auth::user()->id))
							->with($response);
			
		} catch(\Exception $e) {
			dd($e);
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('mensualidades.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;
			
				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a través de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();
					
					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}
					
					// Se retorna al listado de mensualidades con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('mensualidades.index', array('user_id' => Auth::user()->id))
					->with(['validations' => $resultados,
						'line'  => $e->getLine(),
						'code'  => $e->getCode(),
						'input' => $request->input(),
						'type'  => 'error']);
			
			}
		}

	}	
}

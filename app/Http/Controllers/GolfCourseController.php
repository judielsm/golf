<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Golf\GolfCourse;

class GolfCourseController extends Controller
{
	//

	public function index()
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$golf_courses = GolfCourse::where([])->sorted()->get();

		/* Creando Tabla */
		$table_golf_courses = Table::create($golf_courses, ['interval' => 'Intervalo','start_time' => 'hora de inicio','end_time' => 'hasta','status' => 'disponibilidad',]);

		$table_golf_courses->addColumn('edit', 'Editar', function($golf_course) {
			$ruta = route('campo_golf.eidt', $golf_course->id);
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_golf_courses->addColumn('delete', 'Eliminar', function($golf_course) {
			$ruta = route('campo_golf.destroy', $golf_course->id);
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});
		return view('golf_courses.golf_courses', ['table_golf_courses' => $table_golf_courses, 'golf_courses' => $golf_courses] );
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show()
	{



		return view('golf_courses.create_golf_course', ['campo_golf' => GolfCourse::first()]);
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		return view('golf_courses.create_golf_course');
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$golf_course = new GolfCourse();

			$golf_course->interval = $request->interval;
			$golf_course->start_time = $request->start_time;
			$golf_course->end_time = $request->end_time;
			$golf_course->status = $request->status;

			if (!$golf_course->save()) {

				DB::rollback();

				return redirect()->route( 'golf_courses.index' )
								->with( [ 'status' => 'Error al registrar el campo de golf ', 'type' => 'success' ] )
								->withInput();
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);

			return redirect()->route( 'golf_courses.index' )
							->with( [ 'status' => 'Error al registrar campo de golf 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route( 'golf_courses.index' )
						->with( [ 'status' => 'Invitado se ha creado exitosamente', 'type' => 'success' ] );
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $golf_course )
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		$golf_course = GolfCourse::where([
							[ 'id', '=', Auth::user()->id ]
						])->first();

		return view('golf_courses.edit_golf_course', ['golf_course' => $golf_course]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $golf_course
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $golf_course)
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}


		DB::beginTransaction();

		try {

			$golf_course = GolfCourse::where([
								[ 'id', '=', Auth::user()->id ]
							])->first();

			$golf_course->interval = $request->interval;
			$golf_course->start_time = $request->start_time;
			$golf_course->end_time = $request->end_time;
			$golf_course->status = $request->status;

			if (!$golf_course->save()) {
				return redirect()->route('campo_golf.eidt')
					->with(['status' => 'Error al actualizar los datos del campo de golf', 'type' => 'error'])
					->withInput();
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);

			return redirect()->route( 'golf_courses.index' )
							->with( [ 'status' => 'Error al actualizar campo de golf 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route( 'golf_courses.index' )
			->with(['status' => 'Se ha actualizado satisfactoriamente del campo de golf', 'type' => 'success']);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $golf_course
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */

	public function change_status( $element = 'court' )
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}
		$status = 0;

		try {
			$golf_course = GolfCourse::all()->first();

			if ( $element == 'court' )				
			{
				$status = $golf_course->status ? 0 : 1;
				$golf_course->status = $status;
			}
			else
			{
				$status = $golf_course->cars_status ? 0 : 1;
				$golf_course->cars_status = $status;
			}
			if ( $golf_course->save() ) {
				return response()->json(['changed' => true, 'status' => $status]);
			}
		} catch(\Exception $e)
		{
			return response()->json([ 'changed' => false, 'status' => $status , 'message' => 'Error al actualizar el status de la cancha.', 'error' => $e->getMessage() ] );
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $golf_course_id )
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$golf_course = GolfCourse::where([
								[ 'id', '=', $golf_course_id ]
							])->first();

			if (!$golf_course->delete()) {
				return redirect()->route( 'golf_courses.index' )
					->with(['status' => 'Error al eliminar el campos golf', 'type' => 'error']);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);

			return redirect()->route( 'golf_courses.index' )
							->with( [ 'status' => 'Error al eliminar campo de golf 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route( 'golf_courses.index' )
			->with(['status' => 'Se ha eliminado satisfactoriamente el campo de golf', 'type' => 'success']);
	}

	/**
	 * Enable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function enable( $golf_course_id )
	{
		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$golf_course = GolfCourse::where([
								[ 'id', '=', $golf_course_id ]
							])->first();

			$golf_course->enable = 1;

			$golf_course->deleted_at = NULL;

			if ( !$golf_course->save() ) {
				return redirect()->route('campos_golfs.index')
					->with(['status' => 'Error al habilitar el campos golf', 'type' => 'error']);
			} else {
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//	dd($e);

			return redirect()->route( 'campos_golfs.index' )
							->with( [ 'status' => 'Error al habilitar campos golf', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route('campos_golfs.index')
			->with(['status' => 'Se ha habilitado satisfactoriamente el campos golf', 'type' => 'success']);
	}

	/**
	 * Disable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function disable( $golf_course_id )
	{

		if (Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$golf_course = GolfCourse::where([
								[ 'id', '=', $golf_course_id ]
							])->first();

			$golf_course->enable = 0;

			if ( !$golf_course->save() ) {
				return redirect()->route('campos_golfs.index')
					->with(['status' => 'Error al deshabilitar el campos de golf', 'type' => 'error']);
			} else {
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//	dd($e);

			return redirect()->route( 'campos_golfs.index' )
							->with( [ 'status' => 'Error al deshabilitar campos golf', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route('campos_golfs.index')
			->with(['status' => 'Se ha deshabilitado satisfactoriamente el campos golf', 'type' => 'success']);
	}
}

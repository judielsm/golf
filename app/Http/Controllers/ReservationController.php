<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Facades\Golf\Facades\Payment;
use Gbrock\Table\Facades\Table;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Golf\Http\Util\MailToMember;
use DB;
use Excel;
use Log;
use PDF;
use Mail;

use Golf\CommentsReservation;
use Golf\DayDraw;
use Golf\DayBlocked;
use Golf\DrawReservation;
use Golf\Draw;
use Golf\Member;
use Golf\Partner;
use Golf\ReservationDefinition;
use Golf\Reservation;
use Golf\TimeDraw;
use Golf\Tournament;
use Golf\Transaction;
use Golf\TypePayment;
use Golf\User;
use Golf\GreenReservation;

class ReservationController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		$members = Member::get();
		$reservations = Reservation::where([])->with(['user','comments'])->sorted()->orderBy('id', 'ASC')->get();

		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}

		// dd($day_blockeds);
		return view('reservations.all_reservations', [
					'reservations' => $reservations,
					'members' => $members,
				]);
	}

	public function check_reservation(Request $request)
	{

		if (Auth::user()->role_id != 2 && Auth::user()->role_id != 8 && Auth::user()->role_id != 5) {
			return redirect()->route('home');
		}


		try {
			$date = Carbon::createFromFormat('d-m-Y', $request->date);

			$day_draw = DayDraw::whereDate('date', $date->toDateString())
						->where('day_draw_status', '=', 0)
						->first();

			if (!empty($day_draw) && !empty($request->start_time)) {

				$time_draw = TimeDraw::where([
						['start_time', '<=', date('H:i:s', strtotime($request->start_time))],
						['end_time', '>=', date('H:i:s', strtotime($request->start_time))],
						['day_draw_id', '=', $day_draw->id]
					])
					->first();
			}

			$response = array();

			/* code 0 = hay campeonatos*/
			/* code 1 = no disponible*/
			/* code 2 = es sorteable por fin de semana*/
			/* code 3 = es sorteable por que esta agregado como sorteable*/
			/* code 4 = disponible*/

			if (empty($request->date)) {
				$response['response'] = true;
				$response['message'] = "Los campos de fecha y hora no pueden estar vacios";
				$response['code'] = 5;
			}else {
				$tournament = Tournament::whereDate('start_date', '<=', $date->toDateString())
										->whereDate('end_date', '>=', $date->toDateString())
										->first();
				if (empty($tournament)) {
					if (!empty($request->start_time)) {
						$reservation = Reservation::whereDate('date', '=', $date->toDateString())
							->where([
								['start_time', '=', date('H:i', strtotime($request->start_time))],
							])->first();

						if (!empty($time_draw)) {
							$response['response'] = true;
							$response['message'] = "La reserva es sorteable";
							$response['code'] = 3;
						} else {
							if (!empty($reservation)) {
								if (!empty($request->reservation_id) && ($request->reservation_id == $reservation->id)) {
									throw new \Exception("El horario seleccionado es el mismo de la reservacion", 6);
								} else {
									throw new \Exception("Ya existe una reserva para este horario", 1);
								}
							} else {
								$response['response'] = true;
								$response['message'] = "La reserva esta disponible";
								$response['code'] = 4;
							}
						}
					} else {
						$response['response'] = true;
						$response['message'] = "La reserva esta disponible";
						$response['code'] = 4;
					}
				} else {
					if (empty($request->start_time)) {
						$response['response'] = true;
						$response['message'] = "La reserva esta disponible";
						$response['code'] = 4;
					} else {
						if (($tournament->start_time <= $request->start_time) && ($tournament->end_time >= $request->start_time)) {
							$response['response'] = true;
							$response['message'] = "El horario seleccionado no está disponible torneo";
							$response['code'] = 0;
						} else {
							$response['response'] = true;
							$response['message'] = "La reserva esta disponible";
							$response['code'] = 4;
						}
					}
				}
			}
			$response['type'] = "success";

		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getCode();
			$response['line'] = $e->getLine();
			$response['file'] = $e->getFile();
			return response()->json($response);

		}

		$reservations = Reservation::whereDate('date', $date->toDateString())
					->whereIn('status', ['En proceso','Aprobada', 'En proceso','Sorteable'])
					->get();

		$tournaments = Tournament::whereDate('start_date', '<=', $date->toDateString())
									->whereDate('end_date', '>=', $date->toDateString())
									->get();

		$day_draws = DayDraw::whereDate('date', $date->toDateString())
					->with('time_draws')
					//->where('enabled', 0)
					->get();
		$response['reservations'] = $reservations;
		$response['tournaments'] = $tournaments;
		$response['day_draws'] = $day_draws;

		return response()->json($response);
	}

	public function create()
	{
		if (Auth::user()->role_id != 2 && Auth::user()->role_id != 5) {
			return redirect()->route('home');
		}

		$type_payments = TypePayment::get();
		$reservation_definition = ReservationDefinition::where('enabled',1)->orderBy('created_at', 'desc')->get();
		$members = Member::get();

		$draws = Draw::where([
			['draw_date', '>=', Carbon::now()->format('Y-m-d')],
			['draw_status', '=', 0]
		])->get();
		foreach ($draws as $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $day_draw) {
				$day_draw->time_draws;
			}



		}
		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();
		$day_blockeds = DayBlocked::with('time_blockeds')->get();


		return view('reservations.create_reservation',[
			'type_payments' => $type_payments,
			'members' => $members,
			'draws' => $draws,
			'reservation_definition' => $reservation_definition,
			'tournaments' => $tournaments,
			'day_blockeds' => $day_blockeds,
			'validations' => $this->validations

		]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		if (Auth::user()->role_id != 2 &&  Auth::user()->role_id != 8 &&  Auth::user()->role_id != 5 ) {
			return redirect()->route('home');
		}

		$check = $this->check_reservation($request);

		if (($check->original['code'] > 1) && ($check->original['code'] < 5)) {

			DB::beginTransaction();

			try {
				$reservations_defintions_array = array();
				$total_green_fee = 0;
				if (!empty($request->partner_id) && isset($request->reservation_definition_id )) {
					foreach ($request->reservation_definition_id as $key ) {

						$reservation_definition = ReservationDefinition::where([
						['id', '=', $request->reservation_definition_id],
						])->first();
						array_push($reservations_defintions_array, $reservation_definition);
					}

					foreach ($reservations_defintions_array as $key => $value) {
						$total_green_fee += $value->green_fee;
					}
				}

				$reservation = new Reservation();
				$reservation->receipt_payment = $request->receipt_payment;
				//$reservation->reservation_definition_id = $reservations_defintions_array[0]->id;
				// En proceso cuando se crea la reservacion que no es sorteo y Sorteable para cuando el dia y rango es sorteable
				$reservation->status = ($check->original['code'] == 4) ? "Aprobada" : "Sorteable" ;
				$reservation->detail = $request->detail;
				$reservation->date = $request->date;
				$reservation->start_time = $request->start_time;
				$reservation->total_green_fee = $total_green_fee;
				$reservation->user_id = Auth::user()->id;

				if ($reservation->save()) {
					//---------------se gurdan los green con su reservacion--------------------
					foreach ($reservations_defintions_array as $green)
					{
						$greenReservation = new GreenReservation();
						$greenReservation->green_id =  $green->id;
						$greenReservation->reservation_id = $reservation->id;
						$greenReservation->save();
					}


					//---------------se gurdan los green con su reservacion--------------------
					//	crear la registra la solicitud de reservacion para sorteo
					if ($check->original['code'] == 3) {
						$participants_count = sizeof($request->partner_id) +  sizeof($request->member_id);
						if ($participants_count < 3 ) {
							throw new \Exception("La Reserva sorteable debe poseer minimo 3 participantes", 400);
						}
						//se obtiene el DayDraw para la reservacion
						$day_draw = DayDraw::whereDate('date', date('Y-m-d',strtotime($request->date)))
											->where('day_draw_status', '=', 0)
											->first();
						if (empty($day_draw)) {
								throw new \Exception("Error al procesar la reserva", 400);
						}

						//se continua para obetener el TimeDraw correspondiente
						$time_draw = TimeDraw::where([
								['start_time', '<=', date('H:i:s', strtotime($request->start_time))],
								['end_time', '>=', date('H:i:s', strtotime($request->start_time))],
								['day_draw_id', '=', $day_draw->id]
							])
							->first();

						if (empty($time_draw)) {
							throw new \Exception("Error al procesar la reserva", 400);
						}
						$draw_reservation = new DrawReservation();
						$draw_reservation->date = $request->date;
						$draw_reservation->time = $request->start_time;
						$draw_reservation->time_draw_id = $time_draw->id;
						$draw_reservation->reservation_id = $reservation->id;
						if (!$draw_reservation->save()) {
							throw new \Exception("Error al procesar la reserva", 400);
						}
					}

					$reservation->where('id', '=', $reservation->id)->first();

					// se valida si hay pagador
					if ( $total_green_fee != 0 ) {
						if ( !empty( $request->payer_id ) ) {

							//registro de transaccion de pago de greenfee
							$transaction = array();
							$transaction['amount'] = $total_green_fee;
							//1 para socio
							$transaction['type_payer'] = 1;
							//1 para socio
							$transaction['payer_id'] = $request->payer_id;
							$transaction['product_id'] = $reservation->id;
							//1 para reservacion
							$transaction['type_product_id'] = 1;
							$transaction['transaction_status_id'] = 2;
							$transaction['type_payment_id'] = $request->type_payment_id;

							$transaction_response = Payment::transaction($transaction);
							if ($transaction_response['code'] = 0) {
								throw new \Exception($transaction['message'], 400);
							}
						}
					}

					if (!empty($request->member_id)) {
						if (!is_null($reservation->members()->attach($request->member_id))) {
							throw new \Exception("Error al procesar la reserva", 400);
						}
					}
					//formateando data para insercion en tabla
					
					if (!empty($request->partner_id)) {
						$partners = array();
						$exonerateds = isset($request->exonerated)? $request->exonerated:array();
						foreach ($request->partner_id as $key => $value) {
							$is_exonerated = false;
								foreach ($exonerateds as $key2 => $exonerated) {
									if ($exonerated == $value) {
										$is_exonerated = true;
										break;
									}
								}
							if ($is_exonerated) {
								array_push($partners,[ 'green_id' => null,
													'partner_id' =>  $value,
													'exonerated' => 1
													]);
							}else{
							array_push($partners,[ 'green_id' => $request->reservation_definition_id[$key],
													'partner_id' =>  $value,
													'exonerated' => 0
													]);
							}
							
						} 
						
						if (!is_null($reservation->partners()->attach($partners))) {
							throw new \Exception("Error al procesar la reserva", 400);
						}
					}

					if (!empty($request->partner_id)) {
						//actualizacion de la exoneracion por reservacion
						foreach ($request->partner_id as $key => $partner_id) {
							$partner = Partner::where([
								['exonerated', '=', 'Reservacion' ],
								['id', '=', $partner_id ]
							])->first();

							if ($partner) {
								$partner->exonerated = Null;
								if (!$partner->save()) {
									throw new \Exception("Error al procesar la reserva", 400);
								}

							}
						}
					}
				}else {
					throw new \Exception("Error al procesar la reserva", 400);
				}
			} catch(\Exception $e)
			{
				DB::rollback();
				return redirect()->route('reservaciones.index',
										array('user_id' => Auth::user()->id)
									)->with(['status' => $e->getMessage(),
											'line' => $e->getLine(),
											'code' => $e->getCode(),
											'type' => 'error']);
			}
			DB::commit();
			$reservation->partners;
			$reservation->members;

			$transaction_payer = Transaction::where([
									['payer_id','=',$request->payer_id],
									['product_id','=',$reservation->id],
									['type_product_id','=',1]])
									->first();
			if($transaction_payer){
				$transaction_payer->payer;

				$nombre_destinatario = ucwords($transaction_payer->payer->name.' '.$transaction_payer->payer->last_name);

				$parametros = [
								'reservation'=>$reservation,
								'transaccion'=>$transaction_payer,
								'nombre_destinatario' => $nombre_destinatario,
								'total_green_fee' => $total_green_fee
							];

				Mail::send('emails.reservation', $parametros, function ( $m ) use ( $transaction_payer, $nombre_destinatario  ) {

					$m->from(env('MAIL_USERNAME'), 'Reservación Lagunita');
					$m->to($transaction_payer->payer->user->email, $nombre_destinatario)->subject('Reservación');
				});
			}else{
				$reservation->members;
				$member = $reservation->members[0];
				$nombre_destinatario = ucwords($member->name.' '.$member->last_name);
				$correo_destinatario = $member->user->email;
				$parametros = [
								'reservation'=>$reservation,
								'transaccion'=>$transaction_payer,
								'nombre_destinatario' => $nombre_destinatario,
								'total_green_fee' => $total_green_fee
							];

				Mail::send('emails.reservation', $parametros, function ( $m ) use ( $transaction_payer, $nombre_destinatario, $correo_destinatario  ) {

					$m->from(env('MAIL_USERNAME'), 'Reservación Lagunita');
					$m->to($correo_destinatario, $nombre_destinatario)->subject('Reservación');
				});
			}
			if ($request->isUpdate) {
				return $reservation;
			}else{
				return redirect()->route('reservaciones.store',
								array('user_id' => Auth::user()->id)
							)->with([ 'status' => 'La reservación se ha creado exitosamente', 'type' => 'success' ]);
			}
		} else {

			$check = $check->original;
			if ($request->isUpdate) {
				return false;
			}else{
				return redirect()->route('reservaciones.index',
								array('user_id' => Auth::user()->id)
							)->with(['status' => $check['message'],
									'code' => $check['code'],
									'type' => 'error' ]);
			}

		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($user_id, $reservation_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		DB::beginTransaction();

		try {

			$reservation = Reservation::where([
								[ 'id', '=', $reservation_id ]
							])->first();

			foreach ($reservation->members as $key => $value) {
				if (!$value->pivot->delete()) {
					return redirect()->route('reservaciones.index',
								array('user_id' => Auth::user()->id)
							)->with(['status' => 'Error al eliminar la reservación 1', 'type' => 'error']);
				}
			}

			foreach ($reservation->partners as $key => $value) {
				if (!$value->pivot->delete()) {
					return redirect()->route('reservaciones.index',
								array('user_id' => Auth::user()->id)
							)->with(['status' => 'Error al eliminar la reservación 2', 'type' => 'error']);
				}
			}
			if (isset($reservation->draw_reservation)) {
				if (!$reservation->draw_reservation->delete()) {
					return redirect()->route('reservaciones.index',
								array('user_id' => Auth::user()->id)
							)->with(['status' => 'Error al eliminar la reservación 3', 'type' => 'error']);
				}
			}


			if (!$reservation->delete()) {
				return redirect()->route('reservaciones.index',
									array('user_id' => Auth::user()->id)
								)->with(['status' => 'Error al eliminar la reservación 4', 'type' => 'error']);
			}

		} catch(ValidationException $e)
		{
			DB::rollback();
			return redirect()->route('reservaciones.index',
								array('user_id' => Auth::user()->id)
							)->with([ 'status' => 'Error al eliminar la reservación 5', 'type' => 'error' ]);

		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route('reservaciones.index',
								array('user_id' => Auth::user()->id)
							)->with([ 'status' => 'Error al eliminar la reservación 6', 'type' => 'error' ]);
		}
		DB::commit();
		return redirect()->route('reservaciones.index',
							array('user_id' => Auth::user()->id)
						)->with(['status' => 'Se ha eliminado satisfactoriamente la reservación', 'type' => 'success']);
	}

	public function get_reservations(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		try {
			$date = Carbon::createFromFormat('d-m-Y', $request->date);

			$reservations = Reservation::whereDate('date', $date->toDateString())
						->whereIn('status', ['En proceso','Aprobada', 'En proceso','Sorteable'])
						->get();
			if (empty($reservations)) {
				throw new \Exception("No hay reservas para la fecha seleccionada", 400);
			}else {
				$response['reservations'] = $reservations;
				$response['type'] = "success";
			}
		} catch (\Exception $e) {
			$response['status'] = $e->getMessage();
			$response['code'] = $e->getCode();
			$response['line'] = $e->getLine();
			$response['type'] = "error";
		}
		return response()->json($response);
	}

	public function list_reservations(Request $request)
	{

		$reservations = Reservation::where([]);

		if (!is_null($request->start_date)) {
			$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
			$reservations->whereDate('date', '>=', $start_date->toDateString());
		}

		if (!is_null($request->end_date)) {
			$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
			$reservations->whereDate('date', '<=', $end_date->toDateString());
		}else{
			if (!is_null($request->start_date)) {
				$reservations->whereDate('date', '<=', $start_date->toDateString());
			}
		}
		if ($request->status != "null" && !is_null($request->status)) {
			$reservations->where('status', $request->status);
		}
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->get();

		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}

		return view('reservations.list_report', [
					'reservations' => $reservations,
					'input' => $request->input()
				]);
	}

	public function pdf_report_create(Request $request)
	{
		try {


			$vista_url = 'reservations.pdf_report_reservations';

			$reservations = Reservation::where([]);

			if (!is_null($request->start_date)) {
				$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
				$reservations->whereDate('date', '>=', $start_date->toDateString());
			}

			if (!is_null($request->end_date)) {
				$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
				$reservations->whereDate('date', '<=', $end_date->toDateString());
			}else{
				if (!is_null($request->start_date)) {
					$reservations->whereDate('date', '<=', $start_date->toDateString());
				}
			}
			if ($request->status != "null" && !is_null($request->status)) {
				$reservations->where('status', $request->status);
			}
			$reservations = $reservations->orderBy('date','asc')->orderBy('start_time','asc')->paginate(999999);

			$data = array();
			$data['totales'] = $reservations->count();
			$data['sorteables'] = $reservations->where('status','Sorteable')->count();
			$data['aprobadas'] = $reservations->where('status','Aprobada')->count();
			$data['canceladas'] = $reservations->where('status','Cancelada')->count();
			$data['cerradas'] = $reservations->where('status','Cerrada')->count();
			$data['finalizadas'] = $reservations->where('status','Finalizada')->count();
			$data['proceso'] = $reservations->where('status','En proceso')->count();
			$data['reservations'] = $reservations;
			$data['start_date'] = $request->start_date;
			$data['end_date'] = $request->end_date;
			$data['status'] = $request->status;
			$data['title'] = "Reporte de Reservaciones";

			$pdf = PDF::loadView($vista_url, $data);

			return $pdf->download('Reservaciones.pdf');

		} catch (\Exception $e) {
			Log::info('Error al crear pdf'. $e);
			//dd($e);
		}
	}

	public function xls_report_create(Request $request)
	{
		try {

			$reservations = Reservation::where([])->with('members','partners');

			if (!is_null($request->start_date)) {
				$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
				$reservations->whereDate('date', '>=', $start_date->toDateString());
			}

			if (!is_null($request->end_date)) {
				$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
				$reservations->whereDate('date', '<=', $end_date->toDateString());
			}else{
				if (!is_null($request->start_date)) {
					$reservations->whereDate('date', '<=', $start_date->toDateString());
				}
			}
			if ($request->status != "null" && !is_null($request->status)) {
				$reservations->where('status', $request->status);
			}

			$reservations = $reservations->orderBy('date','asc')->orderBy('start_time','asc')->paginate(999999);
			foreach ($reservations as $key => $reservation) {
				$socios = '';
				$invitados = '';
				foreach ($reservation->members as $key => $member) {
					if (empty($socios)) {
						$socios = strtoupper($member->name)." ".strtoupper($member->last_name)." ".$member->identity_card;
					} else {
						$socios .= ', '.PHP_EOL.strtoupper($member->name)." ".strtoupper($member->last_name)." ".$member->identity_card;
					}
				}
				foreach ($reservation->partners as $key => $partner) {
					if (empty($invitados)) {
						$invitados = strtoupper($partner->name)." ".strtoupper($partner->last_name)." ".$partner->identity_card;
					} else {
						$invitados .= ', '.PHP_EOL.strtoupper($partner->name)." ".strtoupper($partner->last_name)." ".$partner->identity_card;
					}
				}
				$reservations_array[] = array(
					"FECHA" => trim($reservation->date),
					"HORA" => $reservation->start_time,
					"ESTADO" => strtoupper($reservation->status),
					"SOCIOS" => ($socios)?$socios:'SIN SOCIOS',
					"INVITADOS" => ($invitados)?$invitados:'SIN INVITADOS',
				);
			}

			$excel = Excel::create('ListadoReservaciones'.date('Ymdhis'), function($excel) use($reservations_array, $reservations) {

				// Set the title
				$excel->setTitle('Listado de reservas '.date('Y-m-d'));

				// Chain the setters
				$excel->setCreator('Lagunita Country Club')->setCompany('Lagunita Country Club');

				$excel->setDescription('Listado de las reservas consultadas en el Sistema para Alquiler de Canchas de Golf Lagunita Country Club');

				$excel->sheet('Listado', function ($sheet) use ($reservations_array, $reservations) {
					$sheet->setOrientation('landscape');
					$sheet->fromArray($reservations_array, NULL, 'A3');

					$data = array();
					$data['totales']['titulo'] = "Nº TOTAL DE RESERVACIONES:";
					$data['totales']['cantidad'] = $reservations->count();
					$data['sorteables']['titulo'] = "Nº TOTAL DE RESERVACIONES SORTEABLES:";
					$data['sorteables']['cantidad'] = $reservations->where('status','Sorteable')->count();
					$data['aprobadas']['titulo'] = "Nº TOTAL DE RESERVACIONES APROBADAS:";
					$data['aprobadas']['cantidad'] = $reservations->where('status','Aprobada')->count();
					$data['canceladas']['titulo'] = "Nº TOTAL DE RESERVACIONES CANCELADAS:";
					$data['canceladas']['cantidad'] = $reservations->where('status','Cancelada')->count();
					$data['cerradas']['titulo'] = "Nº TOTAL DE RESERVACIONES CERRADAS:";
					$data['cerradas']['cantidad'] = $reservations->where('status','Cerrada')->count();
					$data['finalizadas']['titulo'] = "Nº TOTAL DE RESERVACIONES FINALIZADAS:";
					$data['finalizadas']['cantidad'] = $reservations->where('status','Finalizada')->count();
					$data['proceso']['titulo'] = "Nº TOTAL DE RESERVACIONES EN PROCESO:";
					$data['proceso']['cantidad'] = $reservations->where('status','En proceso')->count();

					$row = count($reservations_array)+6;
					foreach ($data as $key => $value) {
						$row++;
						$sheet->row($row, array($value['titulo']." ".$value['cantidad']));

					}
				});
			});
			$excel->export('xls');

		} catch (\Exception $e) {
			Log::info('Error al crear el excel'. $e);

		}
	}

	public function departure_list(Request $request)
	{
		// Se valida si no es un usuario de rol 2
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');


		// La fecha del dia de hoy
		$actual_date = Carbon::today();

		// Se lista las reservaciones donde la fecha sea igual a la fecha de hoy
		$reservations = Reservation::whereDate('date', '=', $actual_date->toDateString());

		// Se ordena la tabla por fecha de forma ascendente junto con la hora
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->get();

		// Se recorre cada socio en invitado del arreglo
		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}


		// Se retorna la vista de lista de salida
		return view('reservations.departure_list', [
			'reservations' => $reservations,
			'input' => $request->input()
		]);

	}
	public function comments(Request $request)
	{
		$reservations = Reservation::where([])->with(['user','comments'])->orderBy('id', 'ASC')->get();
		// dd($reservations);
		return view('reservations.comments', [
					'reservations' => $reservations,
				]);
	}

	public function delete_comment($id)
	{
				if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		$comment = CommentsReservation::find($id);
		if ($comment->delete()) {
				return true;
		}
		return false;

	}
	public function update_comment(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		$comment = CommentsReservation::find($request->comment_id);
		try{
			$comment->date = date('Y-m-d');
			$comment->time =   date('H:i:s',strtotime($request->start_time));
			$comment->comment = $request->comment;
			$comment->save();
		}catch(\Exception $e){
			return false;
		}
		return redirect()->route('reservaciones.comments', array('user_id' => Auth::user()->id));
	}
}

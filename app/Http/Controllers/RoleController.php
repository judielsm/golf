<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\Role;

class RoleController extends Controller
{

	public function index()
	{
		

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$roles = Role::where([
		//					['user_id', '=', $user->user_id],
						])->orderBy('name', 'asc')->get();

		// echo "<pre>";  var_dump($roles); echo "</pre>";
		// return;

		return view('roles.all_roles', ['roles' => $roles] );
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create_role()
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$role = Role::where([
		//					[ 'user_id', '=', $user->user_id ]
						])->first();

		return view('roles.create_role', ['role' => $role]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store_role( Request $request )
	{

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$role = new Role();
		$role->name = $request->name;
		$role->enabled = 1;


		if ($role->save()) {
			return redirect()->route( 'all_roles' )
							->with( [ 'status' => 'Invitado se ha creado exitosamente', 'type' => 'success' ] );
		}else {
			return redirect()->route( 'create_role' )
							->with( [ 'status' => 'Error al registrar invitado ', 'type' => 'success' ] )
							->withInput();
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit_role( $role_id )
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$role = Role::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $role_id ]
						])->first();

		return view('roles.edit_role', ['role' => $role]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $role_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update_role(Request $request, $role_id)
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$role = Role::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $role_id ]
						])->first();
		$role->name = $request->name;

		if ($role->save()) {
			return redirect()->route('all_roles')
				->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$role->name.' '.$role->last_name, 'type' => 'success']);
		} else {
			return redirect()->route('edit_role')
				->with(['status' => 'Error al actualizar los datos de:'.$role->name. ' ' .$role->last_name, 'type' => 'error'])
				->withInput();
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function delete_role( $role_id )
	{

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$role = Role::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $role_id ]
						])->first();

		$name = $role->name.' '.$role->last_name;

		if ($role->delete()) {
			return redirect()->route('all_roles')
				->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
		} else {
			return redirect()->route('all_roles')
				->with(['status' => 'Error al eliminar el invitado:'.$name, 'type' => 'error']);
		}
	}

}

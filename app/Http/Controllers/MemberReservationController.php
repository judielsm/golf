<?php namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Mail;


use Golf\DayDraw;
use Golf\Draw;
use Golf\Partner;
use Golf\Member;
use Golf\ReservationDefinition;
use Golf\DrawReservation;
use Golf\DayBlocked;
use Golf\Reservation;
use Golf\TimeDraw;
use Golf\Tournament;
use Golf\Transaction;
use Golf\TypePayment;
use Golf\User;
use Golf\GreenReservation;
use Golf\MemberReservation;
use Gbrock\Table\Facades\Table;
use Carbon\Carbon;
use Golf\Http\Util\MailToMember;
use Facades\Golf\Facades\Payment;

class MemberReservationController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}

		$day_draws = DayDraw::get();

		$member = Auth::user()->member;

		$reservations = Reservation::where([
			['user_id', '=', Auth::user()->id],
		])->orderBy('created_at', 'desc')->get();
		$members = Member::get();

		foreach ($reservations as $key => $reservation) {

			$reservation->partners;
			$reservation->members;
		}
		return view('member_reservations.all_reservations', [
			'reservations' => $reservations,
			'members' => $members,
			'day_draws' => json_encode($day_draws),
		]);
	}

	public function check_reservation(Request $request)
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}

		try {
			$mem = Auth::user()->member;
			$date = Carbon::createFromFormat('d-m-Y', $request->date);

			$day_draw = DayDraw::whereDate('date', $date->toDateString())
						->where('day_draw_status', '=', 0)
						->first();

			$reservation_in_day = Reservation::select(['reservations.*'])->whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
							->whereDate('date', $date->toDateString())
							->leftjoin('member_reservations', function ($join) {
								$join->on('reservations.id', '=', 'member_reservations.reservation_id');
							})
							->leftjoin('members', function ($join) {
								$join->on('member_reservations.member_id', '=', 'members.id');
							})
							->where('reservations.user_id', '=', Auth::user()->id)
							->first();
			if(!$reservation_in_day){
				$reservation_in_day = MemberReservation::where('member_reservations.member_id', $mem->id)
														->whereIn('reservations.status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
														->leftjoin('reservations','member_reservations.reservation_id','reservations.id')
														->whereDate('date', $date->toDateString())->first();
			}
			if (!empty($day_draw) && !empty($request->start_time)) {
					$time_draw = TimeDraw::where([
							['start_time', '<=', date('H:i:s', strtotime($request->start_time))],
							['end_time', '>=', date('H:i:s', strtotime($request->start_time))],
							['day_draw_id', '=', $day_draw->id]
						])
						->first();
			}

			$response = array();

			if (empty($request->date)) {
				throw new \Exception("Los campos de fecha y hora no pueden estar vacios ", 5);
			}else {
				$tournament = Tournament::whereDate('start_date', '<=', $date->toDateString())
										->whereDate('end_date', '>=', $date->toDateString())
										->first();
										//dd(empty($tournament));
				if (empty($tournament)) {
					if (!empty($request->start_time)) {
						$reservation = Reservation::whereDate('date', '=', $date->toDateString())
							->where([
								['start_time', '=', date('H:i', strtotime($request->start_time))],
							])->first();

						if (!empty($time_draw)) {

							$response['response'] = true;
							$response['message'] = "La reserva es sorteable";
							$response['code'] = 3;
						} else {
							if (!empty($reservation)) {
								if (!empty($request->reservation_id) && ($request->reservation_id == $reservation->id)) {
									throw new \Exception("El horario seleccionado es el mismo de la reservacion", 6);
								} else {
									throw new \Exception("Ya existe una reserva para este horario", 1);
								}
							} else {////
								$response['response'] = true;
								$response['message'] = "La reserva esta disponible";
								$response['code'] = 4;
							}
						}
					} else {
						$response['response'] = true;
						$response['message'] = "La reserva esta disponible";
						$response['code'] = 4;
					}
				} else {
					if (empty($request->start_time)) {
						$response['response'] = true;
						$response['message'] = "La reserva esta disponible";
						$response['code'] = 4;
					} else {
						if (($tournament->start_time <= $request->start_time) && ($tournament->end_time >= $request->start_time)) {
							$response['response'] = true;
							$response['message'] = "El horario seleccionado no está disponible torneo";
							$response['code'] = 0;
						} else {
							$response['response'] = true;
							$response['message'] = "El horario seleccionado no está disponible torneo";
							$response['code'] = 4;
						}
					}
				}
			}
			$response['type'] = "success";

		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getCode();
			return response()->json($response);

		}

		//$date = Carbon::createFromFormat('d-m-Y', $request->date);
		// dd($reservation_in_day);



		$reservations = Reservation::whereDate('date', $date->toDateString())
					->whereIn('status', ['En proceso','Aprobada', 'En proceso','Sorteable'])
					->get();

		$tournaments = Tournament::whereDate('start_date', '<=', $date->toDateString())
									->whereDate('end_date', '>=', $date->toDateString())
									->get();

		$day_draws = DayDraw::whereDate('date', $date->toDateString())
					->with('time_draws')
					//->where('enabled', 0)
					->get();
		//
		// dd($reservation_in_day);
		if (empty($reservation_in_day)) {
			$response['reservation_in_day'] = false;
		}else {
			$response['reservation_in_day'] = true;
		}
		$response['reservations'] = $reservations;
		$response['tournaments'] = $tournaments;
		$response['day_draws'] = $day_draws;


		return response()->json($response);
	}

	public function validate_member_reservation(Request $request)
	{
		try {
			$count = 0;
			$date = Carbon::createFromFormat('d-m-Y', $request->date);
			/*$reservation = Reservation::whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
							->whereDate('date', $date->toDateString())
							->where('members.id', '=', $request->member_id)
							->leftjoin('member_reservations', function ($join) {
								$join->on('reservations.id', '=', 'member_reservations.reservation_id');
							})
							->leftjoin('members', function ($join) {
								$join->on('member_reservations.member_id', '=', 'members.id');
							})->first();*/ //vieja forma
			$reservations = Reservation::whereDate('date', '=' , $date->toDateString())->whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))->with(['members'])->get();
			foreach ($reservations as $key => $reser) {
				foreach ($reser->members as $key => $member) {
					if ($member->id == $request->member_id) {
						$count++;
					}
				}
			}
			$response = array();
			$member = Member::find($request->member_id);
			if ($count <= $this->validations->max_reservation_day_member) {
				$response['validation'] = true;
			}
			else
			{
				throw new \Exception("El socio <b>$member->name $member->last_name</b> posee una reservacion para la fecha seleccionada.", 400);
			}

		} catch (\Exception $e) {
			$response['validation'] = false;
			$response['message'] = $e->getMessage();
			return response()->json($response);
		}

		return response()->json($response);
	}

	public function validate_partner_reservation(Request $request)
	{
		try {

			$date = Carbon::createFromFormat('d-m-Y', $request->date);
			$count = 0;
			$countMonth= 0;
			$reservation = Reservation::whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
							->whereDate('date', $date->toDateString())
							->where('partners.id', '=', $request->partner_id)
							->leftjoin('partner_reservations', function ($join) {
								$join->on('reservations.id', '=', 'partner_reservations.reservation_id');
							})
							->leftjoin('partners', function ($join) {
								$join->on('partner_reservations.partner_id', '=', 'partners.id');
							})->first();
			$partner = Partner::find($request->partner_id);

			if (empty($reservation)) {
				if (!empty($partner->exonerated)){
					$response['validation'] = true;
				} else {
					$reservation = Reservation::whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
									->whereDate('date', $date->toDateString())
									->where('partners.id', '=', $request->partner_id)
									->leftjoin('partner_reservations', function ($join) {
										$join->on('reservations.id', '=', 'partner_reservations.reservation_id');
									})
									->leftjoin('partners', function ($join) {
										$join->on('partner_reservations.partner_id', '=', 'partners.id');
									})->first();

					//dd(empty($reservation));
					$response = array();

					if (empty($reservation)) {
						$response['validation'] = true;
					}
					else
					{

						$reservations = Reservation::whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
										->whereYear('date', $date->year)
										->whereMonth('date', $date->month)
										->where('partners.id', '=', $request->partner_id)
										->join('partner_reservations', function ($join) {
											$join->on('reservations.id', '=', 'partner_reservations.reservation_id');
										})
										->join('partners', function ($join) {
											$join->on('partner_reservations.partner_id', '=', 'partners.id');
										})->orderBy('partners.id')->orderBy('date')->get();

						$response = array();
						//dd(empty($reservations));
						if (empty($reservations)) {
							$response['validation'] = true;
						}
						else
						{
							if (count($reservations) < 2) {
								$response['validation'] = true;
							} else {
								throw new \Exception("El invitado <b> $partner->name $partner->last_name </b> ya exedió el limite de reservaciones por mes.", 400);
							}

						}
					}
				}
			} else {
				throw new \Exception("El invitado <b> $partner->name $partner->last_name </b> posee una reservacion para la fecha seleccionada.", 400);
			}

		} catch (\Exception $e) {
			$response['validation'] = false;
			$response['message'] = $e->getMessage();
			return response()->json($response);
		}

		return response()->json($response);
	}

	public function create()
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}

		$type_payments = TypePayment::get();
		$reservation_definition = ReservationDefinition::where('enabled', 1)->orderBy('created_at', 'desc')->get();
		$day_draws = DayDraw::with('time_draws')->where('day_draw_status', '=', 0)->get();
		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();

		$members = Member::where('id', '<>', Auth::user()->member->id)->get();

		$draws = Draw::where([
			['draw_date', '>=', Carbon::now()->format('Y-m-d')],
			['draw_status', '=', 0]
		])->get();
		foreach ($draws as $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $day_draw) {
				$day_draw->time_draws;
			}
		}

		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();
		$day_blockeds = DayBlocked::with('time_blockeds')->get();


		return view('member_reservations.create_reservation', array('user_id' => Auth::user()->id) ,[
			'type_payments' => $type_payments,
			'members' => $members,
			'reservation_definition' => $reservation_definition,
			'draws' => $draws,
			'tournaments' => $tournaments,
			'day_blockeds' => $day_blockeds,
			'validations' => $this->validations


		]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}
		$check = $this->check_reservation($request);

		if (($check->original['code'] > 1) && ($check->original['code'] < 5)) {

			DB::beginTransaction();

			try {
				$reservation_count= Reservation::where([
					['date','=', $request->date],
					['start_time', '=', $request->start_time]
				])->get()->count();
				if ($reservation_count >0 && $check->original['code'] != 3) {
					throw new \Exception("La Hora a Reservar ya se encuentra ocupada", 400);
				}
				$reservations_defintions_array = array();
				$total_green_fee = 0;
				if (!empty($request->partner_id) && isset($request->reservation_definition_id )) {
					foreach ($request->reservation_definition_id as $key ) {

						$reservation_definition = ReservationDefinition::where([
						['id', '=', $request->reservation_definition_id],
						])->first();
						array_push($reservations_defintions_array, $reservation_definition);
					}

					foreach ($reservations_defintions_array as $key => $value) {
						$total_green_fee += $value->green_fee;
					}
				}

				$reservation = new Reservation();
				$reservation->receipt_payment = $request->receipt_payment;
				//$reservation->reservation_definition_id =  $reservations_defintions_array[0]->id;
				// En proceso cuando se crea la reservacion que no es sorteo y Sorteaable para cuando el dia y rango es sorteable
				$reservation->status = ($check->original['code'] == 4) ? "Aprobada" : "Sorteable" ;
				$reservation->detail = $request->detail;
				$reservation->date = $request->date;
				$reservation->start_time = $request->start_time;
				$reservation->total_green_fee = $total_green_fee;
				$reservation->user_id = Auth::user()->id;

				if ($reservation->save()) {
					//---------------se gurdan los green con su reservacion--------------------
					foreach ($reservations_defintions_array as $green)
					{
						$greenReservation = new GreenReservation();
						$greenReservation->green_id =  $green->id;
						$greenReservation->reservation_id = $reservation->id;
						$greenReservation->save();
					}


					//---------------se gurdan los green con su reservacion--------------------
					//	crear la registra la solicitud de reservacion para sorteo
					if ($check->original['code'] == 3) {
						$participants_count = sizeof($request->partner_id) +  sizeof($request->member_id);
						if ($participants_count < 2 ) {
							throw new \Exception("La Reserva sorteable debe poseer minimo 3 participantes", 400);
						}

						$date = Carbon::createFromFormat('d-m-Y', $request->date);

						//se obtiene el DayDraw para la reservacion
						$day_draw = DayDraw::whereDate('date', $date->toDateString())
											->where('day_draw_status', '=', 0)
											->first();
						if (empty($day_draw)) {
							throw new \Exception("Error al procesar la reserva", 400);
						}

						//se continua para obetener el TimeDraw correspondiente
						$time_draw = TimeDraw::where([
								['start_time', '<=', date('H:i:s', strtotime($request->start_time))],
								['end_time', '>=', date('H:i:s', strtotime($request->start_time))],
								['day_draw_id', '=', $day_draw->id]
							])
							->first();

						if (empty($time_draw)) {
							throw new \Exception("Error al procesar la reserva", 400);
						}
						$draw_reservation = new DrawReservation();
						$draw_reservation->date = $request->date;
						$draw_reservation->time = $request->start_time;
						$draw_reservation->time_draw_id = $time_draw->id;
						$draw_reservation->reservation_id = $reservation->id;
						if (!$draw_reservation->save()) {
							throw new \Exception("Error al procesar la reserva", 400);
						}
					}

					$reservation->where('id', '=', $reservation->id)->first();

					if (!empty($request->member_id)) {
						$reservation->members()->attach($request->member_id);
					}

					$reservation->members()->attach(Auth::user()->member->id);

					if (!empty($request->partner_id)) {
						$partners = array();
						$exonerateds = isset($request->exonerated)? $request->exonerated:array();
						foreach ($request->partner_id as $key => $value) {
							$is_exonerated = false;
								foreach ($exonerateds as $key2 => $exonerated) {
									if ($exonerated == $value) {
										$is_exonerated = true;
										break;
									}
								}
							if ($is_exonerated) {
								array_push($partners,[ 'green_id' => null,
													'partner_id' =>  $value,
													'exonerated' => 1
													]);
							}else{
							array_push($partners,[ 'green_id' => $request->reservation_definition_id[$key],
													'partner_id' =>  $value,
													'exonerated' => 0
													]);
							}
							
						} 
						
						if (!is_null($reservation->partners()->attach($partners))) {
							throw new \Exception("Error al procesar la reserva", 400);
						}
					}

					if (!empty($request->partner_id)) {
						$rd_id = $request->reservation_definition_id;
						//actualizacion de la exoneracion por reservacion
						foreach ($request->partner_id as $key => $partner_id) {
							$partner = Partner::where([
								['exonerated', '=', 'Reservacion' ],
								['id', '=', $partner_id ]
							])->first();

							if ($partner) {
								$partner->exonerated = Null;
								if (!$partner->save()) {
									throw new \Exception("Error al procesar la reserva", 400);
								}

							}
							else{
								/*foreach($reservation->partners() as $rp){
									if ($rp->id == $partner->id) {
										$rp->green_id = $rd_id[0];
										array_splice($rd_id, );
									}
								}*/
							}
						}
					}

					// se valida si hay pagador
					if ( $total_green_fee != 0 ) {

						//registro de transaccion de pago de greenfee
						$transaction = array();
						$transaction['amount'] = $total_green_fee;
						//1 para socio
						$transaction['type_payer'] = 1;
						//1 para socio
						$transaction['payer_id'] = Auth::user()->member->id;
						$transaction['product_id'] = $reservation->id;
						//1 para reservacion
						$transaction['type_product_id'] = 1;
						$transaction['transaction_status_id'] = 2;
						$transaction['type_payment_id'] = $request->type_payment_id;

						$transaction = Payment::transaction($transaction);

						if ($transaction['code'] != 1) {
							throw new \Exception($transaction['message'], 400);
						}
					}
				}else {
					throw new \Exception("Error al procesar la reserva", 400);
				}
			} catch(\Exception $e)
			{
				DB::rollback();
				return redirect()->route('s_reservaciones.create',
										array('user_id' => Auth::user()->id)
									)->with(['line' => $e->getLine(), 'code' => $e->getCode(), 'status' => $e->getMessage(), 'type' => 'error']);
			}
			DB::commit();
			$reservation->partners;
			$reservation->members;

			$transaction_payer = Transaction::where([
									['payer_id','=',Auth::user()->member->id],
									['product_id','=',$reservation->id],
									['type_product_id','=',1]])
									->first();
			if ($transaction_payer) {
				$transaction_payer->payer;

				$nombre_destinatario = ucwords($transaction_payer->payer->name.' '.$transaction_payer->payer->last_name);

				$parametros = [
								'reservation'=>$reservation,
								'transaccion'=>$transaction_payer,
								'nombre_destinatario' => $nombre_destinatario,
								'total_green_fee' => $total_green_fee
							];

				Mail::send('emails.reservation', $parametros, function ( $m ) use ( $transaction_payer, $nombre_destinatario  ) {

					$m->from(env('MAIL_USERNAME'), 'Reservación Lagunita');
					$m->to($transaction_payer->payer->user->email, $nombre_destinatario)->subject('Reservación');
				});
			}else{
				$member = Auth::user()->member;
				$nombre_destinatario = ucwords($member->name.' '.$member->last_name);

				$parametros = [
								'reservation'=>$reservation,
								'transaccion'=>$transaction_payer,
								'nombre_destinatario' => $nombre_destinatario,
								'total_green_fee' => $total_green_fee
							];

				Mail::send('emails.reservation', $parametros, function ( $m ) use ( $transaction_payer, $nombre_destinatario  ) {

					$m->from(env('MAIL_USERNAME'), 'Reservación Lagunita');
					$m->to(Auth::user()->email, $nombre_destinatario)->subject('Reservación');
				});
			}
			if ($request->isUpdate) {
				return true;
			}else{

				return redirect()->route('s_reservaciones.index',
										array('user_id' => Auth::user()->id)
									)->with(['status' => 'Reservación se ha creado exitosamente', 'type' => 'success']);
			}

		} else {
			if ($request->isUpdate) {
				return true;
			}else{
				return redirect()->route('s_reservaciones.index',
										array('user_id' => Auth::user()->id)
									)->with(['status' => 'Reservación no disponible', 'type' => 'error']);
			}
		}
	}
	public function edit($user_id, $reservation_id)
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}
		$type_payments = TypePayment::get();
		$reservation_definition = ReservationDefinition::where('enabled', 1)->orderBy('created_at', 'desc')->get();
		$day_draws = DayDraw::with('time_draws')->where('day_draw_status', '=', 0)->get();
		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();

		$members = Member::get();

		$draws = Draw::where([
			['draw_date', '>=', Carbon::now()->format('Y-m-d')],
			['draw_status', '=', 0]
		])->get();
		foreach ($draws as $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $day_draw) {
				$day_draw->time_draws;
			}
		}

		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();
		$day_blockeds = DayBlocked::with('time_blockeds')->get();
		$reservation = Reservation::find($reservation_id);
		$reservation->members;
		$reservation->partners;
		// dd($reservation);

		return view('member_reservations.edit_reservation', array('user_id' => Auth::user()->id) ,[
			'type_payments' => $type_payments,
			'members' => $members,
			'reservation_definition' => $reservation_definition,
			'draws' => $draws,
			'tournaments' => $tournaments,
			'day_blockeds' => $day_blockeds,
			'reservation' => $reservation,
		]);
	}

	public function update(Request $request, $user_id, $reservation_id)
	{
		//dd($request);
		DB::beginTransaction();

		try{
			$reservation =Reservation::find($reservation_id);
			$reservation->delete();
			$request->isUpdate = true;
			$result = $this->store($request);
			DB::commit();
			if ($result) {
				return redirect()->route('s_reservaciones.index',
											array('user_id' => Auth::user()->id)
										)->with(['status' => 'Reservación se ha editado exitosamente', 'type' => 'success']);

			}else{
				return redirect()->route('s_reservaciones.index',
										array('user_id' => Auth::user()->id)
									)->with(['error' => 'Reservación fallo al editar' , 'type' => 'error']);
			}
		}catch(\Exception $e){
			DB::rollback();
			return redirect()->route('s_reservaciones.index',
										array('user_id' => Auth::user()->id)
									)->with(['error' => 'Reservación fallo al editar' , 'type' => 'error']);
		}

	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($user_id, $reservation_id)
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$reservation = Reservation::where([
								[ 'id', '=', $reservation_id ]
							])->first();

			foreach ($reservation->members as $key => $value) {
				if (!$value->pivot->delete()) {
					return redirect()->route('s_reservaciones.index', array('user_id' => Auth::user()->id))
									->with(['status' => 'Error al eliminar la reservación', 'type' => 'error']);
				}
			}
			foreach ($reservation->partners as $key => $value) {
				if (!$value->pivot->delete()) {
					return redirect()->route('s_reservaciones.index', array('user_id' => Auth::user()->id))
									->with(['status' => 'Error al eliminar la reservación', 'type' => 'error']);
				}
			}

			if (!$reservation->delete()) {
				return redirect()->route('s_reservaciones.index', array('user_id' => Auth::user()->id))
								->with(['status' => 'Error al eliminar la reservación', 'type' => 'error']);
			}

		} catch(ValidationException $e)
		{
			// Rollback and then redirect
			DB::rollback();
			return redirect()->route('s_reservaciones.index', array('user_id' => Auth::user()->id))
							->with(['status' => 'Error al eliminar la reservación', 'type' => 'error']);

		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route('s_reservaciones.index', array('user_id' => Auth::user()->id))
							->with(['status' => 'Error al eliminar la reservación', 'type' => 'error']);
		}

		DB::commit();
		return redirect()->route('s_reservaciones.index', array('user_id' => Auth::user()->id))
						->with(['status' => 'Se ha eliminado satisfactoriamente la reservación', 'type' => 'success']);
	}

	public function get_reservations(Request $request)
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}
		try {

			$date = Carbon::createFromFormat('d-m-Y', $request->date);

			$reservations = Reservation::whereDate('date', $date->toDateString())
						->whereIn('status', ['En proceso','Aprobada', 'En proceso','Sorteable'])
						->get();

			if (empty($reservations)) {
				throw new \Exception("No hay reservas para la fecha seleccionada", 400);
			}else {
				$response['reservations'] = $reservations;
				$response['type'] = "success";
			}
		} catch (\Exception $e) {
			$response['status'] = $e->getMessage();
			$response['code'] = $e->getCode();
			$response['line'] = $e->getLine();
			$response['type'] = "error";
		}
		return response()->json($response);
	}

	public function departure_list(Request $request)
	{
		// Se valida si no es un usuario de rol 6
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');

		// La fecha del dia de hoy
		$actual_date = Carbon::today();

		// Se lista las reservaciones donde la fecha sea igual a la fecha de hoy
		$reservations = Reservation::whereDate('date', '=', $actual_date->toDateString());

		// Se ordena la tabla por fecha de forma ascendente junto con la hora
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->get();

		// Se recorre cada socio en invitado del arreglo
		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}

		// Se retorna la vista de lista de salida
		return view('member_reservations.departure_list', [
			'reservations' => $reservations,
		]);

	}

}

<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Http\Respons;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\Reservation;
use Golf\StarterProcess;
use Golf\CommentsReservation;
class RangerController extends Controller
{
	public function index()
	{

		if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 5  && Auth::user()->role_id != 7 ) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');

		$reservations = Reservation::whereDate('date', Carbon::now()->toDateString())->with(['members','partners', 'comments'])->orderBy('start_time','asc')->get();

		$table_reservations = Table::create($reservations, ['date' => 'Hora de inicio','start_time' => 'Hora de inicio', 'end_time' => 'Hora de salida']);

		return view('starters.reservation_day', ['reservations' => $reservations, 'table_reservations' => $table_reservations] );

	}

	public function departure_list(Request $request)
	{
		// Se valida si no es un usuario de rol 5 y 7
		if (Auth::user()->role_id != 7) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');

		// La fecha del dia de hoy
		$actual_date = Carbon::today();

		// Se lista las reservaciones donde la fecha sea igual a la fecha de hoy
		$reservations = Reservation::whereDate('date', '=', $actual_date->toDateString())->with(['comments']);

		// Se ordena la tabla por fecha de forma ascendente junto con la hora
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->get();

		// Se recorre cada socio en invitado del arreglo
		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
			$reservation->comments;
		}		
		// Se retorna la vista de lista de salida
		return view('rangers.departure_list', [
			'reservations' => $reservations
		]);

	}
	public function add_comment(Request $request)
	{
		
		if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 7 && Auth::user()->role_id != 5 ) {
			return redirect()->route('home');
		}
		try{
			$fecha = explode('-',$request->date);
			$fecha = $fecha[2]."-".$fecha[1]."-".$fecha[0];
			
			$comment = new CommentsReservation();
			$comment->reservation_id = $request->reservation_id;
			$comment->date = date('Y-m-d',strtotime($fecha));
			$comment->time =   date('H:i:s',strtotime($request->time));
			$comment->comment = $request->comment;
			$comment->user_id = Auth::user()->id;
			if (!$comment->save()) {
				
				throw new Exception();
			}
			$comment->user;
		}catch(\Exception $e){
			return response()->json(['status' => false , 'comment' => null]);
		}
		return response()->json(['status' => true , 'comment' => $comment]);
	}

	public function remove_comment( $id )
	{
		if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 7 && Auth::user()->role_id != 5) {
			return redirect()->route('home');
		}
		$comment = CommentsReservation::find($id);
		if ( $comment->delete() )
		{
			return response()->json(true);
		}
		
		return response()->json(false);
	}

}

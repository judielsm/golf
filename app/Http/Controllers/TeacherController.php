<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Golf\Teacher;

use Gbrock\Table\Facades\Table;


class TeacherController extends Controller
{


	public function index()
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}
		$teachers = Teacher::where([])->get();


		return view('teachers.all_teachers', [
			'teachers' => $teachers,
		]);
	}

	public function show(Request $request, $user_id, $teacher_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$teacher = Teacher::find( $teacher_id );

			if (empty($teacher)) {
				throw new \Exception("Error al obtener el profesor", 400);
			}

		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['teacher' => $teacher,
				'type' => 'success']);
	}


	public function create()
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		return view('teachers.create_teacher');
	}

	public function store(Request $request)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:25',
				'last_name' => 'required|min:3|max:25',
				'phone' => 'required|min:5|max:14',
				'email' => 'required|email|min:3|max:50',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea un nuevo profesor
			$teacher = new Teacher();
			$teacher->name = ucwords(strtolower($request->name));
			$teacher->last_name = ucwords(strtolower($request->last_name));
			$teacher->phone = $request->phone;
			$teacher->email = $request->email;

			$name = $teacher->name.' '.$teacher->last_name;

			// Se condiciona si el profesor se guard�
			if (!$teacher->save()) {
				throw new \Exception("Error al almacenar el profesor", 400);
			}

			// Se retorna al listado de profesores con los valores guardados
			DB::commit();
			$response['response'] = $teacher;
			$response['status'] = 'Se ha registrado satisfactoriamente el profesor: '.$name;
			$response['type'] = 'success';
			return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
				->with($response);

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				case ($e instanceof \PDOException):
					return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

				// Se retorna al listado de profesores con los mensajes de diferentes errores
				DB::rollback();
				$response['response'] = false;
				$response['validations'] = $resultados;
				$response['line'] = $e->getLine();
				$response['code'] = $e->getCode();
				$response['input'] = $request->input();
				$response['type'] = 'error';

				return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
					->with($response);
			}
		}
	}


	public function edit($user_id, $teacher_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		if (Teacher::find($teacher_id))
		{
			$teacher = Teacher::where([
						['id', '=', $teacher_id]
						])->first();

			return view('teachers.edit_teacher', ['teacher' => $teacher]);
		}
		else
		{
			return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
				->with(['status' => 'Error al modificar Profesor', 'type' => 'error'])
				->withInput();
		}
	}


	public function update(Request $request, $user_id, $teacher_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:25',
				'last_name' => 'required|min:3|max:25',
				'phone' => 'required|min:5|max:14',
				'email' => 'required|email|min:3|max:50',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se obtiene los profesores ordenados por ID
			$teacher = Teacher::where([
								['id', '=', $teacher_id]
							])->first();
			$teacher->name = ucwords(strtolower($request->name));
			$teacher->last_name = ucwords(strtolower($request->last_name));
			$teacher->phone = $request->phone;
			if (!empty($request->email)) {
				$teacher->user->email = $request->email;
			}

			$name = $teacher->name.' '.$teacher->last_name;

			// Se valida si el profesor no se guardo seguido de un mensaje de error
			if (!$teacher->save()) {
				throw new \Exception('Error al actuañlizar los datos del/la profesor(a): '.$name, 400);
			}

			// Se retorna al listado de profesores con los valores guardados
			DB::commit();
			$response['response'] = $teacher;
			$response['status'] = 'Se ha actualizado satisfactoriamente del/la profesor(a): '.$name;
			$response['type'] = 'success';
			return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
				->with($response);

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				case ($e instanceof \PDOException):
					// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
					return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de profesores con los mensajes de diferentes errores
					DB::rollback();
					$response['response'] = false;
					$response['validations'] = $resultados;
					$response['line'] = $e->getLine();
					$response['code'] = $e->getCode();
					$response['type'] = 'error';

					return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
						->with($response);
			}
		}
	}


	public function destroy($user_id, $teacher_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$teacher = Teacher::where([
								['id', '=', $teacher_id]
							])->first();

			$name = $teacher->name.' '.$teacher->last_name;

			if (!$teacher->delete()) {
				throw new \Exception('Error al eliminar el profesor', 400);
			}

		} catch (\Exception $e) {
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';

			return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
							->with($response);
		}
		DB::commit();
		$response['response'] = $teacher;
		$response['status'] = 'Se ha eliminado satisfactoriamente el profesor: '.$name;
		$response['type'] = 'success';
		return redirect()->route('profesores.index', array('user_id' => Auth::user()->id))
						->with($response);
	}

	public function validate_email_teacher(Request $request)
	{
		$validation_in_teachers = Validator::make($request->input(), [
				'email' => Rule::unique('teachers')->where(function ($query) use (&$request) {
				if (isset($request->teacher_id)) {
					$query->where('id', '!=', $request->teacher_id);
				}
				})
				]);
		if ($validation_in_teachers->fails()) {
			return response()->json(['status' => "La dirección de correo ingresada se encuentra registrada en el sistema.",
					'code' => 1,
					'type' => 'error']);
		} else {
			return response()->json(['status' => "El correo ingresado es válido.",
					'code' => 200,
					'type' => 'success']);
		}
	}

}

<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Facades\Golf\Facades\ValidateRecurringData;
use Faker\Factory;
use Gbrock\Table\Facades\Table;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Log;
use Golf\DayDraw;
use Golf\ListDrawn;
use Golf\DayBlocked;
use Golf\Draw;
use Golf\DrawReservation;
use Golf\TimeDraw;
use Golf\Tournament;
use Golf\Transaction;
use Golf\Reservation;
use Golf\User;
use Golf\Http\Util\MailToMember;
use Golf\EmailJob;
use Mail;
class DrawController extends Controller
{


	public function index()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		$draws = Draw::where('draw_status', '=', 0)->get();
		foreach ($draws as $key => $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $key => $day_draw) {
				$day_draw->time_draws;
			}

		}
		$tournaments = Tournament::where('end_date', '>=', Carbon::now()->format('Y-m-d') )->get();
		$day_blockeds = DayBlocked::with('time_blockeds')->get();


		return view('draws.all_draws', [
				'draws' => $draws,
				'tournaments' => $tournaments,
				'day_blockeds' => $day_blockeds,
			]);
	}

	public function history()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');


		$draws = Draw::where('draw_status', '=', 1)->get();

		//dd(Carbon::parse($draws[0]->day_draws[0]->date)->formatLocalized('%A, %d de %B del %Y'));
		// foreach ($draws as $key => $draw) {
		// 	$draw->day_draws;
		// 	foreach ($draw->day_draws as $key => $day_draw) {
		// 		$day_draw->list_drawns;
		// 	}

		// }
		// dd($draws);

		return view('draws.history_draw',['draws' => $draws]);
	}

	public function search(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		if (empty($request->start_date) || empty($request->draw_date)) {
			$results["response"] = "datos de busqueda incorrectos";
			return response()->json($results, 400);
		}

		$draws = Draw::whereDate([
						['start_date', '=', $request->start_date],
						['draw_date', '=', $request->draw_date],
					])->get();

		$results['draws'] = $draws;

		return response()->json($results, 200);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$draw = new Draw();
			$draw->start_date = $request->start_date;
			$draw->draw_date = $request->draw_date;


			if ($draw->save()) {
				$request_draw = json_decode($request->draw);
				//dd($request_draw);
			foreach ($request_draw->draw_days as $key => $request_day_draw)
				{
					$has_tournament =  ValidateRecurringData::validate_has_tournament( Carbon::parse($request_day_draw->date)->format('d-m-Y'), $request_day_draw->times);

					if ($has_tournament['validation'] == true) {
						throw new \Exception("Los dias seleccionados para sortear incluye dias de torneos.", 400);
					}

					$has_day_draw =  ValidateRecurringData::validate_has_day_draw(Carbon::parse($request_day_draw->date)->format('d-m-Y'), $request_day_draw->times);

					if ($has_day_draw['validation'] == true) {
						throw new \Exception("hay dias seleccionados que ya pertencen a un sorteo.", 400);
					}

					$day_draw = new DayDraw();
					$day_draw->date = $request_day_draw->date;
					$day_draw->draw_id = $draw->id;

					if ($day_draw->save())
					{
						foreach ($request_day_draw->times as $key => $time) {
							$date = Carbon::parse($request_day_draw->date);
							$time_draw = new TimeDraw();
							$time_draw->start_time = date('H:i:s', strtotime($time->start_time));
							$time_draw->end_time = date('H:i:s', strtotime($time->end_time));
							$time_draw->day_draw_id = $day_draw->id;
							if (!$time_draw->save())
							{
								throw new \Exception("Error al registrar sorteo", 400);
							}
						}
					}
					else
					{
						throw new \Exception("Error al registrar sorteo", 400);
					}
				}
			}else {
				throw new \Exception("Error al registrar sorteo", 400);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			$data['status'] = $e->getMessage();
			$data['line'] = $e->getLine();
			$data['code'] = $e->getCode();
			$data['input'] = $request->input();
			$data['type'] = 'error';
			if (!empty($has_tournament) && isset($has_tournament['tournament'])) {
				$data['tournament'] = $has_tournament['tournament'];
			}
			if (!empty($has_day_draw)) {
				$data['day_draw'] = $has_day_draw['day_draw'];
			}

			Log::info("Exception :".$e->getMessage().
						" File :".$e->getFile()." Line :".$e->getLine().
						" Trace :".$e->getTraceAsString()." Line :".$e->getLine());

			return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
							->with($data);
		}

		DB::commit();
		return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
						->with(['status' => 'El sorteo se ha creado exitosamente', 'type' => 'success']);
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($user_id, $draw_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		$draw = Draw::where([
							['id', '=', $draw_id]
						])->with('day_draws')->first();

		// dd($draw);

		return view('draws.edit_draw', ['draw' => $draw]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $draw_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $user_id, $draw_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {


			$draw = Draw::where([
								['id', '=', $draw_id]
							])->first();

			$request_draw = json_decode($request->draw);

			$draw->start_date = $request_draw->start_date;

			$draw->draw_date = $request_draw->draw_date;

			foreach ($draw->day_draws as $key => $day_draw) {
				$day_draw->delete();
			}

			if ($draw->save()) {
				//$request_draw = json_decode($request->draw);

				foreach ($request_draw->draw_days as $key => $request_day_draw) {
						$day_draw = DayDraw::where([
										["date", "=", date('Y-m-d', strtotime($request_day_draw->date))],
										["draw_id", "=", $draw->id]
									])->withTrashed()->first();

						if ($day_draw) {
							if ($day_draw->restore()){

								foreach ($day_draw->time_draws as $key => $time_draw) {
									$time_draw->delete();
								}

								foreach ($request_day_draw->times as $key => $time) {

									$time_draw = TimeDraw::where([
													["start_time", "=", date('H:i:s', strtotime($time->start_time))],
													["end_time", "=", date('H:i:s', strtotime($time->end_time))],
													["day_draw_id", "=", $day_draw->id]
												])->withTrashed()->first();

									if ($time_draw) {

										if (!$time_draw->restore()){
											throw new \Exception("Error al registrar sorteo", 400);
										}
									} else {
										$date = Carbon::parse($request_day_draw->date);
										$has_tournament =  ValidateRecurringData::validate_has_tournament($date->format('d-m-Y'), $request_day_draw->times);

										if ($has_tournament['validation'] == true) {
											throw new \Exception("Los dias seleccionados para sortear incluye dias de torneos.", 400);
										}
										$has_day_draw =  ValidateRecurringData::validate_has_day_draw($date->format('d-m-Y'), $time);

										if ($has_day_draw['validation'] == true) {
											if ($has_day_draw['day_draw']->id != $day_draw->id) {
												throw new \Exception("hay dias seleccionados que ya pertencen a un sorteo.", 400);
											}
										}
										$time_draw = new TimeDraw();
										$time_draw->start_time =date('H:i:s', strtotime($time->start_time)) ;
										$time_draw->end_time = date('H:i:s', strtotime($time->end_time));
										$time_draw->day_draw_id = $day_draw->id;

										if (!$time_draw->save())
										{
											throw new \Exception("Error al registrar sorteo", 400);
										}
									}
								}
							}
							else{
								throw new \Exception("Error al registrar sorteo", 400);
							}
						}
						else {

							$day_draw = new DayDraw();
							$day_draw->date = $request_day_draw->date;
							$day_draw->draw_id = $draw->id;

							if ($day_draw->save())
							{
								foreach ($request_day_draw->times as $key => $time) {
									$date = Carbon::parse($day_draw->date);
									$has_tournament =  ValidateRecurringData::validate_has_tournament($date->format('d-m-Y'), $request_day_draw->times);

									if ($has_tournament['validation'] == true) {
										throw new \Exception("Los dias seleccionados para sortear incluye dias de torneos.", 400);
									}
									$has_day_draw =  ValidateRecurringData::validate_has_day_draw($date->format('d-m-Y'), $time);

									if ($has_day_draw['validation'] == true) {
										if ($has_day_draw['day_draw']->id != $day_draw->id) {
											throw new \Exception("hay dias seleccionados que ya pertencen a un sorteo.", 400);
										}
									}
									$time_draw = new TimeDraw();
									$time_draw->start_time =date('H:i:s', strtotime($time->start_time)) ;
									$time_draw->end_time = date('H:i:s', strtotime($time->end_time));
									$time_draw->day_draw_id = $day_draw->id;
									if (!$time_draw->save())
									{
										throw new \Exception("Error al registrar sorteo", 400);
									}
								}
							}
							else
							{
								throw new \Exception("Error al registrar sorteo", 400);
							}
						}
				}
			}
			else {
				throw new \Exception("Error al registrar sorteo", 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			$data['status'] = $e->getMessage();
			$data['line'] = $e->getLine();
			$data['code'] = $e->getCode();
			$data['input'] = $request->input();
			$data['type'] = 'error';
			// dd($data);

			Log::info("Exception :".$e->getMessage().
						" File :".$e->getFile()." Line :".$e->getLine().
						" Trace :".$e->getTraceAsString()." Line :".$e->getLine());

			return redirect()->route('sorteos.index',
								array('user_id' => Auth::user()->id, 'draw_id' => $draw_id))
							->with(['status' => $e->getMessage(),
								'line' => $e->getLine(),
								'code' => $e->getCode(),
								'input' => $request->input(),
								'type' => 'error']);
		}

		DB::commit();
		return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
						->with(['status' => 'El sorteo se ha actualizado exitosamente', 'type' => 'success'])
						->withInput();
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($user_id, $draw_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {
			$delete = true;
			$draw = Draw::where([
								['id', '=', $draw_id]
							])->first();

			foreach ($draw->day_draws as $key => $day_draw) {
				foreach ($day_draw->time_draws as $key => $time_draw) {
					foreach ($time_draw->draw_reservations as $key => $draw_reservation) {
						if (!time) {
							throw new \Exception("Error al eliminar sorteo", 400);
						}
					}
					if (!$time_draw->delete()) {
						throw new \Exception("Error al eliminar sorteo", 400);
					}
				}
				if (!$day_draw->delete()) {
					throw new \Exception("Error al eliminar sorteo", 400);
				}
			}
			if (!$draw->delete()) {
				throw new \Exception("Error al eliminar sorteo", 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);
			return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
							->with(['status' => $e->getMessage(),
								'line' => $e->getLine(),
								'code' => $e->getCode(),
								'type' => 'error']);
		}

		DB::commit();

		return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
			->with(['status' => 'Se ha eliminado satisfactoriamente el dia sorteable', 'type' => 'success']);
	}

	/**
	 * Enable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function enable($user_id, $draw_id)
	{

		DB::beginTransaction();

		try {
			if (Auth::user()->role_id != 2) {
				return redirect()->route('home');
			}

			$draw = Draw::where([
								['id', '=', $draw_id]
							])->withTrashed()->first();

			$draw->enable = 1;

			$draw->deleted_at = NULL;

			if (!$draw->save()) {
				throw new \Exception("Error al habilitar sorteo", 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
							->with(['status' => $e->getMessage(),
								'line' => $e->getLine(),
								'code' => $e->getCode(),
								'type' => 'error']);
		}

		DB::commit();

		return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
			->with(['status' => 'Se ha habilitado satisfactoriamente el dia sorteable 4', 'type' => 'success']);
	}

	/**
	 * Disable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function disable($user_id, $draw_id)
	{

		DB::beginTransaction();

		try {
			if (Auth::user()->role_id != 2) {
				return redirect()->route('home');
			}

			$draw = Draw::where([
							['id', '=', $draw_id]
						])->with('day_draws')->withTrashed()->first();

			$draw->enable = 0;

			if ($draw->save()) {
				throw new \Exception("Error al deshabilitar sorteo", 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
							->with(['status' => $e->getMessage(),
								'line' => $e->getLine(),
								'code' => $e->getCode(),
								'type' => 'error']);
		}

		DB::commit();
		return redirect()->route('sorteos.index', array('user_id' => Auth::user()->id))
			->with(['status' => 'Se ha deshabilitado satisfactoriamente el dia sorteable', 'type' => 'success']);
	}

	/**
	 * Disable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function draw(Request $request)
	{
		$remanent_reservations = array();
		$reservations_array = array();
		DB::beginTransaction();
		$reservation_ids = array();
		try {
			if (Auth::user()->role_id != 2) {
				return redirect()->route('home');
			}

			$draw = Draw::where([
					['id', '=', $request->draw_id]
				])->with('day_draws')->orderBy('start_date', 'asc')->orderBy('draw_date', 'asc')->first();

			if (empty($draw)) {
				throw new \Exception("No se encontró el sorteo seleccionado.", 400);
			}
			if ($draw->draw_status == 1) {
				throw new \Exception("Este sorteo fue realizado.", 400);
			}

			//lista de reservaciones han sido sorteadas en un time
			$list_reservations_draw = array();
			//lista de times ordenados por numero de draw_reservations
			
			$draw_boolean = false;

			// se reecorre los dias de sorteo
			foreach ($draw->day_draws as $key_day_draw => $day_draw) {
				$blocked_hours = array();
				//se buscan los intervalos bloqueados para este dia
				$day_blocked = DayBlocked::where('date', $day_draw->date )->first();
				  if ($day_blocked) {
					  foreach ($day_blocked->time_blockeds as $key => $time) {
						   $blocked_start_time = Carbon::parse('2017-04-07 '.$time->start_time);
						   $blocked_end_time = Carbon::parse('2017-04-07 '.$time->end_time);
						   $blocked_hora1 = Carbon::parse('2017-04-07 '.$time->start_time);
						   $blocked_interval = ($blocked_start_time->diffInMinutes($blocked_end_time)+10)/10;
								for ($i=0; $i < $blocked_interval; $i++) {
									if ($i == 0) {
											array_push($blocked_hours, $blocked_hora1->format('h:i:s'));
									}else {
											array_push($blocked_hours, $blocked_hora1->addMinute(10)->format('h:i:s'));
									}
								}
					  }
					
				  }
				//-----------------------------------------------
				if ($day_draw->draw_status == 0) {
					$draw_boolean = true;
					// se ordena los time_draws
					$list_times = $day_draw->time_draws->sortBy('id');

					//se recorre la lista de times ordenados por cantida de reservaciones postuladas
					//dd($day_draw->time_draws[0]->draw_reservations);
					foreach ($list_times as $key => $time) {
						$reservations_drws = DrawReservation::where('time_draw_id', $time->id
							)->get();
							$reservation_array = $reservations_drws->toArray();
							$interval = array();
							$start_time = Carbon::parse('2017-04-07 '.$time->start_time);
							$end_time = Carbon::parse('2017-04-07 '.$time->end_time);
							$hora1 = Carbon::parse('2017-04-07 '.$time->start_time);
							$interval_count = ($start_time->diffInMinutes($end_time)+10)/10;
							for ($i=0; $i < $interval_count; $i++) {
								if ($i == 0) {
									array_push($interval, $hora1->format('h:i:s'));
								}else {
									array_push($interval, $hora1->addMinute(10)->format('h:i:s'));
								}
							}
							//-----------------------elimina del intervalo de times si el mismo se encuentra entre horas bloqueadas
							if (count($blocked_hours) >0) {
								foreach ($interval as $key => $value) {
 									 if(in_array($value,$blocked_hours)){
										 array_splice($interval, array_search($value,$interval ) , 1);
									 }
								}
							}
							//--------verificar si esta vacio el intervalo
							if(count($interval)==0){
								foreach($reservation_array as $r){
									$reservation_not = Reservation::find($r['reservation_id']);
									$reservation_not->status = "No Adjudicada";
									$reservation_not->save();
								}
							}
							//-----------------------------------------------------------------------------------------------------
						foreach ($interval as $index => $hour) {
							if (sizeof($reservation_array) > 0 ) {
								$rand = rand(0, sizeof($reservation_array) -1);
								$random_reservation = $reservation_array[$rand];
								$drawR = DrawReservation::find($random_reservation['id']);
								$drawR->time = $hour;
								if($drawR->save()){
									$reservation = 	Reservation::find($random_reservation['reservation_id']);
									$reservation->start_time = $hour;
									$reservation->status = "Aprobada";

									if($reservation->save()){
										$list_drawn = new ListDrawn();
										$list_drawn->reservation_id = $reservation->id;
										$list_drawn->day_draw_id = $day_draw->id;
										if ($list_drawn->save()) {
											$reservation->members;
											$reservation->partners;
											array_splice($reservation_array, $rand, 1);
											array_push($reservation_ids, $reservation->id );
											array_push($reservations_array, $reservation);
										}
										else{
											throw new \Exception("Error al realizar sorteo.", 400);
										}

									}else{
										throw new \Exception("Error al realizar sorteo.", 400);
									}
								}
								else{
									throw new \Exception("Error al realizar sorteo.", 400);
								}
							}else{
								break;
							}
						}
						//----------------------------cambiando de estatus las que no fueron sorteadas------------------------------
						foreach($reservation_array as $not_asigned_reservation){
							$reservation_not = Reservation::find($not_asigned_reservation['reservation_id']);
							$reservation_not->status = "No Adjudicada";
							$reservation_not->save();
						}
					}
					$day_draw->day_draw_status = 1;
					if (!$day_draw->save()) {
						throw new \Exception("Error al realizar sorteo.", 400);
					}

				}
			}
			$draw->draw_status = 1;
			if (!$draw->save()) {
				throw new \Exception("Error al realizar sorteo.", 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();

			return response()->json(['status' => $e->getMessage(),
								'line' => $e->getLine(),
								'code' => $e->getCode(),
								'type' => 'error']);
		}

		DB::commit();

		$day_draws_array = array();
		foreach ($draw->day_draws as $key => $day_draw) {
			$day_draws_array[$key] = $day_draw->id;
			foreach ($day_draw->list_drawns as $key => $drawn) {
				if (!empty($drawn->reservation)) {
					$drawn->reservation->partners;
					$drawn->reservation->members;
				}
			}

		}

		$list_drawn = ListDrawn::whereIn('day_draw_id',$day_draws_array)->get();



		if (empty($list_drawn) || sizeof($list_drawn) == 0) {
			return response()->json(['status' => 'No hay reservaciones para sorteo', 'code' => 0, 'type' => 'success']);
		} else {

			$draw->draw_status = 1;
			//=======================================envio de correos==============================
			
			foreach ($reservations_array  as $key => $reservation) {
				if (sizeof($reservation->partners)>0) {
					$transaction = Transaction::where('product_id', $reservation->id)->first();
					$nombre_destinatario = ucwords($transaction->payer->name.' '.$transaction->payer->last_name);
					$email =  $transaction->payer->user->email;
					$parametros = [
									'reservation'=>$reservation,
									'nombre_destinatario' => $nombre_destinatario
								];
				}else{
					$member = $reservation->members[0];
					$nombre_destinatario = ucwords($member->name.' '.$member->last_name);
					$email =  $member->user->email;
					$parametros = [
									'reservation'=>$reservation,
									'nombre_destinatario' => $nombre_destinatario
								];
				}
				$task = new EmailJob();
				$task->subject = 'Reservación';
				$task->to = $email;
				$task->recipient = $nombre_destinatario;
				$task->data = json_encode($parametros);
				$task->template = 'emails.draws';
				$task->intents =0;
				$task->save();
				
				
			}
			//=======================================envio de correos
			return response()->json(['status' => 'Se ha realizado el sorteo exitosamente', 'code' => 1, 'type' => 'success','draw' => $draw, 'reservations' => $reservations_array]);
		}
	}

	public function sort_by_count_burbble($object, $length, $index)
	{
		for($i=1;$i<$length;$i++)
		{
			for($j=0;$j<$length-$i;$j++)
			{
				if(count($object[$j]->$index) < count($object[$j+1]->$index))
				{
					$k=$object[$j+1];
					$object[$j+1]=$object[$j];
					$object[$j]=$k;
				}
			}
		}

	  return $object;
	}

	public function drawing($object, $length)
	{
		$drawing = $object[rand(0, $length -1)];
		foreach ($object as $key => $draw_reservation) {
			$reservation = $draw_reservation->reservation;
			if (!empty($reservation)) {
				if ($drawing->id == $draw_reservation->id) {
					$reservation->status = "Aprobada";
					$reservation->save();
					//dd($reservation);
				} else {
					$reservation->status = "Cerrada";
					$reservation->save();
					$draw_reservation->delete();
				}
			}
		}

		return $drawing;
	}

	public function validate_existence_reservation_laggards ($previous_day_draw, $draw_reservation)
	{
		$exist = false;
		foreach ($previous_day_draw->time_draws as $key => $time_draw) {
			foreach ($time_draw->draw_reservations as $key => $draw_reservation2) {
				if ((isset($draw_reservation2->reservation)) && (isset($draw_reservation->reservation))) {

					if (
						(isset($draw_reservation2->reservation->user->member)) &&
						(isset($draw_reservation->reservation->user->member))
					)
					{
						if ($draw_reservation2->reservation->user->member->id == $draw_reservation->reservation->user->member->id) {
							$exist = true;
						}
					}
				}

				//	Se compara que el miembro de la reservacion se encuentre en el listado de reservaciones resagadas

			}
		}

	  return $exist = true;
	}

	public function validate_existence_winning_reservation ($list_reservations_draw, $draw_reservation)
	{
		$exist = false;

		if (isset($draw_reservation->reservation)) {
			foreach ($list_reservations_draw as $key_draw => $draw) {

				foreach ($draw as $key_day => $day) {

					foreach ($day as $key_time => $time) {
						if ((isset($draw_reservation->reservation->user->member)) && (isset($time['draw_reservation']->reservation->user->member))) {
							if ($draw_reservation->reservation->user->member->id == $time['draw_reservation']->reservation->user->member->id) {
								$exist = true;
								break;
							}
						}
					}
				}
			}
		}

	  return $exist = true;
	}

	public function delete_selected_team ($time, $index)
	{
		$exist = false;

		if (isset($draw_reservation->reservation)) {
			foreach ($list_reservations_draw as $key_draw => $draw) {

				foreach ($draw as $key_day => $day) {

					foreach ($day as $key_time => $time) {
						if ((isset($draw_reservation->reservation->user->member)) && (isset($time['draw_reservation']->reservation->user->member))) {

							if ($draw_reservation->reservation->user->member->id == $time['draw_reservation']->reservation->user->member->id) {
								$exist = true;
								break;
							}
						}
					}
				}
			}
		}

	  return $exist = true;
	}
	public function calculateIntervalTimes($start, $end)
	{
		$times = array();
		array_push($times, $start);

	}
}

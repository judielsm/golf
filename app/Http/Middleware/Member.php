<?php

namespace Golf\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Member
{
	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	private $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		dd($this->auth->user()->role_id);
		switch ($this->auth->user()->role_id) 
		{
			case '1':
				# Administrador 
				return redirect()->to('root');               
				break;
			case '2':
				# Responsable de Área
				return redirect()->to('admin');  
				break;
			case '3':
				# Secretaria
				return redirect()->to('admin_reservation');  
				break;
			case '4':
				# Profesor
				return redirect()->to('admin_finance');  
				break;
			case '5':
				# Área Legal
				return redirect()->to('starter');  
				break;
			case '6':
				# Área Legal
				//	return redirect()->to('home');
				break;
			default:
				return redirect()->to('/');  
				break;
		}
			
		return $next($request);
	}
}
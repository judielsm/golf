<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class MemberReservation extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'enabled', 'reservation_id', 'member_id',
	];
	protected $sortable = [
		'enabled', 'reservation_id', 'member_id',
	];


	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function members()
	{
		return $this->belongsTo('Golf\Member', 'member_id', 'id');
	}

	public function reservations()
	{
		return $this->belongsTo('Golf\Reservation', 'id', 'reservation_id');
	}

}

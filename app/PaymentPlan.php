<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class PaymentPlan extends Model
{

	use SoftDeletes;
	use Sortable;


	protected $fillable = [
		'name','description','rate'
	];

	protected $sortable = [
		'name','description','rate'
	];

}

<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Member extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'last_name', 'identity_card', 'sex', 'number_action', 'phone', 'handicap', 'user_id', 'enabled'
	];
	protected $sortable = [
		'name', 'last_name', 'identity_card', 'number_action', 'phone', 'handicap', 'user_id', 'enabled'
	];
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function lockers()
	{
		return $this->belongsToMany('Golf\Locker')->using('App\LockerLoan')->withTimestamps();
	}

	public function partners()
	{
		return $this->hasMany('Golf\Partner', 'user_id', 'id');
	}

	public function students()
	{
		return $this->hasMany('Golf\Student', 'id', 'member_id');
	}

	public function reservations()
	{
		return $this->belongsToMany('Golf\Reservation', 'member_reservations');
	}

	public function user()
	{
		return $this->hasOne('Golf\User', 'id', 'user_id')->withTrashed();
	}

	public function categories()
	{
		return $this->belongsToMany('Golf\Category','tournament_inscription_members');
	}

	public function locker_loans()
	{
		//	return $this->belongsToMany('Golf\Members', 'member_id', 'id');
		return $this->hasMany('Golf\LockerLoans', 'member_id', 'id');
	}
	public function payer(){
 		 return $this->hasOne('Transaction', 'id', 'payer_id');
	}

	public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

	public function getLastNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }    
}

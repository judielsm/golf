<?php namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Category extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
	];
	protected $sortable = [
		'name',
	];

	public function tournaments()
	{
		return $this->belongsToMany('Golf\Tournament','tournament_categories')
			->withPivot('start_time_id','enabled')->withTimestamps();
	}

	public function start_times()
	{
		return $this->belongsToMany('Golf\StartTime','tournament_categories')
			->withPivot('tournament_id','enabled');
	}

	public function members()
	{
		return $this->belongsToMany('Golf\Member','tournament_inscription_members');
	}

}

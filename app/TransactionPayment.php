<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TransactionPayment extends Model
{
    use SoftDeletes;

    protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];
	public function transaction()
	{
		return $this->belongsTo('Golf\Transaction', 'transaction_id', 'id');
	}
	public function payment()
	{
		return $this->belongsTo('Golf\PaymentMethod', 'payment_method_id', 'id');
	}
}

<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class StarterProcess extends Model
{

	use SoftDeletes;
	use Sortable;

	public $timestamps = false;
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	// protected $dates = ['deleted_at'];


	protected $fillable = [
		'reservation_id', 'time',
	];

	
}

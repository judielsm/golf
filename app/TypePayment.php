<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class TypePayment extends Model
{
	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'name', 'enabled',
	];
	protected $sortable = [
		'name', 'enabled',
	];
	
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function transactions()
	{
		return $this->belongsTo('Golf\Transaction', 'trans_id', 'id' );
	}

	public function draw_reservations()
	{
		return $this->hasMany('Golf\Transaction', 'type_payment_id', 'id' );
	}
}

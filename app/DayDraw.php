<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class DayDraw extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'date', 'enabled'
	];

	protected $sortable = [
		'date'
	];
	
	protected $dates = [
		'date',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function reservations()
	{
		return $this->belongsToMany('Golf\Reservation', 'draw_reservations');
	}

	public function setDateAttribute($value)
	{
		$this->attributes['date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getDateAttribute()
	{
		return date( 'Y-m-d', strtotime($this->attributes['date']) );
	}

	public function time_draws()
	{
		return $this->hasMany('Golf\TimeDraw', 'day_draw_id', 'id');
	}

	public function list_drawns()
	{
		return $this->hasMany('Golf\ListDrawn', 'day_draw_id', 'id');
	}

	public function draw()
	{
		return $this->belongsTo('Golf\Draw', 'id', 'draw_id' );
	}

}
